<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */

$country = $_GET['skilled_country'];
$country_db = "skilled_us";

switch ($country) {
    case "us":
        $country_db = "skilled_us";
        break;
    case "fr":
        $country_db = "skilled_fr";
        break;
    case "ru":
        $country_db = "skilled_ru";
        break;
    case "nl":
        $country_db = "skilled_nl";
        break;
    case "id":
        $country_db = "skilled_id";
        break;
}

define('DB_NAME', $country_db);

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'admin');

/** MySQL hostname */
define('DB_HOST', 'mysql');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'R9kID?&$Y&P?6pz|U}|3yI;*$A/MQU,C.`la-4q,IDcfSqk`Aq M{Yx$v? ar(K<');
define('SECURE_AUTH_KEY',  'C?aae%q90dQSpOuf TI{.jkjN~DP}Vl0xhP<|wz<> E|MU!x.4C}gBOh,Wu?#Nw7');
define('LOGGED_IN_KEY',    'QK]hj6C6siP<Wkf#3sjo<^0&ilCOCVq=O>U272{2sYHrXc/Aypgjuej{{8]6QY08');
define('NONCE_KEY',        '(it3x--2{tjOvGy%qRCr%Ma{L(0|K:U@>fp#D5O$# 4w|K@,xCMR7sc$?F%<K>3a');
define('AUTH_SALT',        '~u2+x91I3c]wN.>S+H#Y>YN:R^|z}-:7?Joh5vl^u7oGv:lA<?H+uXjhV~[nFnbL');
define('SECURE_AUTH_SALT', 'K*T&3<A5>09*VV`;+NtnT,-@|d!ZGs=y..(dt,@L5T}Gh5k3{<PV5r50o]2|[;j~');
define('LOGGED_IN_SALT',   'ap.7?L7.W4Q;Q2*}~5yjiYvE/3^ig@ek3EZ6Wt:D5ZV}0 4!0>s1~+NAxIXvNm%1');
define('NONCE_SALT',       'D ?7GMs<e5<E,  YJnL4,5&r9tThejCC)5L;*m]3YV-UNxz<B`:gT=mR)+TOm>UT');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'ts_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
