Halo mas Wawan, gimana kabar?
Lama tak jumpa. Barusan dapet email kalo akses ke beberapa repository udah di revoke sama mas Wawan.
Mungkin tinggal ini repository terakhir yang aku masih punya akses.

Sebelumnya maaf kalau nggak ngomong-ngomong mas Wawan aku apply kerja di UGM.
Niat awalku apply kerja di UGM cuma iseng aja mas, beneran iseng doang, nemenin temenku yang apply disana.
Kalau keterima juga nggak aku ambil. Tapi, setelah sebulan lebih Mas Wawan "ngilang", akhirnya aku ambil kerjaan di UGM.
Ya karena nggak ada kabar sama sekali waktu itu dari mas Wawan.

Fadhil juga minta maaf berkali-kali ke aku, "Sori mas udah nyeret mas Suko ke TotalStudio."
Ya aku bilang ke Fadhil mas itu bukan salah dia, aku emang ngerasa keknya bisa lebih berkembang di TotalStudio.
Dan itu yang emang aku rasain setelah beberapa bulan join timnya Mas Wawan.

Kemaren sempet berharap bisa ketemu sama mas Wawan sama temen-temen yang lain juga buat nge-clear in masalah kita ini.
Pas Mas Ary bilang mau ngatur pertemuan buat kita semua, sempet kepikiran sih, "yes, kita ketemu lagi."

Mas Wawan, kalau memang ada masalah terkait perusahaan, cerita ke kita openly aja.
Kita semua udah paham perusahaan baru itu kek gimana, termasuk resiko bangkrut.
Tapi tolong dijelaskan ke kita mas, kenapa bisa gitu.
Yang butuh kepastian bukan cuma cewek aja mas, karyawan juga.

Terakhir denger kabar dari Mas Ary kalau barang-barang di kantor udah dijual2in sama seseorang.
Well mas, satu-satunya barang kantor yang aku jual adalah SSD, itupun setelah aku minta ijin beberapa kali sama Mas Ary.
Mas Ary malah nyaranin jualin laptopnya aja, tapi aku jual SSDnya aja.

Waktu itu mas Wawan juga bilang mau ada evaluasi, pimpinan ke yang dipimpin, yang dipimpin ke yang memimpin.
Kita sebagai followernya mas Wawan pasti punya salah mas, banyak.
Waktu itu aku juga nungguin kapan mengevaluasi, dan dievaluasi of course.

Thanks mas atas pelajaran yang diberikan selama 6 bulan di TotalStudio.
Masih berharap penjelasan dari mas Wawan sih. Best of luck to you, mas.