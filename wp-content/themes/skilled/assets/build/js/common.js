$(document).ready(function () {
    $("#btn-switch-country").change(function () {
        var country = $("#btn-switch-country").val();

        console.log("change country to:" + country);

        if (country == "") {
            window.location.assign("/");
        } else {
            window.location.assign("/" + country + "/");
        }
    });

    $(".navbar-search-button").click(function () {
        $("#navbar-search form").toggleClass('active')
    });
    
    $(".search-overlay").click(function(){
        $('.search-overlay').attr("style", "visibility: hidden");
        $('#navbar-search').attr("style", "visibility: hidden");
    });

    var offset = 300,
            //browser window scroll (in pixels) after which the "back to top" link opacity is reduced
            offset_opacity = 1200,
            //duration of the top scrolling animation (in ms)
            scroll_top_duration = 700,
            //grab the "back to top" link
            $back_to_top = $('.cd-top');

    //hide or show the "back to top" link
    $(window).scroll(function () {
        ($(this).scrollTop() > offset) ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');
        if ($(this).scrollTop() > offset_opacity) {
            $back_to_top.addClass('cd-fade-out');
        }
    });

    //smooth scroll to top
    $back_to_top.on('click', function (event) {
        event.preventDefault();
        $('body,html').animate({
            scrollTop: 0,
        }, scroll_top_duration
                );
    });
    
    $("#more").hide();
    $("#see-more").click(function () {
        var the_html = $("#see-more").html();
        if (the_html == "[See more]") {
            $("#see-more").html("[See less]")
            $("#more").show();
        } else {
            $("#see-more").html("[See more]")
            $("#more").hide();
        }
    });
});
