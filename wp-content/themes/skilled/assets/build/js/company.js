$(document).ready(function () {
    $("#carouselExampleControls").on("slid.bs.carousel", function (a) {
        var s = $("#carouselExampleControls .carousel-item.active").index("#carouselExampleControls .carousel-item");
        $(".company-projects-thumbnail-list a").removeClass("is-active"), $(".company-projects-thumbnail-list a").eq(s).addClass("is-active")
    }), $(".company-projects-thumbnail-list a").click(function (a) {
        $(".company-projects-thumbnail-list a").removeClass("is-active"), $(this).addClass("is-active")
    })

    $("#submit-proposal").click(function () {
        var home_url = $("#home_url").val();
        var proposal_company = $("#proposal_company").val();
        var proposal_name = $("#proposal_name").val();
        var proposal_email = $("#proposal_email").val();
        var proposal_contact = $("#proposal_contact").val();
        var proposal_editor = $("#proposal_editor").val();

        var the_submit = true;

        $("#error-message").html("");

        if (proposal_name == "") {
            the_submit = false;
            $("#proposal_name").addClass("field-must-not-empty");
            $("#error-message").append("Name must not be empty<br>");
        } else {
            $("#proposal_name").removeClass("field-must-not-empty");
        }

        if (proposal_email == "") {
            the_submit = false;
            $("#proposal_email").addClass("field-must-not-empty");
            $("#error-message").append("Email must not be empty<br>");
        } else {
            $("#proposal_email").removeClass("field-must-not-empty");
        }

        if (!validateEmail(proposal_email)) {
            the_submit = false;
            $("#proposal_email").addClass("field-must-not-empty");
            $("#error-message").append("Email not valid<br>");
        } else {
            $("#proposal_email").removeClass("field-must-not-empty");
        }

        if (proposal_contact == "") {
            the_submit = false;
            $("#proposal_contact").addClass("field-must-not-empty");
            $("#error-message").append("Contact must not be empty<br>");
        } else {
            $("#proposal_contact").removeClass("field-must-not-empty");
        }

        if (proposal_contact.length < 9) {
            the_submit = false;
            $("#proposal_contact").addClass("field-must-not-empty");
            $("#error-message").append("Phone number must contains at least 9 characters<br>");
        } else {
            $("#proposal_contact").removeClass("field-must-not-empty");
        }

        if (the_submit == true) {

            $.ajax({
                type: "POST",
                url: home_url + "/wp-admin/admin-ajax.php",
                data: {
                    action: 'save_proposal',
                    'formData': {
                        company: proposal_company,
                        name: proposal_name,
                        email: proposal_email,
                        contact: proposal_contact,
                        editor: tinymce.get('proposal_editor').getContent(),
                    }
                },
                success: function (data) {
                    msg = JSON.parse(data);

                    if (msg.status == true) {
                        $("#myModal").modal("hide");
                        $("#successModal").modal("show");
                        $("#proposal_name").val('');
                        $("#proposal_email").val('');
                        $("#proposal_contact").val('');
                        $("#proposal_editor").html('');
                    } else {
                        $("#myModal").modal("hide");
                        $('#failedmsg').html(msg.message);
                        $("#failedModal").modal("show");
                    }
                }
            });
        } else {
            $("#error-message").append("<br>");
        }

    });

    $("#submit-claim").click(function () {
        console.log("submit");
        var home_url = $("#home_url").val();
        var id_company = $("#id_company").val();

        var the_submit = true;

        if (the_submit == true) {

            $.ajax({
                type: "POST",
                url: home_url + "/wp-admin/admin-ajax.php",
                data: {
                    action: 'save_claim',
                    'formData': {
                        id_company: id_company
                    }
                },
                success: function (data) {
                    msg = JSON.parse(data);

                    if (msg.status == true) {
                        $("#claimModal").modal("hide");
                        $("#successModal").modal("show");
                    } else {    
                        $("#claimModal").modal("hide");
                        $('#failedmsg').html(msg.message);
                        $("#failedModal").modal("show");
                    }
                }
            });
        } else {
            $("#error-message").append("<br>");
        }

    });
});

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

