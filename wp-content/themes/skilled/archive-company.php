<?php get_header(); ?>
<section class="section section-companies">
    <section class="section-inner">
        <div class="breadcrumbs">
            <div class="container">
                <a href="index.html">Home</a> <i class="fa fa-angle-double-right"></i> <a href="/company_guide">Company Guide</a>
            </div>
        </div>
        <div class="page-title">
            <div class="container">
                <h1><?php
                    $obj = get_post_type_object('organization');
                    echo $obj->labels->singular_name;
                    ?></h1>
            </div>
        </div>
        <div class="page-content">
            <div class="container">
                <div class="row">
                    <?php
                    $query = new WP_Query([
                        "post_type" => "organization",
                        "post_parent" => 0
                    ]);


                    if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post();
                            ?>
                            <div class="col-xs-12 col-md-3">
                                <article class="card c_company_category">
                                    <div class="card-header"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
                                    <ul class="list-group list-group-flush">
                                        <?php
                                        $query2 = new WP_Query([
                                            "post_type" => "organization",
                                            "post_parent" => get_the_ID()
                                        ]);
                                        if ($query2->have_posts()) : while ($query2->have_posts()) : $query2->the_post();
                                        ?>
                                        <li class="list-group-item"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                                        <?php
                                            endwhile;
                                        endif;
                                        ?>
                                    </ul>
                                </article>
                            </div>
                            <?php
                        endwhile;
                    else :
                        ?>
                        <div class="col-xs-12 col-md-12">
                            <article class="card c_company_category">
                                <div class="card-header">No post found</div>
                            </article>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>
</section>
<?php get_footer() ?>