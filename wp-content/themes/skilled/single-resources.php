<?php
get_header();
$org_slug = "resources";
switch (DB_NAME) {
    case "skilled_fr":
        $org_slug = "ressources";
        break;
    case "skilled_nl":
        $org_slug = "bronnen";
        break;
}
?>
<section class="section section-resource">
    <section class="section-inner">
        <div class="breadcrumbs">
            <div class="container">
                <a href="<?php echo home_url(); ?>"><?php _e('Home', 'skilled'); ?></a> <i class="fa fa-angle-double-right"></i>
                <a href="<?php echo home_url(); ?>/<?php echo $org_slug; ?>"><?php _e('Resources', 'skilled'); ?></a>
                <i class="fa fa-angle-double-right"></i> <?php the_title(); ?>
            </div>
        </div>
        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="resource-thumbnail">
                            <?php the_post_thumbnail('medium_large') ?>
                        </div>
                        <div class="resource-title">
                            <h1><?php the_title(); ?></h1>
                           <i class="fa fa-calendar"></i> Posted on <?php the_date() ?> &nbsp; <?php the_time() ?> by <?php the_author(); ?> 
                        </div>
                        <div class="resource-text">
                            <?php if (have_posts()) : while (have_posts()) : the_post(); // run the loop  ?>
                                    <?php
                                    the_content();
                                    $img = get_post_meta(get_the_ID(), "_resource_infographic_url", true);
                                    $backlink = get_post_meta(get_the_ID(), "_resource_backlink_url", true);

                                    if (!$backlink) {
                                        $backlink = get_permalink();
                                    }

                                    $images = "";
                                    if (is_array($img)) {
                                        foreach ($img as $i) {
                                            $images .= "<img src='" . $i['_resource_infographic_url'] . "'>";
                                        }
                                    } else {
                                        if (!empty($img)) {
                                            $images .= "<img src='" . $img . "'>";
                                        }
                                    }
                                    ?>

                                    <div class="embed-code">
                                        <h2 class="embed-code-title"><span><?php _e('Embed this on your website/blog', 'skilled'); ?></span></h2>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <textarea class="embed-code-code"><p><a href="<?php echo $backlink; ?>" target="_blank" title="<?php the_title(); ?>"><?php echo $images; ?></a><br>Presented by <a href="https://skilled.co/" target="_blank" title="Skilled.co">Skilled.co</a></p></textarea>
                                            </div>
                                        </div>
                                    </div>
                                <?php endwhile; ?>
                                <div class="embed-code related-articles">
                                    <h2 class="embed-code-title"><span><?php _e('Related Articles', 'skilled'); ?></span></h2>
                                    <div class="embed-code-content">
                                        <div class="row">
                                            <?php
                                            global $post;
                                            $args = array(
                                                'post_type' => 'resources',
                                                'post__not_in' => array($post->ID),
                                                'posts_per_page' => 3
                                            );
                                            $my_query = new wp_query($args);
                                            if ($my_query->have_posts()) {

                                                while ($my_query->have_posts()) {
                                                    $my_query->the_post();
                                                    ?>
                                                    <article class="col-xs-12 col-lg-4">
                                                        <div class="card c_resources">
                                                            <div class="thumbnail">
                                                                <?php
                                                                if (has_post_thumbnail()) {
                                                                    the_post_thumbnail();
                                                                } else {
                                                                    ?>
                                                                    <img src="<?php echo bloginfo("template_url") . "/assets/img/placeholder.jpg"; ?>" alt="<?php the_title(); ?>"  />
                                                                <?php } ?>
                                                            </div>
                                                            <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                                            <div class="content">
                                                                <?php the_excerpt(); ?>
                                                            </div>
                                                        </div>
                                                    </article>
                                                    <?php
                                                }
                                            }
                                            wp_reset_query();
                                            ?>
                                        </div>
                                    </div>
                                </div>

                                <?php
                            else:
                                _e("No record found", "skilled");
                            endif;
                            ?>
                            <div class="comments">
                                <?php
                                if (comments_open() || get_comments_number()) :
                                    comments_template();
                                endif;
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>
<?php get_footer() ?>