<?php get_header(); ?>
<section class="section section-company-top">
    <section class="section-inner">
        <div class="breadcrumbs">
            <div class="container">
                <a href="index.html">Home</a> <i class="fa fa-angle-double-right"></i> <a href="company-index.html">Company </a> <i class="fa fa-angle-double-right"></i> Old City Press
            </div>
        </div>
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <div class="page-title">
                    <div class="container">
                        <h1><?php the_title() ?></h1>
                    </div>
                </div>
                <?php
            endwhile;
        else :
            ?>
            <div class="company-description">
                <h3>Post Not Found</h3>
                <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
            </div>
        <?php endif; ?>
    </section>
</section>
<section class="section section-company">
    <section class="section-inner">
        <div class="container">
            <div class="page-content">
                <div class="row">
                    <div class="col-md-9">
                        <!--
                        <div class="company-logo">
                            <a href="https://skilled.co/company/old-city-press/" target="_blank">
                                <img class="media-object" src="https://skilled.co/wp-content/uploads/2017/01/Old-City-Press.png" alt="Old City Press"/>
                            </a>
                        </div>
                        <div class="company-main-info">
                            <span class="company-info-rating">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star-half-empty"></i>
                                <i class="fa fa-star-o"></i>
                            </span>
                        </div>
                        -->
                        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                                <div class="company-description">
                                    <h3><a href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
                                    <?php the_content(); ?>
                                </div>
                            <?php endwhile;
                        else :
                            ?>
                            <div class="company-description">
                                <h3>Post Not Found</h3>
                                <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
                            </div>
<?php endif; ?>
                    </div>
                    <div class="col-md-3">
                        <div class="company-info" id="company-info">

                            <!--
                            <h2>Company Info</h2>
                            <ul>
                                <li><i class="fa fa-map-marker"></i> <span>Alexandria, Egypt</span></li>
                                <li><i class="fa fa-phone"></i> <span>571-858-5940</span></li>
                                <li><i class="fa fa-user"></i> <span>0-10</span></li>
                                <li><i class="fa fa-credit-card"></i> <span>$25 / hour</span></li>
                            </ul>
                            -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>
<?php get_footer() ?>