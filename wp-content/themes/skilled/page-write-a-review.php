<?php /* Template Name: Member area */ ?>
<?php get_header(); ?>
<?php
if (!is_user_logged_in()) {
    echo '<style>.section-company-settings:before{display:none!important;}</style>';
}
global $current_user;
get_currentuserinfo();
?>
<?php
if (is_user_logged_in()) {
    ?>
    <section class="section section-company-top">
        <section class="section-inner">
            <div class="page-title">
                <div class="container">
                    <h1 style="font-family: Montserrat; padding: 1.5rem 0px;"><?php _e("Hello", "skilled"); ?>, <?php echo $current_user->user_firstname; ?> <?php echo $current_user->user_lastname; ?></h1>
                </div>
            </div>
        </section>
    </section>
<?php }
?>
<section class="section section-company-settings">
    <div class="section-inner">
        <div class="container">
            <div class="row">
                <?php echo apply_filters('the_content', $post->post_content); ?>
            </div>
        </div>
    </div>
</section>
<?php get_footer() ?>