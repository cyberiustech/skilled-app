<?php get_header(); ?>
<section class="section section-banner">
    <div class="section-inner">
        <div class="container">
            <h1 class="section-banner-title">
                <div class="cd-intro">
                    <h1 class="cd-headline letters type">
                        <span><?php _e("Discover and Review<br>the top", "skilled"); ?></span> 
                        <span class="yellow-color title-swap container">
                            <span class="cd-words-wrapper waiting container">
                                <?php
                                $homeurl = get_home_url();
                                if ($homeurl == "https://skilled.co/ru") {
                                    ?>
                                    <b class="is-visible"><?php _e("SEO КОМПАНИЙ", "skilled"); ?></b>
                                    <b><?php _e("ВЕБ-СТУДИЙ", "skilled"); ?></b>
                                    <b><?php _e("РАЗРАБОТЧИКОВ МОБИЛЬНЫХ ПРИЛОЖЕНИЙ", "skilled"); ?></b>
                                    <?php
                                } else {
                                    ?>
                                    <b class="is-visible"><?php _e("WEB DEVELOPERS", "skilled"); ?></b>
                                    <b><?php _e("MOBILE APP DEVELOPERS", "skilled"); ?></b>
                                    <b><?php _e("WEB AGENCIES", "skilled"); ?></b>
                                    <b><?php _e("SEO AGENCIES", "skilled"); ?></b>
                                    <b><?php _e("SPAM FILTERING SERVICES", "skilled"); ?></b>
                                    <?php
                                }
                                ?>
                            </span>
                        </span>
                    </h1>
                </div> <!-- cd-intro -->
            </h1>
            <div class="section-banner-separator"></div>
            <div class="section-banner-description">
                <?php _e("SEO, Web & Mobile Development Firms Ranked by Their Performance", "skilled"); ?>
            </div>
            <div class="section-banner-action">
                <a class="call-to-action" href="<?php echo home_url(); ?>/companies"><i class="fa fa-angle-right"></i> <?php _e("Find Companies", "skilled"); ?></a>
            </div>
        </div>
    </div>
</section>
<section class="section section-features">
    <div class="section-inner">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="feature-title">
                        <div class="feature-title-icon"><i class="fa fa-leaf"></i></div>
                        <div class="feature-title-caption"><?php _e('Solid Data', "skilled") ?></div>
                    </div>
                    <div class="feature-caption">
                        <?php _e('Find best companies that meet your business needs', "skilled"); ?>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="feature-title">
                        <div class="feature-title-icon"><i class="fa fa-smile-o"></i></div>
                        <div class="feature-title-caption"><?php _e('Social Proof', "skilled") ?></div>
                    </div>
                    <div class="feature-caption">
                        <?php _e("All company reviews are real and submitted by their own clients", "skilled"); ?>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="feature-title">
                        <div class="feature-title-icon"><i class="fa fa-bullseye"></i></div>
                        <div class="feature-title-caption"><?php _e("Specific Search", "skilled"); ?></div>
                    </div>
                    <div class="feature-caption">
                        <?php _e("Cover specificity, budget, time and responsiveness requests", "skilled"); ?>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="feature-title">
                        <div class="feature-title-icon"><i class="fa fa-line-chart"></i></div>
                        <div class="feature-title-caption"><?php _e("Continuous Improvement", "skilled"); ?></div>
                    </div>
                    <div class="feature-caption">
                        <?php _e("Contribute by sharing your experience in a review", "skilled"); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section section-featured-companies">
    <div class="section-inner">
        <div class="container">
            <h2 class="section-featured-companies-title"><?php _e("Featured Companies", "skilled"); ?></h2>
            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner" role="listbox">
                    <?php
                    $the_query = new WP_Query([
                        'post_type' => 'company',
                        'posts_per_page' => '12'
                    ]);

                    if ($the_query->have_posts()) {
                        echo '<ul>';
                        $i = 0;
                        while ($the_query->have_posts()) {
                            if ($i == 0) {
                                echo "<div class='carousel-item active'><div class='carousel-item-row row'>";
                            } else if ($i % 4 == 0) {
                                echo "<div class='carousel-item'><div class='carousel-item-row row'>";
                            }
                            $the_query->the_post();
                            $logo = get_post_meta(get_the_ID(), "_company_logo");
                            ?>
                            <div class="carousel-item-row-item col-md-3">
                                <a href="<?php the_permalink(); ?>">
                                    <img class="featured-company-logo" src="<?php echo $logo[0]; ?>">
                                </a>
                                <?php the_title(); ?>
                            </div>
                            <?php
                            $i++;
                            if ($i % 4 == 0) {
                                echo "</div></div>";
                            }
                        }
                        echo '</ul>';
                        wp_reset_postdata();
                    } else {
                        // no posts found
                    }
                    ?>
                </div>
                <a class="carousel-control carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                    <i class="fa fa-angle-left"></i>
                </a>
                <a class="carousel-control carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                    <i class="fa fa-angle-right"></i>
                </a>
            </div>
        </div>
    </div>
</section>
<section class="section section-get-ranked">
    <div class="section-inner">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="section-get-ranked-title">
                        <?php _e("DO YOU PROVIDE A TOP SERVICE<br>IN YOUR INDUSTRY?<br>", "skilled"); ?>
                        <small><?php _e("Get ranked in our directories", "skilled"); ?></small><br>
                        <a href="<?php echo home_url(); ?>/companies"><?php _e("Get Ranked", "skilled"); ?> <i class="fa fa-angle-right"></i></a>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="section-get-ranked-list">
                        <ul>
                            <li><i class="fa fa-leaf"></i> <?php _e("Describe your company with key metrics", "skilled"); ?></li>
                            <li><i class="fa fa-leaf"></i> <?php _e("Provide a good piece of social proof", "skilled"); ?></li>
                            <li><i class="fa fa-leaf"></i> <?php _e("Get the exposure you deserve", "skilled"); ?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php get_footer() ?>