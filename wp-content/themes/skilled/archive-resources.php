<?php
get_header();
global $wp;
$org_slug = "resources";
switch (DB_NAME) {
    case "skilled_fr":
        $org_slug = "ressources";
        break;
    case "skilled_nl":
        $org_slug = "bronnen";
        break;
}
?>
<section class="section section-resources">
    <section class="section-inner">
        <div class="breadcrumbs">
            <div class="container">
                <a href="<?php echo home_url(); ?>"><?php _e('Home', 'skilled'); ?></a> <i class="fa fa-angle-double-right"></i>
                <a href="<?php echo home_url(); ?>/<?php echo $org_slug; ?>"><?php _e('Resources', 'skilled'); ?></a>
            </div>
        </div>
        <div class="page-title">
            <div class="container">
                <h1><?php _e('Resources', 'skilled'); ?></h1>
            </div>
        </div>
        <div class="page-content">
            <div class="container">
                <div class="row">
                    <?php
                    $paged = ( get_query_var('page') ) ? get_query_var('page') : 1;
                    $rtag = ( get_query_var('rtag') ) ? get_query_var('rtag') : "";

                    $args = [
                        "post_type" => "resources",
                        'posts_per_page' => 12,
                        'paged' => $paged,
                        'tax_query' => [
                            "taxonomy" => " tag_term",
                            "field" => "slug",
                            "terms" => $rtag
                        ]
                    ];

                    $the_query = new WP_Query($args);
                    ?>
                    <?php if ($the_query->have_posts()) : while ($the_query->have_posts()) : $the_query->the_post(); // run the loop  ?>
                            <div class="col-md-4">
                                <div class="resources-item">
                                    <div class="resources-thumbnail">
                                         <a href="<?php the_permalink(); ?>">
                                        <?php the_post_thumbnail('medium') ?>
                                        </a>
                                    </div>
                                    <h2 class="resources-title">
                                        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                    </h2>
                                    <div class="resources-shorttext">
                                        <?php
                                        $content = get_the_excerpt();
                                        echo $content;
                                        ?>
                                    </div>
                                    <div class="resources-readmore">
                                        <a href="<?php the_permalink(); ?>" class="btn btn-primary"><?php _e("Read More", "skilled"); ?> ...</a>
                                    </div>
                                </div>
                            </div>
                        <?php endwhile; ?>
                    <?php else: ?>
                        <div class="col-md-12">
                            <div class="resources-item">
                                <h2 class="resources-title">
                                    <a href=""><?php _e("No record found", "skilled"); ?></a>
                                </h2>
                                <div class="resources-shorttext">
                                    <?php _e("No record found", "skilled"); ?>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="pagination-container">
                            <?php
                            $paged_max = $the_query->max_num_pages;
                            $current_page = home_url($wp->request);
                            skilled_pagination($paged, $paged_max, $current_page);
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>


<?php get_footer() ?>