<?php get_header(); ?>
<?php
$org_slug = "companies";
switch (DB_NAME) {
    case "skilled_fr":
        $org_slug = "entreprises";
        break;
    case "skilled_nl":
        $org_slug = "bedrijven";
        break;
}
?>
<section class="section section-companies">
    <section class="section-inner">
        <div class="breadcrumbs">
            <div class="container">
                <a href="<?php echo home_url(); ?>"><?php _e("Home", "skilled"); ?></a> <i class="fa fa-angle-double-right"></i> 
                <a href="<?php echo home_url(); ?>/<?php echo $org_slug; ?>"><?php _e("Company Guide", "skilled"); ?></a>
            </div>
        </div>
        <div class="page-title">
            <div class="container">
                <h1><?php _e("Company Guide", "skilled"); ?></h1>
            </div>
        </div>
        <div class="page-content">
            <div class="container">
                <div class="row">
                    <?php
                    $query = new WP_Query([
                        "post_type" => "organization",
                        "post_parent" => 0,
                        'orderby' => 'ID',
                        'order' => 'asc'
                    ]);


                    if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post();
                            ?>
                            <div class="col-xs-12 col-md-3">
                                <article class="card c_company_category">
                                    <div class="card-header"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
                                    <ul class="list-group list-group-flush">
                                        <?php
                                        $query2 = new WP_Query([
                                            "post_type" => "organization",
                                            "post_parent" => get_the_ID()
                                        ]);
                                        if ($query2->have_posts()) : while ($query2->have_posts()) : $query2->the_post();
                                                ?>
                                                <li class="list-group-item"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                                                <?php
                                            endwhile;
                                        endif;
                                        ?>
                                    </ul>
                                </article>
                            </div>
                            <?php
                        endwhile;
                    else :
                        ?>
                        <div class="col-xs-12 col-md-12">
                            <article class="card c_company_category">
                                <div class="card-header"><?php _e("No record found", "skilled"); ?></div>
                            </article>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>
</section>
<?php get_footer() ?>