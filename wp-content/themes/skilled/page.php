<?php get_header(); ?>
<section class="section section-about">
    <section class="section-inner">
        <div class="breadcrumbs">
            <div class="container">
                <a href="<?php echo home_url(); ?>">Home</a> <i class="fa fa-angle-double-right"></i> <?php the_title(); ?>
            </div>
        </div>
        <div class="page-title">
            <div class="container">
                <h1><?php the_title() ?></h1>
            </div>
        </div>
        <div class="page-content">
            <div class="container">
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                        <?php the_content(); ?>
                        <?php
                    endwhile;
                endif;
                ?>
            </div>
        </div>
    </section>
</section>
<?php get_footer() ?>