<?php
require_once 'inc/SkilledNavWalker.php';
require_once 'inc/force_https.php';

$uri = get_template_directory_uri();
$walker = new wp_bootstrap_navwalker();

$req_uri = explode("/", $_SERVER['REQUEST_URI']);

$country = (isset($req_uri[1]) && !empty($req_uri[1]) ? $req_uri[1] : "us");
$countries = ["us", "fr", "ru", "nl"];
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="<?php echo $uri; ?>/assets/img/favicon.ico">

        <title><?php
            set_page_title($req_uri);
            bloginfo('name');
            ?>
        </title>

        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">

        <!-- Custom styles for this template -->
        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,600,700&amp;subset=latin-ext" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo $uri; ?>/assets/build/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo $uri; ?>/assets/build/rangeSlider/css/ion.rangeSlider.css">
        <link rel="stylesheet" href="<?php echo $uri; ?>/assets/build/rangeSlider/css/ion.rangeSlider.skinFlat.css">
        <link rel="stylesheet" href="<?php echo $uri; ?>/assets/build/icheck/skins/line/yellow.css?v=1.0.2">

        <link rel="stylesheet" href="<?php echo $uri; ?>/assets/build/css/shared.css">
        <link rel="stylesheet" href="<?php echo $uri; ?>/assets/build/css/home.css">
        <link rel="stylesheet" href="<?php echo $uri; ?>/assets/build/css/common.css">
        <link rel="stylesheet" href="<?php echo $uri; ?>/assets/build/css/companies.css">
        <link rel="stylesheet" href="<?php echo $uri; ?>/assets/build/css/company.css">
        <link rel="stylesheet" href="<?php echo $uri; ?>/assets/build/css/reviews.css">
        <link rel="stylesheet" href="<?php echo $uri; ?>/assets/build/css/review.css">
        <link rel="stylesheet" href="<?php echo $uri; ?>/assets/build/css/resources.css">

        <link rel="stylesheet" href="<?php echo $uri; ?>/style.css">

        <script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
        <script src="<?php echo get_template_directory_uri(); ?>/assets/build/rangeSlider/js/ion-rangeSlider/ion.rangeSlider.min.js"></script>
        <script src="<?php echo get_template_directory_uri(); ?>/assets/build/icheck/icheck.js?v=1.0.2"></script>
        <?php
        wp_head();
        ?>
    </head>
    <body>
        <div class="wrapper">
            <header class="header">
                <nav class="navbar navbar-toggleable-md fixed-top" <?php if (is_admin_bar_showing()) { ?>style="margin-top: 32px;"<?php } ?>>
                    <div class="container">
                        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
                            <i class="fa fa-bars"></i>
                        </button>
                        <?php
                        if (trim($req_uri[1]) == "companies" && !empty($req_uri[2]) || trim($req_uri[2]) == "companies" && !empty($req_uri[3])) {
                            ?>
                            <button class="navbar-toggler navbar-toggler-right toggler-company-filter" type="button">
                                <i class="fa fa-sliders"></i>
                            </button>
                            <?php
                        }
                        ?>
                        <a class="navbar-brand" href="<?php echo home_url(); ?>/">
                            <img src="<?php echo $uri; ?>/assets/img/skilled_logo_why.png" class="nav-logo">
                        </a>
                        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
                            <?php
                            if (has_nav_menu('primary-menu')) {
                                wp_nav_menu([
                                    'theme_location' => 'primary-menu',
                                    'depth' => 10,
                                    'menu_class' => 'nav navbar-nav',
                                    'fallback_cb' => 'wp_bootstrap_navwalker::fallback',
                                    'container' => '',
                                    'walker' => $walker
                                ]);
                            }
                            ?>
                            <div style="clear: both;"></div>
                            <?php
                            $homeurl = get_home_url();
                            switch ($homeurl) {
                                case "https://skilled.co":
                                    $member_url = "member";
                                    break;
                                case "https://skilled.co/fr":
                                    $member_url = "membre";
                                    break;
                                case "https://skilled.co/nl":
                                    $member_url = "member";
                                    break;
                                case "https://skilled.co/ru":
                                    $member_url = "member";
                                    break;
                                default:
                                    $member_url = "member";
                                    break;
                            }

                            if (is_user_logged_in()) {
                                echo '<ul id="menu-main-menu" class="nav navbar-nav primary-menu-2">';
                                ?>
                                <div class="navbar-search" id="navbar-search">
                                    <form method="get" action="/">
                                        <input type="text" id="nav-search" class="nav-search" name="s" value="" placeholder="Type search here...">
                                    </form>
                                    <button type="button" class="btn btn-primary navbar-search-button"><i class="fa fa-search"></i></button>
                                </div>
                                <?php
                                echo '<li class="menu-item menu-item-type-post_type menu-item-object-organization nav-item"><a class="nav-link" href="' . home_url() . '/' . $member_url . '/">';
                                _e("My Profile", "skilled");
                                echo '</a></li>';
                                echo '</ul>';
                            } else {
                                echo '<ul id="menu-main-menu" class="nav navbar-nav primary-menu-2">';
                                ?>
                                <div class="navbar-search" id="navbar-search">
                                    <form method="get" action="/">
                                        <input type="text" id="nav-search" class="nav-search" name="s" value="" value="" placeholder="Type search here...">
                                    </form>
                                    <button type="button" class="btn btn-primary navbar-search-button"><i class="fa fa-search"></i></button>
                                </div>
                                
                                <?php
                                echo '<li class="menu-item menu-item-type-post_type menu-item-object-organization nav-item"><a class="nav-link" href="' . home_url() . '/' . $member_url . '/">';
                                _e("Login", "skilled");
                                echo '</a></li></ul>';
                            }
                            /*
                              if (has_nav_menu('primary-menu-2')) {
                              wp_nav_menu([
                              'theme_location' => 'primary-menu-2',
                              'depth' => 2,
                              'menu_class' => 'nav navbar-nav primary-menu-2',
                              'fallback_cb' => 'wp_bootstrap_navwalker::fallback',
                              'walker' => $walker
                              ]);
                              } */
                            ?>
                            <div style="clear: both;"></div>
                            <select id="btn-switch-country" class="dropdown-switch-country">
                                <?php
                                foreach ($countries as $c) {
                                    $selected = "";
                                    if ($c == $country) {
                                        $selected = " selected='selected'";
                                    }
                                    if ($c != "us") {
                                        echo "<option value='" . $c . "'" . $selected . ">" . strtoupper($c) . "</option>";
                                    } else {
                                        echo "<option value=''" . $selected . ">" . strtoupper($c) . "</option>";
                                    }
                                }
                                ?>
                            </select>
                            <div style="clear: both;"></div>
                        </div>
                    </div>
                </nav>
            </header>