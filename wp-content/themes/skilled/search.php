<?php
get_header();
global $wp;
?>
<section class="section section-resources">
    <section class="section-inner">
        <div class="breadcrumbs">
            <div class="container">
                <a href="<?php echo home_url(); ?>"><?php _e("Home", "skilled"); ?></a> <i class="fa fa-angle-double-right"></i> <?php _e("Search Result", "skilled"); ?>
            </div>
        </div>
        <div class="page-title">
            <div class="container">
                <h1>Search for: <?php echo get_search_query(); ?></h1>
            </div>
        </div>
        <div class="page-content">
            <div class="container">
                <div class="row">
                    <?php
                    wp_reset_query();
                    $the_query = new WP_Query([
                        's' => get_search_query(),
                        'post_type' => ['company', 'resources'],
                        'posts_per_page' => -1
                    ]);
                    ?>
                    <?php if ($the_query->have_posts()) : while ($the_query->have_posts()) : $the_query->the_post(); // run the loop  ?>
                            <div class="col-md-12">
                                <div class="resources-item">
                                    <h2 class="resources-title">
                                        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                    </h2>
                                    <div class="resources-shorttext">
                                        <div class="row">
                                            <?php
                                            $postype = get_post_type();

                                            if ($postype == "company") {
                                                $logo = get_post_meta(get_the_ID(), "_company_logo");
                                                $description = get_post_meta(get_the_ID(), "_company_summary");
                                                echo "<div class='col-md-3'>";
                                                echo "<div class='search-result-img'><img src='" . $logo[0] . "'></div>";
                                                echo "</div>";

                                                echo "<div class='col-md-9'>";
                                                echo "<div class='search-result-text'>";
                                                echo short_content($description[0], intval(400));
                                                echo "</div>";
                                                echo "</div>";
                                            } else {
                                                $content = get_the_excerpt();
                                                echo "<div class='col-md-3'>";
                                                echo "<div class='search-result-img'>";
                                                the_post_thumbnail('medium');
                                                echo "</div>";
                                                echo "</div>";
                                                echo "<div class='col-md-9'>";
                                                echo "<div class='search-result-text'>" . $content . "</div>";
                                                echo "</div>";
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    <div class="resources-readmore">
                                        <a href="<?php the_permalink(); ?>" class="btn btn-primary"><?php _e("Read More", "skilled"); ?></a>
                                    </div>
                                </div>
                            </div>
                        <?php endwhile; ?>
                    <?php else: ?>
                        <div class="col-md-12">
                            <div class="resources-item">
                                <h2 class="resources-title">
                                    <a href=""><?php _e("No record found", "skilled"); ?></a>
                                </h2>
                                <div class="resources-shorttext">
                                    <?php _e("No record found", "skilled"); ?>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>
</section>


<?php get_footer() ?>