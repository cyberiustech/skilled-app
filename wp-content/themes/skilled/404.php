<?php get_header(); ?>
<section class="section section-about">
    <section class="section-inner">
        <div class="breadcrumbs">
            <div class="container">
                Home <i class="fa fa-angle-double-right"></i> Not Found
            </div>
        </div>
        <div class="page-title">
            <div class="container">
                <h1>Not Found</h1>
            </div>
        </div>
        <div class="page-content">
            <div class="container">
                <p>Sorry! It seems the page that you are looking for is not here.</p>
            </div>
        </div>
    </section>
</section>

<?php get_footer() ?>