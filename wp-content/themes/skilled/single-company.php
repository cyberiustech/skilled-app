<?php get_header(); ?>
<section class="section section-company-info">
    <div class="section-inner">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="content-container">
                        <div class="content">
                            <div class="breadcrumbs">
                                <a href='<?php echo home_url(); ?>/'><?php _e("Home", "skilled"); ?></a> > <a href='<?php echo home_url(); ?>/companies/'><?php _e("Companies", "skilled"); ?></a> > <?php the_title(); ?>
                            </div>
                            <div class="company-attributes">
                                <?php
                                $c_logo = get_post_meta(get_the_ID(), "_company_logo");

                                $review_data = get_posts([
                                    'post_type' => 'review',
                                    'meta_key' => '_review_company_id',
                                    'meta_value' => get_the_ID()
                                ]);
                                $count_review = 0;
                                $overall_rating = 0;
                                foreach ($review_data as $single_review) {
                                    $temp = get_post_meta($single_review->ID, "_review_rate_overal");
                                    $overall_rating += intval($temp[0]['rating']);
                                    $count_review++;
                                }
                                $avg_rate = ($count_review != 0 ? $overall_rating / $count_review : 0);
                                if ($c_logo[0]) {
                                    ?>
                                    <img class="company-logo" src="<?php echo $c_logo[0]; ?>" alt="<?php the_title(); ?>"/>
                                <?php } ?>
                                <h1 class="company-name"><?php the_title(); ?></h1>
                                <div class="company-rating">
                                    <span class="rating-star">
                                        <?php
                                        $loop = 5;
                                        $dark = $loop - ceil($avg_rate);

                                        for ($i = 0; $i < $avg_rate; $i++) {
                                            ?>
                                            <span class="star-yellow"><i class="fa fa-star"></i></span>
                                            <?php
                                        }
                                        for ($i = 0; $i < $dark; $i++) {
                                            ?>
                                            <span class="star-gray"><i class="fa fa-star"></i></span>
                                            <?php
                                        }
                                        ?>
                                    </span>
                                    <span class="rating-count">
                                        <?php if ($count_review > 0) { ?> Rating <?php echo number_format($avg_rate, 1); ?>/5 <?php } ?> <span class="review-count">(<?php echo $count_review; ?> <?php _e("Review", "skilled") ?>)</span>
                                    </span>
                                </div>
                                <div class="company-description">
                                    <h3>&nbsp;</h3>
                                    <?php
                                    $summary = get_post_meta(get_the_ID(), "_company_summary", true);
                                    $the_content = removeHtmlTags($summary);
                                    echo $the_content;
                                    ?>
                                </div>
                                <!--
                                <h2>Languages and Frameworks</h2>
                                <div class="company-lang-framework">
                                    PHP7, Yii2, Laravel 5
                                </div>-->
                            </div>
                            <?php
                            $c_services = get_post_meta(get_the_ID(), "_company_service");
                            if (count($c_services[0]) == 0 || (count($c_services[0]) == 1 && $c_services[0][0]['_company_service_title'] == "")) {
                                
                            } else {
                                ?>
                                <div class="company-services-container">
                                    <div class="company-services">
                                        <h2 class="company-services-title">
                                            <span class="stripe"></span> <?php _e("SERVICES", "skilled"); ?>
                                        </h2>
                                        <div class="row">
                                            <?php
                                            $i = 0;
                                            foreach ($c_services[0] as $cs) {
                                                if (!empty($cs['_company_service_title'])) {
                                                    ?>
                                                    <div class="col-md-6">
                                                        <div class="service-item-title">
                                                            <span class="service-item-title-number"><?php
                                                                if ($i + 1 < 10) {
                                                                    echo "0";
                                                                }
                                                                echo ($i + 1);
                                                                ?>.</span>
                                                            <span class="service-item-title-name"><?php echo $cs['_company_service_title']; ?>&nbsp;</span>
                                                        </div>
                                                        <div class="service-item-description">
                                                            <?php
                                                            $the_content = removeHtmlTags($cs['_company_service_description']);
                                                            $the_content = $the_content == "" ? $cs['_company_service_summary'] : $the_content;
                                                            ?> &nbsp;
                                                        </div>
                                                    </div>
                                                    <?php
                                                    $i++;
                                                }
                                            }
                                            if ($i == 0) {
                                                ?>
                                                <div class="col-md-12">
                                                    <div class="service-item-title">
                                                        <span class="service-item-title-name"><?php _e("No record found", "skilled"); ?></span>
                                                    </div>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            <?php }
                            ?>
                            <?php
                            $c_projects = get_post_meta(get_the_ID(), "_company_portfolio");

                            if (count($c_projects[0]) > 0) {
                                if (!empty($c_projects[0][0]['_company_portfolio_description']) && !empty($c_projects[0][0]['_company_portfolio_title'])) {
                                    ?>
                                    <div class="company-projects">
                                        <h2 class="company-projects-title"><span class="stripe"></span> <?php _e("TOP PROJECTS", "skilled"); ?></h2>
                                        <div class="company-projects-thumbnail-list">
                                            <?php
                                            wp_reset_query();


                                            $i = 0;
                                            foreach ($c_projects[0] as $cp) {
                                                $is_active = ($i == 0 ? "is-active" : "");
                                                ?>
                                                <a data-target="#carouselExampleControls" data-slide-to="<?php echo $i; ?>" class="<?php echo $is_active; ?>">
                                                    <img class="featured-company-logo" src="<?php echo $cp['_company_portfolio_img']; ?>">
                                                </a>
                                                <?php
                                                $i++;
                                            }
                                            ?>
                                        </div>
                                        <div class="company-projects-list">
                                            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                                                <div class="carousel-inner" role="listbox">
                                                    <?php
                                                    $i = 0;
                                                    foreach ($c_projects[0] as $cp) {
                                                        $active = "";
                                                        if ($i == 0) {
                                                            $active = "active";
                                                        }
                                                        ?>
                                                        <div class="carousel-item <?php echo $active; ?>">
                                                            <div class="carousel-item-row">
                                                                <div class="carousel-item-row-inner">
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <div class="project-thumbnail">
                                                                                <img class="featured-company-logo" src="<?php echo $cp['_company_portfolio_img']; ?>">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="project-content">
                                                                                <div class="project-description">
                                                                                    <h4><?php echo $cp['_company_portfolio_title']; ?></h4>
                                                                                    <p>
                                                                                        <?php
                                                                                        $the_content = short_content($cp['_company_portfolio_description'], 100);
                                                                                        $the_content = removeHtmlTags($the_content);
                                                                                        echo $the_content;
                                                                                        ?>
                                                                                    </p>
                                                                                    <p>
                                                                                        <a href="<?php echo $cp['_company_portfolio_url']; ?>" class="view-full-portfolio" target="_blank" rel="nofollow"><?php _e("VIEW FULL PORTFOLIO", "skilled") ?> <i class="fa fa-arrow-right"></i></a>
                                                                                    </p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <?php
                                                        $i++;
                                                    }
                                                    ?>
                                                </div>
                                                <a class="carousel-control carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                                                    <i class="fa fa-angle-left"></i>
                                                </a>
                                                <a class="carousel-control carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                                                    <i class="fa fa-angle-right"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="company-projects-mobile">
                                            <?php
                                            foreach ($c_projects[0] as $cp) {
                                                ?>
                                                <div class="company-projects-mobile-item">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="project-thumbnail">
                                                                <img class="featured-company-logo" src="<?php echo $cp['_company_portfolio_img']; ?>">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="project-content">
                                                                <div class="project-description">
                                                                    <h4><?php echo $cp['_company_portfolio_title']; ?></h4>
                                                                    <p>
                                                                        <?php
                                                                        $the_content = $cp['_company_portfolio_description'];
                                                                        $the_content = removeHtmlTags($the_content);
                                                                        echo $the_content;
                                                                        ?>
                                                                    </p>
                                                                    <p>
                                                                        <a href="<?php echo $cp['_company_portfolio_url']; ?>" class="view-full-portfolio"><?php _e("VIEW FULL PORTFOLIO", "skilled") ?> <i class="fa fa-arrow-right"></i></a>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                            $post = get_post($post_id);
                            $slug = $post->post_name;
                            ?>
                            <div class="company-reviews-container">
                                <div class="company-reviews">
                                    <h2 class="company-reviews-title">
                                        <span class="stripe"></span> <?php _e("REVIEWS", "skilled"); ?>
                                        <a href="<?php echo home_url(); ?>/write-a-review/?cn=<?php echo $slug; ?>" class="button-add-review desktop"><?php _e("Add Review", "skilled"); ?> <i class="fa fa-plus"></i></a>
                                        <a href="<?php echo home_url(); ?>/write-a-review/?cn=<?php echo $slug; ?>" class="button-add-review mobile"><i class="fa fa-plus"></i></a>
                                    </h2>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <?php
                                            $args = array(
                                                'post_type' => 'review',
                                                'posts_per_page' => -1,
                                                //'meta_query' => [
                                                'meta_key' => '_review_company_id',
                                                'meta_value' => get_the_ID()
                                                    //],
                                            );

                                            $query = new WP_Query($args);

                                            if ($query->have_posts()) :
                                                while ($query->have_posts()) :
                                                    $query->the_post();
                                                    $position = get_post_meta(get_the_ID(), "_review_client_position", true);
                                                    $company_name = get_post_meta(get_the_ID(), "_review_company_name", true);
                                                    $company_url = get_post_meta(get_the_ID(), "_review_company_url", true);
                                                    $project_budget = get_post_meta(get_the_ID(), "_review_project_budget", true);
                                                    $project_duration = get_post_meta(get_the_ID(), "_review_project_duration", true);
                                                    $project_scope = get_post_meta(get_the_ID(), "_review_project_scope", true);
                                                    $recommend_company = get_post_meta(get_the_ID(), "_review_recommend_company", true);
                                                    $hire_company = get_post_meta(get_the_ID(), "_review_hire_company", true);
                                                    $rate_overal = get_post_meta(get_the_ID(), "_review_rate_overal", true);
                                                    ?>
                                                    <div class="review-item">
                                                        <div class="row">
                                                            <div class="col-md-8">
                                                                <div class="row">
                                                                    <div class="col-md-2 avatar-container">
                                                                        <!--<img class="avatar">-->
                                                                        <br><br>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <span class="reviewer-name"><?php the_author(); ?> <small>(<?php echo get_the_date() ?>)</small></span>
                                                                        <div class="company-name"><?php echo $position; ?> at <a href="<?php echo $company_url; ?>" target="_blank"><?php echo $company_name; ?></a></div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div class="star-rating">
                                                                            <?php
                                                                            $loop = 5;
                                                                            $yellow = $rate_overal['rating'];
                                                                            $dark = $loop - ceil($yellow);
                                                                            $dark = $loop - $yellow;

                                                                            for ($i = 0; $i < $yellow; $i++) {
                                                                                ?>
                                                                                <i class="fa fa-star"></i>
                                                                                <?php
                                                                            }
                                                                            for ($i = 0; $i < $dark; $i++) {
                                                                                ?>
                                                                                <i class="fa fa-star" style="color: #818181"></i>
                                                                                <?php
                                                                            }
                                                                            ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <?php
                                                                $review_content = get_the_content();
                                                                ?>
                                                                <div class="row">
                                                                    <div class="col-md-2">
                                                                        <?php if ($review_content) { ?>
                                                                            <i class="fa fa-quote-right"></i>
                                                                        <?php } ?>
                                                                    </div>
                                                                    <div class="col-md-10">
                                                                        <div class="review-content">
                                                                            <?php the_content(); ?>
                                                                        </div>
                                                                        <span class="project-budget"><i class='fa fa-dollar'></i>
                                                                            <?php
                                                                            $homeurl = get_home_url();
                                                                            switch ($homeurl) {
                                                                                case "https://skilled.co":
                                                                                    echo "&dollar;";
                                                                                    break;
                                                                                case "https://skilled.co/fr":
                                                                                    echo "&euro;";
                                                                                    break;
                                                                                case "https://skilled.co/nl":
                                                                                    echo "&euro;";
                                                                                    break;
                                                                                case "https://skilled.co/ru":
                                                                                    echo "&dollar;";
                                                                                    break;
                                                                                default:
                                                                                    echo "&dollar;";
                                                                                    break;
                                                                            }
                                                                            ?>
                                                                            <?php echo $project_budget; ?></span>
                                                                        <span class="project-duration"><i class='fa fa-clock-o'></i>  <?php echo $project_duration; ?> months</span>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-2">
                                                                        <span class="project-scope"><?php _e("project scope", "skilled"); ?></span>
                                                                    </div>
                                                                    <div class="col-md-10">
                                                                        <div class="project-scope-text"><?php echo $project_scope; ?></div>
                                                                        <div class="project-recommendation">
                                                                            <?php
                                                                            if ($recommend_company == "yes") {
                                                                                ?>
                                                                                <i class="fa fa-check"></i> <?php _e("Yes, I recommend this company", "skilled"); ?>
                                                                                <?php
                                                                            } else {
                                                                                ?>
                                                                                <i class="fa fa-times"></i> <?php _e("No, I wouldn't recommend this company", "skilled"); ?>
                                                                                <?php
                                                                            }
                                                                            ?>
                                                                        </div>
                                                                        <div class="project-rehire">
                                                                            <?php
                                                                            if ($hire_company == "yes") {
                                                                                ?>
                                                                                <i class="fa fa-check"></i> <?php _e("Yes, I would hire this company again", "skilled"); ?>
                                                                                <?php
                                                                            } else {
                                                                                ?>
                                                                                <i class="fa fa-times"></i> <?php _e("No, I wouldn't hire this company again", "skilled"); ?>
                                                                                <?php
                                                                            }
                                                                            ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class='review-rating'>
                                                                    <div class="rating-title"><i class="fa fa-graduation-cap"></i> <?php _e("expertise", "skilled"); ?></div>
                                                                    <div class="rating-score">
                                                                        <?php
                                                                        $rate_expertise = get_post_meta(get_the_ID(), "_review_rate_expertise", true);
                                                                        $rate_quality = get_post_meta(get_the_ID(), "_review_rate_quality", true);
                                                                        $rate_duration_delivery = get_post_meta(get_the_ID(), "_review_rate_duration_delivery", true);
                                                                        $rate_support = get_post_meta(get_the_ID(), "_review_rate_support", true);
                                                                        $rate_value = get_post_meta(get_the_ID(), "_review_rate_value", true);
                                                                        $rate_overall = get_post_meta(get_the_ID(), "_review_rate_overal", true);

                                                                        $z = 0;
                                                                        $loop = 5;
                                                                        $yellow = $rate_expertise['rating'];
                                                                        $dark = $loop - $yellow;

                                                                        for ($i = 0; $i < $yellow; $i++) {
                                                                            $first = ($z == 0 ? " first" : "");
                                                                            ?>
                                                                            <span class="rating-score-active<?php echo $first; ?>"></span>
                                                                            <?php
                                                                            $z++;
                                                                        }
                                                                        for ($i = 0; $i < $dark; $i++) {
                                                                            $first = ($z == 0 ? " first" : "");
                                                                            ?>
                                                                            <span class="rating-score-inactive<?php echo $first; ?>"></span>
                                                                            <?php
                                                                            $z++;
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                    <div class="rating-title"><i class="fa fa-star"></i> quality</div>
                                                                    <div class="rating-score">
                                                                        <?php
                                                                        $z = 0;
                                                                        $loop = 5;
                                                                        $yellow = $rate_quality['rating'];
                                                                        $dark = $loop - $yellow;

                                                                        for ($i = 0; $i < $yellow; $i++) {
                                                                            $first = ($z == 0 ? " first" : "");
                                                                            ?>
                                                                            <span class="rating-score-active<?php echo $first; ?>"></span>
                                                                            <?php
                                                                            $z++;
                                                                        }
                                                                        for ($i = 0; $i < $dark; $i++) {
                                                                            $first = ($z == 0 ? " first" : "");
                                                                            ?>
                                                                            <span class="rating-score-inactive<?php echo $first; ?>"></span>
                                                                            <?php
                                                                            $z++;
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                    <div class="rating-title"><i class="fa fa-angle-double-right"></i> <?php _e("duration & delivery", "skilled"); ?></div>
                                                                    <div class="rating-score">
                                                                        <?php
                                                                        $z = 0;
                                                                        $loop = 5;
                                                                        $yellow = $rate_duration_delivery['rating'];
                                                                        $dark = $loop - $yellow;

                                                                        for ($i = 0; $i < $yellow; $i++) {
                                                                            $first = ($z == 0 ? " first" : "");
                                                                            ?>
                                                                            <span class="rating-score-active<?php echo $first; ?>"></span>
                                                                            <?php
                                                                            $z++;
                                                                        }
                                                                        for ($i = 0; $i < $dark; $i++) {
                                                                            $first = ($z == 0 ? " first" : "");
                                                                            ?>
                                                                            <span class="rating-score-inactive<?php echo $first; ?>"></span>
                                                                            <?php
                                                                            $z++;
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                    <div class="rating-title"><i class="fa fa-phone"></i> <?php _e("customer support", "skilled"); ?></div>
                                                                    <div class="rating-score">
                                                                        <?php
                                                                        $z = 0;
                                                                        $loop = 5;
                                                                        $yellow = $rate_support['rating'];
                                                                        $dark = $loop - $yellow;

                                                                        for ($i = 0; $i < $yellow; $i++) {
                                                                            $first = ($z == 0 ? " first" : "");
                                                                            ?>
                                                                            <span class="rating-score-active<?php echo $first; ?>"></span>
                                                                            <?php
                                                                            $z++;
                                                                        }
                                                                        for ($i = 0; $i < $dark; $i++) {
                                                                            $first = ($z == 0 ? " first" : "");
                                                                            ?>
                                                                            <span class="rating-score-inactive<?php echo $first; ?>"></span>
                                                                            <?php
                                                                            $z++;
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                    <div class="rating-title"><i class="fa fa-credit-card"></i> <?php _e("value for money", "skilled"); ?></div>
                                                                    <div class="rating-score">
                                                                        <?php
                                                                        $z = 0;
                                                                        $loop = 5;
                                                                        $yellow = $rate_value['rating'];
                                                                        $dark = $loop - $yellow;

                                                                        for ($i = 0; $i < $yellow; $i++) {
                                                                            $first = ($z == 0 ? " first" : "");
                                                                            ?>
                                                                            <span class="rating-score-active<?php echo $first; ?>"></span>
                                                                            <?php
                                                                            $z++;
                                                                        }
                                                                        for ($i = 0; $i < $dark; $i++) {
                                                                            $first = ($z == 0 ? " first" : "");
                                                                            ?>
                                                                            <span class="rating-score-inactive<?php echo $first; ?>"></span>
                                                                            <?php
                                                                            $z++;
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                    <div class="rating-title"><i class="fa fa-credit-card"></i> <?php _e("overall rating", "skilled"); ?></div>
                                                                    <div class="rating-score">
                                                                        <?php
                                                                        $z = 0;
                                                                        $loop = 5;
                                                                        $yellow = $rate_overall['rating'];
                                                                        $dark = $loop - $yellow;

                                                                        for ($i = 0; $i < $yellow; $i++) {
                                                                            $first = ($z == 0 ? " first" : "");
                                                                            ?>
                                                                            <span class="rating-score-active<?php echo $first; ?>"></span>
                                                                            <?php
                                                                            $z++;
                                                                        }
                                                                        for ($i = 0; $i < $dark; $i++) {
                                                                            $first = ($z == 0 ? " first" : "");
                                                                            ?>
                                                                            <span class="rating-score-inactive<?php echo $first; ?>"></span>
                                                                            <?php
                                                                            $z++;
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php
                                                endwhile;
                                                wp_reset_query();
                                                ?>
                                            <?php else :
                                                ?>
                                                <div class="review-item">
                                                    <div class="row">
                                                        <div class="col-xs-12 col-md-12" style="padding: 1rem 2rem;">
                                                            <a class='be-the-first' href="<?php echo home_url(); ?>/write-a-review/?cn=<?php echo $slug; ?>" class="button-add-review desktop"><?php _e("Be the first to review this company!", "skilled"); ?></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endif;
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="sidebar-container">
                        <div class="sidebar">
                            <?php
                            $c_founded = get_post_meta(get_the_ID(), "_company_founded");
                            $c_tagline = get_post_meta(get_the_ID(), "_company_tagline");
                            $c_url = get_post_meta(get_the_ID(), "_company_url");
                            $c_email = get_post_meta(get_the_ID(), "_company_email");
                            $c_phone = get_post_meta(get_the_ID(), "_company_phone");
                            $c_rate = get_post_meta(get_the_ID(), "_company_rate");
                            $c_employee = get_post_meta(get_the_ID(), "_company_employee");
                            $c_size = get_post_meta(get_the_ID(), "_company_size");

                            $info = "";
                            ?>
                            <div class="company-info" id="company-info">
                                <ul>
                                    <li class="company-logo-container">
                                        <?php if ($c_logo[0]) { ?>
                                            <img class="company-logo" src="<?php echo $c_logo[0]; ?>" alt="<?php the_title(); ?>"/>
                                        <?php } ?>
                                    </li>
                                    <li class="company-btns-container">
                                        <a href="<?php echo $c_url[0] ?>" class="btn btn-primary btn-company-contact visit-website" rel="nofollow" target="_blank"><?php _e("Visit Website", "skilled") ?> <b class="tsi tsi-arrow-right"></b></a>
                                        <a id="request-proposal" class="btn btn-primary btn-company-contact request-proposal" data-toggle="modal" data-target="#myModal"><?php _e("Request Proposal", "skilled") ?></a>
                                        <a id="claim-company" class="btn btn-primary btn-company-contact claim-company" data-toggle="modal" data-target="#claimModal"><?php _e("Claim Company", "skilled") ?></a>
                                    </li>
                                    <?php
                                    $c_location = get_post_meta(get_the_ID(), "_company_location");

                                    $i = 0;
                                    if (!is_array($c_location[0])) {
                                        $c_location[0] = unserialize($c_location[0]);
                                    }

                                    if ($c_location[0]) {
                                        ?>
                                        <li><i class='tsi tsi-map-marker'></i> <span class="sidebar-title"><?php _e("address", "skilled"); ?></span>
                                            <ul class='address-list'>
                                                <?php
                                                foreach ($c_location[0] as $cs) {
                                                    ?>
                                                    <li>
                                                        <?php echo (!empty($cs['_company_location']['address-1']) ? $cs['_company_location']['address-1'] . "<br>" : ""); ?>
                                                        <?php echo (!empty($cs['_company_location']['address-2']) ? $cs['_company_location']['address-2'] . "<br>" : ""); ?>
                                                        <?php echo (!empty($cs['_company_location']['city']) ? $cs['_company_location']['city'] . "<br>" : ""); ?>
                                                        <?php echo (!empty($cs['_company_location']['new_state']) ? $cs['_company_location']['new_state'] . "<br>" : $cs['_company_location']['state'] . "<br>"); ?>
                                                        <?php echo (!empty($cs['_company_location']['new_country']) ? $cs['_company_location']['new_country'] . "<br>" : $cs['_company_location']['country'] . "<br>"); ?>
                                                        <?php echo (!empty($cs['_company_location']['zip']) ? $cs['_company_location']['zip'] . "<br>" : ""); ?>
                                                        <?php echo (!empty($cs['_company_location']['phone']) ? "<span class='company-phone'><i class='fa fa-phone'></i> " . $cs['_company_location']['phone'] . "</span>" : ""); ?>
                                                    </li>
                                                    <?php
                                                    $i++;
                                                }
                                                ?>
                                            </ul>
                                        </li>
                                        <?php
                                    }
                                    ?>
                                    <?php
                                    $term = get_term_by("slug", $c_employee[0], "company_size_term", "ARRAY_A");
                                    $employee_term = $term['name'];
                                    if ($employee_term) {
                                        ?>
                                        <li><i class='tsi tsi-employee'></i> <span class="sidebar-title"><?php _e("employee number", "skilled"); ?></span>
                                            <div>
                                                <?php echo $employee_term; ?>
                                            </div>
                                        </li>
                                        <?php
                                    }
                                    $term = get_term_by("slug", $c_rate[0], "company_rate_term", "ARRAY_A");
                                    if ($term['name']) {
                                        $hourly_term = "";
                                        if (strpos($term['name'], "$") === false) {
                                            $homeurl = get_home_url();
                                            switch ($homeurl) {
                                                case "https://skilled.co":
                                                    $hourly_term .= "&dollar;";
                                                    break;
                                                case "https://skilled.co/fr":
                                                    $hourly_term .= "&euro;";
                                                    break;
                                                case "https://skilled.co/nl":
                                                    $hourly_term .= "&euro;";
                                                    break;
                                                case "https://skilled.co/ru":
                                                    $hourly_term .= "&dollar;";
                                                    break;
                                                default:
                                                    $hourly_term .= "&dollar;";
                                                    break;
                                            }
                                        }
                                        $hourly_term .= $term['name'];
                                        ?>
                                        <li><i class='tsi tsi-hourly-rate'></i> <span class="sidebar-title"><?php _e("average hourly rate", "skilled"); ?></span>
                                            <div>
                                                <?php echo $hourly_term; ?>
                                            </div>
                                        </li>
                                        <?php
                                    }
                                    $term = get_term_by("slug", $c_size[0], "project_size_term", "ARRAY_A");
                                    $budget_term = $term['name'];
                                    if ($budget_term) {
                                        ?>
                                        <li><i class='tsi tsi-budget-project'></i> <span class="sidebar-title"><?php _e("minimum budget project", "skilled"); ?></span>
                                            <div>
                                                <?php echo $budget_term; ?>
                                            </div>
                                        </li>
                                        <?php
                                    }
                                    $client_term = wp_get_post_terms(get_the_ID(), 'client_focus_term', [
                                        'orderby' => 'id',
                                        'order' => 'ASC'
                                    ]);
                                    $clients = [];
                                    foreach ($client_term as $industry) {
                                        $clients[] = $industry->name;
                                    }
                                    $client_focus = implode(", ", $clients);
                                    if ($client_focus) {
                                        ?>
                                        <li><i class='tsi tsi-client-focus'></i> <span class="sidebar-title"><?php echo strtolower(__("client focus", "skilled")); ?></span>
                                            <div>
                                                <?php echo $client_focus; ?>
                                            </div>
                                        </li>
                                        <?php
                                    }
                                    $industry_focus = "";
                                    $industry_term = wp_get_post_terms(get_the_ID(), 'industry_term', [
                                        'orderby' => 'id',
                                        'order' => 'ASC'
                                    ]);
                                    $more = false;
                                    $i = 0;
                                    $industries = [];
                                    foreach ($industry_term as $industry) {
                                        if ($i == 5) {
                                            $more = true;
                                            $industries[] = '<span id="more">' . $industry->name;
                                        } else {
                                            $industries[] = $industry->name;
                                        }
                                        $i++;
                                    }
                                    $industry_focus .= implode(", ", $industries);
                                    if ($more) {
                                        $industry_focus .= "</span>";
                                        $industry_focus .= '                                            <br>
                                            <span id="see-more" style="font-weight: bold;">[See more]</span>';
                                    }
                                    if ($industry_focus) {
                                        ?>
                                        <li>
                                            <i class='tsi tsi-industry-focus'></i> <span class="sidebar-title"><?php echo strtolower(__("industry focus", "skilled")); ?></span>
                                            <div>
                                                <?php echo $industry_focus; ?>
                                            </div>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header modal-header-request-proposal">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><?php echo strtoupper(esc_html_e("Request Proposal", "skilled")) ?></h4>
            </div>
            <?php
            //if (is_user_logged_in()) {
            ?>
            <div class="modal-body modal-body-request-proposal">
                <div id="error-message"></div>
                <div class="form-group">
                    <label><i class="fa fa-user"></i> <?php _e("Name", "skilled") ?></label>
                    <input type="hidden" id="home_url" value="<?php echo home_url(); ?>">
                    <input type="hidden" id="proposal_company" value="<?php echo get_the_ID(); ?>">
                    <input type="text" class="form-control" id="proposal_name">
                </div>
                <div class="form-group">
                    <label><i class="fa fa-envelope"></i> <?php _e("Email Address", "skilled") ?></label>
                    <input type="text" class="form-control" id="proposal_email">
                </div>
                <div class="form-group">
                    <label><i class="fa fa-phone"></i> <?php _e("Contact Number", "skilled") ?></label>
                    <input type="text" class="form-control" id="proposal_contact">
                </div>
                <div class="form-group">
                    <label><i class="fa fa-pencil"></i> <?php _e("Description", "skilled") ?></label>
                    <?php wp_editor($content, "proposal_editor"); ?>
                </div>
            </div>
            <div class="modal-footer modal-footer-request-proposal">
                <button type="button" class="btn btn-primary" id="submit-proposal"><?php _e("Save", "skilled") ?></button>
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php _e("Close", "skilled") ?></button>
            </div>
            <?php
            //} else {
            ?>
            <!-- <div class="modal-body">
            <?php //_e("You have to be logged in to use this feature.", "skilled") ?>
            </div>
            <div class="modal-footer modal-footer-request-proposal">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php _e("Close", "skilled") ?></button>
            </div> -->
            <?php
            //}
            ?>
        </div>
    </div>
</div>
<div id="claimModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header modal-header-request-proposal">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><?php echo strtoupper(esc_html_e("Claim Company", "skilled")) ?></h4>
            </div>
            <?php
            if (is_user_logged_in()) {
                ?>
                <div class="modal-body modal-body-request-proposal">
                    <div id="error-message"></div>
                    <p><?php _e("Are you sure want to claim this as your company?", "skilled") ?></p>
                    <div class="form-group">
                        <input type="hidden" id="home_url" value="<?php echo home_url(); ?>">
                        <input type="hidden" id="id_company" value="<?php echo get_the_ID(); ?>">
                    </div>
                </div>
                <div class="modal-footer modal-footer-request-proposal">
                    <button type="button" class="btn btn-primary" id="submit-claim"><?php _e("Yes", "skilled") ?></button>
                    <button type="button" class="btn btn-default" data-dismiss="modal"><?php _e("No", "skilled") ?></button>
                </div>
                <?php
            } else {
                ?>
                <div class="modal-body">
                    <?php _e("You have to be logged in to use this feature.", "skilled") ?>
                </div>
                <div class="modal-footer modal-footer-request-proposal">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><?php _e("Close", "skilled") ?></button>
                </div> 
                <?php
            }
            ?>
        </div>
    </div>
</div>
<div id="successModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><?php _e("Request Submitted", "skilled") ?></h4>
            </div>
            <div class="modal-body">
                <?php _e("Thank you for your request. It has been submitted.", "skilled") ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php _e("Close", "skilled") ?></button>
            </div>
        </div>
    </div>
</div>
<div id="failedModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><?php _e("Request Failed", "skilled") ?></h4>
            </div>
            <div class="modal-body">
                <p id="failedmsg"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php _e("Close", "skilled") ?></button>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>