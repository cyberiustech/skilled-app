<?php
global $wp;

load_theme_textdomain('skilled', get_template_directory() . '/languages');

register_nav_menu("primary-menu", __("Primary Menu", "skilled-theme"));
//register_nav_menu("primary-menu-2", __("Primary Menu 2", "skilled-theme"));
register_nav_menu("footer-1", __("Footer 1", "skilled-theme"));
register_nav_menu("footer-2", __("Footer 2", "skilled-theme"));
register_nav_menu("footer-3", __("Footer 3", "skilled-theme"));
register_nav_menu("tos-policy-page", __("TOS & Policy Link", "skilled-theme"));

register_sidebar([
    'name' => 'Footer Widget',
    'id' => 'footer-widget',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '<div class="footer-nav-title">',
    'after_title' => '</div>',
]);

add_theme_support('post-thumbnails');

function skilled_pagination($paged, $paged_max, $uri) {
    if ($paged_max > 1) {
        $current_page = $uri;

        $req_uri = $_SERVER['REQUEST_URI'];
        $req_uri = str_replace("page=", "=", $req_uri);
        $parts = explode("?", $req_uri);

        $query_string = "";
        if (!empty($parts[1])) {
            $query_string = $parts[1] . "&";
        }
        ?>
        <ul class="pagination">
            <?php
            ?>
            <li class="page-item"><a class="page-link" href="<?php echo $current_page ?>/?<?php echo $query_string; ?>page=1"><?php _e("First", "skilled"); ?></a></li>
            <?php
            for ($i = 1; $i <= $paged_max; $i++) {
                $active = "";


                if ($i == $paged) {
                    $active = "active";
                }
                if ($i > ($paged - 4) && $i < ($paged + 4)) {
                    ?>
                    <li class="page-item <?php echo $active; ?>"><a class="page-link <?php echo $active; ?>" href="<?php echo $current_page ?>/?<?php echo $query_string; ?>page=<?php echo $i ?>"><?php echo $i ?></a></li>
                    <?php
                }
            }
            if ($paged != $paged_max) {
                ?>
                <li class="page-item"><a class="page-link" href="<?php echo $current_page ?>/?<?php echo $query_string; ?>page=<?php echo $paged_max; ?>"><?php _e("Last", "skilled"); ?></a></li>
                <?php } ?>
        </ul>
        <?php
    }
}

function get_query_string_from_uri($var, $query_string) {
    $var = str_replace($var . "=", "=", $query_string);
    $temp = explode($var . "=", $query_string);
    $temp = explode("&", $temp[1]);
    return $temp[0];
}

function get_skilled_tags($post_id, $term, $post_type) {
    $tags = wp_get_post_terms($post_id, $term, []);

    $str = [];
    foreach ($tags as $tag) {
        $str[] = "<a href='" . home_url() . "/resource/?rtag=" . $tag->slug . "'>" . $tag->name . "</a>";
    }

    return implode(", ", $str);
}

function add_query_vars_filter($vars) {
    $vars[] = "rtag";
    return $vars;
}

add_filter('query_vars', 'add_query_vars_filter');

add_action('after_setup_theme', 'woocommerce_support');

function woocommerce_support() {
    add_theme_support('woocommerce');
}

remove_action('woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action('woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);
add_action('woocommerce_before_main_content', 'my_theme_wrapper_start', 10);
add_action('woocommerce_after_main_content', 'my_theme_wrapper_end', 10);

function my_theme_wrapper_start() {
    echo '<section id="main">';
}

function my_theme_wrapper_end() {
    echo '</section>';
}

function set_page_title($req_uri) {
    if (is_404()) {
        echo "Not Found - ";
    } else if (is_search()) {
        echo get_search_query();
        echo " - ";
    } else if (!is_home()) {
        if (($req_uri[2] == "companies" && $req_uri[3] == "") || ($req_uri[1] == "companies" && $req_uri[2] == "")) {
            $title = esc_html_e("Company Guide", "skilled");
            echo $title;
        } elseif (($req_uri[2] == "resources" && $req_uri[3] == "") || ($req_uri[1] == "resources" && $req_uri[2] == "")) {
            $title = esc_html_e("Resources", "skilled");
            echo $title;
        } else {
            $title = get_the_title();
            (isset($title) ? the_title() : "" );
        }
        echo " - ";
    }
}

function short_content($content, $word_count = 5) {
    $content = strip_tags($content);
    $content = substr($content, 0, $word_count);

    return $content . "...";
}

function custom_rewrite_tag() {
    add_rewrite_tag('%company_rate_term%', '([^&]+)');
    add_rewrite_tag('%company_size_term%', '([^&]+)');
    add_rewrite_tag('%project_size_term%', '([^&]+)');
    add_rewrite_tag('%industry_term%', '([^&]+)');
}

add_action('init', 'custom_rewrite_tag', 10, 0);

function removeHtmlTags($content) {
    return strip_tags($content, "<p><b><i><u><strong><a><img><ul><li><strong><em></blockquote><img><ul><ol><li><code>");
}

function removeHtmlOpenCloseTag($text) {
    $text = str_replace("<", "&lt;", $text);
    $text = str_replace(">", "&gt;", $text);
    return $text;
}

add_action('init', 'customRSS');

function customRSS() {
    add_feed('feedname', 'customRSSFunc');
}

function customRSSFunc() {
    $post_type = get_post_type();
    if ($post_type == "organization") {
        
    }
}
function new_excerpt_more($more) {
       global $post;
    return '<a class="moretag" href="'. get_permalink($post->ID) . '"> →</a>';
}
add_filter('excerpt_more', 'new_excerpt_more');
