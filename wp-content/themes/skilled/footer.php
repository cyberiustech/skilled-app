<?php
$template_uri = get_template_directory_uri();
?>
<footer class="footer">
    <div class="footer-inner">
        <section class="footer-nav">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <?php
                        if (has_nav_menu('footer-1')) {
                            wp_nav_menu([
                                'fallback_cb' => '__return_false',
                                'theme_location' => 'footer-1',
                                'depth' => 1,
                                'menu_class' => "navbar-nav",
                                'items_wrap' => '<div class="footer-nav-title">' . "Skilled.co" . '</div><ul id="%1$s" class="footer-nav-list">%3$s</ul>',
                            ]);
                        }
                        ?>
                    </div>
                    <div class="col-md-3">
                        <?php
                        if (has_nav_menu('footer-2')) {
                            wp_nav_menu([
                                'fallback_cb' => '__return_false',
                                'theme_location' => 'footer-2',
                                'depth' => 1,
                                'menu_class' => "navbar-nav",
                                'items_wrap' => '<div class="footer-nav-title">' . esc_html__("Company Guide", "skilled") . '</div><ul id="%1$s" class="footer-nav-list">%3$s</ul>',
                            ]);
                        }
                        ?>
                    </div>
                    <div class="col-md-3">
                        <?php
                        if (has_nav_menu('footer-3')) {
                            wp_nav_menu([
                                'fallback_cb' => '__return_false',
                                'theme_location' => 'footer-3',
                                'depth' => 1,
                                'menu_class' => "navbar-nav",
                                'items_wrap' => '<div class="footer-nav-title">' . esc_html__("Get In Touch", "skilled") . '</div><ul id="%1$s" class="footer-nav-list">%3$s</ul>',
                            ]);
                        }
                        ?>
                    </div>
                    <div class="col-md-3">
                        <?php
                        if (is_active_sidebar('footer-widget')) {
                            dynamic_sidebar('footer-widget');
                        }
                        ?>
                    </div>
                </div>
            </div>
        </section>
        <section class="footer-copyright">
            <div class="container">
                <div class="row">
                    <div class="col-md-5 footer-all-right-reserved"><?php _e('All rights reserved', 'skilled'); ?> <?php bloginfo('title') ?> © <?php echo date('Y') ?></div>
                    <div class="col-md-7 footer-copyright-list">
                        <?php
                        if (has_nav_menu('tos-policy-page')) {
                            $footerMenu2 = wp_nav_menu([
                                'fallback_cb' => '__return_false',
                                'theme_location' => 'tos-policy-page',
                                'depth' => 1,
                                'menu_class' => "navbar-nav",
                                'items_wrap' => '<ul>%3$s</ul>',
                            ]);
                        }
                        ?>
                    </div>
                </div>
            </div>
        </section>
    </div>
</footer>
</div>
<a href="#0" class="cd-top"><i class="fa fa-caret-up"></i></a>
    <?php wp_footer(); ?>
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?php echo $template_uri; ?>/assets/build/js/modernizr.js"></script>
<script src="<?php echo $template_uri; ?>/assets/build/js/typewrite.js"></script>
<script src="<?php echo $template_uri; ?>/assets/build/js/common.js"></script>
<?php
$uri = $_SERVER['REQUEST_URI'];
$uris = explode("/", $uri);
if (($uris[1] == "company" && isset($uris[2]) && $uris[2] != "") || $uris[2] == "company" && isset($uris[3]) && $uris[3] != "") {
    ?>
    <script src="<?php echo $template_uri; ?>/assets/build/js/company.js"></script>
    <?php
}
if (($uris[1] == "companies" && isset($uris[2]) && $uris[2] != "") || $uris[2] == "companies" && isset($uris[3]) && $uris[3] != "") {
    ?>
    <script src="<?php echo $template_uri; ?>/assets/build/js/company.js"></script>
    <?php
}
?>
</body>
</html>
