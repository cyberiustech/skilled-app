<?php get_header(); ?>

<?php
$org_slug = "companies";
switch (DB_NAME) {
    case "skilled_fr":
        $org_slug = "entreprises";
        break;
    case "skilled_nl":
        $org_slug = "bedrijven";
        break;
}
?>
<section class="section-company-guide-header">
    <section class="section-inner">
        <div class="container">
            <div class="breadcrumbs">
                <a href="<?php echo get_home_url(); ?>/"><?php _e("Home", "skilled") ?></a> <i class="fa fa-angle-double-right"></i> <a href=" <?php echo get_home_url(); ?>/<?php echo $org_slug; ?>/"><?php _e("Company Guide", "skilled") ?></a> <i class="fa fa-angle-double-right"></i>  <?php the_title(); ?>
            </div>
        </div>
    </section>
</section>

<section class="section-company-guide-content">
    <section class="section-inner">
        <div class="container">
            <div class="row">
                <div class="col-md-12"><h1><?php _e("Best", "skilled"); ?> <?php the_title(); ?> <?php _e("Companies", "skilled"); ?> in <?php echo date('Y'); ?></h1></div>
            </div>
            <div class="row">
                <div class="col-md-3 sidebar-container">
                    <div class="sidebar">
                        <div class="filter-result">
                            <div class="filter-result-title">
                                <?php _e("Filter results", "skilled"); ?>
                                <button class="close-button" type="button">
                                    <i class="fa fa-times"></i>
                                </button>
                            </div>
                            <div class="filter-result-content">
                                <form method="GET" action="<?php the_permalink(); ?>">
                                    <div class="form-group">
                                        <label><?php _e("Avg. Hourly Rate", "skilled"); ?></label>
                                        <input type="text" id="range-hourly-rate" name="hourly_rate" value="<?php echo $_GET['hourly_rate']; ?>" />
                                    </div>
                                    <div class="form-group">
                                        <label><?php _e("Employee", "skilled"); ?></label>
                                        <input type="text" id="range-employee" name="employee" value="<?php echo $_GET['employee']; ?>" />
                                    </div>
                                    <div class="form-group">
                                        <label><?php _e("Min. Project Size", "skilled"); ?></label>
                                        <input type="text" id="range-project-size" name="project_size" value="<?php echo $_GET['project_size']; ?>"/>
                                    </div>
                                    <div class="form-group">
                                        <label><?php _e("Client Focus", "skilled"); ?></label>
                                        <?php
                                        $hourlyRate = get_terms('client_focus_term', [
                                            'hide_empty' => false,
                                            'orderby' => 'id',
                                            'order' => 'ASC'
                                        ]);
                                        ?>
                                        <?php
                                        $client_focus = $_GET["client_focus"];
                                        $industry_focus = $_GET["industy_focus"];

                                        if (!is_array($client_focus)) {
                                            $client_focus = [];
                                        }

                                        if (!is_array($industry_focus)) {
                                            $industry_focus = [];
                                        }

                                        $company_guide_id = get_the_ID();


                                        $i = 0;
                                        foreach ($hourlyRate as $rate) {
                                            if (in_array($rate->term_id, $client_focus)) {
                                                echo '<input type="checkbox" id="client_focus_1" name="client_focus[]" value="' . $rate->term_id . '" checked="checked">';
                                            } else {
                                                echo '<input type="checkbox" id="client_focus_1" name="client_focus[]" value="' . $rate->term_id . '">';
                                            }
                                            echo '<label for="client_focus_1">' . $rate->name . '</label>';
                                            $i++;
                                        }
                                        ?>
                                    </div>
                                    <div class="form-group">
                                        <label><?php _e("Industry Focus", "skilled"); ?></label>
                                        <?php
                                        $hourlyRate = get_terms('industry_term', [
                                            'hide_empty' => false,
                                            'orderby' => 'id',
                                            'order' => 'ASC'
                                        ]);
                                        f
                                        ?>
                                        <?php
                                        $more = false;
                                        $i = 0;
                                        foreach ($hourlyRate as $rate) {
                                            if ($i == 10) {
                                                $more = true;
                                                echo '<span id="more">';
                                            }
                                            if (in_array($rate->term_id, $industry_focus)) {
                                                echo '<input type="checkbox" id="industy_focus_1" name="industy_focus[]" value="' . $rate->term_id . '" checked="checked">';
                                            } else {
                                                echo '<input type="checkbox" id="industy_focus_1" name="industy_focus[]" value="' . $rate->term_id . '">';
                                            }
                                            echo '<label for="industy_focus_1">' . $rate->name . '</label>';
                                            $i++;
                                        }
                                        if ($more) {
                                            echo "</span>";
                                            echo '                                            <br>
                                            <span id="see-more" style="font-weight: bold;">[See more]</span>';
                                        }
                                        ?>
                                    </div>
                                    <div class="form-group btn-submit-container">
                                        <br>
                                        <button type="submit" class="btn btn-primary"><?php _e("Submit", "skilled"); ?> <i class="fa fa-arrow-right"></i></button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-9">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#companies" role="tab"><?php _e("Companies", "skilled"); ?></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#guide" role="tab"><?php _e("Buyer Guide", "skilled"); ?></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#comments" role="tab"><?php _e("Comments", "skilled"); ?></a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane description" id="guide" role="tabpanel">
                            <h2 style="text-align: center;"><strong><?php the_title(); ?></strong></h2>
                            <?php
                            the_post();
                            the_content();
                            ?>
                        </div>
                        <div class="tab-pane description" id="comments" role="tabpanel">
                            <?php
                            if (comments_open() || get_comments_number()) :
                                comments_template();
                            endif;
                            ?>
                        </div>
                        <div class="tab-pane companies active" id="companies" role="tabpanel">
                            <?php
                            $tax_query = [];
                            $meta_query = [];
                            $paged = get_query_var('page') ? get_query_var('page') : 1;

                           // $posts_per_page = 10 - $num_sponsored;

                            if (count($tax_query) > 1) {
                                for ($i = count($tax_query); $i >= 0; $i--) {
                                    $tax_query[$i] = (($i > 0) ? $tax_query[$i - 1] : ["relation" => "AND"]);
                                }
                            }

                            if (!empty($_GET['hourly_rate'])) {
                                $post_hourly_rate = $_GET['hourly_rate'];

                                $post_hourly_rate = str_replace("&lt;", "<", $post_hourly_rate);
                                $post_hourly_rate = str_replace("&gt;", ">", $post_hourly_rate);

                                list($min_post_hourly_rate, $max_post_hourly_rate) = explode(";", $post_hourly_rate);

                                $min_post_hourly_rate = str_replace("<", "&lt;", $min_post_hourly_rate);
                                $min_post_hourly_rate = str_replace(">", "&gt;", $min_post_hourly_rate);
                                $max_post_hourly_rate = str_replace("<", "&lt;", $max_post_hourly_rate);
                                $max_post_hourly_rate = str_replace(">", "&gt;", $max_post_hourly_rate);

                                $term = get_term_by('name', $min_post_hourly_rate, 'company_rate_term');
                                $min_term_id = $term->term_id;
                                $term = get_term_by('name', $max_post_hourly_rate, 'company_rate_term');
                                $max_term_id = $term->term_id;

                                $term_ids = @$GLOBALS['wpdb']->get_results("SELECT term_id FROM wpos_term_taxonomy WHERE taxonomy = 'company_rate_term' AND (term_id BETWEEN $min_term_id AND $max_term_id)", OBJECT);
                                $slugs = @$GLOBALS['wpdb']->get_results("SELECT slug FROM wpos_terms WHERE (term_id BETWEEN $min_term_id AND $max_term_id)", OBJECT);

                                if (!empty($term_ids)) {
                                    $results = [];
                                    foreach ($term_ids as $single_term_id) {
                                        $results[] = $single_term_id->term_id;
                                    }

                                    $tax_query[] = [
                                        "taxonomy" => "company_rate_term",
                                        "terms" => $results,
                                        "operator" => "IN"
                                    ];

                                    $slug_queries = [];
                                    foreach ($slugs as $single_slug) {
                                        $slug_queries[] = $single_slug->slug;
                                    }
                                    $meta_query[] = [
                                        'key' => '_company_rate',
                                        'value' => $slug_queries,
                                        'compare' => 'IN'
                                    ];
                                }
                            }

                            if (!empty($_GET["employee"])) {
                                $post_employee = $_GET['employee'];
                                $post_employee = str_replace("&lt;", "<", $post_employee);
                                $post_employee = str_replace("&gt;", ">", $post_employee);

                                list($min_post_employee, $max_post_employee) = explode(";", $post_employee);

                                $min_post_employee = str_replace("<", "&lt;", $min_post_employee);
                                $min_post_employee = str_replace(">", "&gt;", $min_post_employee);
                                $max_post_employee = str_replace("<", "&lt;", $max_post_employee);
                                $max_post_employee = str_replace(">", "&gt;", $max_post_employee);


                                $term = get_term_by('name', $min_post_employee, 'company_size_term');
                                $min_term_id = $term->term_id;
                                $term = get_term_by('name', $max_post_employee, 'company_size_term');
                                $max_term_id = $term->term_id;

                                $term_ids = @$GLOBALS['wpdb']->get_results("SELECT term_id FROM  wpos_term_taxonomy WHERE taxonomy = 'company_size_term' AND (term_id BETWEEN $min_term_id AND $max_term_id)", OBJECT);
                                $slugs = @$GLOBALS['wpdb']->get_results("SELECT slug FROM wpos_terms WHERE (term_id BETWEEN $min_term_id AND $max_term_id)", OBJECT);
                                if (!empty($term_ids)) {
                                    $results = [];
                                    foreach ($term_ids as $single_term_id) {
                                        $results[] = $single_term_id->term_id;
                                    }

                                    $tax_query[] = [
                                        "taxonomy" => "company_size_term",
                                        "terms" => $results,
                                        "operator" => "IN"
                                    ];

                                    $slug_queries = [];
                                    foreach ($slugs as $single_slug) {
                                        $slug_queries[] = $single_slug->slug;
                                    }
                                    $meta_query[] = [
                                        'key' => '_company_employee',
                                        'value' => $slug_queries,
                                        'compare' => 'IN'
                                    ];
                                }
                            }

                            if (!empty($_GET["project_size"])) {
                                $post_project = $_GET['project_size'];

                                $post_project = str_replace("&lt;", "<", $post_project);
                                $post_project = str_replace("&gt;", ">", $post_project);

                                list($min_post_project, $max_post_project) = explode(";", $post_project);

                                $min_post_project = str_replace("<", "&lt;", $min_post_project);
                                $min_post_project = str_replace(">", "&gt;", $min_post_project);
                                $max_post_project = str_replace("<", "&lt;", $max_post_project);
                                $max_post_project = str_replace(">", "&gt;", $max_post_project);


                                $term = get_term_by('name', $min_post_project, 'project_size_term');
                                $min_term_id = $term->term_id;
                                $term = get_term_by('name', $max_post_project, 'project_size_term');
                                $max_term_id = $term->term_id;

                                $term_ids = @$GLOBALS['wpdb']->get_results("SELECT term_id FROM wpos_term_taxonomy WHERE taxonomy = 'project_size_term' AND (term_id BETWEEN $min_term_id AND $max_term_id)", OBJECT);
                                $slugs = @$GLOBALS['wpdb']->get_results("SELECT slug FROM wpos_terms WHERE (term_id BETWEEN $min_term_id AND $max_term_id)", OBJECT);

                                if (!empty($term_ids)) {
                                    $results = [];
                                    foreach ($term_ids as $single_term_id) {
                                        $results[] = $single_term_id->term_id;
                                    }

                                    $tax_query[] = [
                                        "taxonomy" => "project_size_term",
                                        "terms" => $results,
                                        "operator" => "IN"
                                    ];

                                    $slug_queries = [];
                                    foreach ($slugs as $single_slug) {
                                        $slug_queries[] = $single_slug->slug;
                                    }
                                    $meta_query[] = [
                                        'key' => '_company_size',
                                        'value' => $slug_queries,
                                        'compare' => 'IN'
                                    ];
                                }
                            }

                            if (!empty($_GET["industy_focus"])) {
                                $industry = $_GET["industy_focus"];

                                $tax_query[] = [
                                    "taxonomy" => "industry_term",
                                    "terms" => implode(",", $industry),
                                    "operator" => "IN"
                                ];
                            }

                            if (!empty($_GET["client_focus"])) {
                                $industry = $_GET["client_focus"];

                                $tax_query[] = [
                                    "taxonomy" => "client_focus_term",
                                    "terms" => implode(",", $industry),
                                    "operator" => "IN"
                                ];
                            }

                            wp_reset_query();

                            $terms = wp_get_object_terms(get_the_ID(), 'service_term');
                            unset($term_ids);
                            $term_ids = [];
                            if ($terms) :
                                foreach ($terms as $key => $term) {
                                    $term_ids[] = $term->term_id;
                                }
                            endif;
                            $tax_query_all = $tax_query;
                            $tax_query_all[] = array(
                                'taxonomy' => 'service_term',
                                'field' => 'term_id',
                                'terms' => $term_ids
                            );

                            $tax_query_sponsored = array(
                                'taxonomy' => 'service_term',
                                'field' => 'term_id',
                                'terms' => $term_ids,
                            );

                            //==========================================================================================
                            //START QUERYING THE SPONSORED COMPANIES
                            //==========================================================================================
                            //QUERY NEW DATA WHERE COMPANY IS ASSOCIATED DIRECTLY WITH COMPANY GUIDES
                            $sponsored_args = [
                                'fields' => 'ids',
                                'post_type' => 'company',
                                'posts_per_page' => -1,
                                'meta_query' => [
                                    "relation" => "AND",
                                    [
                                        'key' => "_company_guide_" . $company_guide_id,
                                        'value' => 1,
                                        'compare' => '='
                                    ],
                                    [
                                        'key' => "_company_sponsored",
                                        'value' => "yes",
                                        'compare' => '='
                                    ],
                                ]
                            ];

                            $query_2_sponsored = new WP_Query($sponsored_args);
                            wp_reset_query();

                            //QUERY OLD DATA WHERE COMPANY IS ASSOCIATED WITH COMPANY GUIDES THROUGH SERVICES META
                            $terms = wp_get_object_terms(get_the_ID(), 'service_term');
                            $term_ids = [];
                            if ($terms) :
                                foreach ($terms as $key => $term) {
                                    $term_ids[] = $term->term_id;
                                }
                            endif;

                            $query_1_sponsored = new WP_Query([
                                'fields' => 'ids',
                                'post_type' => 'company',
                                'posts_per_page' => -1,
                                'tax_query' => [
                                    $tax_query_sponsored
                                ],
                                'meta_key' => "_company_sponsored",
                                'meta_value' => "yes",
                                'meta_compare' => "=",
                            ]);
                            wp_reset_query();

                            $sponsored_ids = array_merge($query_1_sponsored->posts, $query_2_sponsored->posts);
                            sort($sponsored_ids, SORT_DESC);
                            $sponsored_ids = array_reverse($sponsored_ids);
                            $sponsored_ids = array_unique($sponsored_ids);

                            $query_sponsored = new WP_Query([
                                'post_type' => 'company',
                                'post__in' => $sponsored_ids,
                                'posts_per_page' => -1,
                                'orderby' => 'meta_value_num',
                                'order' => 'DESC'
                            ]);

                            $first = " first";
                            $num_sponsored = count($query_sponsored->posts);
                            if (empty($sponsored_ids)) {
                                $num_sponsored = 0;
                            }

                            // NON SPONSORED
                            $args = [
                                'fields' => 'ids',
                                'post_type' => 'company',
                                'posts_per_page' => -1,
                                'meta_key' => '_company_guide_' . $company_guide_id,
                                'meta_value' => 1,
                                'meta_query' => [
                                    'relation' => 'AND', [
                                        $meta_query
                                    ]
                                ]
                            ];
                            $query_2 = new WP_Query($args);
                            wp_reset_query();

                            //QUERY OLD DATA WHERE COMPANY IS ASSOCIATED WITH COMPANY GUIDES THROUGH SERVICES META
                            $query_1 = new WP_Query([
                                'fields' => 'ids',
                                'post_type' => 'company',
                                'posts_per_page' => -1,
                                'tax_query' => [
                                    'relation' => 'OR',
                                    $tax_query_all
                                ],
                            ]);
                            wp_reset_query();

                            $alltheIDs = array_merge($query_1->posts, $query_2->posts);


                            $review_id = get_posts([
                                'post_type' => 'review',
                                'post_status' =>'publish'
                            ]);
                            foreach ($review_id as $rid) {
                                $rev[] = get_post_meta($rid->ID, "_review_company_id",true);
                            }

                            sort($alltheIDs, SORT_DESC);
                            $alltheIDs = array_reverse($alltheIDs);
                            $alltheIDs = array_unique($alltheIDs);
                            $alltheIDs = array_diff($alltheIDs, $sponsored_ids);
                            $rev = array_intersect($alltheIDs,$rev);
                            $alltheIDs = array_diff($alltheIDs, $rev);
                            if (count($alltheIDs) == 0) {
                                $alltheIDs = array(0);
                            }
                            
                            //global $wpdb;
                            $sponsored_id = join("','",$sponsored_ids);
                            $review_id = join("','",$rev); 
                            $non_sponsored_ids = join("','",$alltheIDs); 

                            $posts_per_page = 10;
                            $offset = ($paged*$posts_per_page)-$posts_per_page;
                            $results = $wpdb->get_results( "SELECT u.* FROM ((SELECT ID FROM wpos_posts WHERE ID IN ('".$sponsored_id."')) UNION ALL (SELECT ID FROM wpos_posts WHERE ID IN ('".$review_id."')) UNION ALL (SELECT ID FROM wpos_posts WHERE ID IN ('".$non_sponsored_ids."') order by rand() limit ".count($alltheIDs).")) AS u limit 10 offset $offset", OBJECT );
                            $i = $first == "" ? 1 : 0;
                            foreach ($results as $post) {

                                $logo = get_post_meta($post->ID, "_company_logo");

                                $review_data = get_posts([
                                    'post_type' => 'review',
                                    'meta_key' => '_review_company_id',
                                    'meta_value' => $post->ID
                                ]);
                                $count_review = 0;
                                $overall_rating = 0;
                                foreach ($review_data as $single_review) {
                                    $temp = get_post_meta($single_review->ID, "_review_rate_overal");
                                    $overall_rating += intval($temp[0]['rating']);
                                    $count_review++;
                                }
                                $avg_rate = ($count_review != 0 ? $overall_rating / $count_review : 0);
                                ?>
                                <div class="company<?php echo $first; ?>">
                                    <div class="company-header">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="company-logo">
                                                    <?php if ($logo[0]) { ?>
                                                        <a href="<?php echo get_the_permalink($post->ID); ?>"><img src="<?php echo $logo[0]; ?>"></a>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="company-name">
                                                    <a href="<?php echo get_the_permalink($post->ID); ?>"><?php echo get_the_title($post->ID); ?></a>
                                                    <?php if (get_post_meta($post->ID,'_company_type',true) == 2) {
                                                        echo '<div class="pull-right premium">[PREMIUM]</div>';
                                                    }?>
                                                    
                                                </div>
                                                <div class="company-rating">
                                                    <?php
                                                    $loop = 5;
                                                    $dark = $loop - ceil($avg_rate);

                                                    for ($i = 0; $i < $avg_rate; $i++) {
                                                        ?>
                                                        <i class="fa fa-star"></i>
                                                        <?php
                                                    }
                                                    for ($i = 0; $i < $dark; $i++) {
                                                        ?>
                                                        <i class="fa fa-star" style="color: #818181"></i>
                                                        <?php
                                                    }
                                                    ?>
                                                    <span class="total-rating">
                                                        <?php
                                                        echo number_format($avg_rate, 1);
                                                        ?>
                                                    </span>
                                                    <span>
                                                        <?php echo $count_review; ?> <?php _e("Review", "skilled") ?> <i class="fa fa-angle-right"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="company-text">
                                        <p>
                                            <?php
                                            $summary = get_post_meta($post->ID, "_company_summary");
                                            echo short_content($summary[0], 250);
                                            ?>
                                            <a href="<?php the_permalink($post->ID); ?>" class="read-more"><?php _e("read more", "skilled"); ?> <i class="fa fa-arrow-right"></i></a>
                                        </p>
                                    </div>
                                    <div class="company-footer">
                                        <div class="company-detail">
                                            <div class="row">
                                                <?php
                                                $location = get_post_meta($post->ID, "_company_location");
                                                $phone = get_post_meta($post->ID, "_company_phone");
                                                $c_rate = get_post_meta($post->ID, "_company_rate");
                                                $c_employee = get_post_meta($post->ID, "_company_employee");
                                                $c_website = get_post_meta($post->ID, "_company_url");
                                                ?>
                                                <div class="col-md-3"><i class="fa fa-map-marker"></i> <span><?php
                                                        $loc = (!empty($location[0][0]['_company_location']['new_country']) ? $location[0][0]['_company_location']['new_country'] : $location[0][0]['_company_location']['country']);
                                                        echo $loc;
                                                        ?></span></div>
                                                <div class="col-md-3"><i class="fa fa-phone"></i> <span><?php echo $location[0][0]['_company_location']['phone']; ?></span></div>
                                                <div class="col-md-3"><i class="fa fa-user"></i> <span><?php echo $c_employee[0]; ?></span></div>
                                                <div class="col-md-3"><i class="fa fa-credit-card"></i> <span><?php
                                                        $homeurl = get_home_url();
                                                        switch ($homeurl) {
                                                            case "https://skilled.co":
                                                                echo "&dollar;";
                                                                break;
                                                            case "https://skilled.co/fr":
                                                                echo "&euro;";
                                                                break;
                                                            case "https://skilled.co/nl":
                                                                echo "&euro;";
                                                                break;
                                                            case "https://skilled.co/ru":
                                                                echo "&dollar;";
                                                                break;
                                                            default:
                                                                echo "&dollar;";
                                                                break;
                                                        }
                                                        
                                                        echo $c_rate[0] ?> / hour</span></div>
                                            </div>
                                        </div>
                                        <div class="company-detail-btn">
                                            <a class="btn btn-primary" style="cursor: pointer" data-toggle="modal" data-target="#myModal" data-id="<?php echo $post->ID; ?>"><?php _e("Request Proposal", "skilled"); ?></a>
                                            <a href="<?php echo $c_website[0]; ?>" class="btn btn-primary" target="_blank" rel="nofollow"><?php _e("Visit Website", "skilled"); ?> <i class="fa fa-arrow-right"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                $i++;
                                $first = "";
                            
                            }
                            
                            ?>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="pagination-container">
                                        <?php
                                        global $wp;
                                        $allpages = array_unique(array_merge($sponsored_ids, $sponsored_ids,$alltheIDs,$rev));
                                        $paged_max = ceil(count($allpages)/$posts_per_page);
                                        $current_page = home_url($wp->request);
                                        skilled_pagination($paged, $paged_max, $current_page);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header modal-header-request-proposal">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><?php echo strtoupper(esc_html_e("Request Proposal", "skilled")) ?></h4>
            </div>
            <div class="modal-body modal-body-request-proposal">
                <div id="error-message"></div>
                <div class="form-group">
                    <label><i class="fa fa-user"></i> <?php _e("Name", "skilled") ?></label>
                    <input type="hidden" id="home_url" value="<?php echo home_url(); ?>">
                    <input type="hidden" id="home_url" value="<?php echo home_url(); ?>">
                    <input type="text" class="form-control" id="proposal_name">
                    <input type="hidden" id="proposal_company" name="proposal_company">
                    <input type="hidden" id="proposal_company" name="proposal_company">
                </div>
                <div class="form-group">
                    <label><i class="fa fa-envelope"></i> <?php _e("Email Address", "skilled") ?></label>
                    <input type="text" class="form-control" id="proposal_email">
                </div>
                <div class="form-group">
                    <label><i class="fa fa-phone"></i> <?php _e("Contact Number", "skilled") ?></label>
                    <input type="text" class="form-control" id="proposal_contact">
                </div>
                <div class="form-group">
                    <label><i class="fa fa-pencil"></i> <?php _e("Description", "skilled") ?></label>
                    <?php wp_editor($content, "proposal_editor"); ?>
                </div>
            </div>
            <div class="modal-footer modal-footer-request-proposal">
                <button type="button" class="btn btn-primary" id="submit-proposal"><?php _e("Save", "skilled") ?></button>
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php _e("Close", "skilled") ?></button>
            </div>
        </div>
    </div>
</div>
<div id="successModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><?php echo strtoupper(esc_html_e("Request Proposal", "skilled")) ?></h4>
            </div>
            <div class="modal-body">
                <?php _e("Thank you for your request. It has been submitted.", "skilled") ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php _e("Close", "skilled") ?></button>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {

        $("#range-hourly-rate").ionRangeSlider({
            type: 'double',
            values: [<?php
                $hourlyRate = get_terms('company_rate_term', [
                    'hide_empty' => false,
                    'orderby' => 'id',
                    'order' => 'ASC'
                ]);
                $rates = [];
                foreach ($hourlyRate as $rate) {
                    $rates[] = "'" . str_replace("'", "`", $rate->name) . "'";
                }
                echo implode(",", $rates);
                ?>],
        });

        $("#range-employee").ionRangeSlider({
            type: 'double',
            values: [<?php
                $size_term = get_terms('company_size_term', [
                    'hide_empty' => false,
                    'orderby' => 'id',
                    'order' => 'ASC'
                ]);
                $sizes = [];
                foreach ($size_term as $size) {
                    $sizes[] = "'" . str_replace("'", "`", $size->name) . "'";
                }
                echo implode(",", $sizes);
                ?>]
        });

        $("#range-project-size").ionRangeSlider({
            type: 'double',
            values: [<?php
                $project_size = get_terms('project_size_term', [
                    'hide_empty' => false,
                    'orderby' => 'id',
                    'order' => 'ASC'
                ]);
                $project_sizes = [];
                foreach ($project_size as $ps) {
                    $project_sizes[] = "'" . str_replace("'", "`", $ps->name) . "'";
                }
                echo implode(",", $project_sizes);
                ?>],
        });

        $('input').each(function () {
            var self = $(this),
                    label = self.next(),
                    label_text = label.text();
            label.remove();
            self.iCheck({
                checkboxClass: 'icheckbox_line-yellow',
                radioClass: 'iradio_line-yellow',
                insert: '<div class="icheck_line-icon"></div>' + label_text
            });
        });
        $(".toggler-company-filter").click(function () {
            var filterResultState = $(".filter-result:visible").length;
            $(".filter-result").fadeIn();
            $(".toggler-company-filter").fadeOut();
            window.scrollTo(0, 0);
        });

        $(".close-button").click(function () {
            $(".filter-result").fadeOut();
            $(".toggler-company-filter").fadeIn();
        });

        $("#myModal").on("show.bs.modal", function (e) {
            var button = $(e.relatedTarget)
            var id = button.data('id');

            $("#proposal_company").val(id);
        });
    });
</script>

<?php get_footer() ?>