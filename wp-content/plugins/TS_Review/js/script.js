jQuery(document).ready(function () {

    var pathArray = window.location.pathname.split('/');

    var captions = {1: 'Very Poor', 2: 'Poor', 3: 'Ok', 4: 'Good', 5: 'Very Good'};
    if (pathArray[1] === "fr") {
        captions = {1: 'Très Faible', 2: 'Faible', 3: 'Ok', 4: 'Bien', 5: 'Très Bien'};
    }
    if (pathArray[1] === "nl") {
        captions = {1: 'erg slecht', 2: 'slecht', 3: 'Ok', 4: 'goed', 5: 'erg goed'};
    }
    console.log("rate")
    jQuery('.ts_rate').rating({
        step: 1,
        starCaptions: captions,
        starCaptionClasses: {1: 'text-danger', 2: 'text-warning', 3: 'text-info', 4: 'text-primary', 5: 'text-success'}
    });

    jQuery('.ts_rate_read').rating({
        displayOnly: true,
        step: 1,
        starCaptions: captions,
        starCaptionClasses: {1: 'text-danger', 2: 'text-warning', 3: 'text-info', 4: 'text-primary', 5: 'text-success'}
    });
});