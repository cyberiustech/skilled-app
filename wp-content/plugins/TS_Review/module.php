<?php
/*
  Plugin Name: Review
  Plugin URI: http://totalstudio.co/
  Description: This plugin is created by Total Studio to manage review for company which is used by Skilled Inc.
  Version: 0.1
  Author: Iwan Safrudin
  Author URI: https://totalstudio.co/about-us/
  Text Domain: totalstudio
 */

add_action('init', 'ts_review', 0);

function ts_review() {
    if (!function_exists('ts_plugin')) {

        function review_plugin_failed() {
            ?>
            <div class="notice notice-error">
                <p><?php _e('TS Review Plugin only works along TS Plugin.', 'skilled'); ?></p>
            </div>
            <?php
        }

        add_action('admin_notices', 'review_plugin_failed');
    } else {
        /* Parent Review */
        $ts_rv = ts_plugin('review', 'Review', 'Reviews');
        $args = array(
            // 'menu_position'=>1,
            'labels' => array(
                'all_items' => 'Review List',
                'add_new_item' => 'Add Review',
                'parent_item_colon' => false
            ),
            'capability_type' => ['review', 'reviews'],
        );

        $ts_rv->postype($args);
    }
}

function ts_review_scripts() {
    wp_register_script('star_rating', plugins_url('/js/star-rating.min.js', __FILE__), array('jquery'), '1.1', true);
    wp_register_script('ts_script', plugins_url('/js/script.js', __FILE__), array('jquery'), '1.1', true);
    wp_enqueue_script('star_rating');
    wp_enqueue_script('ts_script');
}

add_action('wp_enqueue_scripts', 'ts_review_scripts');

function ts_review_styles() {
    wp_register_style('star_rating', plugins_url('/css/star-rating.min.css', __FILE__));
    wp_enqueue_style('star_rating');
}

add_action('wp_enqueue_scripts', 'ts_review_styles');

add_action('cmb2_init', 'cmb2_review_company_information');

function cmb2_review_company_information() {
    $prefix = '_review_';

    $cmb = new_cmb2_box(array(
        'id' => 'review_information',
        'title' => __('Review Company Information', 'cmb2'),
        'object_types' => array('review'),
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true
            )
    );
    $cmb->add_field(array(
        'id' => $prefix . 'type',
        'default' => 'company',
        'type' => 'hidden',
    ));
    $cmb->add_field(array(
        'name' => __('Company being reviewed *'),
        'id' => $prefix . 'company_id',
        'type' => 'select',
        'options_cb' => 'cmb2_get_posttype_options',
        'get_posttype_args' => array(
            'post_type' => 'company',
            'post_status' => 'publish',
            'showposts' => -1
        )
    ));
    $cmb->add_field(array(
        'name' => __('Project Scope', 'skilled'),
        'id' => $prefix . 'project_scope',
        'type' => 'wysiwyg',
        'options' => array(
            'textarea_rows' => 3,
            'media_buttons' => false,
        ),
    ));

    $cmb->add_field(array(
        'name' => __('Project Duration', 'skilled'),
        'id' => $prefix . 'project_duration',
        'type' => 'text',
    ));
    $cmb->add_field(array(
        'name' => __('Project Budget', 'skilled'),
        'id' => $prefix . 'project_budget',
        'type' => 'text',
    ));

    $cmb->add_field(array(
        'name' => __('Company Name', 'skilled'),
        'id' => $prefix . 'company_name',
        'type' => 'text',
    ));

    $cmb->add_field(array(
        'name' => __('Company URL', 'skilled'),
        'id' => $prefix . 'company_url',
        'type' => 'text',
    ));

    $cmb->add_field(array(
        'name' => __('Client Position', 'skilled'),
        'id' => $prefix . 'client_position',
        'type' => 'text',
    ));
    // $cmb->add_field(array(
    //     'name'       => __( 'Project name', 'skilled' ),
    //     'id'         => $prefix . 'post_title',
    //     'type'       => 'text',
    //     ) );
    //  $cmb->add_field( array(
    //         'name'    => 'Image',
    //         'desc'    => 'Upload an image or enter an URL.',
    //         'id'         => $prefix . 'logo',
    //         'type'    => 'file',
    //         'options' => array(
    //             'url' => false,
    //             ),
    //         'text'    => array(
    //             'add_upload_file_text' => 'Add File' 
    //             ),
    //         ) );
    //  $cmb->add_field(array(
    //     'name'       => __( 'Project Tagline', 'skilled' ),
    //     'id'         => $prefix . 'tagline',
    //     'type'       => 'text',
    //     ) );
    // $cmb->add_field( array(
    //     'before_row' => $tab_open,
    //     'name'       => __( 'Background', 'skilled' ),
    //     'id'         => $prefix . 'background',
    //     'type'       => 'wysiwyg',
    //     'options'    => array(
    //         'textarea_rows' => 3,
    //         'media_buttons' => false,
    //         ),
    //     ) );
    // $cmb->add_field( array(
    //     'name'       => __( 'OPPORTUNITY / CHALLENGE', 'skilled' ),
    //     'id'         => $prefix . 'opportunity',
    //     'type'       => 'wysiwyg',
    //     'options'    => array(
    //         'textarea_rows' => 3,
    //         'media_buttons' => false,
    //         ),
    //     ) );
    // $cmb->add_field( array(
    //     'name'       => __( 'Solution', 'skilled' ),
    //     'id'         => $prefix . 'solution',
    //     'type'       => 'wysiwyg',
    //     'options'    => array(
    //         'textarea_rows' => 3,
    //         'media_buttons' => false,
    //         ),
    //     ) );
    // $cmb->add_field( array(
    //     'name'       => __( 'Result', 'skilled' ),
    //     'id'         => $prefix . 'result',
    //     'type'       => 'wysiwyg',
    //     'options'    => array(
    //         'textarea_rows' => 3,
    //         'media_buttons' => false,
    //         ),
    //     ) );





    $cmb->add_field(array(
        'before_row' => $tabPane[1],
        'name' => __('Expertise', 'cmb2'),
        'desc' => __('', 'cmb2'),
        'id' => $prefix . 'rate_expertise',
        'type' => 'star_rating'
    ));


    $cmb->add_field(array(
        'name' => __('Quality', 'cmb2'),
        'desc' => __('', 'cmb2'),
        'id' => $prefix . 'rate_quality',
        'type' => 'star_rating'
    ));




    $cmb->add_field(array(
        'name' => __('Duration & delivery', 'cmb2'),
        'desc' => __('', 'cmb2'),
        'id' => $prefix . 'rate_duration_delivery',
        'type' => 'star_rating'
    ));
    $cmb->add_field(array(
        'name' => __('Customer Support', 'cmb2'),
        'desc' => __('', 'cmb2'),
        'id' => $prefix . 'rate_support',
        'type' => 'star_rating'
    ));

    $cmb->add_field(array(
        'name' => __('Value of money', 'cmb2'),
        'desc' => __('', 'cmb2'),
        'id' => $prefix . 'rate_value',
        'type' => 'star_rating'
    ));

    $cmb->add_field(array(
        'name' => __('Overal Rating', 'cmb2'),
        'desc' => __('', 'cmb2'),
        'id' => $prefix . 'rate_overal',
        'type' => 'star_rating'
    ));


    // $cmb->add_field( array(
    //     'before_row' => $tabPane[2],
    //         'name'       => __( 'Location', 'cmb2' ),
    //         'desc'       => __( '', 'cmb2' ),
    //         'id'         => $prefix . 'location',
    //         'type'       => 'text'
    //         ) );
    //  $cmb->add_field( array(
    //         'name'       => __( 'Employee', 'cmb2' ),
    //         'desc'       => __( '', 'cmb2' ),
    //         'id'         => $prefix . 'employee',
    //         'type'       => 'text'
    //         ) );
    //   $cmb->add_field( array(
    //         'name'       => __( 'Rate', 'cmb2' ),
    //         'desc'       => __( '', 'cmb2' ),
    //         'id'         => $prefix . 'rate',
    //         'type'       => 'text'
    //         ) );


    $cmb->add_field(array(
        'before_row' => $tabPane[3],
        'name' => __('Recommend this company *', 'cmb2'),
        'id' => $prefix . 'recommend_company',
        'type' => 'radio',
        'options' => array(
            'yes' => 'Yes',
            'no' => 'No'
        )
    ));

    $cmb->add_field(array(
        'before_row' => $tabPane[3],
        'name' => __('Hire this company *', 'cmb2'),
        'id' => $prefix . 'hire_company',
        'type' => 'radio',
        'options' => array(
            'yes' => 'Yes',
            'no' => 'No'
        )
    ));
    // $cmb->add_field( array(
    //        'name'       => __( 'Full Name', 'cmb2' ),
    //        'desc'       => __( '', 'cmb2' ),
    //        'id'         => $prefix . 'fullname',
    //        'type'       => 'text'
    //        ) );
    // $cmb->add_field( array(
    //        'name'       => __( 'Company', 'cmb2' ),
    //        'desc'       => __( '', 'cmb2' ),
    //        'id'         => $prefix . 'company',
    //        'type'       => 'text'
    //        ) );
    //   $cmb->add_field( array(
    //        'name'       => __( 'Position', 'cmb2' ),
    //        'desc'       => __( '', 'cmb2' ),
    //        'id'         => $prefix . 'position',
    //        'type'       => 'text'
    //        ) );
    // $cmb->add_field( array(
    //        'name'       => __( 'Phone', 'cmb2' ),
    //        'desc'       => __( '', 'cmb2' ),
    //        'id'         => $prefix . 'phone',
    //        'type'       => 'text'
    //        ) );


    $cmb->add_field(
            array(
                'after_row' => $tab_close,
                'type' => 'title',
                'id' => 'close_title'
            )
    );
}

;

function add_review_func() {
    if (is_user_logged_in()):
        $current_user = wp_get_current_user();
        $the_slug = $_GET['cn'];
        if ($the_slug) {
            $args = array(
                'name' => $the_slug,
                'post_type' => 'company',
                'post_status' => 'publish',
                'numberposts' => 1
            );
            $post = get_posts($args);
            $review_query = new WP_Query(array('post_type' => array('review'), 'author' => get_current_user_id(), 'meta_query' => array(array('key' => '_review_company_id', 'value' => $post[0]->ID))));
            if ($post) :
                $image = get_post_meta($post[0]->ID, "_company_logo", true);
                $return = '<div class="col-md-3">
						    <div class="settings-navigation">
						        <div class="settings-company-logo">
						            <img src="' . $image . '">
						        </div>
						        <div class="settings-company-menu">
						            <ul>
						                <li><a>' . $post[0]->post_title . '</a></li>
						                
						            </ul>
						        </div>
						    </div>
						</div>';
                $return .= '<div class="col-md-9">';
                $return .= '<div class="settings-content">';
                //Content description
                $return .= '<div class="row">';
                $return .= '<div class="col-md-12">';
                $return .= '<div class="settings-box">';
                $return .= '<div class="settings-box-header">'.esc_attr__(strtoupper('Write review for'),'skilled'). ' ' . $post[0]->post_title . '</div>';
                $return .= '<div class="settings-box-body">';
                if ($review_query->post_count > 0) {
                    $return .= '<div id="notice"><div class="alert alert-warning"><strong>Alert!</strong> You already review this company</div></div>';
                } else if ($post[0]->ID == get_user_meta(get_current_user_id(), 'company_id', true)) {
                    $return .= '<div id="notice"><div class="alert alert-warning"><strong>Alert!</strong> You can\'t review your own company</div></div>';
                } else {
                    $return .= '<div id="notice"></div><form id="register" name="register" method="post">';
                    $return .= '<div class="form-group">';
                    $return .= '<label>'.esc_attr__(strtoupper('Your Company'),'skilled'). '</label>
				<input type="text" name="company_name" class="form-control" required>';
                    $return .= '</div>';
                    $return .= '<div class="form-group">';
                    $return .= '<label>'.esc_attr__(strtoupper('Company Url'),'skilled'). '</label>
				<input type="url" name="company_url" class="form-control" placeholder="Example: http://skilled.co" required>';
                    $return .= '</div>';
                    $return .= '<div class="form-group">';
                    $return .= '<label>'.esc_attr__(strtoupper('Your Position'),'skilled'). '</label>
				<input type="text" name="client_position" class="form-control" required>';
                    $return .= '</div>';
                    $return .= '<div class="form-group">';
                    $return .= '<label>'.esc_attr__(strtoupper('Project Scope'),'skilled'). '</label>';
                    ob_start();
                    $settings = array('media_buttons' => false, 'wpautop' => false);
                    wp_editor($content, "project_scope", $settings);
                    $return .= ob_get_clean();
                    $return .= '</div>';
                    $return .= '<div class="form-group">';
                    $return .= '<label>'.esc_attr__(strtoupper('Project Duration (number of months)'),'skilled'). '</label>
                                <input type="number" name="project_duration" class="form-control" required>';
                    $return .= '</div>';
                    $return .= '<div class="form-group">';
                    $return .= '<label>'.esc_attr__(strtoupper('Project Budget'),'skilled'). '</label>
								                <input type="number" name="project_budget" class="form-control" required>';
                    $return .= '</div>';
                    $return .= '<div class="form-group">';
                    $return .= '<label>'.esc_attr__(strtoupper('Review of the company'),'skilled'). '</label>';
                    ob_start();
                    $settings = array('media_buttons' => false, 'wpautop' => false);
                    wp_editor($content, "front-editor", $settings);
                    $return .= ob_get_clean();
                    $return .= '</div>';

                    $return .= '<div class="form-group">';
                    $return .= '<label>'.esc_attr__(('Expertise'),'skilled'). '</label>
			                        			<input name="expertise" class="ts_rate" data-size="xs" required>';
                    $return .= '</div>';


                    $return .= '<div class="form-group">';
                    $return .= '<label>'.esc_attr__(('Quality'),'skilled'). '</label>
			                        			<input name="quality" class="ts_rate" data-size="xs" required>';
                    $return .= '</div>';


                    $return .= '<div class="form-group">';
                    $return .= '<label>'.esc_attr__(('Duration & Delivery'),'skilled'). '</label>
			                        			<input name="duration_delivery" class="ts_rate" data-size="xs" required>';
                    $return .= '</div>';


                    $return .= '<div class="form-group">';
                    $return .= '<label>'.esc_attr__(('Customer Support'),'skilled'). '</label>
			                        			<input name="support" class="ts_rate" data-size="xs" required>';
                    $return .= '</div>';

                    $return .= '<div class="form-group">';
                    $return .= '<label>'.esc_attr__(('value for money'),'skilled'). '</label>
			                        			<input name="value" class="ts_rate" data-size="xs" required>';
                    $return .= '</div>';

                    $return .= '<div class="form-group">';
                    $return .= '<label>'.esc_attr__(('Overall Performance'),'skilled'). '</label>
			                        			<input name="overall" class="ts_rate" data-size="xs" required>';
                    $return .= '</div>';

                    $return .= '<div class="form-group">';
                    $return .= '<input type="checkbox" id="recommendd" name="recommend" value="yes" checked> <label for="recommendd">'.esc_attr__(('Likelihood to recommend'),'skilled'). '</label><br>';
                    $return .= '</div>';
                    $return .= '<div class="form-group">';
                    $return .= '<input type="checkbox" id="rehiree" name="hire" value="yes" checked> <label for="rehiree">'.esc_attr__(('Likelihood to rehire'),'skilled'). '</label><br>';
                    $return .= '</div>';
                    $return .= '<input type="hidden" name="title" value="Review of ' . $post[0]->post_title . ' by ' . $current_user->user_email . '" class="form-control">
								                <input type="hidden" name="company_id" value="' . $post[0]->ID . '" class="form-control">';
                    $return .= '<button class="btn btn-flat btn-primary" type="submit"><div class="cp-spinner cp-meter"></div>Save</button></form></div>';
                }
                $return .= '</div>';
                $return .= '</div>';
                $return .= '</div>';
                $return .= '</div>';
                $return .= '</div>'; //end settings-content
                $return .= '</div>'; //end col-md-9
                $return .= "<script>
						$('#register').submit(function(event) {
							$('#register button').addClass('disable');
							jQuery.ajax({
								type: 'POST',
					            url: '" . get_bloginfo("wpurl") . "/wp-admin/admin-ajax.php',
					            data: {
					            	'formData' :$('#register').serialize(),
					            	'content' : tinymce.get('front-editor').getContent(),
					            	'scope' : tinymce.get('project_scope').getContent(),
					            	'action':'ts_submit_review'
					            },
					            success:function(data) {
					            	console.log(data)
					            	$('#notice').html(\"<div class='alert alert-success'><a href='#'' class='close' data-dismiss='alert'>&times;</a>".__('<strong>Congratulation!</strong> Your review has been submited successfully','skilled')."</div>\");
					            	$('#register').remove();
					            	
					            },
					            error: function(errorThrown){
					                console.log(errorThrown);
					            }
						        });  
						    jQuery('html, body').animate({
					            scrollTop: jQuery('#notice').offset().top - 100
					        }, 200);
							$('#register button').removeClass('disable');
							return false; 
						})
						</script>";
            endif;
        } else {
            $return .= '<script>window.location="' . home_url() . '";</script>';
        }
    else:
        $return .= '<script>window.location="' . home_url() . '/member";</script>';
    endif;
    return $return;
}

add_shortcode('ts_add_review', 'add_review_func');

function ts_submit_review() {
    parse_str($_POST['formData'], $data);
    $my_post = array(
        'post_title' => $data['title'],
        'post_content' => $_POST['content'],
        'post_status' => 'publish',
        'post_type' => 'review',
    );
    $post_id = wp_insert_post($my_post);
    $expertise = array(
        'rating' => $data['expertise']
    );
    $quality = array(
        'rating' => $data['quality']
    );
    $duration = array(
        'rating' => $data['duration_delivery']
    );
    $support = array(
        'rating' => $data['support']
    );
    $value = array(
        'rating' => $data['value']
    );
    $overal = array(
        //'rating' => round(($data['expertise']+$data['quality']+$data['duration_delivery']+$data['support']+$data['value']) / 5)
        'rating' => $data['overall']
    );

    update_post_meta($post_id, '_review_company_name', $data['company_name']);
    update_post_meta($post_id, '_review_company_url', $data['company_url']);
    update_post_meta($post_id, '_review_client_position', $data['client_position']);

    update_post_meta($post_id, '_review_project_scope', $_POST['scope']);
    update_post_meta($post_id, '_review_project_duration', $data['project_duration']);
    update_post_meta($post_id, '_review_project_budget', $data['project_budget']);

    update_post_meta($post_id, '_review_company_id', $data['company_id']);
    update_post_meta($post_id, '_review_rate_expertise', $expertise);
    update_post_meta($post_id, '_review_rate_quality', $quality);
    update_post_meta($post_id, '_review_rate_duration_delivery', $duration);
    update_post_meta($post_id, '_review_rate_support', $support);
    update_post_meta($post_id, '_review_rate_value', $value);
    update_post_meta($post_id, '_review_rate_overal', $overal);

    if ($data['recommend'] == 'yes') {
        update_post_meta($post_id, '_review_recommend_company', 'yes');
    } else {
        update_post_meta($post_id, '_review_recommend_company', 'no');
    }

    if ($data['hire'] == 'yes') {
        update_post_meta($post_id, '_review_hire_company', 'yes');
    } else {
        update_post_meta($post_id, '_review_hire_company', 'no');
    }
    update_post_meta($post_id, '_review_rate_overal', $overal);

    $company_query = new WP_Query(array('post_type' => array('review'), 'meta_query' => array(array('key' => '_review_company_id', 'value' => $data['company_id']))));
    $compay_quality = $company_schedule = $company_cost = $company_recommended = $company_overal = 0;
    while ($company_query->have_posts()) :
        $company_query->the_post();
        $compay_expertise = get_post_meta(get_the_ID(), '_review_rate_expertise', true)['rating'] + $compay_expertise;
        $compay_quality = get_post_meta(get_the_ID(), '_review_rate_quality', true)['rating'] + $compay_quality;
        $company_duration = get_post_meta(get_the_ID(), '_review_rate_duration_delivery', true)['rating'] + $company_duration;
        $company_support = get_post_meta(get_the_ID(), '_review_rate_support', true)['rating'] + $company_support;
        $company_value = get_post_meta(get_the_ID(), '_review_rate_value', true)['rating'] + $company_value;
        $company_overal = get_post_meta(get_the_ID(), '_review_rate_overal', true)['rating'] + $company_overal;
    endwhile;

    $compay_expertise = array(
        'rating' => round($compay_expertise / $company_query->post_count)
    );

    $compay_quality = array(
        'rating' => round($compay_quality / $company_query->post_count)
    );
    $company_value = array(
        'rating' => round($company_value / $company_query->post_count)
    );
    $company_duration = array(
        'rating' => round($company_duration / $company_query->post_count)
    );
    $company_support = array(
        'rating' => round($company_support / $company_query->post_count)
    );
    $company_overal = array(
        'rating' => round($company_overal / $company_query->post_count)
    );

    update_post_meta($data['company_id'], '_company_rate_expertise', $compay_expertise);
    update_post_meta($data['company_id'], '_company_rate_quality', $compay_quality);
    update_post_meta($data['company_id'], '_company_rate_duration_delivery', $company_duration);
    update_post_meta($data['company_id'], '_company_rate_support', $company_support);
    update_post_meta($data['company_id'], '_company_rate_value', $company_value);
    update_post_meta($data['company_id'], '_company_rate_overal', $company_overal);



    $to = get_bloginfo('admin_email');
    $subject = get_post_meta($data['company_id'], '_company_post_title', true) . ' has been reviewed';
    $body = '<!DOCTYPE html>
                <html>
                  <head>
                    <meta name="viewport" content="width=device-width">
                    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                    <title>Skilled Account Information</title>
                    <style type="text/css">
                    /* -------------------------------------
                        INLINED WITH https://putsmail.com/inliner
                    ------------------------------------- */
                    /* -------------------------------------
                        RESPONSIVE AND MOBILE FRIENDLY STYLES
                    ------------------------------------- */
                    @media only screen and (max-width: 620px) {
                      table[class=body] h1 {
                        font-size: 28px !important;
                        margin-bottom: 10px !important; }
                      table[class=body] p,
                      table[class=body] ul,
                      table[class=body] ol,
                      table[class=body] td,
                      table[class=body] span,
                      table[class=body] a {
                        font-size: 16px !important; }
                      table[class=body] .wrapper,
                      table[class=body] .article {
                        padding: 10px !important; }
                      table[class=body] .content {
                        padding: 0 !important; }
                      table[class=body] .container {
                        padding: 0 !important;
                        width: 100% !important; }
                      table[class=body] .main {
                        border-left-width: 0 !important;
                        border-radius: 0 !important;
                        border-right-width: 0 !important; }
                      table[class=body] .btn table {
                        width: 100% !important; }
                      table[class=body] .btn a {
                        width: 100% !important; }
                      table[class=body] .img-responsive {
                        height: auto !important;
                        max-width: 100% !important;
                        width: auto !important; }}
                    /* -------------------------------------
                        PRESERVE THESE STYLES IN THE HEAD
                    ------------------------------------- */
                    @media all {
                      .ExternalClass {
                        width: 100%; }
                      .ExternalClass,
                      .ExternalClass p,
                      .ExternalClass span,
                      .ExternalClass font,
                      .ExternalClass td,
                      .ExternalClass div {
                        line-height: 100%; }
                      .apple-link a {
                        color: inherit !important;
                        font-family: inherit !important;
                        font-size: inherit !important;
                        font-weight: inherit !important;
                        line-height: inherit !important;
                        text-decoration: none !important; }
                      .btn-primary table td:hover {
                        background-color: #34495e !important; }
                      .btn-primary a:hover {
                        background-color: #34495e !important;
                        border-color: #34495e !important; } }
                    </style>
                  </head>
                  <body class="" style="background-color:#f6f6f6;font-family:sans-serif;-webkit-font-smoothing:antialiased;font-size:14px;line-height:1.4;margin:0;padding:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;">
                    <table border="0" cellpadding="0" cellspacing="0" class="body" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;background-color:#f6f6f6;width:100%;">
                      <tr>
                        <td style="font-family:sans-serif;font-size:14px;vertical-align:top;">&nbsp;</td>
                        <td class="container" style="font-family:sans-serif;font-size:14px;vertical-align:top;display:block;max-width:580px;padding:10px;width:580px;Margin:0 auto !important;">
                          <div class="content" style="box-sizing:border-box;display:block;Margin:0 auto;max-width:580px;padding:10px;">
                            <!-- START CENTERED WHITE CONTAINER -->
                            <span class="preheader" style="color:transparent;display:none;height:0;max-height:0;max-width:0;opacity:0;overflow:hidden;mso-hide:all;visibility:hidden;width:0;">This is preheader text. Some clients will show this text as a preview.</span>
                            <table class="main" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;background:#fff;border-radius:3px;width:100%;">
                              <!-- START MAIN CONTENT AREA -->
                              <tr>
                                <td class="wrapper" style="font-family:sans-serif;font-size:14px;vertical-align:top;box-sizing:border-box;padding:20px;">
                                  <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;width:100%;">
                                    <tr>
                                      <td style="font-family:sans-serif;font-size:14px;vertical-align:top;">
                                        <p style="font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;">Hi Skilled Admin,</p>
                                        <p style="font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;">' . $data['title'] . '</p>
                                        <p> Expertise : ' . $data['expertise'] . '</p>
                                        <p> Quality : ' . $data['quality'] . '</p>
                                        <p> Support : ' . $data['support'] . '</p>
                                        <p> Value : ' . $data['value'] . '</p>
                                        <p> Overal : ' . round(($data['expertise'] + $data['quality'] + $data['duration_delivery'] + $data['support'] + $data['value']) / 5) . '</p>
                                        <p style="font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;">Good luck!</p>
                                      </td>
                                    </tr>
                                  </table>
                                </td>
                              </tr>
                              <!-- END MAIN CONTENT AREA -->
                            </table>
                            <!-- START FOOTER -->
                            <div class="footer" style="clear:both;padding-top:10px;text-align:center;width:100%;">
                              <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;width:100%;">
                                <tr>
                                  <td class="content-block" style="font-family:sans-serif;font-size:14px;vertical-align:top;color:#999999;font-size:12px;text-align:center;">
                                    
                                  </td>
                                </tr>
                                <tr>
                                  <td class="content-block powered-by" style="font-family:sans-serif;font-size:14px;vertical-align:top;color:#999999;font-size:12px;text-align:center;">
                                    
                                  </td>
                                </tr>
                              </table>
                            </div>
                            <!-- END FOOTER -->
                            <!-- END CENTERED WHITE CONTAINER -->
                          </div>
                        </td>
                        <td style="font-family:sans-serif;font-size:14px;vertical-align:top;">&nbsp;</td>
                      </tr>
                    </table>
                  </body>
                </html>';

    $headers = array('Content-Type: text/html; charset=UTF-8', 'From: Skilled <no-reply@skilled.co>');

    wp_mail($to, $subject, $body, $headers);

    $to = get_post_meta($data['company_id'], '_company_email', true);
    $name = get_post_meta($data['company_id'], '_company_post_title', true);
    $subject = 'Your company has been reviewed';
    $body = '<!DOCTYPE html>
                <html>
                  <head>
                    <meta name="viewport" content="width=device-width">
                    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                    <title>Skilled Account Information</title>
                    <style type="text/css">
                    /* -------------------------------------
                        INLINED WITH https://putsmail.com/inliner
                    ------------------------------------- */
                    /* -------------------------------------
                        RESPONSIVE AND MOBILE FRIENDLY STYLES
                    ------------------------------------- */
                    @media only screen and (max-width: 620px) {
                      table[class=body] h1 {
                        font-size: 28px !important;
                        margin-bottom: 10px !important; }
                      table[class=body] p,
                      table[class=body] ul,
                      table[class=body] ol,
                      table[class=body] td,
                      table[class=body] span,
                      table[class=body] a {
                        font-size: 16px !important; }
                      table[class=body] .wrapper,
                      table[class=body] .article {
                        padding: 10px !important; }
                      table[class=body] .content {
                        padding: 0 !important; }
                      table[class=body] .container {
                        padding: 0 !important;
                        width: 100% !important; }
                      table[class=body] .main {
                        border-left-width: 0 !important;
                        border-radius: 0 !important;
                        border-right-width: 0 !important; }
                      table[class=body] .btn table {
                        width: 100% !important; }
                      table[class=body] .btn a {
                        width: 100% !important; }
                      table[class=body] .img-responsive {
                        height: auto !important;
                        max-width: 100% !important;
                        width: auto !important; }}
                    /* -------------------------------------
                        PRESERVE THESE STYLES IN THE HEAD
                    ------------------------------------- */
                    @media all {
                      .ExternalClass {
                        width: 100%; }
                      .ExternalClass,
                      .ExternalClass p,
                      .ExternalClass span,
                      .ExternalClass font,
                      .ExternalClass td,
                      .ExternalClass div {
                        line-height: 100%; }
                      .apple-link a {
                        color: inherit !important;
                        font-family: inherit !important;
                        font-size: inherit !important;
                        font-weight: inherit !important;
                        line-height: inherit !important;
                        text-decoration: none !important; }
                      .btn-primary table td:hover {
                        background-color: #34495e !important; }
                      .btn-primary a:hover {
                        background-color: #34495e !important;
                        border-color: #34495e !important; } }
                    </style>
                  </head>
                  <body class="" style="background-color:#f6f6f6;font-family:sans-serif;-webkit-font-smoothing:antialiased;font-size:14px;line-height:1.4;margin:0;padding:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;">
                    <table border="0" cellpadding="0" cellspacing="0" class="body" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;background-color:#f6f6f6;width:100%;">
                      <tr>
                        <td style="font-family:sans-serif;font-size:14px;vertical-align:top;">&nbsp;</td>
                        <td class="container" style="font-family:sans-serif;font-size:14px;vertical-align:top;display:block;max-width:580px;padding:10px;width:580px;Margin:0 auto !important;">
                          <div class="content" style="box-sizing:border-box;display:block;Margin:0 auto;max-width:580px;padding:10px;">
                            <!-- START CENTERED WHITE CONTAINER -->
                            <span class="preheader" style="color:transparent;display:none;height:0;max-height:0;max-width:0;opacity:0;overflow:hidden;mso-hide:all;visibility:hidden;width:0;">This is preheader text. Some clients will show this text as a preview.</span>
                            <table class="main" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;background:#fff;border-radius:3px;width:100%;">
                              <!-- START MAIN CONTENT AREA -->
                              <tr>
                                <td class="wrapper" style="font-family:sans-serif;font-size:14px;vertical-align:top;box-sizing:border-box;padding:20px;">
                                  <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;width:100%;">
                                    <tr>
                                      <td style="font-family:sans-serif;font-size:14px;vertical-align:top;">
                                        <p style="font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;">Hi ' . $name . ',</p>
                                        <p style="font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;">' . $data['title'] . '</p>
                                        <p> Expertise : ' . $data['expertise'] . '</p>
                                        <p> Quality : ' . $data['quality'] . '</p>
                                        <p> Support : ' . $data['support'] . '</p>
                                        <p> Value : ' . $data['value'] . '</p>
                                        <p> Overal : ' . round(($data['expertise'] + $data['quality'] + $data['duration_delivery'] + $data['support'] + $data['value']) / 5) . '</p>
                                        <p style="font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;">Good luck!</p>
                                      </td>
                                    </tr>
                                  </table>
                                </td>
                              </tr>
                              <!-- END MAIN CONTENT AREA -->
                            </table>
                            <!-- START FOOTER -->
                            <div class="footer" style="clear:both;padding-top:10px;text-align:center;width:100%;">
                              <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;width:100%;">
                                <tr>
                                  <td class="content-block" style="font-family:sans-serif;font-size:14px;vertical-align:top;color:#999999;font-size:12px;text-align:center;">
                                    
                                  </td>
                                </tr>
                                <tr>
                                  <td class="content-block powered-by" style="font-family:sans-serif;font-size:14px;vertical-align:top;color:#999999;font-size:12px;text-align:center;">
                                    
                                  </td>
                                </tr>
                              </table>
                            </div>
                            <!-- END FOOTER -->
                            <!-- END CENTERED WHITE CONTAINER -->
                          </div>
                        </td>
                        <td style="font-family:sans-serif;font-size:14px;vertical-align:top;">&nbsp;</td>
                      </tr>
                    </table>
                  </body>
                </html>';

    $headers = array('Content-Type: text/html; charset=UTF-8', 'From: Skilled <no-reply@skilled.co>');

    wp_mail($to, $subject, $body, $headers);

    echo $company_overal;
    die();
}

add_action('wp_ajax_ts_submit_review', 'ts_submit_review');
add_action('wp_ajax_nopriv_ts_submit_review', 'ts_submit_review');
