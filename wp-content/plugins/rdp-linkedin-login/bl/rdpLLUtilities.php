<?php if ( ! defined('WP_CONTENT_DIR')) exit('No direct script access allowed'); ?>
<?php

class RDP_LL_Utilities {
    public static $sessionIsValid = false;
    
    static function abortExecution(){
        $rv = false;
        $wp_action = RDP_LL_Utilities::globalRequest('action');
        if($wp_action == 'heartbeat')$rv = true;
        $isScriptStyleImg = RDP_LL_Utilities::isScriptStyleImgRequest();
        if($isScriptStyleImg)$rv = true;           
        return $rv;
    }//abortExecution    
    
    public static function globalRequest( $name, $default = '' ) {
        $RV = '';
        $array = $_GET;

        if ( isset( $array[ $name ] ) ) {
                $RV = $array[ $name ];
        }else{
            $array = $_POST;
            if ( isset( $array[ $name ] ) ) {
                    $RV = $array[ $name ];
            }                
        }
        
        if(empty($RV) && !empty($default)) return $default;
        return $RV;
    }     
    
    static function getClientIP() {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
        
        if($ipaddress === '::1')$ipaddress = '127.0.0.1';        
        return $ipaddress;
    }

    static function fetch($method, $resource, $access_token,$inputs = null)
    {
        $params = array('oauth2_access_token' => $access_token,
                        'format' => 'json',
                  );
        
        if(is_array($inputs))$params = array_merge($params, $inputs);

        // Need to use HTTPS
        $url = 'https://api.linkedin.com' . $resource . '?' . http_build_query($params);

        $response = wp_remote_get( $url );
        return $response;
    }  //fetch
    
    static function fetch2($resource, $access_token,$inputs = null)
    {
        $params = "oauth2_access_token=$access_token&format=json";
        // Tell streams to make a (GET, POST, PUT, or DELETE) request 
      
        if(!empty($inputs))$params .= '&' . $inputs;

        // Need to use HTTPS
        $url = 'https://api.linkedin.com' . $resource . '?' . $params;

        $response = wp_remote_get( $url );
        return $response;
    }  //fetch2
    
    static function renderTokenExpiredMessage(){
        $sHTML = '<div id="comment-response-container" class="alert">Your LinkedIn session has expired. Please sign out, and then sign in again with LinkedIn, to view the content.</div>';
        
        return $sHTML;
    }//renderTokenExpiredMessage
    
    static function handleUserRegistration($user){
       
        $userID = username_exists( $user->emailAddress );
        $userID = apply_filters( 'rdp_ll_before_insert_user', $userID, $user );
        if (is_numeric($userID)){
            if(property_exists($user, 'publicProfileUrl')){
                update_user_meta($userID, 'rdp_ll_public_profile_url', $user->publicProfileUrl);
            }
            if(property_exists($user, 'pictureUrl')){
                update_user_meta($userID, 'rdp_ll_picture_url', $user->pictureUrl);
            }
            if(property_exists($user, 'headline')){
                update_user_meta($userID, 'rdp_ll_headline', $user->headline);
            }            
            if(property_exists($user, 'location') && !empty($user->location->name)){
                update_user_meta($userID, 'rdp_ll_location', $user->location->name);
            }            
            return true;
        }

        $userdata = array(
            'user_login'    =>  $user->emailAddress,
            'user_pass'  => wp_generate_password( $length=8, $include_standard_special_chars=false ),
            'first_name'    =>  $user->firstName,
            'last_name'    =>  $user->lastName,
            'user_email' => $user->emailAddress,
            'user_url'  =>  $user->publicProfileUrl,           
            'show_admin_bar_front' => 'false',
            'role'  => 'company',
            'display_name' => trim($user->firstName.' '.$user->lastName)
            );            
        $userID = wp_insert_user($userdata);
        $RV = false;
        //On success
        if( !is_wp_error($userID) ) {
            $wp_user = get_user_by( 'id', $userID );
            if(property_exists($user, 'publicProfileUrl')){
                update_user_meta($userID, 'rdp_ll_public_profile_url', $user->publicProfileUrl);
            }
            if(property_exists($user, 'pictureUrl')){
                update_user_meta($userID, 'rdp_ll_picture_url', $user->pictureUrl);
            }
            if(property_exists($user, 'headline')){
                update_user_meta($userID, 'rdp_ll_headline', $user->headline);
            }            
            if(property_exists($user, 'location') && !empty($user->location->name)){
                update_user_meta($userID, 'rdp_ll_location', $user->location->name);
            }            
            $RV = true;
            
            if( is_multisite() ){
                global $blog_id; 
                if ( !is_user_member_of_blog( $userID, $blog_id ) ){
                    add_user_to_blog($blog_id, $userID, get_option('default_role'));
                }
            }
            
            do_action('rdp_ll_after_insert_user', $wp_user, $user);
        }         
        return $RV;
    }//handle_user_registration
    
    static function handleRegisteredUserSignOn($user){
        $fLoggedIn = is_user_logged_in();
        $sUserEmail =  $user->emailAddress;
        $fLoggedIn = apply_filters( 'rdp_ll_before_registered_user_login', $fLoggedIn, $sUserEmail );
        if ($fLoggedIn) return;

        $wp_user = get_user_by( 'email', $sUserEmail );
        if( !$wp_user )$wp_user = get_user_by( 'login', $sUserEmail);
        if( $wp_user ) {
            clean_user_cache($wp_user->ID);
            wp_clear_auth_cookie();            
            wp_set_current_user( $wp_user->ID, $wp_user->user_login );
            wp_set_auth_cookie($wp_user->ID, false);
            do_action( 'wp_login', $wp_user->user_login, $wp_user);
            do_action('rdp_ll_after_registered_user_login', $wp_user);  
        }else do_action('rdp_ll_registered_user_login_fail',$user);

    }//handleRegisteredUserSignOn

    static function logoutURL($logout_url, $redirect ) {
        $params['rdpllaction'] = 'logout';
        $url = add_query_arg($params);
        return $url;        
    }//logoutURL
     
    static function clearQueryParams(){
        $arr_params = array();
        foreach($_GET as $query_string_variable => $value) {
            if(substr($query_string_variable, 0, 5) == 'rdpll')$arr_params[$query_string_variable] = false;
            if( $query_string_variable == 'wikiembed-override-url')$arr_params[$query_string_variable] = false;
            if( $query_string_variable == 'rdp_we_resource')$arr_params[$query_string_variable] = false;
         }
         return $arr_params;
    }//clearQueryParams
    
    static function isScriptStyleImgRequest(){
        $url = (isset($_SERVER['REQUEST_URI']))? $_SERVER['REQUEST_URI'] : '';        
        $imgExts = array("js", "css","gif", "jpg", "jpeg", "png", "tiff", "tif", "bmp");
        $url_parts = parse_url($url);        
        $path = (empty($url_parts["path"]))? '' : $url_parts["path"];
        $urlExt = pathinfo($path, PATHINFO_EXTENSION);
        return in_array($urlExt, $imgExts);
    }//isScriptStyleImgRequest  
    
    static function is_valid_url ($url="") {

            if ($url=="") {
                $url=$this->url;
            }

            $url = @parse_url($url);

            if ( ! $url) {


                return false;
            }

            $url = array_map('trim', $url);
            $url['port'] = (!isset($url['port'])) ? 80 : (int)$url['port'];
            $path = (isset($url['path'])) ? $url['path'] : '';

            if ($path == '') {
                $path = '/';
            }

            $path .= ( isset ( $url['query'] ) ) ? "?$url[query]" : '';



            if ( isset ( $url['host'] ) AND $url['host'] != gethostbyname ( $url['host'] ) ) {
                if ( PHP_VERSION >= 5 ) {
                    $headers = get_headers("$url[scheme]://$url[host]:$url[port]$path");
                }
                else {
                    $fp = fsockopen($url['host'], $url['port'], $errno, $errstr, 30);

                    if ( ! $fp ) {
                        return false;
                    }
                    fputs($fp, "HEAD $path HTTP/1.1\r\nHost: $url[host]\r\n\r\n");
                    $headers = fread ( $fp, 128 );
                    fclose ( $fp );
                }
                $headers = ( is_array ( $headers ) ) ? implode ( "\n", $headers ) : $headers;
                return ( bool ) preg_match ( '#^HTTP/.*\s+[(200|301|302)]+\s#i', $headers );
            }

            return false;
        }    

}//RDP_LL_Utilities

/* EOF */
