<?php if ( ! defined('WP_CONTENT_DIR')) exit('No direct script access allowed'); ?>
<?php

class RDP_LINKEDIN_LOGIN {
    private $_key = '';
    private $_datapass = null;
    private $_version;
    private $_options = array();
    
    public function __construct($version,$options){
        $this->_version = $version;
        $this->_options = $options;        

        add_shortcode('rdp-linkedin-login', array(&$this, 'shortcode_login'));        
        add_shortcode('rdp-linkedin-login-member-count', array(&$this, 'shortcode_member_count'));        

    }//__construct
    
    function run() {
        if ( defined( 'DOING_AJAX' ) ) return;        
        $fDoingLogOut = (isset($_GET['action']) && $_GET['action'] == 'logout')? true : false;   
        if($fDoingLogOut)return;

        $fLLRegisterNewUser = isset($this->_options['fLLRegisterNewUser'])? $this->_options['fLLRegisterNewUser'] : 'off';
        if($fLLRegisterNewUser == 'on' && is_user_logged_in()){
            $current_user = wp_get_current_user();
            $this->_key = md5($current_user->user_email);
        }

        if(!has_filter('widget_text','do_shortcode'))add_filter('widget_text','do_shortcode',11);
        $this->_datapass = RDP_LL_DATAPASS::get($this->_key); 

        if(isset($_GET['rdpllaction']) && $_GET['rdpllaction'] == 'logout'){
            self::handleLogout($this->_datapass);
        }

        if(!$this->_datapass->data_filled()) return;
        if($this->_datapass->tokenExpired()) return;

        $storedIP = $this->_datapass->ipAddress_get();
        $currentIP = RDP_LL_Utilities::getClientIP();
        $ipVerified = ($storedIP === $currentIP );
        $rdpligrt =  $this->_datapass->sessionNonce_get();
        $rdpligrtAction = 'rdp-ll-read-'.$this->_key; 
        if($rdpligrt === 'new'){
            $rdpligrt = wp_create_nonce( $rdpligrtAction );
            $this->_datapass->sessionNonce_set($rdpligrt);
            $this->_datapass->save();
        }
        $nonceVerified = wp_verify_nonce( $rdpligrt, $rdpligrtAction );
        if($nonceVerified && $ipVerified ){
            RDP_LL_Utilities::$sessionIsValid = true;
        }else{
            if(is_user_logged_in()) wp_logout();
        }
    }//run
    
    public function shortcode_member_count($atts) {
        $url = (isset($atts['url']))? $atts['url'] : '' ;
        $count = '';
        if($url){
            // Remove all illegal characters from a url
            $url = filter_var($url, FILTER_SANITIZE_URL);
            // Validate url
            if (filter_var($url, FILTER_VALIDATE_URL) === false){
                return $count;
            }
            
            $response = wp_remote_post( $url);            
            if ( !is_wp_error( $response ) ) {
                $code = $response['response']['code'];
                if($code  != 200)return $count;
                $count = wp_remote_retrieve_body($response);                
            }            

        }else{
            global $wpdb;
            $nTotal = 0;        
            $sSQL = "Select count(*)num From wp_users;";
            $row = $wpdb->get_row($sSQL); 
            if($wpdb->num_rows){
                $nTotal = $row->num; 
            }   

            $count = number_format($nTotal, 0, '.', ',');               
        }
        
        return $count;
    }//shortcode_member_count
    
   
    public function shortcode_login(){
        if(isset($_GET['rdpllaction']) && $_GET['rdpllaction'] == 'logout')return;
        $fIsLoggedIn = false;
        $token = $this->_datapass->access_token_get();
        if (RDP_LL_Utilities::$sessionIsValid && !empty($token))$fIsLoggedIn = true;

        $sStatus = ($fIsLoggedIn)? "true":"false";
        
        $sHTML = '';

        if($sStatus == 'false'){
             $sHTML .= '<div id="login-linkedin" class="btnRDPLLogin" style="cursor: pointer;width: 320px;margin: -20px auto 40px;display: inherit;background: #007bb6;color: #fff;line-height: 40px;text-align: center;font-weight: bold;"><img style="width: 34px;margin-top: -5px;" src="' . plugins_url( 'images/linkedin-icon.png' , __FILE__ ) . '" > Sign in with Linkedin</div>';
            // $sHTML .= '<img style="cursor: pointer;width: 320px;margin: -20px auto 20px;display: inherit;" class="btnRDPLLogin" src="' . plugins_url( 'images/js-signin.png' , __FILE__ ) . '" > ';
        }else{
            
        }
        
        add_action('wp_footer', array(&$this,'renderUserActionsSubmenu'), 9);
        $this->handleScripts($sStatus);
        return apply_filters( 'rdp_ll_render_login', $sHTML, $sStatus );
    }//shortcode_login
    
    public function renderUserActionsSubmenu(){
        echo $this->_datapass->submenuCode_get();
        wp_print_scripts();
        wp_print_styles();
        remove_action('wp_footer', array(&$this,'renderUserActionsSubmenu'));
    }//renderUserActionsSubmenu

   
    public function scriptsEnqueue(){
        // GLOBAL FRONTEND SCRIPTS
        wp_enqueue_script( 'rdp-ll-global', plugins_url( 'js/script.global.js' , __FILE__ ), array( 'jquery','jquery-query' ), $this->_version, TRUE);        
        $params = array(
            'ajax_url' => admin_url( 'admin-ajax.php' ),
            'site_url' => get_site_url()
        );      
        wp_localize_script( 'rdp-ll-global', 'rdp_ll_global', $params );   
    }//scriptsEnqueue

    private function handleScripts($status){
        if(wp_style_is( 'rdp-ll-style-common', 'enqueued' )) return;
        // LinkedIn CSS
        wp_register_style( 'rdp-ll-style-common', plugins_url( 'style/linkedin.common.css' , __FILE__ ) );
	wp_enqueue_style( 'rdp-ll-style-common' );
        
        // RDP Linkedin Login CSS
        wp_register_style( 'rdp-ll-style', plugins_url( 'style/default.css' , __FILE__ ),null, $this->_version );
        wp_enqueue_style( 'rdp-ll-style' );        
        
        $filename = get_stylesheet_directory() .  '/linkedin-login.custom.css';
        if (file_exists($filename)) {
            wp_register_style( 'rdp-ll-style-custom',get_stylesheet_directory_uri() . '/linkedin-login.custom.css',array('rdp-ll-style' ) );
            wp_enqueue_style( 'rdp-ll-style-custom' );
        }
        
        // RDP LL login script
        wp_enqueue_script( 'rdp-ll-login', plugins_url( 'js/script.login.js' , __FILE__ ), array( 'jquery','jquery-query','rdp-ll-global' ), $this->_version, TRUE);
        $url = get_home_url();
        $params = array(
            'ajax_url' => admin_url( 'admin-ajax.php' ),
            'loginurl' => $url 
        );
        wp_localize_script( 'rdp-ll-login', 'rdp_ll_login', $params );

        // Position Calculator
        if(!wp_script_is('jquery-position-calculator'))wp_enqueue_script( 'jquery-position-calculator', plugins_url( 'js/position-calculator.min.js' , __FILE__ ), array( 'jquery' ), '1.1.2', TRUE);

        do_action( 'rdp_ll_after_scripts_styles');
    }//handleScripts



    public static function handleLogout($datapass = null){
        if($datapass != null && $datapass->data_filled()){
            RDP_LL_DATAPASS::delete($datapass->key());            
        }

        $params = RDP_LL_Utilities::clearQueryParams();
        $url = add_query_arg($params);
        
        // log the user out of WP, as well
        if(is_user_logged_in()){
            $url = wp_logout_url( $url );
        }

        // Hack to deal with 'headers already sent' on Linux servers
        // and persistent browser session cookies
        echo "<meta http-equiv='Refresh' content='0; url={$url}'>";
        ob_flush(); // ob_start() called near top of /rdp-linkedin-login/index.php
        exit;
    }//handleLogout
    
    
}//class RDP_LINKEDIN_LOGIN


/* EOF */
 