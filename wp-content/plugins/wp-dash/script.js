jQuery(document).ready(function(){
    //if (sessionStorage.getItem('msg') != undefined) {
        console.log(sessionStorage.getItem('msg'))
        console.log('f')
    //}
})
var file_frame = null;
function open_media_uploader_video(param = null){
    if ( file_frame ) {
            file_frame.open();
            return;
        } 

        file_frame = wp.media.frames.file_frame = wp.media({
            title: 'Select Image',
            button: {
                text: jQuery( this ).data( 'uploader_button_text' ),
            },
            library: { 
                type: 'image' // limits the frame to show only images
            },
            multiple: false // set this to true for multiple file selection
        });

        file_frame.on( 'select', function() {
            attachment = file_frame.state().get('selection').first().toJSON();
            console.log(param);
            $('#imgalert').remove();
            if (param == 'logo') {
                if (attachment.width != attachment.height) {
                    $('.faupload').prepend('<div id="imgalert"><div class="alert alert-warning"><strong>Failed!</strong> Your image should be square.</div></div>')
                }else{
                    jQuery( '#frontend-image' ).attr('src', attachment.url);
                    jQuery( '#frontend-id' ).val(attachment.id);
                }
            }else{
                if (attachment.width < attachment.height) {
                    $('.faupload').prepend('<div id="imgalert"><div class="alert alert-warning"><strong>Failed!</strong> Your image should be landscape.</div></div>')
                }else{
                    jQuery( '#frontend-image' ).attr('src', attachment.url);
                    jQuery( '#frontend-id' ).val(attachment.id);
                }
            }
            $('.faupload').addClass("uploaded")
            // do something with the file here
            //jQuery( '#frontend-button' ).hide();
            
        });

        file_frame.open();
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
 
jQuery(document).ready(function($) {
    $("#preloader").css({'visibility':'hidden'});
    // for lost password
    $("form#lostPasswordForm").submit(function(){
        var submit = $("div#lostPassword #submit"),
            preloader = $("div#lostPassword #preloader"),
            message = $("div#lostPassword #message"),
            contents = {
                action:     'lost_pass',
                nonce:      this.rs_user_lost_password_nonce.value,
                user_login: this.user_login.value
            };
        
        // disable button onsubmit to avoid double submision
        submit.attr("disabled", "disabled").addClass('disabled');
        
        // Display our pre-loading
        preloader.css({'visibility':'visible'});
        
        $.post( theme_ajax.url, contents, function( data ){
            submit.removeAttr("disabled").removeClass('disabled');
            
            // hide pre-loader
            preloader.css({'visibility':'hidden'});
            
            // display return data
            console.log(data);
            // display return data
            if (data == 0) {
                var output = "<div class='alert alert-warning'>There is no user registered with that email address</div>";
            }else{
                var output = "<div class='alert alert-success'>"+data+"</div>";
            }
            message.html( output );
        });
        
        return false;
    });
    
    
    // for reset password
    $("form#resetPasswordForm").submit(function(){
        var submit = $("div#lostPassword #submit"),
            preloader = $("div#lostPassword #preloader"),
            message = $("div#lostPassword #message"),
            contents = {
                action:     'reset_pass',
                nonce:      this.rs_user_reset_password_nonce.value,
                pass1:      this.pass1.value,
                pass2:      this.pass2.value,
                user_key:   this.user_key.value,
                user_login: this.user_login.value
            };
        
        // disable button onsubmit to avoid double submision
        submit.attr("disabled", "disabled").addClass('disabled');
        
        // Display our pre-loading
        preloader.css({'visibility':'visible'});
        
        $.post( theme_ajax.url, contents, function( data ){
            submit.removeAttr("disabled").removeClass('disabled');
            
            // hide pre-loader
            preloader.css({'visibility':'hidden'});
            
            // display return data
            var output = "<div class='alert alert-warning'>"+data+"</div>";
            message.html( output );
        });
        
        return false;
    });

    // for update profile
    $("form#ts_edit_profile").submit(function(){
        var submit = $("div#ts_edit_profile #submit");
        var    preloader = $("#preloader");
        var    message = $("#message");
        if (this.pass1.value != this.pass2.value) {
            var output = "<div class='alert alert-warning'>The passwords you entered did not match</div>";
            message.html( output );
            $('#pass1').val("");
            $('#pass2').val("");
            $('#pass1').css("border","solid 1px red");
            $('#pass2').css("border","solid 1px red");
        }else{
            var contents = {
                    action:     'edit_profile',
                    nonce:      this.ts_update_profile_nonce.value,
                    pass1:      this.pass1.value,
                    pass2:      this.pass2.value,
                    url:   this.url.value,
                    first_name: this.first_name.value,
                    last_name:   this.last_name.value,
                    email: this.email.value
                };
            
            // disable button onsubmit to avoid double submision
            submit.attr("disabled", "disabled").addClass('disabled');
            
            // Display our pre-loading
            preloader.css({'visibility':'visible'});
            
            $.post( theme_ajax.url, contents, function( data ){
                submit.removeAttr("disabled").removeClass('disabled');
                
                // hide pre-loader
                preloader.css({'visibility':'hidden'});
                var output = "<div class='alert alert-success'>"+data+"</div>";
                // display return data
                message.html( output );

            });
        }
        return false;
    });
    
});