<?php

/*
  Plugin Name: Wordpress Front End Publishing
  Plugin URI: http://faqihamruddin.com
  Description: This plugin made with love by faqih amruddin yusuf.
  Version: 1.0
  Author: Faqih Amruddin Yusuf
  Author URI: http://faqihamruddin.com
  License: GPLv2
 */

function showNoCompanyWarning() {
    return "<div class='alert alert-warning'><strong>" . esc_html__('Welcome to your Skilled Account!', 'skilled') . "</strong> " . esc_html__('Start by creating a profile for your company.', 'skilled') . "</div>";
}

add_action('pre_get_posts', 'users_own_attachments');
add_action('admin_init', 'my_remove_menu_pages');

function my_remove_menu_pages() {

    global $user_ID;

    if (current_user_can('company')) {
        remove_menu_page('edit.php');
        remove_menu_page('upload.php');
        remove_menu_page('tools.php');
        remove_menu_page('edit-comments.php');
        remove_menu_page('index.php');
        remove_menu_page('edit.php?post_type=company');
    } elseif (current_user_can('administrator')) {
        remove_menu_page('edit.php');
        remove_menu_page('upload.php');
        //remove_menu_page('tools.php');
        //remove_menu_page('themes.php');
        remove_menu_page('plugins.php');
        remove_menu_page('options-general.php');
    }
}

function dashboard_redirect() {
    wp_redirect(admin_url('profile.php'));
}

add_action('load-index.php', 'dashboard_redirect');

function users_own_attachments($wp_query_obj) {

    global $current_user, $pagenow;

    $is_attachment_request = ($wp_query_obj->get('post_type') == 'attachment');

    if (!$is_attachment_request)
        return;

    if (!is_a($current_user, 'WP_User'))
        return;

    if (!in_array($pagenow, array('upload.php', 'admin-ajax.php')))
        return;

    if (!current_user_can('delete_pages'))
        $wp_query_obj->set('author', $current_user->ID);

    return;
}

function add_roles_on_plugin_activation() {
    add_role('company', 'Company', array('read' => true, 'edit_posts' => true, 'delete_posts' => true, 'upload_files' => true));
    add_role('reviewer', 'Reviewer', array('read' => true, 'edit_posts' => true, 'delete_posts' => true, 'upload_files' => true));
    add_role('super-admin', 'Super Admin', array('activate_plugins' => true, 'delete_others_pages' => true, 'delete_others_posts' => true, 'delete_pages' => true, 'delete_posts' => true, 'delete_private_pages' => true, 'delete_private_posts' => true, 'delete_published_pages' => true, 'delete_published_posts' => true, 'edit_dashboard' => true, 'edit_others_pages' => true, 'edit_others_posts' => true, 'edit_pages' => true, 'edit_posts' => true, 'edit_private_pages' => true, 'edit_private_posts' => true, 'edit_published_pages' => true, 'edit_published_posts' => true, 'edit_theme_options' => true, 'export' => true, 'import' => true, 'list_users' => true, 'manage_categories' => true, 'manage_links' => true, 'manage_options' => true, 'moderate_comments' => true, 'promote_users' => true, 'publish_pages' => true, 'publish_posts' => true, 'read_private_pages' => true, 'read_private_posts' => true, 'read' => true, 'remove_users' => true, 'switch_themes' => true, 'upload_files' => true, 'customize' => true, 'delete_site' => true));
}

register_activation_hook(__FILE__, 'add_roles_on_plugin_activation');

function fa_scripts() {
    wp_register_script('rsclean-request-script', plugins_url('/script.js', __FILE__), array('jquery'), '1.1', true);
    wp_enqueue_script('rsclean-request-script');
// localize wp-ajax, notice the path to our theme-ajax.js file
    wp_localize_script('rsclean-request-script', 'theme_ajax', array(
        'url' => admin_url('admin-ajax.php'),
        'site_url' => get_bloginfo('url'),
        'theme_url' => get_bloginfo('template_directory')
    ));
}

add_action('wp_enqueue_scripts', 'fa_scripts');

function fa_styles() {
    wp_register_style('wptuts_admin_stylesheet', plugins_url('/style.css', __FILE__));
    wp_enqueue_style('wptuts_admin_stylesheet');
}

add_action('wp_enqueue_scripts', 'fa_styles');

function enqueue_media_uploader() {
    //this function enqueues all scripts required for media uploader to work
    wp_enqueue_media();
}

add_action("wp_enqueue_scripts", "enqueue_media_uploader");
add_action('wp_login_failed', 'my_front_end_login_fail');  // hook failed login

function my_front_end_login_fail($username) {
    $referrer = $_SERVER['HTTP_REFERER'];  // where did the post submission come from?
    // if there's a valid referrer, and it's not the default log-in screen
    if (!empty($referrer) && !strstr($referrer, 'wp-login') && !strstr($referrer, 'wp-admin')) {
        wp_redirect($referrer . '?login=failed');  // let's append some information (login=failed) to the URL for the theme to use
        exit;
    }
}

function falogin_func($atts) {
    $atts = shortcode_atts(array(
        'foo' => 'no foo',
            ), $atts, 'bartag');
    global $user_login, $company_id;

    $url = get_the_permalink();
    if (is_user_logged_in()) :
        $user_info = get_userdata(get_current_user_id());
        $company_id = get_user_meta(get_current_user_id(), 'company_id', true);
        $type = implode(', ', $user_info->roles);

        if ($type == 'company') {
            $image = get_post_meta($company_id, "_company_logo", true);
            $return = '<div class="col-md-3">
                            <div class="settings-navigation">';
            if ($image != null) {
                $return .= '<div class="settings-company-logo">
                                    <img src="' . $image . '">
                                </div>';
            }
            $return .= '<div class="settings-company-menu">
                                    <ul>
                                        <li><a href="' . $url . '"><i class="fa fa-list-alt"></i> ' . esc_html__('My Company', 'skilled') . '</a>';
            if ($company_id != null) {
                $return .= '                    <li><a href="' . $url . '?option=list&data=company_service"><i class="fa fa-list-alt"></i> ' . esc_html__('My Services', 'skilled') . '</a></li>
                                                <li><a href="' . $url . '?option=list&data=company_portfolio"><i class="fa fa-list-alt"></i> ' . esc_html__('My Projects', 'skilled') . '</a></li>';
            }
            $return .= '</li>';
            $return .= '<li><a href="' . $url . '?option=list&data=company_review"><i class="fa fa-list-alt"></i> ' . esc_html__('My Reviews', 'skilled') . '</a></li>';
            if ($company_id != null) {

                $return .= '<li><a href="' . $url . '?option=list&data=proposal"><i class="fa fa-list-alt"></i> ' . esc_html__('Proposal for Me', 'skilled') . '</a></li>';
            }
            $return .= '<li><a href="' . $url . '?option=profile"><i class="fa fa-list-alt"></i> ' . esc_html__('My Account', 'skilled') . '</a></li>';
            $return .= '<li><a href="' . wp_logout_url(home_url('/')) . '"><i class="fa fa-lock"></i> ' . esc_html__('Logout', 'skilled') . '</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>';
            $return .= '<div class="col-md-9">';
            $return .= '<div class="settings-content">';
            if ($_GET['option'] == 'add-new') {
                $return .= '<div id="faalert"></div>';
                $return .= '<div class="row"><div class="col-md-12"><div class="settings-box">';
                $return .= '<div class="settings-box-header">' . esc_html__('New', 'skilled') . ' ' . esc_html__(str_replace('_', ' ', $_GET['data']), "skilled") . '</div><div class="settings-box-body"><p>';
                $return .= '<script>jQuery(document).ready(function(){
                    if (sessionStorage.getItem("msg") != undefined) {
                        $("#notice").html(sessionStorage.getItem("msg"))
                        sessionStorage.removeItem("msg");
                    }
                })</script>';
                $return .= "<div id='notice'></div>";
                if ($_GET['data'] == 'company_portfolio') {
                    $return .= '<div class="form-group"><label>' . esc_html__('Project Name', 'skilled') . '</label>';
                    $return .= '<input type="text" name="title" id="fatitle" class="form-control" style="margin-bottom:10px">';
                    $return .= '</div>';
                    $return .= '<div class="form-group"><label>' . esc_html__('Who did you do the project for?', 'skilled') . '</label>';
                    $return .= '<input type="text" name="client" id="faclient" class="form-control" style="margin-bottom:10px">';
                    $return .= '</div>';
                    $return .= '<div class="form-group"><label>' . esc_html__('URL to your full portfolio on your website', 'skilled') . '</label>';
                    $return .= '<input type="text" name="url" id="faurl" class="form-control" style="margin-bottom:10px">';
                    $return .= '</div>';
                    $return .= esc_html__('What was the project about? (Describe in 1 sentence)', 'skilled');

                    ob_start();
                    wp_editor($content, "front-editor", array('media_buttons' => false, 'wpautop' => false));
                    $return .= ob_get_clean();
                    $return .= '<div id="other_character_count" style="padding: 4px;"></div>';
                    $return .= "<style>.faupload:before {content: '" . esc_html__("Upload a picture that best represents your project (only wide image)", "skilled") . "'; left: 0%;margin-left:0;text-align:center;width:100%; font-family: Montserrat; text-transform: none;}</style>";
                    $return .= '<div class="col-md-12">';
                    $return .= '<div class="faupload" onclick="open_media_uploader_video()"><img id="frontend-image" src="' . $thumbnail . '"></div>';
                    $return .= '<input type="hidden" name="attch" id="frontend-id" value="' . get_post_thumbnail_id($_GET['id']) . '">';
                    $return .= '</div>';
                } else if ($_GET['data'] == 'company_service') {

                    if (isset($_GET['from'])) {
                        $return .= "<div class='alert alert-success'>New service was saved. You can add another service.</div>";
                    }

                    $return .= '<div class="form-group"><label>' . esc_html__('Title', 'skilled') . '</label>';
                    $return .= '<input type="text" name="title" id="fatitle" class="form-control" style="margin-bottom:10px">';
                    $return .= '</div>';
                    ob_start();
                    wp_editor($content, "front-editor", array('media_buttons' => false, 'wpautop' => false));
                    $return .= ob_get_clean();
                    $return .= '<div id="other_character_count" style="padding: 4px;"></div>';
                }
                $return .= '<div class="row">';
                $return .= '<div class="col-md-12" style="text-align: right;">';
                $return .= '<br><button class="btn btn-flat btn-primary" onclick="fa_save_post(\'' . $_GET['data'] . '\', false)">' . esc_html__("Save", "skilled") . '</button>';

                if ($_GET['data'] == 'company_service') {
                    $return .= ' <button class="btn btn-flat btn-primary fasavebtn" onclick="fa_save_post(\'' . $_GET['data'] . '\', true)">' . esc_html__("Save and Add New", "skilled") . '</button>';
                }

                $return .= '</div>';
                $return .= '</div>';
                $return .= '</p></div></div></div></div>';
            } else if ($_GET['option'] == 'add-new-company') {
                $return .= apply_filters('the_content', '[faregister]');
            } else if ($_GET['option'] == 'profile') {
                $return .= apply_filters('the_content', '[ts_profile]');
            } else if ($_GET['option'] == 'delete-post') {
                $data = get_post_meta($company_id, "_" . $_GET['data'], true);
                unset($data[$_GET['id']]); // remove item at index 0
                update_post_meta($company_id, "_" . $_GET['data'], array_values($data));
                $return .= '<script>sessionStorage.setItem("msg",\'<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert">&times;</a><strong>Congratulation!</strong> Your data has been deleted successfully</div>\');window.location = "' . $url . '?option=list&data=' . $_GET['data'] . '"</script>';
            } else if ($_GET['option'] == 'publish-post') {
                $my_post = array(
                    'ID' => $_GET['id'],
                    'post_status' => 'publish',
                );
                $id = wp_update_post($my_post);
                $return .= '<script>window.location = "' . $url . '?data=' . $_GET['data'] . '&status=3"</script>';
            } else if ($_GET['option'] == 'edit-post') {
                global $current_user;
                $return .= '<div id="faalert"></div>';
                $return .= '<div class="row"><div class="col-md-12"><div class="settings-box">';
                $return .= '<div class="settings-box-header">' . esc_html__('Edit', 'skilled') . ' ' . esc_html__(str_replace('_', ' ', $_GET['data']), 'skilled') . '</div><div class="settings-box-body"><p>';
                $content_post = get_post_meta($company_id, "_" . $_GET['data'], true);
                if ($_GET['data'] == 'company_portfolio') {
                    $return .= '<div class="form-group"><label>' . esc_html__('Project Name', 'skilled') . '</label>';
                    $return .= '<input type="text" name="title" id="fatitle" class="form-control" style="margin-bottom:10px" value="' . $content_post[$_GET['id']]['_' . $_GET['data'] . '_title'] . '">';
                    $return .= '</div>';
                    $return .= '<div class="form-group"><label>' . esc_html__('Who did you do the project for?', 'skilled') . '</label>';
                    $return .= '<input type="text" name="client" id="faclient" class="form-control" style="margin-bottom:10px" value="' . $content_post[$_GET['id']]['_' . $_GET['data'] . '_client'] . '">';
                    $return .= '</div>';
                    $return .= '<div class="form-group"><label>' . esc_html__('URL to your full portfolio on your website', 'skilled') . '</label>';
                    $return .= '<input type="text" name="url" id="faurl" class="form-control" style="margin-bottom:10px" value="' . $content_post[$_GET['id']]['_' . $_GET['data'] . '_url'] . '">';
                    $return .= '</div>';
                    $return .= esc_html__('What was the project about? (Describe in 1 sentence)', 'skilled');
                    $return .= '';
                    ob_start();
                    $content = $content_post[$_GET['id']]['_' . $_GET['data'] . '_description'];
                    $content = apply_filters('the_content', $content);
                    $content = str_replace(']]>', ']]&gt;', $content);
                    wp_editor($content, "front-editor", array('media_buttons' => false, 'wpautop' => false));
                    $return .= ob_get_clean();
                    $return .= '<div id="other_character_count" style="padding: 4px;"></div>';
                    $thumbnail = $content_post[$_GET['id']]['_' . $_GET['data'] . '_img'];
                    $return .= "<style>.faupload:before {content: 'Upload a picture that best represents your project (only wide image)'; left: 0%;margin-left:0;text-align:center;width:100%; font-family: Montserrat; text-transform: none;}</style>";
                    $return .= '<div class="faupload" onclick="open_media_uploader_video()"><img id="frontend-image" src="' . $thumbnail . '"></div>';
                    $return .= '<input type="hidden" name="attch" id="frontend-id" value="' . $thumbnail . '">';
                } else if ($_GET['data'] == 'company_service') {
                    $return .= '<div class="form-group"><label>' . esc_html__("Service Name", "skilled") . '</label>';
                    $return .= '<input type="text" name="title" id="fatitle" class="form-control" style="margin-bottom:10px" value="' . $content_post[$_GET['id']]['_' . $_GET['data'] . '_title'] . '">';
                    $return .= '</div>';
                    ob_start();
                    $content = $content_post[$_GET['id']]['_' . $_GET['data'] . '_summary'];
                    $content = apply_filters('the_content', $content);
                    $content = str_replace(']]>', ']]&gt;', $content);
                    wp_editor($content, "front-editor", array('media_buttons' => false, 'wpautop' => false));
                    $return .= ob_get_clean();
                    $return .= '<div id="other_character_count" style="padding: 4px;"></div>';
                }
                $return .= '<div class="row">';
                $return .= '<div class="col-md-12" style="text-align: right;">';
                $return .= '<button class="btn btn-flat btn-primary fasavebtn" onclick="fa_save_post(\'' . $_GET['data'] . '\', false)">' . esc_html__("Save", "skilled") . '</button>';
                $return .= '</div>';
                $return .= '</div>';
                $return .= '</p></div></div></div></div>';
            } else if ($_GET['option'] == 'list') {
                $return .= '<script>jQuery(document).ready(function(){
                    if (sessionStorage.getItem("msg") != undefined) {
                        $("#notice").html(sessionStorage.getItem("msg"))
                        sessionStorage.removeItem("msg");
                    }
                })</script>';
                $return .= '<div class="row"><div class="col-md-12"><div class="settings-box">';

                $return .= '<div id="notice"></div>';

                if ($_GET['data'] == 'company_review') {
                    if (isset($_GET['id'])) {
                        $content_post = new WP_Query(array(
                            'post_type' => 'review',
                            'post__in' => array($_GET['id'])
                        ));
                        if ($content_post->have_posts()) {
                            while ($content_post->have_posts()) : $content_post->the_post();
                                $return .= '<div class="settings-box-header">
                                            ' . get_the_title() . '
                                            </div>
                                            <div class="settings-box-body">';
                                $return .= "<h2>" . get_the_author() . '</h2>';

                                $return .= '<div class="overal"><input name="value" class="ts_rate_read xs" data-size="xs"  value="' . get_post_meta(get_the_ID(), '_review_rate_overal', true)['rating'] . '"></div>';
                                $return .= "<small>" . get_post_meta(get_the_ID(), '_review_client_position', true) . " at <a href='" . get_post_meta(get_the_ID(), '_review_company_url', true) . "' target='_blank'>" . get_post_meta(get_the_ID(), '_review_company_name', true) . "</a></small><br><br>";
                                $return .= apply_filters('the_content', get_post_meta(get_the_ID(), '_review_project_scope', true));
                                $return .= '<div class="row"><div class="col-md-6"><h2>' . get_post_meta(get_the_ID(), '_review_project_duration', true) . ' months</h2></div><div class="col-md-6"><h2>$' . get_post_meta(get_the_ID(), '_review_project_budget', true) . '</h2></div></div>';
                                $return .= apply_filters('the_content', get_the_content());

                                $return .= '<div class="form-group">';
                                $return .= '<label style="text-transform:uppercase">' . esc_html__(strtolower('Expertise'), 'skilled') . '</label>
                                            <input name="expertise" class="ts_rate_read" data-size="xs" value="' . get_post_meta(get_the_ID(), '_review_rate_expertise', true)['rating'] . '">';
                                $return .= '</div>';


                                $return .= '<div class="form-group">';
                                $return .= '<label>' . esc_html__('Quality', 'skilled') . '</label>
                                            <input name="quality" class="ts_rate_read" data-size="xs"  value="' . get_post_meta(get_the_ID(), '_review_rate_quality', true)['rating'] . '">';
                                $return .= '</div>';


                                $return .= '<div class="form-group">';
                                $return .= '<label>' . esc_html__('Duration & Delivery', 'skilled') . '</label>
                                            <input name="duration_delivery" class="ts_rate_read" data-size="xs"  value="' . get_post_meta(get_the_ID(), '_review_rate_duration_delivery', true)['rating'] . '">';
                                $return .= '</div>';


                                $return .= '<div class="form-group">';
                                $return .= '<label>' . esc_html__('Customer Support', 'skilled') . '</label>
                                            <input name="support" class="ts_rate_read" data-size="xs"  value="' . get_post_meta(get_the_ID(), '_review_rate_support', true)['rating'] . '">';
                                $return .= '</div>';

                                $return .= '<div class="form-group">';
                                $return .= '<label>' . esc_html__('Value for money', 'skilled') . '</label>
                                            <input name="value" class="ts_rate_read" data-size="xs"  value="' . get_post_meta(get_the_ID(), '_review_rate_value', true)['rating'] . '">';
                                $return .= '</div>';

                                $return .= '<div class="form-group">';
                                if (get_post_meta(get_the_ID(), '_review_recommend_company', true) == 'yes') {
                                    $return .= '<i class="fa fa-check"> Yes, I recommended this company</i>';
                                } else {
                                    $return .= '<i class="fa fa-close"> No, I didn\'t recommended this company</i>';
                                }
                                $return .= '</div>';

                                $return .= '<div class="form-group">';
                                if (get_post_meta(get_the_ID(), '_review_hire_company', true) == 'yes') {
                                    $return .= '<i class="fa fa-check"> Yes, I would hire this company again</i>';
                                } else {
                                    $return .= '<i class="fa fa-close"> No, I wouldn\'t hire this company again</i>';
                                }
                                $return .= '</div>';
                                $return .= '</div>';
                            endwhile;
                        } else {
                            $return .= '<script>window.location = "' . $url . '?option=list&data=company_review"</script>';
                        }
                    } else {
                        $return .= '<div class="settings-box-header">
                                            ' . esc_html__('My Reviews', 'skilled') . '
                                        </div>
                                        <div class="settings-box-body">';
                        $return .= '<table class="table table-bordered table-striped">';
                        $return .= '<th>No</th><th width="20%">' . esc_html__('Title', 'skilled') . '</th><th width="50%">' . esc_html__('Company', 'skilled') . '</th><th width="50%">' . esc_html__('Rating', 'skilled') . '</th><th width="20%">' . esc_html__("Action", "skilled") . '</th>';

                        $args = array(
                            'post_type' => 'review',
                            'author' => get_current_user_id()
                        );
                        $postslist = new WP_Query($args);
                        if ($postslist->have_posts()) :
                            $no = 1;
                            while ($postslist->have_posts()) : $postslist->the_post();
                                $return .= '<tr>';
                                $return .= '<td>';
                                $return .= $no;
                                $return .= '</td>';
                                $return .= '<td>';
                                $return .= get_the_title();
                                $return .= '</td>';
                                $return .= '<td>' . get_the_title(get_post_meta(get_the_ID(), '_review_company_id', true)) . '</td>';
                                $return .= '<td>';
                                for ($i = 1; $i <= 5; $i++) {
                                    $selected = "";
                                    if (!empty(get_post_meta(get_the_ID(), '_review_rate_overal', true)['rating']) && $i <= get_post_meta(get_the_ID(), '_review_rate_overal', true)['rating']) {
                                        $selected = "selected";
                                    }
                                    $return .= '<span class="star ' . $selected . '"><i class="fa fa-star"></i></span>';
                                }
                                $return .= '</td>';
                                $return .= '<td><a href="' . $url . '?option=list&data=company_review&id=' . get_the_ID() . '" class="btn btn-primary-orig" title="View Review"><i class="fa fa-search"></i></a></td>';
                                $return .= '</tr>';
                                $no++;
                            endwhile;
                        endif;
                        $return .= '</table>';
                        $return .= '</div>';
                    }
                } else if ($_GET['data'] == 'company_portfolio') {
                    $data = get_post_meta($company_id, "_" . $_GET['data'], true);
                    $return .= '<div class="settings-box-header">
                                        ' . esc_html__(strtolower('Company Portfolio'), 'skilled') . '
                                    </div>
                                    <div class="settings-box-body"><p>';
                    if ($company_id == null) {
                        $return .= showNoCompanyWarning();
                    } else {
                        $return .= '<a class="btn btn-primary" href="' . $url . '?data=' . $_GET['data'] . '&option=add-new"><i class="fa fa-plus"></i> ' . esc_html__('Create New Portfolio', 'skilled') . '</a>';
                        $return .= '<table class="table table-bordered table-striped">';
                        $return .= '<th>No</th><th width="20%">' . esc_html__("Service Name", "skilled") . '</th><th width="50%">' . esc_html__("Description", "skilled") . '</th><th>' . esc_html__("Action", "skilled") . '</th>';
                        $id = 0;
                        if ($data == null) {
                            
                        } else {
                            foreach ($data as $key) {
                                $return .= '<tr>';
                                $return .= '<td>';
                                $return .= $id + 1;
                                $return .= '</td>';
                                $return .= '<td>';
                                $return .= $key['_company_portfolio_title'];
                                $return .= '</td>';
                                $return .= '<td>' . $key['_company_portfolio_description'] . '</td>';
                                $return .= '<td><a href="' . $url . '?data=' . $_GET['data'] . '&option=edit-post&id=' . $id . '" class="btn btn-primary-orig" title="Edit Service"><i class="fa fa-pencil"></i></a> <a href="' . $url . '?data=' . $_GET['data'] . '&option=delete-post&id=' . $id . '" class="btn btn-danger" title="Delete Service"><i class="fa fa-trash"></i></a></td>';
                                $return .= '</tr>';
                                $id++;
                            };
                        }
                    }
                    $return .= '</table>';
                } else if ($_GET['data'] == 'proposal') {

                    if ($company_id == null) {
                        $return .= showNoCompanyWarning();
                    } else {
                        $args = [
                            "post_type" => "proposal",
                            "meta_key" => "proposal_company",
                            "meta_value" => $company_id
                        ];

                        $return .= '<div class="settings-box-header">' . esc_html__('Proposal for My Company', 'skilled') . '</div>
                            <div class="settings-box-body"><p>';

                        if (empty($_GET['id'])) {
                            $proposal = get_posts($args);
                            $return .= "<table class='table table-striped table-bordered'>";
                            $return .= "<thead>";
                            $return .= "<tr>";
                            $return .= "<th>" . esc_html__('Title', 'skilled') . "</th>";
                            $return .= "<th>" . esc_html__('Sender Name', 'skilled') . "</th>";
                            $return .= "<th></th>";
                            $return .= "</tr>";
                            $return .= "</thead>";
                            $return .= "<tbody>";

                            foreach ($proposal as $p) {
                                $name = get_post_meta($p->ID, "proposal_name");

                                $return .= "<tr>";
                                $return .= "<td>" . $p->post_title . "</td>";
                                $return .= "<td>" . $name [0] . "</td>";
                                $return .= "<td><a href='?option=list&data=proposal&id=" . $p->ID . "' class='btn btn-primary-orig'><i class='fa fa-eye'></i></a></td>";
                                $return .= "</tr>";
                            }
                            $return .= "</tbody>";
                            $return .= "</table>";
                        } else {
                            $proposal_id = $_GET['id'];

                            $args = [
                                "post_type" => "proposal",
                                "p" => $proposal_id
                            ];

                            $pquery = new WP_Query($args);
                            $return .= "<a href='?option=list&data=proposal' class='btn btn-primary-orig'><i class='fa fa-angle-double-left'></i> " . esc_html__('Back to Proposal List', 'skilled') . "</a>";
                            $return .= "<table class='table table-striped table-bordered'>";
                            $return .= "<tbody>";

                            if ($pquery->have_posts()) :
                                while ($pquery->have_posts()) :
                                    $pquery->the_post();
                                    $name = get_post_meta(get_the_ID(), "proposal_name");
                                    $email = get_post_meta(get_the_ID(), "proposal_email");
                                    $contact = get_post_meta(get_the_ID(), "proposal_contact");

                                    $return .= "<tr><td>" . esc_html__('Title', 'skilled') . "</td>";
                                    $return .= "<td>" . get_the_title() . "</td></tr>";
                                    $return .= "<tr><td>" . esc_html__('Sender Name', 'skilled') . "</td>";
                                    $return .= "<td>" . $name [0] . "</td></tr>";
                                    $return .= "<tr><td>" . esc_html__('Email', 'skilled') . "</td>";
                                    $return .= "<td>" . $email [0] . "</td></tr>";
                                    $return .= "<tr><td>" . esc_html__('Contact', 'skilled') . "</td>";
                                    $return .= "<td>" . $contact [0] . "</td></tr>";
                                    $return .= "<tr><td>Content</td>";
                                    $return .= "<td>" . get_the_content() . "</td></tr>";
                                endwhile;
                                wp_reset_query();
                            else:

                            endif;
                            $return .= "</tbody>";
                            $return .= "</table>";
                        }
                        $return .= "</p></div>";
                    }
                } else {
                    $data = get_post_meta($company_id, "_" . $_GET['data'], true);
                    $return .= '<div class="settings-box-header">
                                        ' . esc_html__(strtolower("Company Service"), "skilled") . '
                                    </div>
                                    <div class="settings-box-body"><p>';
                    if ($company_id == null) {
                        $return .= showNoCompanyWarning();
                    } else {
                        $return .= '<a class="btn btn-primary" href="' . $url . '?data=' . $_GET['data'] . '&option=add-new"><i class="fa fa-plus"></i> ' . esc_html__("Create New Service", "skilled") . '</a>';
                        $return .= '<table class="table table-bordered table-striped">';
                        $return .= '<th>' . esc_html__("No", "skilled") . '</th><th width="20%">' . esc_html__("Service Name", "skilled") . '</th><th width="50%">' . esc_html__("Description", "skilled") . '</th><th>' . esc_html__("Action", "skilled") . '</th>';
                        $id = 0;
                        if ($data == null) {
                            
                        } else {

                            foreach ($data as $key) {
                                $return .= '<tr>';
                                $return .= '<td>';
                                $return .= $id + 1;
                                $return .= '</td>';
                                $return .= '<td>';
                                $return .= $key['_company_service_title'];
                                $return .= '</td>';
                                $return .= '<td>' . $key['_company_service_description'] . '</td>';
                                $return .= '<td><a href="' . $url . '?data=' . $_GET['data'] . '&option=edit-post&id=' . $id . '" class="btn btn-primary-orig" title="Edit Service"><i class="fa fa-pencil"></i></a> <a href="' . $url . '?data=' . $_GET['data'] . '&option=delete-post&id=' . $id . '" class="btn btn-danger" title="Delete Service"><i class="fa fa-trash"></i></a></td>';
                                $return .= '</tr>';
                                $id++;
                            };
                        }
                        $return .= '</table>';
                    }
                }

                $return .= '</div></div></div>';
            } else {
                global $post, $current_user;
                $return .= '<div id="faalert"></div>';
                if ($company_id == null) {
                    $return .= "<div style='padding: 1rem;'>";
                    $return .= showNoCompanyWarning();
                    $return .= '<a class="btn btn-primary" href="' . $url . '?data=' . $_GET['data'] . '&option=add-new-company"><i class="fa fa-plus"></i> ' . esc_html__("Create New Company", "skilled") . '</a>';
                    $return .= "</div>";
                } else {
                    //Content description
                    $company_size_term = get_terms(array('taxonomy' => 'company_size_term', 'hide_empty' => false, 'orderby' => 'id', 'order' => 'asc'));
                    $company_rate_term = get_terms(array('taxonomy' => 'company_rate_term', 'hide_empty' => false, 'orderby' => 'id', 'order' => 'asc',));
                    $project_size_term = get_terms(array('taxonomy' => 'project_size_term', 'hide_empty' => false, 'orderby' => 'id', 'order' => 'asc',));
                    $return .= '<script>jQuery(document).ready(function(){
                                    if (sessionStorage.getItem("msg") != undefined) {
                                        $("#notice").html(sessionStorage.getItem("msg"))
                                        sessionStorage.removeItem("msg");
                                    }
                                })</script>';
                    $return .= '<div class="row">';
                    $return .= '<div class="col-md-12">';
                    $return .= '<div class="settings-box">';
                    $return .= '<div class="settings-box-header"> ' . esc_html__('Company Description', 'skilled') . '</div>';
                    $return .= '<div class="settings-box-body">';
                    $return .= '<div id="notice"></div>
                            <form id="register" name="register" method="post">
                            <div class="form-group">
                                <label>' . esc_html__('Company Name', 'skilled') . '</label>
                                <input type="text" id="company_name" name="company_name" class="form-control" value="' . get_post_meta($company_id, '_company_post_title', true) . '"/>
                            </div>
                            <div class="form-group">
                                <label>' . esc_html__('Company Size', 'skilled') . '</label>
                                <select class="form-control" name="company_employee">';

                    foreach ($company_size_term as $key) {
                        if (get_post_meta($company_id, '_company_employee', true) == $key->slug) {
                            $return .= '<option value="' . $key->term_id . '" selected>' . $key->name . '</option>';
                        } else {
                            $return .= '<option value="' . $key->term_id . '">' . $key->name . '</option>';
                        }
                    };

                    $homeurl = get_home_url();
                    switch ($homeurl) {
                        case "https://skilled.co":
                            $currency = "&dollar;";
                            break;
                        case "https://skilled.co/fr":
                            $currency = "&euro;";
                            break;
                        case "https://skilled.co/nl":
                            $currency = "&euro;";
                            break;
                        case "https://skilled.co/ru":
                            $currency = "$";
                            break;
                        default:
                            $currency = "&dollar;";
                            break;
                    }
                    $return .= '</select>
                            </div>
                            <div class="form-group">
                                <label>' . esc_html__('Min. Project Budget', 'skilled') . '</label>
                                <select class="form-control" name="company_size">';

                    foreach ($project_size_term as $key) {
                        if (get_post_meta($company_id, '_company_size', true) == $key->slug) {
                            $return .= '<option value="' . $key->term_id . '" selected>' . $key->name . '</option>';
                        } else {
                            $return .= '<option value="' . $key->term_id . '">' . $key->name . '</option>';
                        }
                    };
                    $return .= '</select>
                            </div>
                            <div class="form-group">
                                <label>' . esc_html__('Average Hourly Rates', 'skilled') . '</label>
                                <select class="form-control" name="company_rate">';
                    foreach ($company_rate_term as $key) {
                        if (get_post_meta($company_id, '_company_rate', true) == $key->slug) {
                            $return .= '<option value="' . $key->term_id . '" selected>' . $key->name . '</option>';
                        } else {
                            $return .= '<option value="' . $key->term_id . '">' . $key->name . '</option>';
                        }
                    };
                    $return .= '</select>
                            </div>
                            <div class="form-group">
                                <label>' . esc_html__('Website URL', 'skilled') . '</label>
                                <input type="url" name="company_url" class="form-control" value="' . get_post_meta($company_id, '_company_url', true) . '"/>
                            </div>
                            <div class="form-group">
                                <label>' . esc_html__('Phone', 'skilled') . '</label>
                                <input type="text" name="company_phone" class="form-control" value="' . get_post_meta($company_id, '_company_phone', true) . '"/>
                            </div>
                            <div class="form-group">
                                <label>' . esc_html__('Email', 'skilled') . '</label>
                                <input type="email" id="company_email" name="company_email" class="form-control" value="' . get_post_meta($company_id, '_company_email', true) . '"/>
                            </div>
                            <div class="form-group">
                                <label>' . esc_html__('Company Description', 'skilled') . ' (' . esc_html__('Max length: 200 words', 'skilled') . ')</label>';
                    ob_start();
                    $content = get_post_meta($company_id, '_company_summary', true);
                    $content = apply_filters('the_content', $content);
                    $content = str_replace(']]>', ']]&gt;', $content);
                    wp_editor($content, "company-description", array('media_buttons' => false, 'wpautop' => false));
                    $return .= ob_get_clean();

                    $return .= '<div id="company_description_character_count" style="padding: 4px;"></div>';
                    $return .= '</div>';
                    $return .= "<style>.faupload:before {content: '" . esc_html__('Add Logo', 'skilled') . " (" . esc_html__('Only square image', 'skilled') . ")'; left: 0%;margin-left:0;text-align:center;width:100%;font-family: Montserrat; text-transform: none;}</style>";
                    $return .= '<div class="faupload" onclick="open_media_uploader_video(\'logo\')"><img id="frontend-image"></div>';
                    $return .= '<input type="hidden" name="attch" id="frontend-id">';

                    $company_guide = get_posts([
                        "post_type" => "organization",
                        "posts_per_page" => -1,
                        "orderby" => "ID",
                        "order" => "ASC"
                    ]);

                    $return .= '
                            <div class="form-group">
                                <label>' . esc_html__('What category would you like to be featured in?', 'skilled') . '</label>
                                    ';
                    $val = get_post_meta($company_id, '_company_guide', true);
                    foreach ($company_guide as $cg) {
                        $label = $cg->post_title;
                        if ($cg->post_parent != 0) {
                            $parent = get_post($cg->post_parent);
                            $label = $parent->post_title . " - " . $label;
                        }
                        $val = get_post_meta($company_id, '_company_guide', true);
                        if (is_array($val)) {
                            if (in_array($cg->ID, $val)) {
                                $return .= "<br><input type='checkbox' name='company_guide[]' value='" . $cg->ID . "' id='label-company-guide-" . $cg->ID . "' checked> <label for='label-company-guide-" . $cg->ID . "'>" . $cg->post_title . "</label>";
                            } else {
                                $return .= "<br><input type='checkbox' name='company_guide[]' value='" . $cg->ID . "' id='label-company-guide-" . $cg->ID . "'> <label for='label-company-guide-" . $cg->ID . "'>" . $cg->post_title . "</label>";
                            }
                        } else {
                            $return .= "<br><input type='checkbox' name='company_guide[]' value='" . $cg->ID . "' id='label-company-guide-" . $cg->ID . "'> <label for='label-company-guide-" . $cg->ID . "'>" . $cg->post_title . "</label>";
                        }
                    }
                    $return .= '
                            </div>
                                    ';

                    $return .= '
                                <style>
                                    .address-group{
                                        border: 1px solid #dfdfdf;
                                        background: 1px solid #fafafa;
                                        padding: 1rem;
                                        margin-bottom: 0.5rem;
                                        margin-right: 1rem;
                                        width: 47%;
                                        display: inline-block;
                                    }
                                    .address-group input,.address-group select{
                                        display: block;
                                        margin-bottom: 0.5rem;
                                        border-radius: 0px;
                                    }
                                </style>
                            <div class="form-group">
                                <label>' . esc_html__('Which of the following priority would you prefer to be?', 'skilled') . '</label>
                                    <select name="company_type" id="account_type" class="form-control">';
                    $return .= (get_post_meta($company_id, '_company_type', true) == 2) ? '<option value="2" selected>' . esc_html__('Sponsored: Ranked at the very top of results as a priority partner (paid membership)', 'skilled') . '</option>' : '<option value="2">' . esc_html__('Sponsored: Ranked at the very top of results as a priority partner (paid membership)', 'skilled') . '</option>';
                    $return .= (get_post_meta($company_id, '_company_type', true) == 1) ? '<option value="1" selected>' . esc_html__('Badged: Secondary site priority for adding skilled badge with link to public profile (free)', 'skilled') . '</option>' : '<option value="1">' . esc_html__('Badged: Secondary site priority for adding skilled badge with link to public profile (free)', 'skilled') . '</option>';
                    $return .= (get_post_meta($company_id, '_company_type', true) == 3) ? '<option value="3" selected>' . esc_html__('Organic: Organic Ranking based on quality and number of user reviews (free)', 'skilled') . '</option>' : '<option value="0">' . esc_html__('Organic: Organic Ranking based on quality and number of user reviews (free)', 'skilled') . '</option>';

                    $return .= '
                                    </select>
                            </div>

                                <!-- COMPANY ADDRESS FIELDS BEGIN HERE! -->

                            <div class="form-group">
                                <label>' . esc_html__('Addresses', 'skilled') . '</label>
                                    <br>';
                    $val = get_post_meta($company_id, '_company_location', true);
                    $i = 0;
                    if (is_array($val)) {
                        foreach ($val as $add) {
                            $return .= '<div class="address-group" style="width: 100%;">
                                                <input type="text" placeholder="' . esc_html__('Address Line 1', 'skilled') . '" name="address_line_1[]" class="form-control" value="' . $add['_company_location']['address-1'] . '">
                                                <input type="text" placeholder="' . esc_html__('Address Line 2', 'skilled') . '" name="address_line_2[]" class="form-control" value="' . $add['_company_location']['address-2'] . '">
                                                <input type="text" placeholder="' . esc_html__('Phone', 'skilled') . '" name="address_phone[]" class="form-control" value="' . $add['_company_location']['phone'] . '">
                                                <input type="text" placeholder="' . esc_html__('ZIP', 'skilled') . '" name="address_zip[]" class="form-control" value="' . $add['_company_location']['zip'] . '">
                                                <select name="address_country[]" id="country-1" class="form-control" onchange="selectState(this, 1)">
                                                <option value="">- ' . esc_html__('Select Country', 'skilled') . ' -</option>
                                                ';
                            $list = get_terms('company_country', [
                                'hide_empty' => false,
                                'orderby' => 'name',
                                'order' => 'asc'
                            ]);

                            $country_options = '';
                            foreach ($list as $li) {
                                if ($add['_company_location']['new_country'] == $li->name) {
                                    $country_options .= '<option value="' . $li->name . '" selected>' . $li->name . '</option>';
                                } else {
                                    $country_options .= '<option value="' . $li->name . '">' . $li->name . '</option>';
                                }
                            }
                            $return .= $country_options;

                            $return .= '
                                                </select>
                                                <input type="text" placeholder="' . esc_html__('State', 'skilled') . '" name="address_state[]" class="form-control" value="' . $add['_company_location']['new_state'] . '">
                                                <input type="text" placeholder="' . esc_html__('City', 'skilled') . '" name="address_city[]" class="form-control" value="' . $add['_company_location']['city'] . '">';
                            if ($i == 0) {
                                $return .= '<div style="text-align: right">
                                                        <button type="button" class="btn btn-primary-orig" id="add-address"><i class="fa fa-plus"></i></button>
                                                    </div>';
                            } else {
                                $return .= '<div style="text-align: right">    <button type="button" class="btn btn-danger" onclick="removeAddress(this)"><i class="fa fa-trash"></i></button></div>';
                            }
                            $return .= ' </div>';
                            $i++;
                        }
                    } else {
                        $return .= '<div class="address-group" style="width: 100%;">
                                            <input type="text" placeholder="' . esc_html__('Address Line 1', 'skilled') . '" name="address_line_1[]" class="form-control" value="' . $add['_company_location']['address-1'] . '">
                                            <input type="text" placeholder="' . esc_html__('Address Line 2', 'skilled') . '" name="address_line_2[]" class="form-control" value="' . $add['_company_location']['address-2'] . '">
                                            <input type="text" placeholder="' . esc_html__('Phone', 'skilled') . '" name="address_phone[]" class="form-control" value="' . $add['_company_location']['phone'] . '">
                                            <input type="text" placeholder="' . esc_html__('ZIP', 'skilled') . '" name="address_zip[]" class="form-control" value="' . $add['_company_location']['zip'] . '">
                                            <select name="address_country[]" id="country-1" class="form-control" onchange="selectState(this, 1)">
                                            <option value="">- ' . esc_html__('Select Country', 'skilled') . ' -</option>
                                            ';
                        $list = get_terms('company_country', [
                            'hide_empty' => false,
                            'orderby' => 'name',
                            'order' => 'asc'
                        ]);

                        $country_options = '';
                        foreach ($list as $li) {
                            if ($add['_company_location']['new_country'] == $li->name) {
                                $country_options .= '<option value="' . $li->name . '" selected>' . $li->name . '</option>';
                            } else {
                                $country_options .= '<option value="' . $li->name . '">' . $li->name . '</option>';
                            }
                        }
                        $return .= $country_options;
                        $return .= '
                                            </select>
                                            <input type="text" placeholder="' . esc_html__('State', 'skilled') . '" name="address_state[]" id="state-1" class="form-control" value="' . $add['_company_location']['new_state'] . '">
                                            <input type "text" placeholder="' . esc_html__('City', 'skilled') . '" name="address_city[]" id="city-1" class="form-control" value="' . $add['_company_location']['city'] . '">
                                            ';
                        if ($i == 0) {
                            $return .= '<div style="text-align: right">
                                                    <button type="button" class="btn btn-primary-orig" id="add-address"><i class="fa fa-plus"></i></button>
                                                </div>';
                        } else {
                            $return .= '<div style="text-align: right">    <button type="button" class="btn btn-danger" onclick="removeAddress(this)"><i class="fa fa-trash"></i></button></div>';
                        }
                        $return .= ' </div>';
                    }
                    $return .= '<div id="address-additional"></div>
                            </div>
                                <script>
                                function removeAddress(the_element){
                                    the_element.parentNode.parentNode.parentNode.removeChild(the_element.parentNode.parentNode);
                                }
                                $(document).ready(function(){
                                    var the_count = 2;
                                    $(\'#add-address\').click(function(){
                                        var txt = \'\';

                                        txt += \'<div class="address-group" id="address-\'+the_count+\'">\';
                                        txt += \'<input type="text" placeholder="' . esc_html__('Address Line 1', 'skilled') . '" name="address_line_1[]" class="form-control">\';
                                        txt += \'<input type="text" placeholder="' . esc_html__('Address Line 2', 'skilled') . '" name="address_line_2[]" class="form-control">\';
                                        txt += \'<input type="text" placeholder="' . esc_html__('Phone', 'skilled') . '" name="address_phone[]" class="form-control">\';
                                        txt += \'<input type="text" placeholder="' . esc_html__('ZIP', 'skilled') . '" name="address_zip[]" class="form-control">\';
                                        txt += \'<select name="address_country[]" id="country-\'+the_count+\'" class="form-control" onchange="selectState(this, \'+the_count+\')">\';
                                        txt += \'<option value="">- ' . esc_html__('Select Country', 'skilled') . ' -</option>\';
                                        txt += \'' . $country_options . '\';
                                        txt += \'</select>\';
                                        txt += \'<input type="text" placeholder="State" name="address_state[]" id="state-1" class="form-control">\';
                                        txt += \'<input type "text" placeholder="City" name="address_city[]" id="city-1" class="form-control">\';
                                        txt += \'<div style="text-align: right">\';
                                        txt += \'    <button type="button" class="btn btn-danger" onclick="removeAddress(this)"><i class="fa fa-trash"></i></button>\';
                                        txt += \'</div>\';
                                        txt += \'</div>\';


                                        $(\'#address-additional\').append(txt);
                                        the_count++;
                                    });
                                });
                                </script>';
                    $client_focus = get_terms(array('taxonomy' => 'client_focus_term', 'hide_empty' => false, 'orderby' => "term_id", 'order' => 'ASC'));
                    $return .= ' 
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                <label>' . esc_html__('What type of companies do you work with?', 'skilled') . '</label><br>
                                        ';

                    $client_fcs = get_the_terms($company_id, 'client_focus_term');
                    if ($client_fcs != null) {
                        foreach ($client_fcs as $key) {
                            $client[] = $key->term_id;
                        }
                    }
                    if ($client != null) {
                        foreach ($client_focus as $key) {
                            if (in_array($key->term_id, $client)) {
                                $return .= '<input type="checkbox" name="client_focus[]" value="' . $key->term_id . '" id="client-' . $key->term_id . '" checked> <label for="client-' . $key->term_id . '">' . $key->name . '</label><br>';
                            } else {
                                $return .= '<input type="checkbox" name="client_focus[]" value="' . $key->term_id . '" id="client-' . $key->term_id . '"> <label for="client-' . $key->term_id . '">' . $key->name . '</label><br>';
                            }
                        };
                    } else {
                        foreach ($client_focus as $key) {
                            $return .= '<input type="checkbox" name="client_focus[]" value="' . $key->term_id . '" id="industry-' . $key->term_id . '"> <label for="industry-' . $key->term_id . '">' . $key->name . '</label><br>';
                        };
                    }
                    $return .= ' 
                                </div>
                            </div>';
                    $industry_focus = get_terms(array('taxonomy' => 'industry_term', 'hide_empty' => false,));
                    $industry = get_the_terms($company_id, 'industry_term');
                    if ($industry != null) {
                        foreach ($industry as $key) {
                            $ind[] = $key->term_id;
                        }
                    } else {
                        $ind = array();
                    }
                    //BEGIN CLIENT FOCUS META BOX
                    $return .= ' 
                            <div class="col-md-6">
                                <div class="form-group">
                                <label>' . esc_html__("What industries are your clients usually from?", "skilled") . '</label><br>
                                        ';

                    foreach ($industry_focus as $key) {
                        if (in_array($key->term_id, $ind)) {
                            $return .= '<input type="checkbox" name="industry_focus[]" value="' . $key->term_id . '" id="industry-' . $key->term_id . '" checked> <label for="industry-' . $key->term_id . '">' . $key->name . '</label><br>';
                        } else {
                            $return .= '<input type="checkbox" name="industry_focus[]" value="' . $key->term_id . '" id="industry-' . $key->term_id . '"> <label for="industry-' . $key->term_id . '">' . $key->name . '</label><br>';
                        }
                    };
                    $return .= ' 
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>' . esc_html__('What certification does your company has?', 'skilled') . '</label>
                            <input type="text" name="company_certification" class="form-control" value="' . get_post_meta($company_id, '_company_certification', true) . '"/>
                        </div>
                        <div class="form-group">
                            <label>' . esc_html__('Do you use any specific software which your clients should know about?', 'skilled') . '</label>
                            <input type="text" name="company_software" class="form-control" value="' . get_post_meta($company_id, '_company_software', true) . '"/>
                        </div>
                        <div class="form-group">
                            <label>' . esc_html__('Did your company get any awards you would like to mention?', 'skilled') . '?</label>
                            <input type="text" name="company_awards" class="form-control" value="' . get_post_meta($company_id, '_company_awards', true) . '"/>
                        </div>
                            ';


                    $return .= '<button class="btn btn-flat btn-primary" type="submit">' . esc_html__('Save', 'skilled') . '</button>
                                            </form>
                                        </div>
                                    </div>';
                    $return .= '</div>';
                    $return .= '</div>';
                    $return .= '</div>';
                    $return .= '</div>';

                    $return .= "<script>
                    $('#register').submit(function(event) {
                        $('#register button').addClass('disable');
                        var max_words = tinymce.get('company-description').getContent();
                        //console.log('match: '+max_words.match(/href=\"([^\'\\\"]+)/g));
                        
                        if($('#company_name').val() == '' || $('#company_email').val() == ''){
                            $('#notice').html(\"<div class='alert alert-warning'><a href='#'' class='close' data-dismiss='alert'>&times;</a><strong>Warning!</strong> Please complete all fields.</div>\");
                            jQuery('html, body').animate({
                                        scrollTop: jQuery('#notice').offset().top - 100
                                    }, 200);
                            $('#register button').removeClass('disable');

                            if($('#company_email').val() == ''){
                                $('#company_email').css('border','solid 2px #d9534f');
                            }

                            if($('#company_name').val() == ''){
                                $('#company_name').css('border','solid 2px #d9534f');
                            }
                        }else if(max_words.split(' ').length > 200){
                            $('#notice').html(\"<div class='alert alert-warning'><a href='#'' class='close' data-dismiss='alert'>&times;</a><strong>Warning!</strong> You have exceeded the maximum word for company description</div>\");
                            jQuery('html, body').animate({
                                        scrollTop: jQuery('#notice').offset().top - 100
                                    }, 200);
                            $('#register button').removeClass('disable');
                        }else if(max_words.match(/href=\"([^\'\\\"]+)/g) != null){
                            $('#notice').html(\"<div class='alert alert-warning'><a href='#'' class='close' data-dismiss='alert'>&times;</a><strong>Warning!</strong> You are not allowed to use a hyperlink in the company description</div>\");
                            jQuery('html, body').animate({
                                        scrollTop: jQuery('#notice').offset().top - 100
                                    }, 200);
                            $('#register button').removeClass('disable');
                        }else{
                            jQuery.ajax({
                                method: 'POST',
                                url: '" . get_bloginfo("wpurl") . "/wp-admin/admin-ajax.php',
                                data: {
                                    formData: $('#register').serialize(),
                                    summary: tinymce.get('company-description').getContent(),
                                    action: 'update_company',
                                },
                                success:function(data) {
                                    console.log(data)
                                    if (data == 'Email already used') {
                                        $('#notice').html(\"<div class='alert alert-warning'><a href='#'' class='close' data-dismiss='alert'>&times;</a><strong>Warning!</strong> Your email is already in use.</div>\");
                                    }else{
                                        $('#notice').html(\"<div class='alert alert-success'><a href='#'' class='close' data-dismiss='alert'>&times;</a>" . __("<strong>Congratulation!</strong> Your company has been successfully added", "skilled") . "</div>\");
                                        $('input[name=\'company_email\']').css('border','1px solid rgba(0,0,0,.15)');
                                        $('input[name=\'company_name\']').css('border','1px solid rgba(0,0,0,.15)');
                                        $('input[name=\'company_email\']').css('border-radius','0px');
                                        $('input[name=\'company_name\']').css('border-radius','0px');
                                    }
                                    jQuery('html, body').animate({
                                        scrollTop: jQuery('#notice').offset().top - 100
                                    }, 200);
                                    $('#register button').removeClass('disable');
                                },
                                error: function(errorThrown){
                                    console.log(errorThrown);
                                }
                            });
                        }
                        return false;
                    });
                    </script>";
                    //Save Button
                }
            }
            $return .= '</div>';
            $return .= '</div>';

            $homeurl = get_home_url();
            switch ($homeurl) {
                case "https://skilled.co":
                    $member_url = "member";
                    break;
                case "https://skilled.co/fr":
                    $member_url = "membre";
                    break;
                case "https://skilled.co/nl":
                    $member_url = "member";
                    break;
                case "https://skilled.co/ru":
                    $member_url = "member";
                    break;
                default:
                    $member_url = "member";
                    break;
            }

            $return .= "<script>function fa_save_post(data,baru) {
                            var service = data;
                            var the_content = tinymce.get('front-editor').getContent();
                            console.log(the_content.match(/href=\"([^\"]*\")/g));
                            
                        };</script>";
        } else {
            $return .= '<script>window.location = "' . admin_url() . '"</script>';
        }
    else:
        $return .= '<style>
            #loginform{
                margin-top: 1.5rem !important;
            }
            </style>';
        $return .= '<div class="col-md-12">
                    <div class="settings-box login">
                            <div class="row">
                                <div class="col-md-4"></div>
                                <div class="col-md-4">
                                    <div class="settings-box-header login">
                                        <h2 style="font-weight: 500; color: #666666; margin: 2rem auto 0px auto; text-align: center; text-align: left;">
                                Login to Skilled!
                            </h2>
                            </div>
                                </div>
                                <div class="col-md-4"></div>
                                <div class="col-md-12">
                            <div class="settings-box-body login" style="margin-top: 0px;">';
        if (isset($_GET['login']) && $_GET['login'] == 'failed') :
            $return .= '<div class="alert alert-danger" style="margin:10px auto;width:33%;"><strong><i class="fa fa-warning"></i> Invalid username or password.</strong></div>';
        endif;
        $args = array(
            'echo' => true,
            'redirect' => $url,
            'form_id' => 'loginform',
            'label_username' => esc_html__('Username', "skilled"),
            'label_password' => esc_html__('Password', "skilled"),
            'label_remember' => esc_html__('Remember Me', "skilled"),
            'label_log_in' => esc_html__('Log In', "skilled"),
            'id_username' => 'user_login',
            'id_password' => 'user_pass',
            'id_remember' => 'rememberme',
            'id_submit' => 'wp-submit',
            'remember' => true,
            'value_username' => NULL,
            'value_remember' => true
        );
        ob_start();
        wp_login_form($args);
        $return .= ob_get_clean();
        include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

        // check for plugin using plugin name
        if (is_plugin_active('rdp-linkedin-login/index.php')) {
            $return .= do_shortcode('[rdp-linkedin-login]');
        }

        $register_url = "register";
        $register_caption = "Register";
        switch (DB_NAME) {
            case "skilled_fr":
                $register_url = "registrer";
                $register_caption = "Registrer";
                break;
            case "skilled_nl":
                //$org_slug = "bronnen";
                break;
        }

        $return .= '<span style="width: 300px;margin: -20px auto 20px;display: inherit;"><a href="' . home_url() . '/' . $register_url . '/">' . $register_caption . '</a>';
        $return .= '<a href="' . get_permalink(get_page_by_path('forgot-password')) . '" style="float:right;">Forgot Password?</a></span>';
        $return .= '</div></div></div></div>';
    endif;
    return $return;
}

add_shortcode('falogin', 'falogin_func');

//update function
function update_company() {
    $the_post_id = get_user_meta(get_current_user_id(), 'company_id', true);
    // The $_REQUEST contains all the data sent via ajax
    if (isset($_REQUEST)) {
        parse_str($_POST['formData'], $data);

        //if (username_exists($data ['company_email'])) {
        //    echo "Email already used";
        //} else {
        update_post_meta($the_post_id, '_company_post_title', $data['company_name']);

        $employee = get_term_by('id', absint($data['company_employee']), 'company_size_term');
        update_post_meta($the_post_id, '_company_employee', $employee->slug);

        $size = get_term_by('id', absint($data['company_size']), 'project_size_term');
        update_post_meta($the_post_id, '_company_size', $size->slug);

        $rate = get_term_by('id', absint($data['company_rate']), 'company_rate_term');
        update_post_meta($the_post_id, '_company_rate', $rate->slug);

        update_post_meta($the_post_id, '_company_url', $data['company_url']);
        update_post_meta($the_post_id, '_company_phone', $data['company_phone']);
        update_post_meta($the_post_id, '_company_email', $data['company_email']);
        update_post_meta($the_post_id, '_company_summary', $_POST['summary']);
        update_post_meta($the_post_id, '_company_logo_id', $data['attch']);
        update_post_meta($the_post_id, '_company_logo', wp_get_attachment_url($data['attch']));

        update_post_meta($the_post_id, '_company_guide', $data['company_guide']);
        update_post_meta($the_post_id, '_company_type', $data['company_type']);

        wp_set_post_terms($the_post_id, $data['client_focus'], 'client_focus_term');
        wp_set_post_terms($the_post_id, $data['industry_focus'], 'industry_term');

        print_r($data['client_focus']);

        if ($data['company_type'] == 2) {
            $to = get_bloginfo('admin_email');
            $name = $data['company_name'];
            $subject = 'Request Sponsor';
            $body = '<!DOCTYPE html>
                            <html>
                              <head>
                                <meta name="viewport" content="width=device-width">
                                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                                <title>Skilled Account Information</title>
                                <style type="text/css">
                                /* -------------------------------------
                                    INLINED WITH https://putsmail.com/inliner
                                ------------------------------------- */
                                /* -------------------------------------
                                    RESPONSIVE AND MOBILE FRIENDLY STYLES
                                ------------------------------------- */
                                @media only screen and (max-width: 620px) {
                                  table[class=body] h1 {
                                    font-size: 28px !important;
                                    margin-bottom: 10px !important; }
                                  table[class=body] p,
                                  table[class=body] ul,
                                  table[class=body] ol,
                                  table[class=body] td,
                                  table[class=body] span,
                                  table[class=body] a {
                                    font-size: 16px !important; }
                                  table[class=body] .wrapper,
                                  table[class=body] .article {
                                    padding: 10px !important; }
                                  table[class=body] .content {
                                    padding: 0 !important; }
                                  table[class=body] .container {
                                    padding: 0 !important;
                                    width: 100% !important; }
                                  table[class=body] .main {
                                    border-left-width: 0 !important;
                                    border-radius: 0 !important;
                                    border-right-width: 0 !important; }
                                  table[class=body] .btn table {
                                    width: 100% !important; }
                                  table[class=body] .btn a {
                                    width: 100% !important; }
                                  table[class=body] .img-responsive {
                                    height: auto !important;
                                    max-width: 100% !important;
                                    width: auto !important; }}
                                /* -------------------------------------
                                    PRESERVE THESE STYLES IN THE HEAD
                                ------------------------------------- */
                                @media all {
                                  .ExternalClass {
                                    width: 100%; }
                                  .ExternalClass,
                                  .ExternalClass p,
                                  .ExternalClass span,
                                  .ExternalClass font,
                                  .ExternalClass td,
                                  .ExternalClass div {
                                    line-height: 100%; }
                                  .apple-link a {
                                    color: inherit !important;
                                    font-family: inherit !important;
                                    font-size: inherit !important;
                                    font-weight: inherit !important;
                                    line-height: inherit !important;
                                    text-decoration: none !important; }
                                  .btn-primary table td:hover {
                                    background-color: #34495e !important; }
                                  .btn-primary a:hover {
                                    background-color: #34495e !important;
                                    border-color: #34495e !important; } }
                                </style>
                              </head>
                              <body class="" style="background-color:#f6f6f6;font-family:sans-serif;-webkit-font-smoothing:antialiased;font-size:14px;line-height:1.4;margin:0;padding:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;">
                                <table border="0" cellpadding="0" cellspacing="0" class="body" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;background-color:#f6f6f6;width:100%;">
                                  <tr>
                                    <td style="font-family:sans-serif;font-size:14px;vertical-align:top;">&nbsp;</td>
                                    <td class="container" style="font-family:sans-serif;font-size:14px;vertical-align:top;display:block;max-width:580px;padding:10px;width:580px;Margin:0 auto !important;">
                                      <div class="content" style="box-sizing:border-box;display:block;Margin:0 auto;max-width:580px;padding:10px;">
                                        <!-- START CENTERED WHITE CONTAINER -->
                                        <span class="preheader" style="color:transparent;display:none;height:0;max-height:0;max-width:0;opacity:0;overflow:hidden;mso-hide:all;visibility:hidden;width:0;">This is preheader text. Some clients will show this text as a preview.</span>
                                        <table class="main" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;background:#fff;border-radius:3px;width:100%;">
                                          <!-- START MAIN CONTENT AREA -->
                                          <tr>
                                            <td class="wrapper" style="font-family:sans-serif;font-size:14px;vertical-align:top;box-sizing:border-box;padding:20px;">
                                              <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;width:100%;">
                                                <tr>
                                                  <td style="font-family:sans-serif;font-size:14px;vertical-align:top;">
                                                    <p style="font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;">Hi Skilled Admin,</p>
                                                    <p style="font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;">' . $name . ' has been request to be sponsor</p>
                                                    <p style="font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;">Good luck!</p>
                                                  </td>
                                                </tr>
                                              </table>
                                            </td>
                                          </tr>
                                          <!-- END MAIN CONTENT AREA -->
                                        </table>
                                        <!-- START FOOTER -->
                                        <div class="footer" style="clear:both;padding-top:10px;text-align:center;width:100%;">
                                          <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;width:100%;">
                                            <tr>
                                              <td class="content-block" style="font-family:sans-serif;font-size:14px;vertical-align:top;color:#999999;font-size:12px;text-align:center;">
                                                
                                              </td>
                                            </tr>
                                            <tr>
                                              <td class="content-block powered-by" style="font-family:sans-serif;font-size:14px;vertical-align:top;color:#999999;font-size:12px;text-align:center;">
                                                
                                              </td>
                                            </tr>
                                          </table>
                                        </div>
                                        <!-- END FOOTER -->
                                        <!-- END CENTERED WHITE CONTAINER -->
                                      </div>
                                    </td>
                                    <td style="font-family:sans-serif;font-size:14px;vertical-align:top;">&nbsp;</td>
                                  </tr>
                                </table>
                              </body>
                            </html>';

            $headers = array('Content-Type: text/html; charset=UTF-8', 'From: Skilled <no-reply@skilled.co>');

            wp_mail($to, $subject, $body, $headers);
        }
        $thisPostMeta = get_post_meta($the_post_id);
        foreach ($thisPostMeta as $key => $val) {
            if (strpos($key, "company_guide_")) {
                update_post_meta($the_post_id, $key, 0);
            }
        }
        for ($i = 0; $i < count($data['company_guide']); $i++) {
            update_post_meta($the_post_id, "_company_guide_" . $data['company_guide'][$i], 1);
        }

        $address = [];
        for ($i = 0; $i < count($data['address_line_1']); $i++) {
            $address[$i][_company_location] = [
                "address-1" => $data['address_line_1'][$i],
                "address-2" => $data['address_line_2'][$i],
                "phone" => $data['address_phone'][$i],
                "zip" => $data['address_zip'][$i],
                "new_country" => $data['address_country'][$i],
                "new_state" => $data['address_state'][$i],
                "city" => $data['address_city'][$i],
            ];
        }
        update_post_meta($the_post_id, '_company_location', $address);
        wp_set_post_terms($the_post_id, $data['client_focus'], 'client_focus_term');

        wp_set_post_terms($the_post_id, $data['industry_focus'], 'industry_term');

        update_post_meta($the_post_id, '_company_certification', $data['company_certification']);
        update_post_meta($the_post_id, '_company_software', $data['company_software']);
        update_post_meta($the_post_id, '_company_awards', $data['company_awards']);

        wp_set_post_terms($the_post_id, array($data['company_employee']), 'company_size_term');
        wp_set_post_terms($the_post_id, array($data['company_rate']), 'company_rate_term');
        wp_set_post_terms($the_post_id, array($data['company_size']), 'project_size_term');
        //}
        die();
    }

    // Always die in functions echoing ajax content
    die();
}

add_action('wp_ajax_update_company', 'update_company');
add_action('wp_ajax_nopriv_update_company', 'update_company');

function submit_ajax_request() {
    $company_id = get_user_meta(get_current_user_id(), 'company_id', true);
    // The $_REQUEST contains all the data sent via ajax
    if (isset($_REQUEST)) {

        // Let's take the data that was sent and do something with it
        // Create post object
        // Insert the post into the database
        if ($_REQUEST['act'] == 'edit-post') {
            //var_dump($company_id);
            if ($_REQUEST['data'] == 'company_portfolio') {
                $data = get_post_meta($company_id, "_company_portfolio", true);
                $data[$_REQUEST['id']] = array(
                    '_company_portfolio_title' => wp_strip_all_tags($_REQUEST['title']),
                    '_company_portfolio_description' => $_REQUEST['content'],
                    '_company_portfolio_client' => $_REQUEST['client'],
                    '_company_portfolio_url' => $_REQUEST['url'],
                    '_company_portfolio_id' => $_REQUEST['image'],
                    '_company_portfolio_img' => wp_get_attachment_url($_REQUEST['image']),
                );
                update_post_meta($company_id, '_company_portfolio', $data);
            } else {
                $data = get_post_meta($company_id, "_company_service", true);
                $data[$_REQUEST['id']] = array(
                    '_company_service_title' => wp_strip_all_tags($_REQUEST['title']),
                    '_company_service_description' => $_REQUEST['content']
                );
                update_post_meta($company_id, '_company_service', $data);
            }
        } else if ($_REQUEST['act'] == 'add-new') {
            if ($_REQUEST['data'] == 'company_portfolio') {
                if (get_post_meta($company_id, "_company_portfolio", true)) {
                    $data = get_post_meta($company_id, "_company_portfolio", true);
                } else {
                    $data = array();
                }
                $input[] = array(
                    '_company_portfolio_title' => wp_strip_all_tags($_REQUEST['title']),
                    '_company_portfolio_description' => $_REQUEST['content'],
                    '_company_portfolio_client' => $_REQUEST['client'],
                    '_company_portfolio_url' => $_REQUEST['url'],
                    '_company_portfolio_id' => $_REQUEST['image'],
                    '_company_portfolio_img' => wp_get_attachment_url($_REQUEST['image']),
                );
                $new_data = array_merge($data, $input);
                update_post_meta($company_id, '_company_portfolio', $new_data);
            } else if ($_REQUEST['data'] == 'company_service') {
                if (get_post_meta($company_id, "_company_service", true)) {
                    $data = get_post_meta($company_id, "_company_service", true);
                } else {
                    $data = array();
                }
                $input[] = array(
                    '_company_service_title' => wp_strip_all_tags($_REQUEST['title']),
                    '_company_service_description' => $_REQUEST['content']
                );
                $new_data = array_merge($data, $input);
                update_post_meta($company_id, '_company_service', $new_data);
            }
        } else {
            update_post_meta($company_id, '_company_summary', $_REQUEST['content']);
        }

        // Now we'll return it to the javascript function
        // Anything outputted will be returned in the response
        // If you're debugging, it might be useful to see what was sent in the $_REQUEST
        // print_r($_REQUEST);
    }

    // Always die in functions echoing ajax content
    die();
}

add_action('wp_ajax_submit_ajax_request', 'submit_ajax_request');
add_action('wp_ajax_nopriv_submit_ajax_request', 'submit_ajax_request');

function get_state() {
    $country = $_POST['country'];

    $state = [];
    query_posts([
        'post_type' => 'skilled_state',
        'posts_per_page' => -1,
        'tax_query' => [
            [
                'taxonomy' => 'company_country',
                'field' => 'slug',
                'terms' => $country
            ]
        ]
    ]);

    while (have_posts()) {
        the_post();
        $state[] = [
            "id" => get_the_ID(),
            "caption" => get_the_title()];
    }
    wp_reset_query();

    echo json_encode($state);

    wp_die();
}

add_action('wp_ajax_get_state', 'get_state');
add_action('wp_ajax_nopriv_get_state', 'get_state');

function get_city() {
    $state = $_POST['state'];

    $city = [];
    query_posts([
        'post_type' => 'company_country_city',
        'posts_per_page' => -1,
        'orderby' => 'title',
        'order' => 'ASC',
        'meta_key' => 'city-select-state',
        'meta_value' => $state,
    ]);

    while (have_posts()) {
        the_post();
        $city[] = [
            "id" => get_the_ID(),
            "caption" => get_the_title()];
    }
    wp_reset_query();

    echo json_encode($city);

    wp_die();
}

add_action('wp_ajax_get_city', 'get_city');
add_action('wp_ajax_nopriv_get_city', 'get_city');

function faregister_func() {
    $company_size_term = get_terms(array('taxonomy' => 'company_size_term', 'hide_empty' => false, 'orderby' => 'id', 'order' => 'asc'));
    $company_rate_term = get_terms(array('taxonomy' => 'company_rate_term', 'hide_empty' => false, 'orderby' => 'id', 'order' => 'asc'));
    $project_size_term = get_terms(array('taxonomy' => 'project_size_term', 'hide_empty' => false, 'orderby' => 'id', 'order' => 'asc'));
    $return = '<div class="row">
                <div class="col-md-12">
                    <div class="settings-box">
                        <div class="settings-box-body">
                            <div id="notice"></div>
                            <form id="register" name="register" method="post">
                            <div class="form-group">
                                <label>' . esc_html__('Company Name', 'skilled') . '</label>
                                <input type="text" id="company_name" name="company_name" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <label>' . esc_html__('Company Size', 'skilled') . '</label>
                                <select class="form-control" name="company_employee">';

    foreach ($company_size_term as $key) {
        $return .= '<option value="' . $key->term_id . '">' . $key->name . '</option>';
    };

    $return .= '</select>
                            </div>
                            <div class="form-group">
                                <label>' . esc_html__('Min. Project Budget', 'skilled') . '</label>
                                <select class="form-control" name="company_size">';

    foreach ($project_size_term as $key) {
        $return .= '<option value="' . $key->term_id . '">' . $key->name . ' </option>';
    };
    $return .= '</select>
                            </div>
                            <div class="form-group">
                                <label>' . esc_html__('Average Hourly Rates', 'skilled') . '</label>
                                <select class="form-control" name="company_rate">';
    foreach ($company_rate_term as $key) {
        $return .= '<option value="' . $key->term_id . '">' . $key->name . ' </option>';
    };
    $return .= '</select>
                            </div>
                            <div class="form-group">
                                <label>' . esc_html__('Website URL', 'skilled') . '</label>
                                <input type="url" name="company_url" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <label>' . esc_html__('Phone', 'skilled') . '</label>
                                <input type="text" name="company_phone" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <label>' . esc_html__('Email', 'skilled') . '</label>
                                <input type="email" id="company_email" name="company_email" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <label>' . esc_html__('Company Description', 'skilled') . ' (' . esc_html__('Max length: 200 words', 'skilled') . ')</label>';
    ob_start();
    $content = $content_post[$_GET['id']]['_company_service_description'];
    $content = apply_filters('the_content', $content);
    $content = str_replace(']]>', ']]&gt;', $content);
    wp_editor($content, "company-description", array('media_buttons' => false, 'wpautop' => false));
    $return .= ob_get_clean();

    $return .= '<div id="company_description_character_count" style="padding: 4px;"></div>';

    $return .= '</div>';
    $return .= "<style>.faupload:before {content: '" . esc_html__('Add Logo', 'skilled') . " (" . esc_html__('Only square image', 'skilled') . ")'; left: 0%;margin-left:0;text-align:center;width:100%; font-family: Montserrat; text-transform: none;}</style>";
    $return .= '<div class="faupload" onclick="open_media_uploader_video(\'logo\')"><img id="frontend-image"></div>';
    $return .= '<input type="hidden" name="attch" id="frontend-id">';

    $company_guide = get_posts([
        "post_type" => "organization",
        "posts_per_page" => -1,
        "orderby" => "ID",
        "order" => "ASC"
    ]);

    $return .= '
                            <div class="form-group">
                                <label>' . esc_html__('What category would you like to be featured in?', 'skilled') . '</label>
                                    ';
    foreach ($company_guide as $cg) {
        $label = $cg->post_title;
        if ($cg->post_parent != 0) {
            $parent = get_post($cg->post_parent);
            $label = $parent->post_title . " - " . $label;
        }
        $return .= "<br><input type='checkbox' name='company_guide[]' value='" . $cg->ID . "' id='label-company-guide-" . $cg->ID . "'> <label for='label-company-guide-" . $cg->ID . "'>" . $cg->post_title . "</label>";
    }
    $return .= '
                            </div>
            ';

    $return .= '
                                <style>
                                    .address-group{
                                        border: 1px solid #dfdfdf;
                                        background: 1px solid #fafafa;
                                        padding: 1rem;
                                        margin-bottom: 0.5rem;
                                        margin-right: 1rem;
                                        width: 47%;
                                        display: inline-block;
                                    }
                                    .address-group input,.address-group select{
                                        display: block;
                                        margin-bottom: 0.5rem;
                                        border-radius: 0px;
                                    }
                                </style>
                            <div class="form-group">
                                <label>' . esc_html__('Which of the following priority would you prefer to be?', 'skilled') . '</label>
                                    <select name="company_type" id="account_type" class="form-control">
                                        <option value="2" selected>' . esc_html__('Sponsored: Ranked at the very top of results as priority partner (paid membership)', 'skilled') . '</option>
                                        <option value="1" selected>' . esc_html__('Badged: Secondary site priority for adding skilled badge with link to public profile', 'skilled') . '</option>
                                        <option value="3" selected>' . esc_html__('Organic: Organic Ranking based on quality and number of user reviews (free)', 'skilled') . '</option>
                                    </select>
                            </div>

                                <!-- COMPANY ADDRESS FIELDS BEGIN HERE! -->

                            <div class="form-group">
                                <label>' . esc_html__('Addresses', 'skilled') . '</label>
                                    <br>
                                    <div class="address-group" style="width: 100%;">
                                        <input type="text" placeholder="' . esc_html__('Address Line 1', 'skilled') . '" name="address_line_1[]" class="form-control">
                                        <input type="text" placeholder="' . esc_html__('Address Line 2', 'skilled') . '" name="address_line_2[]" class="form-control">
                                        <input type="text" placeholder="' . esc_html__('Phone', 'skilled') . '" name="address_phone[]" class="form-control">
                                        <input type="text" placeholder="' . esc_html__('ZIP', 'skilled') . '" name="address_zip[]" class="form-control">
                                        <select name="address_country[]" id="country-1" class="form-control" onchange="selectState(this, 1)">
                                        <option value="">- ' . esc_html__('Select Country', 'skilled') . ' -</option>
                                        ';
    $list = get_terms('company_country', [
        'hide_empty' => false,
        'orderby' => 'name',
        'order' => 'asc'
    ]);

    $country_options = '';
    foreach ($list as $li) {
        $country_options .= '<option value="' . $li->name . '">' . $li->name . '</option>';
    }
    $return .= $country_options;

    $return .= '
                                        </select>
                                        <input type="text" placeholder="' . esc_html__('State', 'skilled') . '" name="address_state[]" id="state-1" class="form-control">
                                        <input type="text" placeholder="' . esc_html__('City', 'skilled') . '" name="address_city[]" id="city-1" class="form-control">';
    $return .= '
                                        <div style="text-align: right">
                                            <button type="button" class="btn btn-primary-orig" id="add-address"><i class="fa fa-plus"></i></button>
                                        </div>
                                    </div>
                                    <div id="address-additional"></div>
                            </div>
                                <script>
                                function removeAddress(the_element){
                                    the_element.parentNode.parentNode.parentNode.removeChild(the_element.parentNode.parentNode);
                                }
                                $(document).ready(function(){
                                    var the_count = 2;

                                    $(\'#add-address\').click(function(){
                                        var txt = \'\';

                                        txt += \'<div class="address-group" id="address-\'+the_count+\'">\';
                                        txt += \'<input type="text" placeholder="' . esc_html__('Address Line 1', 'skilled') . '" name="address_line_1[]" class="form-control">\';
                                        txt += \'<input type="text" placeholder="' . esc_html__('Address Line 2', 'skilled') . '" name="address_line_2[]" class="form-control">\';
                                        txt += \'<input type="text" placeholder="' . esc_html__('Phone', 'skilled') . '" name="address_phone[]" class="form-control">\';
                                        txt += \'<input type="text" placeholder="' . esc_html__('ZIP', 'skilled') . '" name="address_zip[]" class="form-control">\';
                                        txt += \'<select name="address_country[]" id="country-\'+the_count+\'" class="form-control" onchange="selectState(this, \'+the_count+\')">\';
                                        txt += \'<option value="">- ' . esc_html__('Select Country', 'skilled') . ' -</option>\';
                                        txt += \'' . $country_options . '\';
                                        txt += \'</select>\';
                                        txt += \'<input type="text" placeholder="' . esc_html__('State', 'skilled') . '" name="address_state[]" id="state-\'+the_count+\'" class="form-control">\';
                                        txt += \'<input type="text" placeholder="' . esc_html__('City', 'skilled') . '" name="address_city[]" id="city-\'+the_count+\'" class="form-control">\';
                                        txt += \'<div style="text-align: right">\';
                                        txt += \'    <button type="button" class="btn btn-danger" onclick="removeAddress(this)"><i class="fa fa-trash"></i></button>\';
                                        txt += \'</div>\';
                                        txt += \'</div>\';


                                        $(\'#address-additional\').append(txt);
                                        the_count++;
                                    });
                                });
                                </script>

                                <!-- ======================================================================== -->
                                <!-- ======================================================================== -->
                                <!--                 COMPANY ADDRESS FIELDS END HERE!                         -->
                                <!-- ======================================================================== -->
                                <!-- ======================================================================== -->

            ';

    $client_focus = get_terms(array('taxonomy' => 'client_focus_term', 'hide_empty' => false, 'orderby' => "term_id", 'order' => 'ASC'));
    //BEGIN CLIENT FOCUS META BOX
    $return .= ' 
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                <label>' . esc_html__('What type of companies do you work with?', 'skilled') . '</label><br>
                        ';
    foreach ($client_focus as $key) {
        $return .= '<input type="checkbox" name="client_focus[]" value="' . $key->term_id . '" id="client-' . $key->term_id . '"> <label for="client-' . $key->term_id . '">' . $key->name . '</label><br>';
    };
    $return .= ' 
                </div>
            </div>';
    $industry_focus = get_terms(array('taxonomy' => 'industry_term', 'hide_empty' => false,));
    //BEGIN CLIENT FOCUS META BOX
    $return .= ' 
            <div class="col-md-6">
                <div class="form-group">
                <label>' . esc_html__('What industries are your clients usually from?', 'skilled') . '</label><br>';
    foreach ($industry_focus as $key) {
        $return .= '<input type="checkbox" name="industry_focus[]" value="' . $key->term_id . '" id="industry-' . $key->term_id . '"> <label for="industry-' . $key->term_id . '">' . $key->name . '</label><br>';
    };
    $return .= ' 
                </div>
            </div>
        </div>
        <div class="form-group">
            <label>' . esc_html__('What certification does your company has?', 'skilled') . '</label>
            <input type="text" name="company_certification" class="form-control"/>
        </div>
        <div class="form-group">
            <label>' . esc_html__('Do you use any specific software which your clients should know about?', 'skilled') . '</label>
            <input type="text" name="company_software" class="form-control"/>
        </div>
        <div class="form-group">
            <label>' . esc_html__('Did your company get any awards you would like to mention?', 'skilled') . '</label>
            <input type="text" name="company_awards" class="form-control"/>
        </div>
            ';


    $return .= '<button class="btn btn-flat btn-primary" type="submit">' . esc_html__('Save', 'skilled') . '</button>
                            </form>
                        </div>
                    </div>
                </div>
                </div>';
    $homeurl = get_home_url();
    switch ($homeurl) {
        case "https://skilled.co":
            $member_url = "member";
            break;
        case "https://skilled.co/fr":
            $member_url = "membre";
            break;
        case "https://skilled.co/nl":
            $member_url = "member";
            break;
        case "https://skilled.co/ru":
            $member_url = "member";
            break;
        default:
            $member_url = "member";
            break;
    }
    $return .= "<script>
                $('#register').submit(function(event) {
                    $('#register button').addClass('disable');
                    var max_words = tinymce.get('company-description').getContent();
                    console.log(max_words.match(/href=\"([^\'\\\"]+)/g));
        
                    if($('#company_name').val() == '' || $('#company_email').val() == ''){
                        $('#notice').html(\"<div class='alert alert-warning'><a href='#'' class='close' data-dismiss='alert'>&times;</a><strong>Warning!</strong> Please complete all fields.</div>\");
                        jQuery('html, body').animate({
                                    scrollTop: jQuery('#notice').offset().top - 100
                                }, 200);
                        $('#register button').removeClass('disable');
                        
                        if($('#company_email').val() == ''){
                            $('#company_email').css('border','solid 2px #d9534f');
                        }

                        if($('#company_name').val() == ''){
                            $('#company_name').css('border','solid 2px #d9534f');
                        }
                    }else if(max_words.split(' ').length > 200){
                        $('#notice').html(\"<div class='alert alert-warning'><a href='#'' class='close' data-dismiss='alert'>&times;</a><strong>Warning!</strong> You have exceeded the maximum word for company description</div>\");
                        jQuery('html, body').animate({
                                    scrollTop: jQuery('#notice').offset().top - 100
                                }, 200);
                        $('#register button').removeClass('disable');
                    }else if(max_words.match(/href=\"([^\'\\\"]+)/g) != null){
                        $('#notice').html(\"<div class='alert alert-warning'><a href='#'' class='close' data-dismiss='alert'>&times;</a><strong>Warning!</strong> Vous n'êtes pas autorisé à utiliser un lien hypertexte dans la description de l'entreprise</div>\");
                        jQuery('html, body').animate({
                                    scrollTop: jQuery('#notice').offset().top - 100
                                }, 200);
                        $('#register button').removeClass('disable');
                    }else{
                        jQuery.ajax({
                            method: 'POST',
                            url: '" . get_bloginfo("wpurl") . "/wp-admin/admin-ajax.php',
                            data: {
                                formData: $('#register').serialize(),
                                summary: tinymce.get('company-description').getContent(),
                                action: 'register_request'
                            },
                            success:function(data) {
                                console.log(data)
                                if (data == 'Email already used') {
                                    $('#notice').html(\"<div class='alert alert-warning'><a href='#'' class='close' data-dismiss='alert'>&times;</a>" . __("<strong>Warning!</strong> Your email is already in use", "skilled") . ".</div>\");
                                }else{
                                    sessionStorage.setItem('msg',\"<div class='alert alert-success'><a href='#'' class='close' data-dismiss='alert'>&times;</a>" . __("<strong>Congratulation!</strong> Your company has been successfully added", "skilled") . "</div>\");
                                    window.location = '" . get_bloginfo("wpurl") . "/" . $member_url . "/';
                                }
                                jQuery('html, body').animate({
                                    scrollTop: jQuery('#notice').offset().top - 100
                                }, 200);
                                $('#register button').removeClass('disable');
                            },
                            error: function(errorThrown){
                                console.log(errorThrown);
                            }
                        }); 
                    }
                return false;
                });
        </script>";
    return $return;
}

add_shortcode('faregister', 'faregister_func')

;

//Register function
function register_request() {

    // The $_REQUEST contains all the data sent via ajax
    if (isset($_REQUEST)) {
        parse_str($_POST['formData'], $data);

        //if (username_exists($data ['company_email'])) {
        //    echo "Email already used";
        //} else {
        $my_post = array(
            'post_title' => $data['company_name'],
            'post_status' => 'draft',
            'post_type' => 'company',
        );

        print_r($_POST['formData']);

        $the_post_id = wp_insert_post($my_post);

        update_post_meta($the_post_id, '_company_post_title', $data['company_name']);

        $employee = get_term_by('id', absint($data['company_employee']), 'company_size_term');
        update_post_meta($the_post_id, '_company_employee', $employee->slug);

        $size = get_term_by('id', absint($data['company_size']), 'project_size_term');
        update_post_meta($the_post_id, '_company_size', $size->slug);

        $rate = get_term_by('id', absint($data['company_rate']), 'company_rate_term');
        update_post_meta($the_post_id, '_company_rate', $rate->slug);

        update_post_meta($the_post_id, '_company_url', $data['company_url']);
        update_post_meta($the_post_id, '_company_phone', $data['company_phone']);
        update_post_meta($the_post_id, '_company_email', $data['company_email']);
        update_post_meta($the_post_id, '_company_summary', $_POST['summary']);
        update_post_meta($the_post_id, '_company_logo_id', $data['attch']);
        update_post_meta($the_post_id, '_company_logo', wp_get_attachment_url($data['attch']));

        update_post_meta($the_post_id, '_company_guide', $data['company_guide']);
        update_post_meta($the_post_id, '_company_type', $data['company_type']);

        for ($i = 0; $i < count($data['company_guide']); $i++) {
            update_post_meta($the_post_id, "_company_guide_" . $data['company_guide'][$i], 1);
        }
        $address = [];
        for ($i = 0; $i < count($data['address_line_1']); $i++) {
            $address[$i][_company_location] = [
                "address-1" => $data['address_line_1'][$i],
                "address-2" => $data['address_line_2'][$i],
                "phone" => $data['address_phone'][$i],
                "zip" => $data['address_zip'][$i],
                "new_country" => $data['address_country'][$i],
                "new_state" => $data['address_state'][$i],
                "city" => $data['address_city'][$i],
            ];
        }
        update_post_meta($the_post_id, '_company_location', $address);

        wp_set_post_terms($the_post_id, $data['client_focus'], 'client_focus_term');

        wp_set_post_terms($the_post_id, $data['industry_focus'], 'industry_term');

        update_post_meta($the_post_id, '_company_certification', $data['company_certification']);
        update_post_meta($the_post_id, '_company_software', $data['company_software']);
        update_post_meta($the_post_id, '_company_awards', $data['company_awards']);

        wp_set_post_terms($the_post_id, array($data['company_employee']), 'company_size_term');
        wp_set_post_terms($the_post_id, array($data['company_rate']), 'company_rate_term');
        wp_set_post_terms($the_post_id, array($data['company_size']), 'project_size_term');

        // If you're debugging, it might be useful to see what was sent in the $_REQUEST
        // print_r($_REQUEST);
        if (is_user_logged_in()) {
            $user_id = get_current_user_id();
            update_user_meta($user_id, 'company_id', $the_post_id);
        } else {
            $random_password = wp_generate_password($length = 12, $include_standard_special_chars = false);
            $userdata = array(
                'user_login' => $data['company_email'],
                'user_email' => $data['company_email'],
                'user_pass' => $random_password,
                'role' => 'company'
            );

            $user_id = wp_insert_user($userdata);

            update_user_meta($user_id, 'company_id', $the_post_id);

            $to = $data['company_email'];
            $name = $data['company_name'];
            $subject = 'Skilled Account Information';
            $body = '<!DOCTYPE html>
                            <html>
                              <head>
                                <meta name="viewport" content="width=device-width">
                                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                                <title>Skilled Account Information</title>
                                <style type="text/css">
                                /* -------------------------------------
                                    INLINED WITH https://putsmail.com/inliner
                                ------------------------------------- */
                                /* -------------------------------------
                                    RESPONSIVE AND MOBILE FRIENDLY STYLES
                                ------------------------------------- */
                                @media only screen and (max-width: 620px) {
                                  table[class=body] h1 {
                                    font-size: 28px !important;
                                    margin-bottom: 10px !important; }
                                  table[class=body] p,
                                  table[class=body] ul,
                                  table[class=body] ol,
                                  table[class=body] td,
                                  table[class=body] span,
                                  table[class=body] a {
                                    font-size: 16px !important; }
                                  table[class=body] .wrapper,
                                  table[class=body] .article {
                                    padding: 10px !important; }
                                  table[class=body] .content {
                                    padding: 0 !important; }
                                  table[class=body] .container {
                                    padding: 0 !important;
                                    width: 100% !important; }
                                  table[class=body] .main {
                                    border-left-width: 0 !important;
                                    border-radius: 0 !important;
                                    border-right-width: 0 !important; }
                                  table[class=body] .btn table {
                                    width: 100% !important; }
                                  table[class=body] .btn a {
                                    width: 100% !important; }
                                  table[class=body] .img-responsive {
                                    height: auto !important;
                                    max-width: 100% !important;
                                    width: auto !important; }}
                                /* -------------------------------------
                                    PRESERVE THESE STYLES IN THE HEAD
                                ------------------------------------- */
                                @media all {
                                  .ExternalClass {
                                    width: 100%; }
                                  .ExternalClass,
                                  .ExternalClass p,
                                  .ExternalClass span,
                                  .ExternalClass font,
                                  .ExternalClass td,
                                  .ExternalClass div {
                                    line-height: 100%; }
                                  .apple-link a {
                                    color: inherit !important;
                                    font-family: inherit !important;
                                    font-size: inherit !important;
                                    font-weight: inherit !important;
                                    line-height: inherit !important;
                                    text-decoration: none !important; }
                                  .btn-primary table td:hover {
                                    background-color: #34495e !important; }
                                  .btn-primary a:hover {
                                    background-color: #34495e !important;
                                    border-color: #34495e !important; } }
                                </style>
                              </head>
                              <body class="" style="background-color:#f6f6f6;font-family:sans-serif;-webkit-font-smoothing:antialiased;font-size:14px;line-height:1.4;margin:0;padding:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;">
                                <table border="0" cellpadding="0" cellspacing="0" class="body" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;background-color:#f6f6f6;width:100%;">
                                  <tr>
                                    <td style="font-family:sans-serif;font-size:14px;vertical-align:top;">&nbsp;</td>
                                    <td class="container" style="font-family:sans-serif;font-size:14px;vertical-align:top;display:block;max-width:580px;padding:10px;width:580px;Margin:0 auto !important;">
                                      <div class="content" style="box-sizing:border-box;display:block;Margin:0 auto;max-width:580px;padding:10px;">
                                        <!-- START CENTERED WHITE CONTAINER -->
                                        <span class="preheader" style="color:transparent;display:none;height:0;max-height:0;max-width:0;opacity:0;overflow:hidden;mso-hide:all;visibility:hidden;width:0;">This is preheader text. Some clients will show this text as a preview.</span>
                                        <table class="main" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;background:#fff;border-radius:3px;width:100%;">
                                          <!-- START MAIN CONTENT AREA -->
                                          <tr>
                                            <td class="wrapper" style="font-family:sans-serif;font-size:14px;vertical-align:top;box-sizing:border-box;padding:20px;">
                                              <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;width:100%;">
                                                <tr>
                                                  <td style="font-family:sans-serif;font-size:14px;vertical-align:top;">
                                                  ';
            $body .= '
                                                    <p style="font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;">Hi ' . $name . ',</p>
                                                    <p style="font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;">Welcome to Skilled. Your acccount has been setup with this login information</p>
                                                    <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;box-sizing:border-box;width:100%;">
                                                      <tbody>
                                                        <tr>
                                                          <td align="left" style="font-family:sans-serif;font-size:14px;vertical-align:top;padding-bottom:15px;">
                                                            <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;width:100%;width:auto;">
                                                              <tbody>
                                                                <tr>
                                                                  <td style="font-family:sans-serif;font-size:14px"> 
                                                                  Username: ' . $to . '<br> Password: ' . $random_password . '</td>
                                                                </tr>
                                                              </tbody>
                                                            </table>
                                                          </td>
                                                        </tr>
                                                      </tbody>
                                                    </table>
                                                    <p style="font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;">Thank you for registering in Skilled.</p>';
            switch (DB_NAME) {
                case "skilled_fr":
                    $body .= '
                                                    <p style="font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;">Bonjour ' . $name . ',</p>
                                                    <p style="font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;">Bienvenue sur Skilled. Votre compte a Ã©tÃ© crÃ©Ã© avec les informations de connexion suivantes :</p>
                                                    <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;box-sizing:border-box;width:100%;">
                                                      <tbody>
                                                        <tr>
                                                          <td align="left" style="font-family:sans-serif;font-size:14px;vertical-align:top;padding-bottom:15px;">
                                                            <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;width:100%;width:auto;">
                                                              <tbody>
                                                                <tr>
                                                                  <td style="font-family:sans-serif;font-size:14px"> 
                                                                  Identifiant: ' . $to . '<br> Mot de passe: ' . $random_password . '</td>
                                                                </tr>
                                                              </tbody>
                                                            </table>
                                                          </td>
                                                        </tr>
                                                      </tbody>
                                                    </table>
                                                    <p style="font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;">Connectez vous dÃ¨s maintenant sur https://skilled.co/fr/membre/ et crÃ©ez votre fiche entreprise sur notre site !</p>';
                    break;
                //case "skilled_nl":
                //    $slug = "bronnen";
                //    break;
            }
            $body .= '
                                                  </td>
                                                </tr>
                                              </table>
                                            </td>
                                          </tr>
                                          <!-- END MAIN CONTENT AREA -->
                                        </table>
                                        <!-- START FOOTER -->
                                        <div class="footer" style="clear:both;padding-top:10px;text-align:center;width:100%;">
                                          <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;width:100%;">
                                            <tr>
                                              <td class="content-block" style="font-family:sans-serif;font-size:14px;vertical-align:top;color:#999999;font-size:12px;text-align:center;">
                                                
                                              </td>
                                            </tr>
                                            <tr>
                                              <td class="content-block powered-by" style="font-family:sans-serif;font-size:14px;vertical-align:top;color:#999999;font-size:12px;text-align:center;">
                                                
                                              </td>
                                            </tr>
                                          </table>
                                        </div>
                                        <!-- END FOOTER -->
                                        <!-- END CENTERED WHITE CONTAINER -->
                                      </div>
                                    </td>
                                    <td style="font-family:sans-serif;font-size:14px;vertical-align:top;">&nbsp;</td>
                                  </tr>
                                </table>
                              </body>
                            </html>';

            $headers = array('Content-Type: text/html; charset=UTF-8', 'From: Skilled <no-reply@skilled.co>');

            wp_mail($to, $subject, $body, $headers);
        }
        $to = get_bloginfo('admin_email');
        $name = $data['company_name'];
        $subject = 'New Company Registration';
        $body = '<!DOCTYPE html>
                        <html>
                          <head>
                            <meta name="viewport" content="width=device-width">
                            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                            <title>Skilled Account Information</title>
                            <style type="text/css">
                            /* -------------------------------------
                                INLINED WITH https://putsmail.com/inliner
                            ------------------------------------- */
                            /* -------------------------------------
                                RESPONSIVE AND MOBILE FRIENDLY STYLES
                            ------------------------------------- */
                            @media only screen and (max-width: 620px) {
                              table[class=body] h1 {
                                font-size: 28px !important;
                                margin-bottom: 10px !important; }
                              table[class=body] p,
                              table[class=body] ul,
                              table[class=body] ol,
                              table[class=body] td,
                              table[class=body] span,
                              table[class=body] a {
                                font-size: 16px !important; }
                              table[class=body] .wrapper,
                              table[class=body] .article {
                                padding: 10px !important; }
                              table[class=body] .content {
                                padding: 0 !important; }
                              table[class=body] .container {
                                padding: 0 !important;
                                width: 100% !important; }
                              table[class=body] .main {
                                border-left-width: 0 !important;
                                border-radius: 0 !important;
                                border-right-width: 0 !important; }
                              table[class=body] .btn table {
                                width: 100% !important; }
                              table[class=body] .btn a {
                                width: 100% !important; }
                              table[class=body] .img-responsive {
                                height: auto !important;
                                max-width: 100% !important;
                                width: auto !important; }}
                            /* -------------------------------------
                                PRESERVE THESE STYLES IN THE HEAD
                            ------------------------------------- */
                            @media all {
                              .ExternalClass {
                                width: 100%; }
                              .ExternalClass,
                              .ExternalClass p,
                              .ExternalClass span,
                              .ExternalClass font,
                              .ExternalClass td,
                              .ExternalClass div {
                                line-height: 100%; }
                              .apple-link a {
                                color: inherit !important;
                                font-family: inherit !important;
                                font-size: inherit !important;
                                font-weight: inherit !important;
                                line-height: inherit !important;
                                text-decoration: none !important; }
                              .btn-primary table td:hover {
                                background-color: #34495e !important; }
                              .btn-primary a:hover {
                                background-color: #34495e !important;
                                border-color: #34495e !important; } }
                            </style>
                          </head>
                          <body class="" style="background-color:#f6f6f6;font-family:sans-serif;-webkit-font-smoothing:antialiased;font-size:14px;line-height:1.4;margin:0;padding:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;">
                            <table border="0" cellpadding="0" cellspacing="0" class="body" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;background-color:#f6f6f6;width:100%;">
                              <tr>
                                <td style="font-family:sans-serif;font-size:14px;vertical-align:top;">&nbsp;</td>
                                <td class="container" style="font-family:sans-serif;font-size:14px;vertical-align:top;display:block;max-width:580px;padding:10px;width:580px;Margin:0 auto !important;">
                                  <div class="content" style="box-sizing:border-box;display:block;Margin:0 auto;max-width:580px;padding:10px;">
                                    <!-- START CENTERED WHITE CONTAINER -->
                                    <span class="preheader" style="color:transparent;display:none;height:0;max-height:0;max-width:0;opacity:0;overflow:hidden;mso-hide:all;visibility:hidden;width:0;">This is preheader text. Some clients will show this text as a preview.</span>
                                    <table class="main" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;background:#fff;border-radius:3px;width:100%;">
                                      <!-- START MAIN CONTENT AREA -->
                                      <tr>
                                        <td class="wrapper" style="font-family:sans-serif;font-size:14px;vertical-align:top;box-sizing:border-box;padding:20px;">
                                          <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;width:100%;">
                                            <tr>
                                              <td style="font-family:sans-serif;font-size:14px;vertical-align:top;">
                                                <p style="font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;">Hi Skilled Admin,</p>
                                                <p style="font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;">' . $name . ' has been registered as new company</p>
                                                <p style="font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;">Good luck!</p>
                                              </td>
                                            </tr>
                                          </table>
                                        </td>
                                      </tr>
                                      <!-- END MAIN CONTENT AREA -->
                                    </table>
                                    <!-- START FOOTER -->
                                    <div class="footer" style="clear:both;padding-top:10px;text-align:center;width:100%;">
                                      <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;width:100%;">
                                        <tr>
                                          <td class="content-block" style="font-family:sans-serif;font-size:14px;vertical-align:top;color:#999999;font-size:12px;text-align:center;">
                                            
                                          </td>
                                        </tr>
                                        <tr>
                                          <td class="content-block powered-by" style="font-family:sans-serif;font-size:14px;vertical-align:top;color:#999999;font-size:12px;text-align:center;">
                                            
                                          </td>
                                        </tr>
                                      </table>
                                    </div>
                                    <!-- END FOOTER -->
                                    <!-- END CENTERED WHITE CONTAINER -->
                                  </div>
                                </td>
                                <td style="font-family:sans-serif;font-size:14px;vertical-align:top;">&nbsp;</td>
                              </tr>
                            </table>
                          </body>
                        </html>';

        $headers = array('Content-Type: text/html; charset=UTF-8', 'From: Skilled <no-reply@skilled.co>');

        wp_mail($to, $subject, $body, $headers);
        echo $user_id;
        //}
        die();
    }

    // Always die in functions echoing ajax content
    die();
}

add_action('wp_ajax_register_request', 'register_request');
add_action('wp_ajax_nopriv_register_request', 'register_request');

function fareviewregister_func() {
    $return .= '
    <style>
        .form-control{
            border-radius: 0px;
        }
    </style>
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <div class="box">
                <div class="box-header">
                    Register to Skilled!
                </div>
                <div class="box-body">
                    <div id="notice"></div>
                    <form id="register" name="register" method="post">
                        <div class="form-group">
                            <label>First Name</label>
                            <input type="text" name="first_name" class="form-control"/>
                        </div>
                        <div class="form-group">
                            <label>Last Name</label>
                            <input type="text" name="last_name" class="form-control"/>
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" name="email" class="form-control"/>
                        </div>
                        <div class="form-group">
                            By registering, you agree to our <a href="/term-of-service/">Terms of Service</a> and <a href="/privacy-policy/">Privacy Policy</a>
                        </div>
                        <button class="btn btn-primary button button-primary" type="submit">
                            <div class="cp-spinner cp-meter"></div>
                            Register
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    ';
    $return .= "
    <script>
        $('#register').submit(function(event) {
        $('#register button').addClass('disable');
                if ($('input[name=\'email\']').val() == ''){
        $('#notice').html(\"<div class='alert alert-warning'><a href='#'' class='close' data-dismiss='alert'>&times;</a><strong>Warning!</strong> Please complete all fields.</div>\");
            $('input[name=\'company_email\']').css('border','solid 2px #d9534f')
        } else{
        jQuery.ajax({
        type: 'POST',
                url: '" . get_bloginfo("wpurl") . "/wp-admin/admin-ajax.php',
                data: {
                'formData' :$('#register').serialize(),
                        'action':'reviewregister_request'
                },
                success:function(data) {
                console.log(data)
                        if (data == 'Email already used') {
                $('#notice').html(\"<div class='alert alert-warning'><a href='#'' class='close' data-dismiss='alert'>&times;</a><strong>Warning!</strong> Your email is already in use.</div>\");
                } else{
                $('#notice').html(\"<div class='alert alert-success'><a href='#'' class='close' data-dismiss='alert'>&times;</a><strong>Congratulation!</strong> Please check your email for login information</div>\");
                }

                },
                        error: function(errorThrown){
                        console.log(errorThrown);
                        }
                });
                }
                jQuery('html, body').animate({
                scrollTop: jQuery('#notice').offset().top - 100
                }, 200);
                        $('#register button').removeClass('disable');
                        return false;
                })
    </script>
    ";
    return $return;
}

add_shortcode('fareviewregister', 'fareviewregister_func');

function reviewregister_request() {

    // The $_REQUEST contains all the data sent via ajax
    if (isset($_REQUEST)) {
        parse_str($_POST['formData'], $data);
        if (username_exists($data['email']) || email_exists($data['email'])) {
            echo "Email already used";
        } else {
            // print_r($_REQUEST);
            $random_password = wp_generate_password($length = 12, $include_standard_special_chars = false);
            $userdata = array(
                'first_name' => $data['first_name'],
                'last_name' => $data['last_name'],
                'user_email' => $data['email'],
                'user_login' => $data['email'],
                'user_pass' => $random_password,
                'role' => 'company'
            );

            $user_id = wp_insert_user($userdata);

            $to = $data['email'];
            $name = $data['first_name'];
            $subject = 'Skilled Account Information';
            $body = '<!DOCTYPE html>
                        <html>
                          <head>
                            <meta name="viewport" content="width=device-width">
                            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                            <title>Skilled Account Information</title>
                            <style type="text/css">
                            /* -------------------------------------
                                INLINED WITH https://putsmail.com/inliner
                            ------------------------------------- */
                            /* -------------------------------------
                                RESPONSIVE AND MOBILE FRIENDLY STYLES
                            ------------------------------------- */
                            @media only screen and (max-width: 620px) {
                              table[class=body] h1 {
                                font-size: 28px !important;
                                margin-bottom: 10px !important; }
                              table[class=body] p,
                              table[class=body] ul,
                              table[class=body] ol,
                              table[class=body] td,
                              table[class=body] span,
                              table[class=body] a {
                                font-size: 16px !important; }
                              table[class=body] .wrapper,
                              table[class=body] .article {
                                padding: 10px !important; }
                              table[class=body] .content {
                                padding: 0 !important; }
                              table[class=body] .container {
                                padding: 0 !important;
                                width: 100% !important; }
                              table[class=body] .main {
                                border-left-width: 0 !important;
                                border-radius: 0 !important;
                                border-right-width: 0 !important; }
                              table[class=body] .btn table {
                                width: 100% !important; }
                              table[class=body] .btn a {
                                width: 100% !important; }
                              table[class=body] .img-responsive {
                                height: auto !important;
                                max-width: 100% !important;
                                width: auto !important; }}
                            /* -------------------------------------
                                PRESERVE THESE STYLES IN THE HEAD
                            ------------------------------------- */
                            @media all {
                              .ExternalClass {
                                width: 100%; }
                              .ExternalClass,
                              .ExternalClass p,
                              .ExternalClass span,
                              .ExternalClass font,
                              .ExternalClass td,
                              .ExternalClass div {
                                line-height: 100%; }
                              .apple-link a {
                                color: inherit !important;
                                font-family: inherit !important;
                                font-size: inherit !important;
                                font-weight: inherit !important;
                                line-height: inherit !important;
                                text-decoration: none !important; }
                              .btn-primary table td:hover {
                                background-color: #34495e !important; }
                              .btn-primary a:hover {
                                background-color: #34495e !important;
                                border-color: #34495e !important; } }
                            </style>
                          </head>
                          <body class="" style="background-color:#f6f6f6;font-family:sans-serif;-webkit-font-smoothing:antialiased;font-size:14px;line-height:1.4;margin:0;padding:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;">
                            <table border="0" cellpadding="0" cellspacing="0" class="body" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;background-color:#f6f6f6;width:100%;">
                              <tr>
                                <td style="font-family:sans-serif;font-size:14px;vertical-align:top;">&nbsp;</td>
                                <td class="container" style="font-family:sans-serif;font-size:14px;vertical-align:top;display:block;max-width:580px;padding:10px;width:580px;Margin:0 auto !important;">
                                  <div class="content" style="box-sizing:border-box;display:block;Margin:0 auto;max-width:580px;padding:10px;">
                                    <!-- START CENTERED WHITE CONTAINER -->
                                    <span class="preheader" style="color:transparent;display:none;height:0;max-height:0;max-width:0;opacity:0;overflow:hidden;mso-hide:all;visibility:hidden;width:0;">This is preheader text. Some clients will show this text as a preview.</span>
                                    <table class="main" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;background:#fff;border-radius:3px;width:100%;">
                                      <!-- START MAIN CONTENT AREA -->
                                      <tr>
                                        <td class="wrapper" style="font-family:sans-serif;font-size:14px;vertical-align:top;box-sizing:border-box;padding:20px;">
                                          <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;width:100%;">
                                            <tr>
                                              <td style="font-family:sans-serif;font-size:14px;vertical-align:top;">
                                                <p style="font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;">Hi ' . $name . ',</p>
                                                <p style="font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;">Welcome to Skilled. Your acccount has been setup with this login information</p>
                                                <table border="0" cellpadding="0" cellspacing="0"  style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;box-sizing:border-box;width:100%;">
                                                  <tbody>
                                                    <tr>
                                                      <td align="left" style="font-family:sans-serif;font-size:14px;vertical-align:top;padding-bottom:15px;">
                                                        <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;width:100%;width:auto;">
                                                          <tbody>
                                                            <tr>
                                                              <td style="font-family:sans-serif;font-size:14px"> 
                                                              Username: ' . $to . '<br> Password: ' . $random_password . '</td>
                                                            </tr>
                                                          </tbody>
                                                        </table>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                                <p style="font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;">Good luck!</p>
                                              </td>
                                            </tr>
                                          </table>
                                        </td>
                                      </tr>
                                      <!-- END MAIN CONTENT AREA -->
                                    </table>
                                    <!-- START FOOTER -->
                                    <div class="footer" style="clear:both;padding-top:10px;text-align:center;width:100%;">
                                      <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;width:100%;">
                                        <tr>
                                          <td class="content-block" style="font-family:sans-serif;font-size:14px;vertical-align:top;color:#999999;font-size:12px;text-align:center;">
                                            
                                          </td>
                                        </tr>
                                        <tr>
                                          <td class="content-block powered-by" style="font-family:sans-serif;font-size:14px;vertical-align:top;color:#999999;font-size:12px;text-align:center;">
                                            
                                          </td>
                                        </tr>
                                      </table>
                                    </div>
                                    <!-- END FOOTER -->
                                    <!-- END CENTERED WHITE CONTAINER -->
                                  </div>
                                </td>
                                <td style="font-family:sans-serif;font-size:14px;vertical-align:top;">&nbsp;</td>
                              </tr>
                            </table>
                          </body>
                        </html>';

            $headers = array('Content-Type: text/html; charset=UTF-8', 'From: Skilled <no-reply@skilled.co>');
            wp_mail($to, $subject, $body, $headers);
            echo $user_id . "=" . $random_password;
        }
        die();
    }

    // Always die in functions echoing ajax content
    die();
}

add_action('wp_ajax_reviewregister_request', 'reviewregister_request');
add_action('wp_ajax_nopriv_reviewregister_request', 'reviewregister_request');


//Remove Admin bar
add_action('after_setup_theme', 'remove_admin_bar');

function remove_admin_bar() {
    if (!current_user_can('administrator') && !is_admin()) {
        show_admin_bar(false);
    }
}

//forgot password
function ts_forgot_password() {
    if (!is_user_logged_in()) {
        $return = '
        <div id="lostPassword">
            <div id="message"></div>
            <form id="lostPasswordForm" method="post">';
        ob_start();
        if (function_exists(
                        'wp_nonce_field'))
            wp_nonce_field('rs_user_lost_password_action', 'rs_user_lost_password_nonce');

        $return .= ob_get_clean();
        $return .= '<div class="form-group">
                    <label for="user_login">' . esc_attr('Username or E-mail:') . '<br />
                        <input type="text" name="user_login" id="user_login" value="' . esc_attr($user_login) . '" size="20" />
                    </label>
                    </div>';
        ob_start();
        do_action('lostpassword_form');
        $return .= ob_get_clean();
        $return .= '<p class="submit">
                    <input type="submit" name="wp-submit" id="wp-submit" value="' . esc_attr('Get New Password') . '" />
                    <img src="' . plugin_dir_url(__FILE__) . '/spinner.gif" id="preloader" alt="Preloader" style="margin-top: -60px;margin-left: 10px;"/>
                </p>
            </form>
        </div>';
    } else {
        $return .= '<script>window.location = "' . admin_url() . '"</script>';
    }
    return $return;
}

add_shortcode('ts_forgot_password', 'ts_forgot_password');

// New Password
function ts_new_password() {
    if (!is_user_logged_in()) {
        $return = '<div class="col-md-12"><div class="settings-box"><div class="settings-box-body">';
        $return .= '<div id="lostPassword">
            <div id="message"></div>';

        $errors = new WP_Error();
        $user = check_password_reset_key($_GET['key'], $_GET['login']);

        if (is_wp_error($user)) {
            if ($user->get_error_code() === 'expired_key')
                $errors->add('expiredkey', __('Sorry, that key has expired. Please try again.'));
            else if ($user->get_error_code() === 'invalid_key')
                $errors->add('invalidkey', __('Sorry, that key does not appear to be valid.'));
        }

        // display error message
        if ($errors->get_error_code()) {
            $return .= "<div class='alert alert-warning'>" . $errors->get_error_message($errors->get_error_code()) . "</div>";
        } else {
            $return .= '<form id="resetPasswordForm" method="post" autocomplete="off">';
            // this prevent automated script for unwanted spam
            ob_start();
            if (function_exists('wp_nonce_field'))
                wp_nonce_field('rs_user_reset_password_action', 'rs_user_reset_password_nonce');

            $return .= ob_get_clean();
            $return .= '<input type="hidden" name="user_key" id="user_key" value="' . esc_attr($_GET['key']) . '" autocomplete="off" />';
            $return .= '<input type="hidden" name="user_login" id="user_login" value="' . esc_attr($_GET['login']) . '" autocomplete="off" />';

            $return .= '<div class="form-group">
                    <label for="pass1">' . esc_attr('New password', 'skilled') . '<br />
                    <input type="password" name="pass1" id="pass1" size="20" value="" autocomplete="off"/></label>
                </div>
                <div class="form-group">
                    <label for="pass2">' . esc_attr('Confirm new password', 'skilled') . '<br />
                    <input type="password" name="pass2" id="pass2" size="20" value="" autocomplete="off" /></label>
                </div>';

            ob_start();
            do_action('resetpass_form', $user);
            $return .= ob_get_clean();
            $return .= '<p class="submit"><input type="submit" name="wp-submit" id="wp-submit" value="' . esc_attr('Reset Password') . '" />
                        <img src="' . plugin_dir_url(__FILE__) . '/spinner.gif" id="preloader" alt="Preloader"  style="margin-top: -60px;margin-left: 10px;"/>
                    </p>
                </form>';
            $return .= "";
        }
        echo '        </div>';
        $return .= '</div></div></div>';
    } else {
        $return .= '<script>window.location = "' . admin_url() . '"</script>';
    }
    return $return;
}

add_shortcode('ts_new_password', 'ts_new_password');

// Callback function

add_action('wp_ajax_nopriv_lost_pass', 'lost_pass_callback');
add_action('wp_ajax_lost_pass', 'lost_pass_callback');

function lost_pass_callback() {

    global $wpdb, $wp_hasher;

    $nonce = $_POST['nonce'];

    if (!wp_verify_nonce($nonce, 'rs_user_lost_password_action'))
        die('Security checked!');

    $user_login = $_POST['user_login'];

    $errors = new WP_Error();

    if (empty($user_login)) {
        $errors->add('empty_username', __('<strong>ERROR</strong>: Enter a username or e-mail address.'));
    } else if (strpos($user_login, '@')) {
        $user_data = get_user_by('email', trim($user_login));
        if (empty($user_data))
            $errors->add('invalid_email', __('<strong>ERROR</strong>: There is no user registered with that email address.'));
    } else {
        $login = trim($user_login);
        $user_data = get_user_by('login', $login);
    } do_action('lostpassword_post', $errors);

    if ($errors->get_error_code()) {
        return $errors;
    }

    if (!$user_data) {
        $errors->add('invalidcombo', __('<strong>ERROR</strong>: Invalid username or email.'));

        return $errors;
    }

    $user_login = $user_data->user_login;
    $user_email = $user_data->user_email;
    $key = get_password_reset_key($user_data);

    if (is_wp_error($key)) {
        return $key;
    }

    $message = __('Someone requested to reset the password for the following account:') . "\r\n\r\n";
    $message .= network_home_url('/') . "\r\n\r\n";
    $message .= sprintf(__('Username: %s'), $user_login) . "\r\n\r\n";
    $message .= __('If this was a mistake, just ignore this email and nothing will happen.') . "\r\n\r\n";
    $message .= __('To reset your password, visit the following address:') . "\r\n\r\n";
    //$message .= network_site_url("wp-login.php?action=rp&key=$key&login=" . rawurlencode($user_login), 'login') . "\r\n";

    $message .= get_permalink(get_page_by_path('new-password')) . "/?action=rp&key=$key&login=" . rawurlencode($user_login) . "\r\n";

    if (is_multisite()) {
        $blogname = $GLOBALS['current_site']->site_name;
    } else {
        $blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);
    }

    $title = sprintf(__('[%s] Password Reset'), $blogname);

    $title = apply_filters('retrieve_password_title', $title, $user_login, $user_data);

    $message = apply_filters('retrieve_password_message', $message, $key, $user_login, $user_data);

    if (wp_mail($user_email, wp_specialchars_decode($title), $message))
        $errors->add('confirm', __('Check your e-mail for the reset password link.'), 'message');
    else
        $errors->add('could_not_sent', __('The e-mail could not be sent.') . "<br />\n" . __('Possible reason: your host may have disabled the mail() function.'), 'message');

    // display error message
    if ($errors->get_error_code())
        echo '<p class="error">' . $errors->get_error_message($errors->get_error_code()) . '</p>';

    // return proper result
    wp_die();
}

add_action('wp_ajax_nopriv_reset_pass', 'reset_pass_callback');
add_action('wp_ajax_reset_pass', 'reset_pass_callback');
/*
 *  @desc   Process reset password
 */

function reset_pass_callback() {

    $errors = new WP_Error();
    $nonce = $_POST['nonce'];

    if (!wp_verify_nonce($nonce, 'rs_user_reset_password_action'))
        die('Security checked!');

    $pass1 = $_POST['pass1'];
    $pass2 = $_POST['pass2'];
    $key = $_POST['user_key'];
    $login = $_POST['user_login'];

    $user = check_password_reset_key($key, $login);

    // check to see if user added some string
    if ($pass1 == "" || $pass2 == "")
        $errors->add('password_required', __('All fields are required.'));

    // is pass1 and pass2 match?
    if (isset($pass1) && $pass1 != $pass2)
        $errors->add('password_reset_mismatch', __('The passwords do not match.'));

    /**
     * Fires before the password reset procedure is validated.
     *
     * @since 3.5.0
     *
     * @param object           $errors WP Error object.
     * @param WP_User|WP_Error $user   WP_User object if the login and reset key match. WP_Error object otherwise.
     */
    do_action('validate_password_reset', $errors, $user);

    if ((!$errors->get_error_code() ) && isset($pass1) && !empty($pass1)) {
        reset_password($user, $pass1);
        $errors->add('password_reset', __('Your password has been reset.'));
    }

    // display error message
    if ($errors->get_error_code())
        echo '<p class="error">' . $errors->get_error_message($errors->get_error_code()) . '</p>';

    // return proper result
    die();
}

//shortcode edit profile
function ts_profile() {
    global $current_user;
    $return .= '<div class="row"><div class="col-md-12"><div class="settings-box"><div class="settings-box-header">' . esc_attr__('My Account', 'skilled') . '</div><div class="settings-box-body">';

    if (!is_user_logged_in()) :
        $return .= '<p class="alert alert-warning">' . esc_attr('You must be logged in to edit your profile.', 'profile') . '</p>';
    else :
        if (count(
                        $error) > 0)
            $return .= '<p class="error">' . implode("<br />", $error) . '</p>';
        $return .= '<form method="post" id="ts_edit_profile" action="' . get_the_permalink() . '">';
        $return .= '<div id="message"></div>';
        ob_start();
        if (function_exists(
                        'wp_nonce_field'))
            wp_nonce_field('ts_update_profile_action', 'ts_update_profile_nonce');
        $return .= ob_get_clean();
        $return .= '<p class="form-group">
                        <label for="first-name">' . esc_attr__('First Name', 'skilled') . '</label>
                        <input class="form-control" name="first_name" type="text" id="first_name" value="' . get_the_author_meta('first_name', $current_user->ID) . '" />
                    </p><!-- .form-username -->
                    <p class="form-group">
                        <label for="last-name">' . esc_attr__('Last Name', 'skilled') . '</label>
                        <input class="form-control" name="last_name" type="text" id="last_name" value="' . get_the_author_meta('last_name', $current_user->ID) . '" />
                    </p><!-- .form-username -->
                    <p class="form-group">
                            <label for="email">' . esc_attr__('E-mail', 'skilled') . ' *</label>
                        <input class="form-control" name="email" type="text" id="email" value="' . get_the_author_meta('user_email', $current_user->ID) . '" readonly/>
                    </p><!-- .form-email -->
                    <p class="form-group">
                        <label for="url">' . esc_attr__('Website', 'skilled') . '</label>
                        <input class="form-control" name="url" type="text" id="url" value="' . get_the_author_meta('user_url', $current_user->ID) . '" />
                    </p><!-- .form-url -->
                    <p class="form-group">
                        <label for="pass1">' . esc_attr__('Password', 'skilled') . ' * </label>
                        <input class="form-control" name="pass1" type="password" id="pass1" />
                    </p><!-- .form-password -->
                    <p class="form-group">
                        <label for="pass2">' . esc_attr__('Repeat Password', 'skilled') . ' *</label>
                        <input class="form-control" name="pass2" type="password" id="pass2" />
                    </p><!-- .form-password -->';
        ob_start();
        do_action('edit_user_profile', $current_user);
        $return .= ob_get_clean();
        $return .= '<p class="form-submit">
                        ' . $referer . '
                        <input name="updateuser" type="submit" id="submit" class="btn btn-flat btn-primary" value="' . esc_attr__('Update', 'skilled') . '" />
                        <img src="' . plugin_dir_url(__FILE__) . '/spinner.gif" id="preloader" alt="Preloader"  style="margin-top: -60px;margin-left: 10px;"/>';
        ob_start();
        wp_nonce_field('update-user');
        $return .= ob_get_clean();
        $return .= '<input name="action" type="hidden" id="action" value="update-user" /></p></form>';
    endif;
    $return .= '</div></div></div>';
    $return .= '</div>';
    return $return;
}

add_shortcode(
        'ts_profile', 'ts_profile');

//edit profile callback

function edit_profile_callback() {
    /* Get user info. */
    global $current_user, $wp_roles;

    $errors = new WP_Error();
    $nonce = $_POST['nonce'];

    if (!wp_verify_nonce($nonce, 'ts_update_profile_action'))
        die('Security issue!');


    /* If profile was saved, update profile. */
    if ('POST' == $_SERVER ['REQUEST_METHOD'] && !empty($_POST['action']) && $_POST['action'] == 'edit_profile') {

        /* Update user password. */
        if (!empty($_POST['pass1']) && !empty($_POST['pass2'])) {
            if ($_POST['pass1'] == $_POST['pass2'])
                wp_update_user(array('ID' => $current_user->ID, 'user_pass' => esc_attr($_POST['pass1'])));
            else
                $errors->add('The passwords you entered do not match.  Your password was not updated.', 'profile');
        }

        /* Update user information. */
        if (!empty($_POST['url']))
            wp_update_user(array('ID' => $current_user->ID, 'user_url' => esc_url($_POST['url'])));
        if (!empty($_POST['email'])) {
            if (!is_email(esc_attr($_POST['email'])))
                $errors->add('The Email you entered is not valid.  please try again.', 'profile');
            elseif (email_exists(esc_attr($_POST['email'])) != $current_user->id)
                $errors->add('This email is already used by another user.  try a different one.', 'profile');
            else {
                wp_update_user(array('ID' => $current_user->ID, 'user_email' => esc_attr($_POST['email'])));
            }
        }

        if (!empty($_POST['first_name']))
            update_user_meta($current_user->ID, 'first_name', esc_attr($_POST['first_name']));
        if (!empty($_POST['last_name']))
            update_user_meta($current_user->ID, 'last_name', esc_attr($_POST['last_name']));
        if (!empty($_POST['description']))
            update_user_meta($current_user->ID, 'description', esc_attr($_POST['description']));
        if (!empty($_POST['url']))
            update_user_meta($current_user->ID, 'user_url', esc_attr($_POST['url']));
        if (!empty($_POST['pass1']))
            wp_set_password($_POST['pass1'], $current_user->ID);

        /* Redirect so the page will show updated info. */
        /* I am not Author of this Code- i dont know why but it worked for me after changing below line to if ( count($error) == 0 ){ */
        if ((!$errors->get_error_code())) {
            //action hook for plugins and extra fields saving
            do_action('edit_user_profile_update', $current_user->ID);
            $errors->add('profile_update', __('Your profile has been updated.'));
        }
        if ($errors->get_error_code())
            echo '<p class="error">' . $errors->get_error_message($errors->get_error_code()) . '</p>';
        die();
    }
}

add_action('wp_ajax_nopriv_edit_profile', 'edit_profile_callback');
add_action('wp_ajax_edit_profile', 'edit_profile_callback');

add_filter('tiny_mce_before_init', 'wpse24113_tiny_mce_before_init');

function wpse24113_tiny_mce_before_init($initArray) {
    if ($initArray['selector'] == "#company-description") {
        $initArray['setup'] = <<<JS
[function(ed) {
        ed.on( 'keypress', function(e) {
            //var content = ed.getContent().replace(
            //    /(<[a-zA-Z\/][^<>]*>|\[([^\]]+)\])|(\s+)/ig, '' );

            var content = ed.getContent()
            var words = content.split(' ');
            console.log(words.length);

            if (words.length>200){
                tinymce.dom.Event.cancel(e);
                document.getElementById("company_description_character_count").innerHTML = "You have exceeds the maximum words allowed in this field.";
            }else{
                document.getElementById("company_description_character_count").innerHTML = "Word Count: "+words.length;
            }
        } );
        ed.on( 'paste', function(e) {
            //var content = ed.getContent().replace(
            //    /(<[a-zA-Z\/][^<>]*>|\[([^\]]+)\])|(\s+)/ig, '' );

            var content = ed.getContent()

            var words = content.split(' ');
            console.log(words.length);

            if (words.length>200){
                tinymce.dom.Event.cancel(e);
                document.getElementById("company_description_character_count").innerHTML = "You have exceeds the maximum words allowed in this field.";
            }else{
                document.getElementById("company_description_character_count").innerHTML = "Word Count: "+words.length;
            }
        } );
}][0]
JS;
    }
    if ($initArray['selector'] == "#front-editor") {
        $initArray['setup'] = <<<JS
[function(ed) {
        ed.on( 'keypress', function(e) {
            //var content = ed.getContent().replace(
            //    /(<[a-zA-Z\/][^<>]*>|\[([^\]]+)\])|(\s+)/ig, '' );

            var content = ed.getContent()

            var words = content.split(' ');
            console.log(words.length);

            if (words.length>200){
                tinymce.dom.Event.cancel(e);
                document.getElementById("other_character_count").innerHTML = "You have exceeds the maximum words allowed in this field.";
            }else{
                document.getElementById("other_character_count").innerHTML = "Word Count: "+words.length;
            }
        } );
        ed.on( 'paste', function(e) {
            //var content = ed.getContent().replace(
            //    /(<[a-zA-Z\/][^<>]*>|\[([^\]]+)\])|(\s+)/ig, '' );

            var content = ed.getContent()

            var words = content.split(' ');
            console.log(words.length);

            if (words.length>200){
                tinymce.dom.Event.cancel(e);
                document.getElementById("other_character_count").innerHTML = "You have exceeds the maximum words allowed in this field.";
            }else{
                document.getElementById("other_character_count").innerHTML = "Word Count: "+words.length;
            }
        } );
}][0]
JS;
    }

    $initArray['statusbar'] = false;
    return $initArray;
}

function sar_attachment_redirect() {
    global $post;
    if (is_attachment() && isset($post->post_parent) && is_numeric($post->post_parent) && ($post->post_parent != 0)) {
        wp_redirect(get_permalink($post->post_parent), 301); // permanent redirect to post/page where image or document was uploaded
        exit;
    } elseif (is_attachment() && isset($post->post_parent) && is_numeric($post->post_parent) && ($post->post_parent < 1)) {   // for some reason it doesnt works checking for 0, so checking lower than 1 instead...
        wp_redirect(get_bloginfo('wpurl'), 302); // temp redirect to home for image or document not associated to any post/page
        exit;
    }
}

add_action('template_redirect', 'sar_attachment_redirect', 1);

function kill_wp($post) {
    global $post;
    if ($post->post_type == 'company') {
        $to = get_post_meta($post->ID, '_company_email', true);
        $name = get_the_title();
        $subject = 'Company Registration Approval';
        $body = '<!DOCTYPE html>
                    <html>
                      <head>
                        <meta name="viewport" content="width=device-width">
                        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                        <title>Skilled Account Information</title>
                        <style type="text/css">
                        /* -------------------------------------
                            INLINED WITH https://putsmail.com/inliner
                        ------------------------------------- */
                        /* -------------------------------------
                            RESPONSIVE AND MOBILE FRIENDLY STYLES
                        ------------------------------------- */
                        @media only screen and (max-width: 620px) {
                          table[class=body] h1 {
                            font-size: 28px !important;
                            margin-bottom: 10px !important; }
                          table[class=body] p,
                          table[class=body] ul,
                          table[class=body] ol,
                          table[class=body] td,
                          table[class=body] span,
                          table[class=body] a {
                            font-size: 16px !important; }
                          table[class=body] .wrapper,
                          table[class=body] .article {
                            padding: 10px !important; }
                          table[class=body] .content {
                            padding: 0 !important; }
                          table[class=body] .container {
                            padding: 0 !important;
                            width: 100% !important; }
                          table[class=body] .main {
                            border-left-width: 0 !important;
                            border-radius: 0 !important;
                            border-right-width: 0 !important; }
                          table[class=body] .btn table {
                            width: 100% !important; }
                          table[class=body] .btn a {
                            width: 100% !important; }
                          table[class=body] .img-responsive {
                            height: auto !important;
                            max-width: 100% !important;
                            width: auto !important; }}
                        /* -------------------------------------
                            PRESERVE THESE STYLES IN THE HEAD
                        ------------------------------------- */
                        @media all {
                          .ExternalClass {
                            width: 100%; }
                          .ExternalClass,
                          .ExternalClass p,
                          .ExternalClass span,
                          .ExternalClass font,
                          .ExternalClass td,
                          .ExternalClass div {
                            line-height: 100%; }
                          .apple-link a {
                            color: inherit !important;
                            font-family: inherit !important;
                            font-size: inherit !important;
                            font-weight: inherit !important;
                            line-height: inherit !important;
                            text-decoration: none !important; }
                          .btn-primary table td:hover {
                            background-color: #34495e !important; }
                          .btn-primary a:hover {
                            background-color: #34495e !important;
                            border-color: #34495e !important; } }
                        </style>
                      </head>
                      <body class="" style="background-color:#f6f6f6;font-family:sans-serif;-webkit-font-smoothing:antialiased;font-size:14px;line-height:1.4;margin:0;padding:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;">
                        <table border="0" cellpadding="0" cellspacing="0" class="body" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;background-color:#f6f6f6;width:100%;">
                          <tr>
                            <td style="font-family:sans-serif;font-size:14px;vertical-align:top;">&nbsp;</td>
                            <td class="container" style="font-family:sans-serif;font-size:14px;vertical-align:top;display:block;max-width:580px;padding:10px;width:580px;Margin:0 auto !important;">
                              <div class="content" style="box-sizing:border-box;display:block;Margin:0 auto;max-width:580px;padding:10px;">
                                <!-- START CENTERED WHITE CONTAINER -->
                                <span class="preheader" style="color:transparent;display:none;height:0;max-height:0;max-width:0;opacity:0;overflow:hidden;mso-hide:all;visibility:hidden;width:0;">This is preheader text. Some clients will show this text as a preview.</span>
                                <table class="main" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;background:#fff;border-radius:3px;width:100%;">
                                  <!-- START MAIN CONTENT AREA -->
                                  <tr>
                                    <td class="wrapper" style="font-family:sans-serif;font-size:14px;vertical-align:top;box-sizing:border-box;padding:20px;">
                                      <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;width:100%;">
                                        <tr>
                                          <td style="font-family:sans-serif;font-size:14px;vertical-align:top;">
                                            <p style="font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;">Hi ' . $name . ',</p>
                                            <p style="font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;">Congratulation, your company has been approved</p>
                                            <p style="font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;">Good luck!</p>
                                          </td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                  <!-- END MAIN CONTENT AREA -->
                                </table>
                                <!-- START FOOTER -->
                                <div class="footer" style="clear:both;padding-top:10px;text-align:center;width:100%;">
                                  <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;width:100%;">
                                    <tr>
                                      <td class="content-block" style="font-family:sans-serif;font-size:14px;vertical-align:top;color:#999999;font-size:12px;text-align:center;">
                                        
                                      </td>
                                    </tr>
                                    <tr>
                                      <td class="content-block powered-by" style="font-family:sans-serif;font-size:14px;vertical-align:top;color:#999999;font-size:12px;text-align:center;">
                                        
                                      </td>
                                    </tr>
                                  </table>
                                </div>
                                <!-- END FOOTER -->
                                <!-- END CENTERED WHITE CONTAINER -->
                              </div>
                            </td>
                            <td style="font-family:sans-serif;font-size:14px;vertical-align:top;">&nbsp;</td>
                          </tr>
                        </table>
                      </body>
                    </html>';

        $headers = array('Content-Type: text/html; charset=UTF-8', 'From: Skilled <no-reply@skilled.co>');

        wp_mail($to, $subject, $body, $headers);
    }
}

add_action('draft_to_publish', 'kill_wp');
