<?php
/*
  Plugin Name: Location
  Plugin URI: http://totalstudio.co/
  Description: This plugin is created by Total Studio to manage list of countries, states/provinces, and cities in four countries which are US, Russia, Netherlands, and France.
  Version: 0.1
  Author: Suko Tyas Pernanda
  Author URI: https://totalstudio.co/about-us/
  Text Domain: totalstudio
 */


//add_action('init', 'ts_location_init', 0);

//add_action('admin_init', 'ts_organization_allow', 999);
function location_plugin_failed() {
    ?>
    <div class="notice notice-error">
        <p><?php _e('TS Location Plugin only works along TS Plugin.', 'skilled'); ?></p>
    </div>
    <?php
}

function ts_location_init() {
    if (!function_exists('ts_plugin')) {
        add_action('admin_notices', 'location_plugin_failed');
    } else {

        //DEFINE COUNTRY POST TYPE
        //$ts = ts_plugin('skilled_country', 'Country', 'Countries');
        //$args = [
        //    'supports' => ['title'],
        //    'rewrite' => [
        //        'slug' => 'country'
        //    ],
        //    'capability_type' => ['country', 'countries'],
        //];
        //$ts->postype($args);
        //DEFINE STATE or PROVINCE POST TYPE
        $ts = ts_plugin('skilled_state', 'State', 'States');
        $args = [
            'supports' => ['title'],
            'rewrite' => [
                'slug' => 'state'
            ],
            'capability_type' => ['state', 'states'],
        ];

        $ts->postype($args);
        $ts->taxonomy("skilled_country", "Country", "Countries", "", [
            "hierarchical" => true,
            'show_in_menu' => true,
        ]);

        //DEFINE CITY POST TYPE
        $ts_ct = ts_plugin('skilled_country_city', 'City', 'Cities');
        $argsss = [
            'supports' => ['title'],
            'rewrite' => [
                'slug' => 'city'
            ],
            'capability_type' => ['city', 'cities'],
        ];

        $ts_ct->postype($argsss);
    }
}

//add_action('cmb2_init', 'cmb2_country');

function cmb2_country() {
//    $cmb_country = new_cmb2_box(array(
//        'id' => 'country_information',
//        'title' => 'Country Information',
//        'object_types' => array('skilled_country'), // Post type
//        'context' => 'normal',
//        'priority' => 'high',
//        'show_names' => true
//    ));
//
//    $cmb_country->add_field(array(
//        'name' => 'Country Name',
//        'id' => 'country_name',
//        'desc' => 'Describe the short country name here. <br>Example: US',
//        'type' => 'text',
//    ));
//    // FETCH COMPANY GUIDES
//    $country = [];
//    query_posts(['post_type' => 'skilled_country']);
//    while (have_posts()) {
//        the_post();
//        $country[get_the_ID()] = get_the_title();
//    }
//    wp_reset_query();
//    $cmb_state = new_cmb2_box(array(
//        'id' => 'state_information',
//        'title' => 'State Information',
//        'object_types' => array('skilled_state'), // Post type
//        'context' => 'normal',
//        'priority' => 'high',
//        'show_names' => true
//    ));
//    $cmb_state->add_field(array(
//        'name' => 'Country Name',
//        'id' => 'country_name',
//        'type' => 'select',
//        'options' => $country
//    ));
//
    // FETCH STATE
    $state = [];
    query_posts([
        'post_type' => 'skilled_state',
        'posts_per_page' => -1
    ]);
    $i = 0;
    while (have_posts()) {
        the_post();
        $id = get_the_ID();
        $term = get_the_terms(get_the_ID(), "skilled_country");
        $state[$id] = strtoupper($term[0]->slug) . " - " . get_the_title();
        $i++;
    }
    wp_reset_query();

    $cmb_city = new_cmb2_box(array(
        'id' => 'city_information',
        'title' => 'City Information',
        'object_types' => array('skilled_country_city'), // Post type
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true
    ));

    $cmb_city->add_field(array(
        'name' => 'State',
        'id' => 'city-select-state',
        'type' => 'select',
        'options' => $state
    ));
}
