<?php

/**
 * @package TotalStudio
 */
/*
  Plugin Name: TS Plugin
  Plugin URI: http://totalstudio.co/
  Description: This plugin is require for all the plugins from Total Studio
  Version: 0.1
  Author: Danang Widiantoro
  Author URI: https://totalstudio.co/about-us/
  Text Domain: totalstudio
 */

class TS_Plugin {

    private $plugin = 'pluginname';
    private $singular = 'singular';
    private $plural = 'plural';

    function __construct($plugin, $singular, $plural) {
        $this->set($plugin, $singular, $plural);
    }

    public function set($plugin, $singular, $plural) {
        $this->plugin = $plugin;
        $this->singular = $singular;
        $this->plural = $plural;
    }

    public function postype($args = array()) {
        $type = array(
            'label' => __($this->singular, $this->plugin),
            'labels' => array(
                'name' => _x($this->plural, $this->plural, $this->plugin),
                'singular_name' => _x($this->singular, $this->singular, $this->plugin),
                'menu_name' => __($this->plural, $this->plugin),
                'name_admin_bar' => __($this->plural, $this->plugin),
                'archives' => __($this->singular . ' Archives', $this->plugin),
                'parent_item_colon' => __('Parent ' . $this->singular . ':', $this->plugin),
                'all_items' => __('All ' . $this->plural, $this->plugin),
                'add_new_item' => __('Add New ' . $this->singular, $this->plugin),
                'add_new' => __('Add New', $this->plugin),
                'new_item' => __('New ' . $this->singular, $this->plugin),
                'edit_item' => __('Edit ' . $this->singular, $this->plugin),
                'update_item' => __('Update ' . $this->singular, $this->plugin),
                'view_item' => __('View ' . $this->singular, $this->plugin),
                'search_items' => __('Search ' . $this->singular, $this->plugin),
                'not_found' => __('Not found', $this->plugin),
                'not_found_in_trash' => __('Not found in Trash', $this->plugin),
                'featured_image' => __('Featured Image', $this->plugin),
                'set_featured_image' => __('Set featured image', $this->plugin),
                'remove_featured_image' => __('Remove featured image', $this->plugin),
                'use_featured_image' => __('Use as featured image', $this->plugin),
                'insert_into_item' => __('Insert into item', $this->plugin),
                'uploaded_to_this_item' => __('Uploaded to this item', $this->plugin),
                'items_list' => __($this->plural . ' list', $this->plugin),
                'items_list_navigation' => __($this->plural . ' list navigation', $this->plugin),
                'filter_items_list' => __('Filter ' . $this->plural . ' list', $this->plugin),
            ),
            // 'supports'            => false,
            'hierarchical' => true,
            'public' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'menu_position' => 100,
            'show_in_admin_bar' => true,
            'show_in_nav_menus' => true,
            'can_export' => true,
            'has_archive' => true,
            'exclude_from_search' => true,
            'publicly_queryable' => true,
            'rewrite' => array(
                'slug' => $this->plugin,
                'with_front' => true,
                'pages' => true,
                'feeds' => false,
            ),
            'capability_type' => 'page',
        );
        if (!empty($args) && is_array($args)) {
            $type = array_replace_recursive($type, $args);
        }
        register_post_type($this->plugin, $type);
    }

    public function taxonomy($taxonomy, $singular = '', $plural = '', $object_type = '', $args = '') {
        if (empty($singular)) {
            $singular = ucwords($taxonomy);
        }
        if (empty($plural)) {
            $plural = ucwords($singular) . 's';
        }
        if (empty($object_type)) {
            $object_type = array($this->plugin);
        }
        $taxo_args = array(
            'labels' => array(
                'name' => _x($plural, 'Taxonomy General Name', $this->plugin),
                'singular_name' => _x($singular, 'Taxonomy Singular Name', $this->plugin),
                'menu_name' => __($plural, $this->plugin),
                'all_items' => __('All Items', $this->plugin),
                'parent_item' => __('Parent Item', $this->plugin),
                'parent_item_colon' => __('Parent Item:', $this->plugin),
                'new_item_name' => __('New Item Name', $this->plugin),
                'add_new_item' => __('Add New Item', $this->plugin),
                'edit_item' => __('Edit Item', $this->plugin),
                'update_item' => __('Update Item', $this->plugin),
                'view_item' => __('View Item', $this->plugin),
                'add_or_remove_items' => __('Add or remove items', $this->plugin),
                'choose_from_most_used' => __('Choose from the most used', $this->plugin),
                'popular_items' => __('Popular Items', $this->plugin),
                'search_items' => __('Search Items', $this->plugin),
                'not_found' => __('Not Found', $this->plugin),
                'no_terms' => __('No items', $this->plugin),
                'items_list' => __('Items list', $this->plugin),
                'items_list_navigation' => __('Items list navigation', $this->plugin),
            ),
            'hierarchical' => true,
            'public' => false,
            'show_ui' => true,
            'show_admin_column' => true,
            'show_in_nav_menus' => true,
            'show_tagcloud' => true,
        );

        if (!empty($args) && is_array($args)) {
            $taxo_args = array_replace_recursive($taxo_args, $args);
        }
        //register_taxonomy($this->plugin . '_' . $taxonomy . '_term', $object_type, $taxo_args);
        register_taxonomy($taxonomy, $object_type, $taxo_args); //modified by Suko
    }

    /*
      How to use $options:
      $options = array(
      'input_attr'    => string $text_attribute_for_html_input,
      'input_label'   => string $text_for_label_or_placeholder,
      'callback_args' => array $callback_arguments,
      // other than variable above is consider as options for multiple inputs
      );
      Simple Example:
      $options = array(
      'value1' => 'label1',
      'value2' => 'label2',
      'value3' => 'label3',
      'value4' => 'label4'
      );
      $ts->metabox('checkbox', '', 'checkbox', array('input_label' => 'ok bro...!!'));
      $ts->metabox('checkboxes', '', 'checkboxes', $options);
      $ts->metabox('html', '', 'html');
      $ts->metabox('radio', '', 'radio', $options);
      $ts->metabox('select', '', 'select', $options);
      $ts->metabox('text', '', 'text');
      $ts->metabox('textarea', '', 'textarea');
      $ts->metabox('upload', '', 'upload');
     */

    public function metabox($id, $title = '', $callback = '', $options = array(), $context = 'advanced', $priority = 'default') {
        $i = func_num_args();
        if ($i > 0) {
            $args = array();
            // ID
            $args[0] = $id; //modified by Suko
            //$args[0] = '_' . $this->plugin . '_' . $id;
            // Title
            $args[1] = esc_html__((!empty($title) ? $title : $id), $this->plugin);
            // Callback
            $args[2] = $input = $callback;
            // WP_Screen
            $args[3] = $this->plugin;
            // Context
            $args[4] = $context;
            // Priority
            $args[5] = $priority;
            // Callback Args
            $args[6] = $options;

            if (empty($callback) || !function_exists($callback)) {
                if (empty($callback)) {
                    $callback = 'text';
                }
                $args[2] = $input = strtolower(trim($callback));
                if ($args[2] == 'upload') {
                    add_action('post_edit_form_tag', function() {
                        echo ' enctype="multipart/form-data"';
                    });
                }
                if ($args[2] == "company_guide") {
                    
                }
                $args[2] = function($post, $metabox) use ($args) {
                    $input = $args[2];
                    $options = $args[6];
                    $paths = array(
                        get_theme_root() . '/' . get_template() . '/template/',
                        __DIR__ . '/template/',
                    );
                    $file_path = 'none';
                    foreach ($paths as $p) {
                        $p .= is_admin() ? 'admin/' : 'public/';
                        if (file_exists($p . $input . '.php')) {
                            $file_path = $p . $input . '.php';
                            break;
                        }
                    }
                    if (file_exists($file_path)) {
                        $is_single = TS_Plugin::is_single($input);
                        $metabox['value'] = !empty($post->ID) ? get_post_meta($post->ID, $metabox['id'], true) : ($is_single ? '' : array());
                        $metabox['input_attr'] = '';
                        $metabox['input_label'] = '';
                        $metabox['options'] = array();
                        if (!$is_single && !is_array($metabox['value'])) {
                            $metabox['value'] = !empty($metabox['value']) ? array($metabox['value']) : array();
                        }
                        if (!empty($metabox['args']['input_attr'])) {
                            $metabox['input_attr'] = $metabox['args']['input_attr'];
                        }
                        if (!empty($metabox['args']['input_label'])) {
                            $metabox['input_label'] = $metabox['args']['input_label'];
                        }
                        if (!empty($options) && is_array($options)) {
                            foreach ($options as $key => $value) {
                                if (!in_array($key, array('input_attr', 'input_label', 'callback_args'))) {
                                    $metabox['options'][$key] = $value;
                                }
                            }
                        }
                        wp_nonce_field($post->ID, $metabox['id']);
                        switch ($input) {
                            case 'checkbox':
                                $metabox['input_attr'] .= ' checked="true"';
                                break;
                        }
                        extract($metabox);
                        include $file_path;
                    }
                };
            }

            // Update metabox Data
            add_action('save_post', function($post_id, $post, $update) use ($args, $input) {
                // if (!isset($_POST[$args[0]]) || !wp_verify_nonce($_POST[$args[0]], $args[0]))
                // 	return $post_id;

                if (current_user_can('edit_posts', $post_id) || current_user_can('edit_pages', $post_id)) {
                    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
                        return $post_id;

                    if ($args[3] != @$_POST['post_type'])
                        return $post_id;

                    $field = $args[0];
                    switch ($input) {
                        case 'upload':
                            // pr(wp_upload_dir(), $_FILES, __FILE__.':'.__LINE__);die();
                            if (is_uploaded_file($_FILES[$field]['tmp_name'])) {
                                if (preg_match('~^(.*?)\.([^\.]+)$~is', $_FILES[$field]['name'], $m)) {
                                    $name = strtolower(preg_replace(array('~\&~i', '~[^a-z0-9\-_]~i', '~' . preg_quote('-', '~') . '{2,}~i'), array('n', '-', '-'), trim($m[1])));
                                    $ext = strtolower($m[2]);
                                    $dir = wp_upload_dir();
                                    if (!in_array($ext, array('php', 'java', 'jar', 'py', 'ruby', 'c', 'r', 'go', 'pl'))) {
                                        if (move_uploaded_file($_FILES[$field]['tmp_name'], $dir['path'] . '/' . $name . '.' . $ext)) {
                                            $fileUrl = $dir['url'] . '/' . $name . '.' . $ext;
                                            $oldUrl = get_post_meta($post_id, $field, true);
                                            if (!empty($oldUrl) && $oldUrl != $fileUrl) {
                                                $oldPath = str_replace(get_site_url() . '/', get_home_path(), $oldUrl);
                                                if (file_exists($oldPath)) {
                                                    @unlink($oldPath);
                                                }
                                            }
                                            update_post_meta($post_id, $field, $fileUrl);
                                        } else {
                                            die('Sorry, failed to upload file');
                                        }
                                    } else {
                                        die('Sorry, you cannot upload file script using TS Plugin');
                                    }
                                }
                            }
                            // die('disini');
                            // pr($_FILES, $_POST, __FILE__.':'.__LINE__);die();
                            break;
                        case 'checkbox':
                            $_POST[$field] = @$_POST[$field] == '1' ? '1' : '0';
                            break;
                        case 'company_guide':

                            break;
                        default:
                            update_post_meta($post_id, $field, @$_POST[$field]);
                            break;
                    }
                }
                return $post_id;
            }, 10, 3);

            // Delete metabox Data
            add_action('do_meta_boxes', function($post_id, $post, $update) use ($args, $input) {
                // if (!isset($_POST[$args[0]]) || !wp_verify_nonce($_POST[$args[0]], $post_id))
                // 	return $post_id;

                if (current_user_can('delete_posts', $post_id) || current_user_can('delete_pages', $post_id)) {
                    // if($args[3] != $_POST['post_type'])
                    // 	return $post_id;

                    $value = get_post_meta($post_id, $args[0], TS_Plugin::is_single($input));
                    delete_post_meta($post_id, $args[0], $value);
                }
                return $post_id;
            }, 10, 3);

            // Add Metabox
            add_action('add_meta_boxes', function() use ($args) {
                call_user_func_array('add_meta_box', $args);
            });
        }
    }

    public static function is_single($input_type) {
        $out = true;
        switch ($input_type) {
            case 'checkboxes':
                $out = false;
                break;
        }
        return $out;
    }

}

add_action("updated_post_meta", "interrupt_company_guide_save");

function interrupt_company_guide_save($meta_id) {
    global $post, $wp_query;
    if (!empty($meta_id)) {
        $meta = get_post_meta_by_id($meta_id);

        if ($meta->meta_key == "_company_guide") {
            //update all _company_guide_x to 0
            $thisPostMeta = get_post_meta($meta->post_id);
            foreach ($thisPostMeta as $key => $val) {
                if (strpos($key, "company_guide_")) {
                    update_post_meta($meta->post_id, $key, 0);
                }
            }

            $values = $meta->meta_value;
            foreach ($values as $val) {
                update_post_meta($meta->post_id, "_company_guide_" . $val, 1);
            }
            //die;
        }
    }
}

function ts_plugin($plugin, $singular = '', $plural = '') {
    if (empty($singular)) {
        $singular = ucwords($plugin);
    }
    if (empty($plural)) {
        $plural = ucwords($singular) . 's';
    }
    return new TS_Plugin($plugin, $singular, $plural);
}

add_action('admin_init', 'add_theme_caps');

function add_theme_caps() {
    //Administrator role
    $role = get_role( 'administrator' );

    //permission for company guide
    $role->add_cap( 'edit_organization' ); 
    $role->add_cap( 'publish_organization' ); 
    $role->add_cap( 'delete_organization' ); 
    $role->add_cap( 'edit_organizations' ); 
    $role->add_cap( 'publish_organizations' ); 
    $role->add_cap( 'delete_organizations' ); 
    $role->add_cap( 'edit_others_organization' ); 
    $role->add_cap( 'read_private_organization' ); 
    $role->add_cap( 'edit_others_organizations' ); 
    $role->add_cap( 'read_private_organizations' ); 
    
    //permission for company
    $role->add_cap( 'edit_company' ); 
    $role->add_cap( 'publish_company' ); 
    $role->add_cap( 'delete_company' ); 
    $role->add_cap( 'edit_companies' ); 
    $role->add_cap( 'publish_companies' ); 
    $role->add_cap( 'delete_companies' ); 
    $role->add_cap( 'edit_others_company' ); 
    $role->add_cap( 'read_private_company' ); 
    $role->add_cap( 'edit_others_companies' ); 
    $role->add_cap( 'read_private_companies' ); 
    
    //permission for reviews
    $role->add_cap( 'edit_review' ); 
    $role->add_cap( 'publish_review' ); 
    $role->add_cap( 'delete_review' ); 
    $role->add_cap( 'edit_reviews' ); 
    $role->add_cap( 'publish_reviews' ); 
    $role->add_cap( 'delete_reviews' ); 
    $role->add_cap( 'edit_others_reviews' ); 
    $role->add_cap( 'read_private_reviews' );
    
    //permission for proposal
    $role->add_cap( 'edit_proposal' ); 
    $role->add_cap( 'publish_proposal' ); 
    $role->add_cap( 'delete_proposal' ); 
    $role->add_cap( 'edit_proposals' ); 
    $role->add_cap( 'publish_proposals' ); 
    $role->add_cap( 'delete_proposals' ); 
    $role->add_cap( 'edit_others_proposals' ); 
    $role->add_cap( 'read_private_proposals' );
    
    //permission for resource
    $role->add_cap( 'edit_resource' ); 
    $role->add_cap( 'publish_resource' ); 
    $role->add_cap( 'delete_resource' ); 
    $role->add_cap( 'edit_resources' ); 
    $role->add_cap( 'publish_resources' ); 
    $role->add_cap( 'delete_resources' ); 
    $role->add_cap( 'edit_others_resources' ); 
    $role->add_cap( 'read_private_resources' );
    
    //Editor role
    $role = get_role( 'editor' );

    //permission for company guide
    $role->add_cap( 'edit_organization' ); 
    $role->add_cap( 'publish_organization' ); 
    $role->add_cap( 'edit_organizations' ); 
    $role->add_cap( 'publish_organizations' ); 
    $role->add_cap( 'edit_others_organizations' ); 
    $role->add_cap( 'read_private_organizations' ); 
    
    //permission for company
    $role->add_cap( 'edit_company' ); 
    $role->add_cap( 'publish_company' ); 
    $role->add_cap( 'edit_companies' ); 
    $role->add_cap( 'publish_companies' ); 
    $role->add_cap( 'edit_others_companies' ); 
    $role->add_cap( 'read_private_companies' ); 
    
    //permission for reviews
    $role->add_cap( 'edit_review' ); 
    $role->add_cap( 'publish_review' ); 
    $role->add_cap( 'edit_reviews' ); 
    $role->add_cap( 'publish_reviews' ); 
    $role->add_cap( 'edit_others_reviews' ); 
    $role->add_cap( 'read_private_reviews' );
    
    //permission for proposal
    $role->add_cap( 'edit_proposal' ); 
    $role->add_cap( 'publish_proposal' ); 
    $role->add_cap( 'edit_proposals' ); 
    $role->add_cap( 'publish_proposals' ); 
    $role->add_cap( 'edit_others_proposals' ); 
    $role->add_cap( 'read_private_proposals' );
    
    //permission for resource
    $role->add_cap( 'edit_resource' ); 
    $role->add_cap( 'publish_resource' ); 
    $role->add_cap( 'edit_resources' ); 
    $role->add_cap( 'publish_resources' ); 
    $role->add_cap( 'edit_others_resources' ); 
    $role->add_cap( 'read_private_resources' );
    
    //Editor role
    $role = get_role( 'contributor' );
    
    //permission for company
    $role->add_cap( 'edit_company' ); 
    $role->add_cap( 'publish_company' ); 
    $role->add_cap( 'edit_companies' ); 
    $role->add_cap( 'publish_companies' ); 
    
    //permission for resource
    $role->add_cap( 'edit_resource' ); 
    $role->add_cap( 'publish_resource' ); 
    $role->add_cap( 'edit_resources' ); 
    $role->add_cap( 'publish_resources' ); 
    
    $role = get_role( 'company' );
    
    //permission for company
    $role->add_cap( 'edit_company' ); 
    $role->add_cap( 'publish_company' ); 
    $role->add_cap( 'edit_companies' ); 
    $role->add_cap( 'publish_companies' ); 
    
    //permission for company
    $role->add_cap( 'edit_review' ); 
    $role->add_cap( 'publish_review' ); 
    $role->add_cap( 'edit_reviews' ); 
    $role->add_cap( 'publish_reviews' ); 
    
    //permission for company
    $role->add_cap( 'edit_proposal' ); 
    $role->add_cap( 'publish_proposal' ); 
    $role->add_cap( 'edit_proposal' ); 
    $role->add_cap( 'publish_proposal' ); 
    
}

