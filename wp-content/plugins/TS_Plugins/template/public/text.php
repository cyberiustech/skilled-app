<?php
/*
Example::
$ts->metabox(string $id, string $title, 'text', array $options = null, string $context = 'advanced', string $priority = 'default');
*/
?>
<div class="form-group">
	<label><?php echo $title; ?></label>
	<input type="text" class="form-control" name="<?php echo $id; ?>" value="<?php echo esc_attr($value); ?>" title="<?php echo $title; ?>" placeholder="<?php echo $input_label; ?>" <?php echo $input_attr; ?> />
</div>