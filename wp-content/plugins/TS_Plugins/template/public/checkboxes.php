<?php
/*
  Example::
  $options = array(
  'value1' => 'label1',
  'value2' => 'label2',
  'value3' => 'label3',
  'value4' => 'label4'
  );
  $ts->metabox(string $id, string $title, 'checkboxes', $options, string $context = 'advanced', string $priority = 'default');
 */
?>
<div class="form-group">
    <label><?php echo $title; ?></label>
    <?php
	if (!empty($options) && is_array($options))
	{
		foreach ($options as $key => $label)
		{
            $attr = $input_attr;
            $attr.= in_array($key, $value) ? ' checked="true"' : '';
            ?>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="<?php echo $id; ?>[]" value="<?php echo $key; ?>" title="<?php echo $title; ?>" <?php echo $attr; ?> />
        <?php echo $label; ?>
                </label>
            </div>
        <?php
    }
	}else{
		echo '<h1>Please Provide Array Option in 4th argument </h1>check out example script in '.__FILE__;
}
?>
</div>