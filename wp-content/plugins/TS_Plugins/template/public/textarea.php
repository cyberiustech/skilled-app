<?php
/*
Example::
$ts->metabox(string $id, string $title, 'textarea', array $options = null, string $context = 'advanced', string $priority = 'default');
*/
?>
<div class="form-group">
	<label><?php echo $title; ?></label>
	<textarea class="form-control" name="<?php echo $id; ?>" title="<?php echo $title; ?>" placeholder="<?php echo $input_label; ?>" <?php echo $input_attr; ?>><?php echo esc_attr($value); ?></textarea>
</div>