<?php
/*
Example::
$ts->metabox(string $id, string $title, 'upload', array $options = null, string $context = 'advanced', string $priority = 'default');
*/
$value = esc_attr($value);
?>
<div class="form-group">
	<label><?php echo $title; ?></label>
	<?php
	if (!empty($value))
	{
		?>
		<a href="<?php echo $value; ?>" target="_blank">
		<?php
		if (preg_match('~\.([^\.]+)$~', $value, $m))
		{
			switch ($m[1])
			{
				case 'jpg':
				case 'jpeg':
				case 'gif':
				case 'png':
				case 'bmp':
					?>
					<img src="<?php echo $value; ?>" alt="" style="max-width: 100%;" />
					<?php
					break;
				case 'swf':
					?>
					<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0" width="100%" height="150" />
						<param name="movie" value="<?php echo $value; ?>">
						<param name="quality" value="high">
						<embed src="<?php echo $value; ?>" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" width="100%" height="150" />
					</object>
					<?php
					break;
				default:
					echo $value;
					break;
			}
		}
		?>
		</a><br />
		<?php
	}
	?>
	<input type="file" class="form-control ts_uploader" name="<?php echo $id; ?>" id="<?php echo $id; ?>" value="<?php echo esc_attr($value); ?>" title="<?php echo $title; ?>" placeholder="<?php echo $input_label; ?>" <?php echo $input_attr; ?> />
	?>
</div>
