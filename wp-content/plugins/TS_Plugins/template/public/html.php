<?php
/*
Example::
$ts->metabox(string $id, string $title, 'html', array $options = null, string $context = 'advanced', string $priority = 'default');
*/
?>
<div class="form-group">
	<label><?php echo $title; ?></label>
	<?php wp_editor($value, $id.'editor', array('textarea_name' => $id)); ?>
</div>