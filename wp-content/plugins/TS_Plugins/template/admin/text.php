<?php
/*
Example::
$ts->metabox(string $id, string $title, 'text', array $options = null, string $context = 'advanced', string $priority = 'default');
*/
?>
<input type="text" class="form-control" name="<?php echo $id; ?>" value="<?php echo esc_attr($value); ?>" title="<?php echo $title; ?>" placeholder="<?php echo $input_label; ?>" style="width: 100%;" <?php echo $input_attr; ?> />