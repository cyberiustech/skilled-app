<?php
/*
Example::
$options = array(
	'value1' => 'label1',
	'value2' => 'label2',
	'value3' => 'label3',
	'value4' => 'label4'
	);
$ts->metabox(string $id, string $title, 'select', $options, string $context = 'advanced', string $priority = 'default');
*/
?>
<select class="form-control" name="<?php echo $id; ?>" title="<?php echo $title; ?>" placeholder="<?php echo $input_label; ?>" style="width: 100%;" <?php echo $input_attr; ?> />
	<?php
	foreach ($options as $key => $label)
	{
		$attr = ($value == $key) ? ' selected' : '';
		?>
		<option value="<?php echo $key; ?>"<?php echo $attr ?>><?php echo $label; ?></option>
		<?php
	}
	?>
</select>