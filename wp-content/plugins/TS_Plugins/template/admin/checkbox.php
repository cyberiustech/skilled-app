<?php
/*
Example::
$options = array(
	'input_label' => 'Yes, I agree';
	);
$ts->metabox(string $id, string $title, 'checkbox', $options, string $context = 'advanced', string $priority = 'default');
*/
?>
<div class="checkbox">
	<label>
		<input type="checkbox" name="<?php echo $id; ?>" value="1" title="<?php echo $title; ?>" <?php echo $input_attr; ?> />
		<?php echo !empty($input_label) ? $input_label : $title; ?>
	</label>
</div>