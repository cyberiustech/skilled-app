<?php
/*
  Plugin Name: Company
  Plugin URI: http://totalstudio.co/
  Description: This plugin is created by Total Studio to manage company information which is used by Skilled Inc.
  Version: 0.1
  Author: Danang Widiantoro
  Author URI: https://totalstudio.co/about-us/
  Text Domain: totalstudio
 */
add_action('init', 'ts_company_init', 0);

function ts_company_init() {
    if (!function_exists('ts_plugin')) {

        function company_plugin_failed() {
            ?>
            <div class="notice notice-error">
                <p><?php _e('TS Company Plugin only works along TS Plugin.', 'skilled'); ?></p>
            </div>
            <?php
        }

        add_action('admin_notices', 'company_plugin_failed');
    } else {

        $ts = ts_plugin('company', 'Company', 'Companies');

        $slug = "company";
        switch (DB_NAME) {
            case "skilled_fr":
                $slug = "entreprise";
                break;
            case "skilled_nl":
                $slug = "bedrijf";
                break;
        }
        // DEFINE POST TYPE
        $args = array(
            'supports' => array('title', 'comments'),
            'has_archive' => false,
            'rewrite' => [
                "slug" => $slug,
                "feeds" => true
            ],
            'capability_type' => ['company', 'companies'],
        );
        $ts->postype($args);

        // FETCH COMPANY GUIDES
        $guides = array();
        query_posts(array('post_type' => 'organization', 'posts_per_page' => -1, 'post_status' => 'publish', 'orderby' => 'id', order => 'asc'));
        while (have_posts()) {
            the_post();
            $guides[get_the_ID()] = get_the_title();
        }
        wp_reset_query();

        $ts->taxonomy("company_country", "Country", "Countries", "", []);
        $ts->metabox("_company_guide", 'Company Guide', 'checkboxes', $guides);
        $ts->metabox("_company_type", 'Account Type', 'select', [3 => "Organic", 1 => "Badged", 2 => "Sponsored"]);
    }
}

//if (function_exists('ts_plugin')) {

add_action('cmb2_init', 'cmb2_company_information');

function cmb2_company_information() {

    $prefix = '_company_';

    $tabs = array(
        "Profile Summary",
        "Profile Detail",
        "Location",
        "Review"
    );
    $navTabs = '<ul class="nav nav-tabs" role="tablist">';
    $tabPane = array();
    foreach ($tabs as $key => $tab) {
        /*  $navTabs .='<li class="nav-item ">
          <a class="nav-link '.(($key === 0) ? "active": false).'" data-toggle="tab" href="#tab'.$key.'" role="tab">'.$tab.'</a>
          </li>'; */
        $tabPane[$key] = '</div><div class="tab-pane" id="tab' . $key . '" role="tabpanel">';
    }
    $navTabs .= '</ul>';
    $tab_open = '<div class="custom_tabs">' . $navTabs . '<div class="tab-content"><div class="tab-pane active" id="tab0" role="tabpanel">';
    $tab_close = '</div></div></div>';


    /**
     * Initiate the metabox
     */
    $cmb = new_cmb2_box(array(
        'id' => 'company_information',
        'title' => __('Company Information', 'cmb2'),
        'object_types' => array('company'), // Post type
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true
    ));

    $cmb->add_field(array(
        'name' => 'Logo',
        'desc' => 'Upload an image or enter an URL.',
        'id' => $prefix . 'logo',
        'type' => 'file',
        'options' => array(
            'url' => false,
        ),
        'text' => array(
            'add_upload_file_text' => 'Add File'
        ),
    ));

    $cmb->add_field(array(
        'name' => __('Website URL', 'cmb2'),
        'before_row' => $tab_open,
        'desc' => __('', 'cmb2'),
        'id' => $prefix . 'url',
        'type' => 'text_url'
    ));
    $cmb->add_field(array(
        'name' => __('Phone', 'cmb2'),
        'desc' => __('', 'cmb2'),
        'id' => $prefix . 'phone',
        'type' => 'text'
    ));
    $cmb->add_field(array(
        'name' => __('Email', 'cmb2'),
        'desc' => __('', 'cmb2'),
        'id' => $prefix . 'email',
        'type' => 'text_email'
    ));

    $cmb->add_field(array(
        'name' => 'Company Rate',
        'id' => $prefix . 'rate',
        'type' => 'select',
        'options_cb' => 'cmb2_get_term_options',
        'get_terms_args' => array(
            'taxonomy' => 'company_rate_term',
            'hide_empty' => false,
        )
    ));

    $cmb->add_field(array(
        'name' => 'Company Size',
        'id' => $prefix . 'employee',
        'type' => 'select',
        'options_cb' => 'cmb2_get_term_options',
        'get_terms_args' => array(
            'taxonomy' => 'company_size_term',
            'hide_empty' => false,
        )
    ));

    $cmb->add_field(array(
        'name' => 'Project Size',
        'id' => $prefix . 'size',
        'type' => 'select',
        'options_cb' => 'cmb2_get_term_options',
        'get_terms_args' => array(
            'taxonomy' => 'project_size_term',
            'hide_empty' => false,
        )
    ));

    $cmb->add_field(array(
        'after_row' => $tabPane[1],
        'name' => __('Company summary', 'skilled'),
        'id' => $prefix . 'summary',
        'type' => 'wysiwyg',
        'options' => array(
            'wpautop' => false,
            'textarea_rows' => 3,
            'media_buttons' => false,
        ),
    ));
    //    $cmb->add_field(array(
    //        'name' => __('Keys Client', 'skilled'),
    //        'id' => $prefix . 'client_key',
    //        'type' => 'wysiwyg',
    //        'options' => array(
    //            'textarea_rows' => 3,
    //            'media_buttons' => false,
    //        ),
    //    ));
    $cmb->add_field(array(
        'name' => __('Certifications', 'skilled'),
        'id' => $prefix . 'certification',
        'type' => 'wysiwyg',
        'options' => array(
            'wpautop' => false,
            'textarea_rows' => 3,
            'media_buttons' => false,
        ),
    ));
    $cmb->add_field(array(
        'name' => __('Software Used', 'cmb2'),
        'desc' => __('', 'cmb2'),
        'id' => $prefix . 'software',
        'type' => 'text'
    ));
    $cmb->add_field(array(
        'after_row' => $tabPane[2],
        'name' => __('Awards', 'cmb2'),
        'desc' => __('', 'cmb2'),
        'id' => $prefix . 'awards',
        'type' => 'text'
    ));

    $group_field_id = $cmb->add_field(array(
        'id' => $prefix . 'location',
        'type' => 'group',
        'repeatable' => true,
        'options' => array(
            'group_title' => __('Location {#}', 'cmb2'),
            'add_button' => __('Add Another Location', 'cmb2'),
            'remove_button' => __('Remove Location', 'cmb2'),
            'sortable' => true,
            'closed' => false,
        ),
    ));
    $cmb->add_group_field($group_field_id, array(
        'before_row' => $tabPane[3],
        'id' => $prefix . 'location',
        'type' => 'address',
    ));

    $group_field_id = $cmb->add_field(array(
        'id' => $prefix . 'service',
        'type' => 'group',
        'repeatable' => true,
        'options' => array(
            'group_title' => __('Service {#}', 'cmb2'),
            'add_button' => __('Add Another Service', 'cmb2'),
            'remove_button' => __('Remove Service', 'cmb2'),
            'sortable' => true,
            'closed' => false,
        ),
    ));
    $cmb->add_group_field($group_field_id, array(
        'id' => $prefix . 'service_title',
        'name' => __('Title', 'skilled'),
        'type' => 'text',
    ));
    $cmb->add_group_field($group_field_id, array(
        'id' => $prefix . 'service_description',
        'name' => __('Description', 'skilled'),
        'type' => 'textarea',
    ));


    $group_field_id = $cmb->add_field(array(
        'id' => $prefix . 'portfolio',
        'type' => 'group',
        'repeatable' => true,
        'options' => array(
            'group_title' => __('Portfolio {#}', 'cmb2'),
            'add_button' => __('Add Another Portfolio', 'cmb2'),
            'remove_button' => __('Remove Portfolio', 'cmb2'),
            'sortable' => true,
            'closed' => false,
        ),
    ));
    $cmb->add_group_field($group_field_id, array(
        'id' => $prefix . 'portfolio_title',
        'name' => __('Title', 'skilled'),
        'type' => 'text',
    ));
    $cmb->add_group_field($group_field_id, array(
        'id' => $prefix . 'portfolio_description',
        'name' => __('About', 'skilled'),
        'type' => 'textarea',
    ));
    $cmb->add_group_field($group_field_id, array(
        'id' => $prefix . 'portfolio_client',
        'name' => __('Client', 'skilled'),
        'type' => 'textarea',
    ));
    $cmb->add_group_field($group_field_id, array(
        'id' => $prefix . 'portfolio_url',
        'name' => __('Full URL', 'skilled'),
        'type' => 'text',
    ));
    $cmb->add_group_field($group_field_id, array(
        'id' => $prefix . 'portfolio_img',
        'name' => __('Full URL', 'skilled'),
        'type' => 'file',
        'options' => array(
            'url' => false,
        ),
        'text' => array(
            'add_upload_file_text' => 'Add File'
        ),
    ));

    $cmb->add_field(
            array(
                'after_row' => $tab_close,
                'type' => 'title',
                'id' => 'close_title'
            )
    );
}

add_action('admin_menu', 'sponsored_company_list');

function sponsored_company_list() {
    add_menu_page('Sponsored Company', 'Sponsored Companies', 'manage_options', 'TS_Company/sponsored.php', '', 'dashicons-star-filled', 90);
    add_menu_page('Drafted Company', 'Draft Companies', 'manage_options', 'TS_Company/drafted.php', '', 'dashicons-vault', 90);
    //add_menu_page('Company Guide Migration', 'Company Guide Migration', 'manage_options', 'TS_Company/migration.php', '', 'dashicons-image-rotate-right', 90);
}

add_shortcode("skilled_company_companyguide_migration", "skilled_migrate");

function skilled_migrate() {
    $terms = get_terms([
        "taxonomy" => "service_term",
        "orderby" => "count",
        "order" => "desc"
    ]);
    echo "<pre>";
    print_r($terms);
    echo "</pre>";
}

add_shortcode("skilled_update_meta", "skilled_meta");

function skilled_meta() {
    global $wpdb;
    $args = array(
        'post_type' => 'company',
        'posts_per_page' => -1
    );
    $the_query = new WP_Query($args);
    while ($the_query->have_posts()) : $the_query->the_post();
        $service = get_the_terms(get_the_ID(), 'service_term');

        if ($service) {
            $guide = null;
            foreach ($service as $key) {
                $posttitle = $key->name;
                $postid = $wpdb->get_var("SELECT ID FROM $wpdb->posts WHERE post_title = '" . $posttitle . "' and post_type = 'organization'");
                if ($postid) {
                    $postid = $postid;
                } else {
                    $postid = wp_insert_post(array('post_title' => $key->name, 'post_type' => 'organization', 'post_status' => 'publish'));
                    //$postid = 0;
                }
                $guide[] = $postid;
            }
            if (get_post_meta($post->ID, '_company_guide', FALSE)) {
                update_post_meta(get_the_ID(), "_company_guide", $guide);
            } else {
                add_post_meta(get_the_ID(), "_company_guide", $guide);
            }
            echo get_the_title() . "= updated <br>";
        }
    endwhile;
}

add_shortcode('skilled_sponsored_company', 'display_sponsored_company');

function display_sponsored_company() {
    $action = $_GET['action'];
    $
            $paged = $_GET['paged'] != "" ? $_GET['paged'] : 1;
    $posts_per_page = 10;

    if (empty($action)) {
        $all_companies = get_posts([
            "posts_per_page" => -1,
            "post_type" => "company",
            "meta_key" => "_company_type",
            "meta_value" => 2
        ]);
        $companies = get_posts([
            "posts_per_page" => $posts_per_page,
            "paged" => $paged,
            "post_type" => "company",
            "meta_key" => "_company_type",
            "meta_value" => 2
        ]);
        ob_start();
        ?>
        <style>
            .skilled_company_logo{
                max-width: 96px;
                max-height: 48px;
                width: auto;
                height: auto;
                display: block;
            }
        </style>
        <div class="wrap">
            <h1>Sponsored Company</h1>
            <hr class="wp-header-end">
            <br>
            <table class="wp-list-table widefat fixed striped pages">
                <thead>
                    <tr>
                        <th style="width: 40px; text-align: center;">ID</th>
                        <th style="width: 80px; text-align: center;"> </th>
                        <th>Company Name</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($companies as $c) {
                        $summary = get_post_meta($c->ID, "_company_summary");
                        $logo = get_post_meta($c->ID, "_company_logo");
                        $type = get_post_meta($c->ID, "_company_type");

                        $sponsored = get_post_meta($c->ID, "_company_sponsored");

                        echo "<tr>";
                        echo "<td>" . $c->ID . "</td>";
                        echo "<td>" . "<img class='skilled_company_logo' src='" . $logo[0] . "'>" . "</td>";
                        echo "<td>" . $c->post_title . "<br>";
                        echo "<span class='trash'>";
                        echo " <a class='submitdelete' href='" . get_bloginfo('url') . "/wp-admin/post.php?post=" . $c->ID . "&action=edit'>Edit company</a>";
                        echo "</span> | ";
                        echo "<span class='trash'>";
                        echo " <a class='submitdelete' href='" . get_the_permalink($c->ID) . "'>View company</a>";
                        echo "</span>";
                        echo "</td>";
                        echo "<td>";
                        if (!empty($sponsored[0]) && $sponsored[0] == "yes") {
                            echo "<span style='color: green; font-weight: bold;'>Sponsor Active</span>";
                        } else {
                            echo "<span style='color: red; font-weight: bold;'>Sponsor Not Active</span>";
                        }
                        echo "</td>";
                        echo "<td>";
                        if (!empty($sponsored[0]) && $sponsored[0] == "yes") {
                            echo "<span class='trash'>";
                            echo " <a class='button-secondary' href='" . get_home_url() . "/wp-admin/admin.php?page=TS_Company/sponsored.php&cid=" . $c->ID . "&action=change&status=no'>Cancel Sponsor</a>";
                            echo "</span>";
                        } else {
                            echo " <a class='button-primary' href='" . get_home_url() . "/wp-admin/admin.php?page=TS_Company/sponsored.php&cid=" . $c->ID . "&action=change&status=yes'>Approve Sponsor</a>";
                        }
                        echo "</td>";
                        echo "</tr>";
                    }
                    ?>
                </tbody>
            </table>
            <?php styleForThisPageOnlyLOL(); ?>
            <div class="pagination-links">
                <?php
                $num_of_pages = ceil(count($all_companies) / $posts_per_page);

                for ($i = 1; $i <= $num_of_pages; $i++) {
                    if ($i == $paged) {
                        echo "<a class='active' href='admin.php?page=TS_Company%2Fdrafted.php&paged=" . $i . "'>" . $i . "</a>";
                    } else {
                        echo "<a href='admin.php?page=TS_Company%2Fdrafted.php&paged=" . $i . "'>" . $i . "</a>";
                    }
                }
                ?>
            </div>
        </div>
        <?php
        $display = ob_get_clean();
        echo $display;
    } elseif ($action == "change") {
        $new_status = $_GET['status'];
        $company_id = $_GET['cid'];
        update_post_meta($company_id, "_company_sponsored", $new_status);
        echo "<meta http-equiv='refresh' content='0;url=" . get_home_url() . "/wp-admin/admin.php?page=TS_Company/sponsored.php&changed=true'>";
    }
}

add_shortcode('skilled_drafted_company', 'display_drafted_company');

function display_drafted_company() {
    $action = $_GET['action'];
    $paged = $_GET['paged'] != "" ? $_GET['paged'] : 1;
    $posts_per_page = 10;

    if (empty($action)) {
        $all_companies = get_posts([
            "posts_per_page" => -1,
            "post_type" => "company",
            "post_status" => "draft",
        ]);
        $companies = get_posts([
            "posts_per_page" => $posts_per_page,
            "paged" => $paged,
            "post_type" => "company",
            "post_status" => "draft",
        ]);
        ob_start();
        ?>
        <style>
            .skilled_company_logo{
                max-width: 96px;
                max-height: 48px;
                width: auto;
                height: auto;
                display: block;
            }
        </style>
        <div class="wrap">
            <h1>Draft Company</h1>
            <hr class="wp-header-end">
            <br>
            <table class="wp-list-table widefat fixed striped pages">
                <thead>
                    <tr>
                        <th style="width: 40px; text-align: center;">ID</th>
                        <th style="width: 80px; text-align: center;"> </th>
                        <th>Company Name</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($companies as $c) {
                        $summary = get_post_meta($c->ID, "_company_summary");
                        $logo = get_post_meta($c->ID, "_company_logo");
                        $type = get_post_meta($c->ID, "_company_type");

                        $sponsored = get_post_meta($c->ID, "_company_sponsored");

                        echo "<tr>";
                        echo "<td>" . $c->ID . "</td>";
                        echo "<td>" . "<img class='skilled_company_logo' src='" . $logo[0] . "'>" . "</td>";
                        echo "<td>" . $c->post_title . "<br>";
                        echo "<span class='trash'>";
                        echo " <a class='submitdelete' href='" . get_bloginfo('url') . "/wp-admin/post.php?post=" . $c->ID . "&action=edit'>Edit company</a>";
                        echo "</span> | ";
                        echo "<span class='trash'>";
                        echo " <a class='submitdelete' href='" . get_the_permalink($c->ID) . "'>View company</a>";
                        echo "</span>";
                        echo "</td>";
                        echo "<td>";
                        echo "<span style='color: green; font-weight: bold;'>" . get_post_status($c->ID) . "</span>";
                        echo "</td>";
                        echo "<td>";
                        if (get_post_status($c->ID) == 'publish') {
                            echo "<span class='trash'>";
                            echo " <a class='submitdelete' href='" . get_home_url() . "/wp-admin/admin.php?page=TS_Company/drafted.php&cid=" . $c->ID . "&action=change&status=draft'>Unpublish</a>";
                            echo "</span>";
                        } else {
                            echo " <a class='button-primary' href='" . get_home_url() . "/wp-admin/admin.php?page=TS_Company/drafted.php&cid=" . $c->ID . "&action=change&status=publish'>Publish</a>";
                        }
                        echo "</td>";
                        echo "</tr>";
                    }
                    ?>
                </tbody>
            </table>
            <?php styleForThisPageOnlyLOL(); ?>
            <div class="pagination-links">
                <?php
                $num_of_pages = ceil(count($all_companies) / $posts_per_page);

                for ($i = 1; $i <= $num_of_pages; $i++) {
                    if ($i == $paged) {
                        echo "<a class='active' href='admin.php?page=TS_Company%2Fdrafted.php&paged=" . $i . "'>" . $i . "</a>";
                    } else {
                        echo "<a href='admin.php?page=TS_Company%2Fdrafted.php&paged=" . $i . "'>" . $i . "</a>";
                    }
                }
                ?>
            </div>
        </div>
        <?php
        $display = ob_get_clean();
        echo $display;
    } elseif ($action == "change") {
        $new_status = $_GET['status'];
        $company_id = $_GET['cid'];
        // Update post 37
        $my_post = array(
            'ID' => $company_id,
            'post_status' => $new_status,
        );

        wp_update_post($my_post);
        echo "<meta http-equiv='refresh' content='0;url=" . get_home_url() . "/wp-admin/admin.php?page=TS_Company/drafted.php&changed=true'>";
    }
}

function styleForThisPageOnlyLOL() {
    ?>
    <style>
        .pagination-links{
            margin-top: 1rem;
        }
        .pagination-links a{
            display: inline-block;
            width: 28px;
            height: 28px;
            line-height: 28px;
            text-align: center;
            border: 1px solid #cacaca;
            background: #fdfdfd;
            margin-right: 4px;
            text-decoration: none;
        }
        .pagination-links a:hover{
            background: #efefef;
        }
        .pagination-links a.active{
            background: #efefef;
        }
    </style>
    <?php
}



//
// function save_company_meta( $post_id, $post, $update ) {

//     /*
//      * In production code, $slug should be set only once in the plugin,
//      * preferably as a class property, rather than in each function that needs it.
//      */
//     $post_type = get_post_type($post_id);

//     // If this isn't a 'book' post, don't update it.
//     if ( "company" != $post_type ) return;

//     // - Update the post's metadata.

//    update_post_meta($post_id, '_company_post_title', get_the_title());

//     $employee = get_term_by('id', absint($_POST['_company_employee']), 'company_size_term');
//     update_post_meta($post_id, '_company_employee', $employee->slug);

//     $size = get_term_by('id', absint($_POST['_company_size']), 'project_size_term');
//     update_post_meta($post_id, '_company_size', $size->slug);

//     $rate = get_term_by('id', absint($_POST['_company_rate']), 'company_rate_term');
//     update_post_meta($post_id, '_company_rate', $rate->slug);
// }
// add_action( 'save_post', 'save_company_meta', 10, 3 );