<?php

/*
  Plugin Name: Prevent duplicate title
  Plugin URI: http://faqihamruddin.com
  Description: This plugin made with love by faqih amruddin yusuf.
  Version: 1.0
  Author: Faqih Amruddin Yusuf
  Author URI: http://faqihamruddin.com
  License: GPLv2
 */

function disallow_posts_with_same_title($messages) {
    global $post;
    global $wpdb ;
    $title = $post->post_title;
    $post_id = $post->ID ;
    $wtitlequery = "SELECT post_title FROM $wpdb->posts WHERE post_status = 'publish' AND post_type = 'company' AND post_title = '{$title}' AND ID != {$post_id} " ;
 
    $wresults = $wpdb->get_results( $wtitlequery) ;
 
    if ( $wresults ) {
        $error_message = 'This title is already used. Please choose another';
        add_settings_error('post_has_links', '', $error_message, 'error');
        settings_errors( 'post_has_links' );
        $post->post_status = 'trash';
        wp_update_post($post);
        //wp_delete_post($post->ID);
        return;
    }else{
      $new_slug = sanitize_title( $post->post_title );
      if ( $post->post_name != $new_slug )
      {
          wp_update_post(
              array (
                  'ID'        => $post->ID,
                  'post_name' => $new_slug
              )
          );
      }
    }
    return $messages;

}
add_action('post_updated_messages', 'disallow_posts_with_same_title');
