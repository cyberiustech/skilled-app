<?php

// Register Custom Taxonomy
function customer_size_taxonomy() {
	$labels = array(
		'name'                       => _x( 'Customer Sizes', 'Taxonomy General Name', 'skilled' ),
		'singular_name'              => _x( 'Customer Size', 'Taxonomy Singular Name', 'skilled' ),
		'menu_name'                  => __( 'Customer Sizes', 'skilled' ),
		'all_items'                  => __( 'All Items', 'skilled' ),
		'parent_item'                => __( 'Parent Item', 'skilled' ),
		'parent_item_colon'          => __( 'Parent Item:', 'skilled' ),
		'new_item_name'              => __( 'New Item Name', 'skilled' ),
		'add_new_item'               => __( 'Add New Item', 'skilled' ),
		'edit_item'                  => __( 'Edit Item', 'skilled' ),
		'update_item'                => __( 'Update Item', 'skilled' ),
		'view_item'                  => __( 'View Item', 'skilled' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'skilled' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'skilled' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'skilled' ),
		'popular_items'              => __( 'Popular Items', 'skilled' ),
		'search_items'               => __( 'Search Items', 'skilled' ),
		'not_found'                  => __( 'Not Found', 'skilled' ),
		'no_terms'                   => __( 'No items', 'skilled' ),
		'items_list'                 => __( 'Items list', 'skilled' ),
		'items_list_navigation'      => __( 'Items list navigation', 'skilled' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => false,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => false,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'customer_size_term', array( 'company' ), $args );
}
add_action( 'init', 'customer_size_taxonomy', 0 );