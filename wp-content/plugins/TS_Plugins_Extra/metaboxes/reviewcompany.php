<?php

/*
add_action( 'cmb2_init', 'cmb2_review_company_information' );
function cmb2_review_company_information() {

    $prefix = '_review_';

    $tabs = array(
        "Overview",
        "Rating",
        "Project",
        "Contact"
        );
    $navTabs = '<ul class="nav nav-tabs" role="tablist">';
    $tabPane = array();
    foreach ($tabs as $key => $tab) {
        $navTabs .='<li class="nav-item ">
        <a class="nav-link '.(($key === 0) ? "active": false).'" data-toggle="tab" href="#tab'.$key.'" role="tab">'.$tab.'</a>
    </li>';
    $tabPane[$key] = '</div><div class="tab-pane" id="tab'.$key.'" role="tabpanel">';
}
$navTabs .='</ul>';
$tab_open = '<div class="custom_tabs">'.$navTabs.'<div class="tab-content"><div class="tab-pane active" id="tab0" role="tabpanel">';
$tab_close = '</div></div></div>';



$cmb = new_cmb2_box( array(
    'id'            => 'review_information',
    'title'         => __( 'Review Company Information', 'cmb2' ),
    'object_types'  => array( 'review' ),
    'context'       => 'normal',
    'priority'      => 'high',
    'show_names'    => true
    )
);
$cmb->add_field( array(
    'id'         => $prefix . 'type',
    'default' => 'company',
    'type' => 'hidden',
) );
$cmb->add_field( array(
    'name'        => __( 'Company being reviewed *' ),
    'id'         => $prefix . 'company_id',
    'type'           => 'select',
    'options_cb'     => 'cmb2_get_posttype_options',
    'get_posttype_args' => array(
        'post_type'   => 'company',
        'post_status' => 'publish'
        )
    ) );
$cmb->add_field(array(

    'name'       => __( 'Project name', 'skilled' ),
    'id'         => $prefix . 'post_title',
    'type'       => 'text',
    ) );
 $cmb->add_field( array(

        'name'    => 'Image',
        'desc'    => 'Upload an image or enter an URL.',
        'id'         => $prefix . 'logo',
        'type'    => 'file',
        'options' => array(
            'url' => false,
            ),
        'text'    => array(
            'add_upload_file_text' => 'Add File'
            ),
        ) );
 $cmb->add_field(array(

    'name'       => __( 'Project Tagline', 'skilled' ),
    'id'         => $prefix . 'tagline',
    'type'       => 'text',
    ) );
$cmb->add_field( array(
    'before_row' => $tab_open,
    'name'       => __( 'Background', 'skilled' ),
    'id'         => $prefix . 'background',
    'type'       => 'wysiwyg',
    'options'    => array(
        'textarea_rows' => 3,
        'media_buttons' => false,
        ),
    ) );
$cmb->add_field( array(

    'name'       => __( 'OPPORTUNITY / CHALLENGE', 'skilled' ),
    'id'         => $prefix . 'opportunity',
    'type'       => 'wysiwyg',
    'options'    => array(
        'textarea_rows' => 3,
        'media_buttons' => false,
        ),
    ) );
$cmb->add_field( array(

    'name'       => __( 'Solution', 'skilled' ),
    'id'         => $prefix . 'solution',
    'type'       => 'wysiwyg',
    'options'    => array(
        'textarea_rows' => 3,
        'media_buttons' => false,
        ),
    ) );
$cmb->add_field( array(
    'name'       => __( 'Result', 'skilled' ),
    'id'         => $prefix . 'result',
    'type'       => 'wysiwyg',
    'options'    => array(
        'textarea_rows' => 3,
        'media_buttons' => false,
        ),
    ) );





$cmb->add_field( array(
    'before_row' => $tabPane[1],
    'name'       => __( 'Quality', 'cmb2' ),
    'desc'       => __( '', 'cmb2' ),
    'id'         => $prefix . 'rate_quality',
    'type'       => 'star_rating'
    ) );


$cmb->add_field( array(
    'name'       => __( 'Schedule', 'cmb2' ),
    'desc'       => __( '', 'cmb2' ),
    'id'         => $prefix . 'rate_schedule',
    'type'       => 'star_rating'
    ) );




$cmb->add_field( array(
    'name'       => __( 'Cost', 'cmb2' ),
    'desc'       => __( '', 'cmb2' ),
    'id'         => $prefix . 'rate_cost',
    'type'       => 'star_rating'
    ) );
$cmb->add_field( array(
    'name'       => __( 'Willing to refer', 'cmb2' ),
    'desc'       => __( '', 'cmb2' ),
    'id'         => $prefix . 'rate_recomended',
    'type'       => 'star_rating'
    ) );

$cmb->add_field( array(
    'name'       => __( 'Overal Rating', 'cmb2' ),
    'desc'       => __( '', 'cmb2' ),
    'id'         => $prefix . 'rate_overal',
    'type'       => 'star_rating'
    ) );


$cmb->add_field( array(
    'before_row' => $tabPane[2],
        'name'       => __( 'Location', 'cmb2' ),
        'desc'       => __( '', 'cmb2' ),
        'id'         => $prefix . 'location',
        'type'       => 'text'
        ) );


 $cmb->add_field( array(
        'name'       => __( 'Employee', 'cmb2' ),
        'desc'       => __( '', 'cmb2' ),
        'id'         => $prefix . 'employee',
        'type'       => 'text'
        ) );
  $cmb->add_field( array(
        'name'       => __( 'Rate', 'cmb2' ),
        'desc'       => __( '', 'cmb2' ),
        'id'         => $prefix . 'rate',
        'type'       => 'text'
        ) );


$cmb->add_field( array(
    'before_row' => $tabPane[3],
    'name'       => __( 'Attribution *', 'cmb2' ),
    'desc'       => __( 'By default reviews are attributed. Select anonymous to have all personally identifiable information removed.', 'cmb2' ),
    'id'         => $prefix . 'contact_attribution',
    'type'    => 'radio',
    'options' => array(
        'attributed' => 'Attributed',
        'anonymous' => 'Anonymous'
        )
    ) );
 $cmb->add_field( array(
        'name'       => __( 'Full Name', 'cmb2' ),
        'desc'       => __( '', 'cmb2' ),
        'id'         => $prefix . 'fullname',
        'type'       => 'text'
        ) );
 $cmb->add_field( array(
        'name'       => __( 'Company', 'cmb2' ),
        'desc'       => __( '', 'cmb2' ),
        'id'         => $prefix . 'company',
        'type'       => 'text'
        ) );

   $cmb->add_field( array(
        'name'       => __( 'Position', 'cmb2' ),
        'desc'       => __( '', 'cmb2' ),
        'id'         => $prefix . 'position',
        'type'       => 'text'
        ) );
 $cmb->add_field( array(
        'name'       => __( 'Phone', 'cmb2' ),
        'desc'       => __( '', 'cmb2' ),
        'id'         => $prefix . 'phone',
        'type'       => 'text'
        ) );


$cmb->add_field(
    array(
        'after_row' => $tab_close,
        'type' => 'title',
        'id'   => 'close_title'
        )
    );
}
*/