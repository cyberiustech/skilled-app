<?php


//add_action( 'cmb2_init', 'cmb2_resource_information' );



/**
 * Define the metabox and field configurations.
 *
function cmb2_resource_information() {

    $prefix = '_resource_';


    /**
     * Initiate the metabox
     *


    $cmb = new_cmb2_box( array(
        'id'            => 'resource_information',
        'title'         => __( 'EMBED CODE', 'cmb2' ),
        'object_types'  => array( 'resources' ), // Post type
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true
        ) );
    
    $cmb->add_field(array(

        'name'       => __( 'Infographic URL', 'skilled' ),
        'id'         => $prefix . 'infographic_url',
        'type'       => 'text',
        ) );

    $cmb->add_field(array(

        'name'       => __( 'Backlink URL (optional)', 'skilled' ),
        'desc' => __('Default is current post url', 'skilled'),
        'id'         => $prefix . 'backlink_url',
        'type'       => 'text',
        ) );


    $group_field_id = $cmb->add_field( array(
        'id'          => $prefix.'reff',
        'type'        => 'group',
        'repeatable'  => true, 
        'options'     => array(
            'group_title'   => __( 'Reff {#}', 'cmb2' ),
            'add_button'    => __( 'Add Another Reff', 'cmb2' ),
            'remove_button' => __( 'Remove Reff', 'cmb2' ),
            'sortable'      => true, 
            'closed'     => false,
            ),
        ) );
    $cmb->add_group_field( $group_field_id, array(
        'id'   => $prefix . 'reff_url',
        'name'       => __( 'URL', 'skilled' ),
        'type' => 'text',
        ) );
    $cmb->add_group_field( $group_field_id, array(
        'id'   => $prefix . 'reff_follow',
        'name'       => __( 'Do Follow', 'skilled' ),
        'type'             => 'radio',
        'show_option_none' => true,
        'options'          => array(
            'yes'   => __( 'Yes', 'cmb2' )
            )
        ) );

    
}
*/
