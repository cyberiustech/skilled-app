<?php

/**
 * Returns options markup for a country select field.
 * @param  mixed $value Selected/saved state
 * @return string       html string containing all country options
 */
function cmb2_get_country_options($value = false) {
    $list = get_terms('company_country', [
        'hide_empty' => false
    ]);

    $country_options = '';
    foreach ($list as $li) {
        $country_options .= '<option value="' . $li->name . '" ' . selected($value, $li->name, false) . '>' . $li->name . '</option>';
    }
    return $country_options;
}

/**
 * Returns options markup for a state select field.
 * @param  mixed $value Selected/saved state
 * @return string       html string containing all state options
 */
function cmb2_get_state_options($value = false) {
    $state_list = array(
        'AL' => 'Alabama',
        'AK' => 'Alaska',
        'AZ' => 'Arizona',
        'AR' => 'Arkansas',
        'CA' => 'California',
        'CO' => 'Colorado',
        'CT' => 'Connecticut',
        'DE' => 'Delaware',
        'DC' => 'District Of Columbia',
        'FL' => 'Florida',
        'GA' => 'Georgia',
        'HI' => 'Hawaii',
        'ID' => 'Idaho',
        'IL' => 'Illinois',
        'IN' => 'Indiana',
        'IA' => 'Iowa',
        'KS' => 'Kansas',
        'KY' => 'Kentucky',
        'LA' => 'Louisiana',
        'ME' => 'Maine',
        'MD' => 'Maryland',
        'MA' => 'Massachusetts',
        'MI' => 'Michigan',
        'MN' => 'Minnesota',
        'MS' => 'Mississippi',
        'MO' => 'Missouri',
        'MT' => 'Montana',
        'NE' => 'Nebraska',
        'NV' => 'Nevada',
        'NH' => 'New Hampshire',
        'NJ' => 'New Jersey',
        'NM' => 'New Mexico',
        'NY' => 'New York',
        'NC' => 'North Carolina',
        'ND' => 'North Dakota',
        'OH' => 'Ohio',
        'OK' => 'Oklahoma',
        'OR' => 'Oregon',
        'PA' => 'Pennsylvania',
        'RI' => 'Rhode Island',
        'SC' => 'South Carolina',
        'SD' => 'South Dakota',
        'TN' => 'Tennessee',
        'TX' => 'Texas',
        'UT' => 'Utah',
        'VT' => 'Vermont',
        'VA' => 'Virginia',
        'WA' => 'Washington',
        'WV' => 'West Virginia',
        'WI' => 'Wisconsin',
        'WY' => 'Wyoming'
    );

    $state = [];
    query_posts([
        'post_type' => 'skilled_state',
        'posts_per_page' => -1
    ]);
    $i = 0;
    while (have_posts()) {
        the_post();
        $id = get_the_ID();
        $term = get_the_terms(get_the_ID(), "skilled_country");
        $state[$id] = strtoupper($term[0]->slug) . " - " . get_the_title();
    }
    wp_reset_query();
    $state_options = '';
    foreach ($state as $abrev => $state) {
        $state_options .= '<option value="' . $abrev . '" ' . selected($value, $abrev, false) . '>' . $state . '</option>';
    }
    return $state_options;
}

function cmb2_get_city_options($value = false) {
    $list = [];
    query_posts([
        'post_type' => 'skilled_country_city',
        'posts_per_page' => -1,
        'orderby' => 'title',
        'order' => 'ASC'
    ]);

    $i = 0;
    while (have_posts()) {
        the_post();
        $id = get_the_ID();
        $meta = get_post_meta($id, "city-select-state");
        $the_state = get_the_title($meta[0]);
        $term = get_the_terms($meta[0], "skilled_country");

        $list[$id] = strtoupper($term[0]->slug) . " - " . $the_state . " - " . get_the_title();
    }
    wp_reset_query();

    $state_options = '';
    foreach ($list as $abrev => $state) {
        $state_options .= '<option value="' . $abrev . '" ' . selected($value, $abrev, false) . '>' . $state . '</option>';
    }
    return $state_options;
}
