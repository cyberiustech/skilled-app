<?php


/**
 * Gets a number of terms and displays them as options
 * @param  CMB2_Field $field 
 * @return array An array of options that matches the CMB2 options array
 */
function cmb2_get_posttype_options( $field ) {
    $args = $field->args( 'get_posttype_args' );
    $args = is_array( $args ) ? $args : array();

    $args = wp_parse_args( $args, array( 'post_type' => 'company' ) );

    $query = new WP_Query($args);
    $result = array();
    if($query->posts)
        foreach($query->posts as $post) {
            $result[$post->ID] = $post->post_title;
        }

    return $result;
}