<?php


/**
 * Gets a number of terms and displays them as options
 * @param  CMB2_Field $field 
 * @return array An array of options that matches the CMB2 options array
 */
function cmb2_get_term_options( $field ) {
    $args = $field->args( 'get_terms_args' );
    $args = is_array( $args ) ? $args : array();

    $args = wp_parse_args( $args, array( 'taxonomy' => 'category' ) );

    $taxonomy = $args['taxonomy'];

    $terms = (array) cmb2_utils()->wp_at_least( '4.5.0' )
        ? get_terms( $args )
        : get_terms( $taxonomy, $args );

    // Initate an empty array
    $term_options = array();
    if ( ! empty( $terms ) ) {
        foreach ( $terms as $term ) {
            $term_options[ $term->slug ] = $term->name;
        }
    }
    return $term_options;
}



/**
 * Gets a number of terms and displays them as options
 * @param  CMB2_Field $field 
 * @return array An array of options that matches the CMB2 options array
 */
function cmb2_get_term_options_string_multi( $args, $value = false ) {
  
    $args = is_array( $args ) ? $args : array();

    $args = wp_parse_args( $args, array( 'taxonomy' => 'category', 'parent' => 0 ) );

    $taxonomy = $args['taxonomy'];
    

   $terms = (array) cmb2_utils()->wp_at_least( '4.5.0' )
        ? get_terms( $args )
        : get_terms( $taxonomy, $args );
    

    // Initate an empty array
    $term_options = '';
    if ( ! empty( $terms ) ) {
        foreach ( $terms as $term ) {
            $term_options .= '<option value="'. $term->slug .'" '. selected( $value, $term->slug , false ) .'>'. $term->name .'</option>';
          
        }
    }
   
    return $term_options;
}