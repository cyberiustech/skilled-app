<?php

/**
 * Render Address Field
 */
function cmb2_render_address_field_callback($field, $value, $object_id, $object_type, $field_type) {
    $value = wp_parse_args($value, array(
        'address-1' => '',
        'address-2' => '',
        'city' => '',
        'state' => '',
        'country' => '',
        'zip' => '',
        'phone' => ''
    ));
    ?>
    <style>
        .row{
            width: 100%;
            display: block;
        }
        .form-group label{
            display: block;
            width: 50%;
            margin: 0.5rem 0rem;
        }
        .form-group .form-control{
            display: block;
            width: 50%;
            margin-bottom: 1rem;
            height: 36px;
        }
    </style>
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="form-group">
                <label  for="<?php echo $field_type->_id('_address_1'); ?>"><?php _e('Address 1'); ?></label>

                <?php
                echo $field_type->input(array(
                    'class' => 'form-control',
                    'name' => $field_type->_name('[address-1]'),
                    'id' => $field_type->_id('_address_1'),
                    'value' => $value['address-1']
                ));
                ?>

            </div>
        </div>
        <div class="col-xs-12 col-md-4">
            <div class="form-group">
                <label  for="<?php echo $field_type->_id('_address_2'); ?>"><?php _e('Address 2'); ?></label>

                <?php
                echo $field_type->input(array(
                    'class' => 'form-control',
                    'name' => $field_type->_name('[address-2]'),
                    'id' => $field_type->_id('_address_2'),
                    'value' => $value['address-2']
                ));
                ?>
            </div>
        </div>
        <div class="col-xs-12 col-md-4">
            <div class="form-group">
                <label  for="<?php echo $field_type->_id('_phone'); ?>"><?php _e('Phone'); ?></label>

                <?php
                echo $field_type->input(array(
                    'class' => 'form-control',
                    'name' => $field_type->_name('[phone]'),
                    'id' => $field_type->_id('_phone'),
                    'value' => $value['phone']
                ));
                ?>
            </div>
        </div>
        <div class="col-xs-12 col-md-4">
            <div class="form-group">
                <label  for="<?php echo $field_type->_id('_zip'); ?>"><?php _e('Zip'); ?></label>

                <?php
                echo $field_type->input(array(
                    'class' => 'form-control',
                    'name' => $field_type->_name('[zip]'),
                    'id' => $field_type->_id('_zip'),
                    'value' => $value['zip']
                ));
                ?>
            </div>
        </div>
        <div class="col-xs-12 col-md-4">
            <div class="form-group">
                <label  for="<?php echo $field_type->_id('_city'); ?>"><?php _e('City'); ?></label>

                <?php
                echo $field_type->input(array(
                    'class' => 'form-control',
                    'name' => $field_type->_name('[city]'),
                    'id' => $field_type->_id('_city'),
                    'value' => $value['city']
                ));
                ?>
            </div>
        </div>
        <div class="col-xs-12 col-md-4">
            <div class="form-group">
                <label  for="<?php echo $field_type->_id('_state'); ?>"><?php _e('State'); ?></label>

                <?php
                echo $field_type->input(array(
                    'class' => 'form-control',
                    'name' => $field_type->_name('[new_state]'),
                    'id' => $field_type->_id('_state'),
                    'value' => $value['new_state']
                ));
                ?>
            </div>
        </div>
        <div class="col-xs-12 col-md-4">
            <div class="form-group">
                <label  for="<?php echo $field_type->_id('_country'); ?>"><?php _e('Country');?></label>

                <?php
                echo $field_type->select(array(
                    'class' => 'form-control',
                    'name' => $field_type->_name('[new_country]'),
                    'id' => $field_type->_id('_country'),
                    'options' => cmb2_get_country_options($value['new_country']),
                    'value'=> $value['new_country']
                ));
                ?>
            </div>
        </div>
    </div>
    <?php
}

add_filter('cmb2_render_address', 'cmb2_render_address_field_callback', 10, 5);
