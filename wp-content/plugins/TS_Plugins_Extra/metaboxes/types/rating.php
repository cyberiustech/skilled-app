<?php

function cmb2_render_star_rating_field_callback( $field, $value, $object_id, $object_type, $field_type ) {
	$value = wp_parse_args($value, array(
		'rating' => 0,
		'detail' => ''
		) );
	?>
		<section id="<?php echo $field_type->_id( false ); ?>">
			<fieldset>
				<span class="star-cb-group">
				<select class="form-control" id="<?php echo $field_type->_name( '[rating]' ) ?>" name="<?php echo $field_type->_name( '[rating]' ) ?>">
					<?php
						
						for ( $y = 0; $y < 11; $y++ ) {
							?>
								<option value="<?php echo $y; ?>" <?php if($y == $value['rating']) { ?>selected="selected" <?php } ?> >
								<?php echo $y; ?>
								</option>
							<?php
							
						}
					?>
					</select>
				</span>
			</fieldset>
			
			<div class="form-group">
				<label  for="<?php echo $field_type->_id( '_detail' ); ?>"><?php _e('Detail'); ?></label><br />
				<?php echo $field_type->textarea( array(
					'class' => 'form-control',
					'name'  => $field_type->_name( '[detail]' ),
					'id'    => $field_type->_id( '_detail' ),
					'value' => $value['detail']
					) ); ?>
			</div>
		</section>
	<?php
	echo $field_type->_desc( true );
}
add_filter( 'cmb2_render_star_rating', 'cmb2_render_star_rating_field_callback', 10, 5 );