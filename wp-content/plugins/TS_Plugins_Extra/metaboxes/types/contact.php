<?php


/**
 * Render Address Field
 */
function cmb2_render_contact_field_callback( $field, $value, $object_id, $object_type, $field_type ) {
	$value = wp_parse_args($value, array(
		'company' => '',
		'position' => '',
		'first_name' => '',
		'last_name' => '',
		'phone' => '',
		'email'      => '',
		'detail'     => ''
		) );
	?>
	<div class="row">
		<div class="col-xs-12 col-md-6">
			<div class="form-group">
				<label  for="<?php echo $field_type->_id( '_first_name' ); ?>"><?php _e('First Name', 'skilled'); ?></label>
				<?php echo $field_type->input( array(
					'class' => 'form-control',
					'name'  => $field_type->_name( '[first_name]' ),
					'id'    => $field_type->_id( '_first_name' ),
					'value' => $value['first_name']
					) ); ?>
			</div>
			<div class="form-group">
				<label  for="<?php echo $field_type->_id( '_last_name' ); ?>"><?php _e('Last Name', 'skilled'); ?></label>
				<?php echo $field_type->input( array(
					'class' => 'form-control',
					'name'  => $field_type->_name( '[last_name]' ),
					'id'    => $field_type->_id( '_last_name' ),
					'value' => $value['last_name']
					) ); ?>
			</div>
			<div class="form-group">
				<label  for="<?php echo $field_type->_id( '_phone' ); ?>"><?php _e('Phone', 'skilled'); ?></label>
				
				<?php echo $field_type->input(array(
					'class' => 'form-control',
					'name'  => $field_type->_name( '[phone]' ),
					'id'    => $field_type->_id( '_phone' ),
					'value' => $value['phone']
					) ); ?>
			</div>
			<div class="form-group">
				<label  for="<?php echo $field_type->_id( '_email' ); ?>"><?php _e('Email', 'skilled'); ?></label>
				<?php echo $field_type->input( array(
					'class' => 'form-control',
					'name'  => $field_type->_name( '[email]' ),
					'id'    => $field_type->_id( '_email' ),
					'value' => $value['email']
					) ); ?>
			</div>
		</div>
		<div class="col-xs-12 col-md-6">
			<div class="form-group">
				<label  for="<?php echo $field_type->_id( '_detail' ); ?>"><?php _e('Detail'); ?></label>
				<?php echo $field_type->wysiwyg( array(
					'class' => 'form-control',
					'name'  => $field_type->_name( '[detail]' ),
					'id'    => $field_type->_id( '_detail' ),
					'value' => $value['detail']
					) ); ?>
			</div>
		</div>
		
	</div>
	<?php
}
add_filter( 'cmb2_render_contact', 'cmb2_render_contact_field_callback', 10, 5 );