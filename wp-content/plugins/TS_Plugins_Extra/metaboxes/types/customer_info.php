<?php


/**
 * Render Address Field
 */
function cmb2_render_customer_info_field_callback( $field, $value, $object_id, $object_type, $field_type ) {
	
	$value = wp_parse_args($value, array(
		'size' => '',
		'budget' => '',
		'service' => '',
		'phone' => '',
		'email'      => '',
		'detail'     => ''
		) );
	?>
	<div class="row">
		<div class="col-xs-12 col-md-6">
			<div class="form-group">
				<label  for="<?php echo $field_type->_id( '_size' ); ?>"><?php _e('Company Size', 'skilled'); ?></label>
				<?php echo $field_type->select( array(
					'class' => 'form-control',
					'name'  => $field_type->_name( '[size]' ),
					'id'    => $field_type->_id( '_size' ),
					'options'  => cmb2_get_term_options_string_multi( 
						array(
							'taxonomy'   => 'customer_size_term',
							'hide_empty' => false
							),
						$value['size']
						)
					) ); ?>
			</div>
			<div class="form-group">
				<label  for="<?php echo $field_type->_id( '_industry' ); ?>"><?php _e('Industry', 'skilled'); ?></label>
				<?php echo $field_type->select( array(
					'class' => 'form-control',
					'name'  => $field_type->_name( '[industry]' ),
					'id'    => $field_type->_id( '_industry' ),
					'options'  => cmb2_get_term_options_string_multi( 
						array(
							'taxonomy'   => 'industry_term',
							'hide_empty' => false
							),
						$value['industry']
						)
					)
				) ; ?>
			</div>
		</div>
		<div class="col-xs-12 col-md-6">
			<div class="form-group">
				<label  for="<?php echo $field_type->_id( '_detail' ); ?>"><?php _e('Detail'); ?></label>
				<?php echo $field_type->wysiwyg( array(
					'class' => 'form-control',
					'name'  => $field_type->_name( '[detail]' ),
					'id'    => $field_type->_id( '_detail' ),
					'value' => $value['detail']
					) ); ?>
			</div>
		</div>
		
	</div>
	<?php
}
add_filter( 'cmb2_render_customer_info', 'cmb2_render_customer_info_field_callback', 10, 5 );