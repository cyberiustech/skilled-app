<?php


	function cmb2_render_industry_field_callback( $field, $value, $object_id, $object_type, $field_type ) {
		$taxonomy = array('industry_term');
		$terms = get_terms($taxonomy, array(
			"orderby"    => "count",
			'parent'            => 0,
			'hierarchical'      => true,
			"hide_empty" => false
			)
		);

		?>
		<div id="industry_total" class="card c_input_counter">
	<div class="card-header"><?php _e('Industry focus total =', 'skilled'); ?> <span class="counter_percent">0%</span></div>
	
	</div>
		<div class="cmb-row cmb-repeat-group-wrap cmb-type-group cmb2-id-industry">
			<div data-groupid="industry" id="industry_repeat" class="cmb-nested cmb-field-list cmb-repeatable-group non-sortable non-repeatable" >
				<div class="postbox cmb-row cmb-repeatable-grouping" data-iterator="0">
					<h3 class="cmb-group-title cmbhandle-title"><span>Industry</span></h3>
					<div class="inside cmb-td cmb-nested cmb-field-list">
						<div class="row">
							<?php
							foreach($terms as $term) {  ?>
								<div class="col-xs-6 col-md-3">
									<div class="form-group">
										<label  for="<?php echo $field_type->_id( $term->slug); ?>"><?php echo $term->name; ?></label>
										<div class="input-group">
											<?php echo $field_type->input( array(
												'class' => 'form-control industry_input_country',
												'name'  => $field_type->_name( '['.$term->slug.']' ),
												'id'    => $field_type->_id('_'.$term->slug.''),
												'value' => isset($value[$term->slug]) ? $value[$term->slug] : 0,
												'type'  => 'number',
												) ); ?>
											<div class="input-group-addon">%</div>
										</div>
									</div>
								</div>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php

		}
		add_filter( 'cmb2_render_industry', 'cmb2_render_industry_field_callback', 10, 5 );