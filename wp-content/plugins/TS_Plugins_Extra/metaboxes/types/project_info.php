<?php


/**
 * Render Address Field
 */
function cmb2_render_project_info_field_callback( $field, $value, $object_id, $object_type, $field_type ) {
	
	$value = wp_parse_args($value, array(
		'name' => '',
		'budget' => '',
		'service' => '',
		'phone' => '',
		'email'      => '',
		'detail'     => ''
		) );
	?>
	<div class="row">
		<div class="col-xs-12 col-md-6">
			
			<div class="form-group">
				<label  for="<?php echo $field_type->_id( '_budget' ); ?>"><?php _e('Budget', 'skilled'); ?></label>
				<?php echo $field_type->select( array(
					'class' => 'form-control',
					'name'  => $field_type->_name( '[budget]' ),
					'id'    => $field_type->_id( '_budget' ),
					'options'  => cmb2_get_term_options_string_multi( 
						array(
							'taxonomy'   => 'project_size_term',
							'hide_empty' => false
							),
						$value['budget']
						)
					) ); ?>
			</div>
			<div class="form-group">
				<label  for="<?php echo $field_type->_id( '_service' ); ?>"><?php _e('Service', 'skilled'); ?></label>
				<?php echo $field_type->select( array(
					'class' => 'form-control',
					'name'  => $field_type->_name( '[service]' ),
					'id'    => $field_type->_id( '_service' ),
					'options'  => cmb2_get_term_options_string_multi( 
						array(
							'taxonomy'   => 'service_term',
							'hide_empty' => false
							),
						$value['service']
						)
					)
				) ; ?>
			</div>
			<div class="form-group">
				<label  for="<?php echo $field_type->_id( '_budget' ); ?>"><?php _e('Industry', 'skilled'); ?></label>
				<?php echo $field_type->select( array(
					'class' => 'form-control',
					'name'  => $field_type->_name( '[industry]' ),
					'id'    => $field_type->_id( '_industry' ),
					'options'  => cmb2_get_term_options_string_multi( 
						array(
							'taxonomy'   => 'service_term',
							'hide_empty' => false
							),
						$value['industry']
						)
					)
				) ; ?>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label  for="<?php echo $field_type->_id( '_start_date' ); ?>"><?php _e('Start', 'skilled'); ?></label>
						<?php echo $field_type->text_date( array(

							'name'  => $field_type->_name( '[start_date]' ),
							'id'    => $field_type->_id( '_start_date' ),
							'value' => $value['start_date']
							)
						) ; ?>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label  for="<?php echo $field_type->_id( '_end_date' ); ?>"><?php _e('End', 'skilled'); ?></label>
						<?php echo $field_type->text_date( array(

							'name'  => $field_type->_name( '[end_date]' ),
							'id'    => $field_type->_id( '_end_date' ),
							'value' => $value['end_date']
							)
						) ; ?>
					</div>
				</div>
				<div class="col-md-12">
				</div>
			</div>
			
			
			
		</div>
		<div class="col-xs-12 col-md-6">
			<div class="form-group">
				<label  for="<?php echo $field_type->_id( '_detail' ); ?>"><?php _e('Detail'); ?></label>
				<?php echo $field_type->wysiwyg( array(
					'class' => 'form-control',
					'name'  => $field_type->_name( '[detail]' ),
					'id'    => $field_type->_id( '_detail' ),
					'value' => $value['detail']
					) ); ?>
			</div>
		</div>
		
	</div>
	<?php
}
add_filter( 'cmb2_render_project_info', 'cmb2_render_project_info_field_callback', 10, 5 );