<?php


function cmb2_render_service_field_callback( $field, $value, $object_id, $object_type, $field_type ) {
	$taxonomy = array('service_term');
	$terms = get_terms($taxonomy, array(
		"orderby"    => "count",
		'parent'            => 0,
		'hierarchical'      => true,
		"hide_empty" => false
		)
	); ?>

	<div id="service_total" class="card c_input_counter">
	<div class="card-header"><?php _e('Service Line total =', 'skilled'); ?> <span class="counter_percent">0%</span></div>
	<div class="card-block"><p><?php _e('Indicate 1-5 service lines that your company offers. For the service line(s) you choose, provide the percent focus. The percent of the chosen service lines must add up to 100. Max 5 service lines.', 'skilled'); ?></p></div>
	</div>
	<?php
	foreach($terms as $term) {
		$subterms = get_terms($taxonomy,array(
			'parent'   => $term->term_id,
			'hide_empty' => false
			)); ?>
		<div class="cmb-row cmb-repeat-group-wrap cmb-type-group cmb2-id-<?php echo $term->slug; ?>">
			<div data-groupid="<?php echo $term->slug; ?>" id="<?php echo $term->slug; ?>_repeat" class="cmb-nested cmb-field-list cmb-repeatable-group non-sortable non-repeatable" >
				<div class="postbox cmb-row cmb-repeatable-grouping" data-iterator="0">
					<h3 class="cmb-group-title cmbhandle-title"><span><?php echo $term->name; ?></span></h3>
					<div class="inside cmb-td cmb-nested cmb-field-list">
						<div class="row">
							<?php
							foreach ( $subterms as $subterm ) { ?>
								<div class="col-xs-6 col-md-3">
									<div class="form-group">
										<label  for="<?php echo $field_type->_id( $subterm->slug); ?>"><?php echo $subterm->name; ?></label>
										<div class="input-group">
											<?php echo $field_type->input( array(
												'class' => 'form-control service_input_counter',
												'name'  => $field_type->_name( '['.$subterm->slug.']' ),
												'id'    => $field_type->_id('_'.$subterm->slug.''),
												'value' => isset($value[$subterm->slug]) ? $value[$subterm->slug] : 0,
												'type'  => 'number',
												) ); ?>
											<div class="input-group-addon">%</div>
										</div>
									</div>
								</div>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php
		}
	}
	add_filter( 'cmb2_render_service', 'cmb2_render_service_field_callback', 10, 5 );