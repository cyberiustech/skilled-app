<?php


add_action( 'cmb2_init', 'cmb2_solution_information' );



/**
 * Define the metabox and field configurations.
 */
function cmb2_solution_information() {

    $prefix = '_solution_';

    $tabs = array(
        "Software Summary",
        "Pricing"
        );
    $navTabs = '<ul class="nav nav-tabs" role="tablist">';
    $tabPane = array();
    foreach ($tabs as $key => $tab) {
        $navTabs .='<li class="nav-item ">
        <a class="nav-link '.(($key === 0) ? "active": false).'" data-toggle="tab" href="#tab'.$key.'" role="tab">'.$tab.'</a>
    </li>';
    $tabPane[$key] = '</div><div class="tab-pane" id="tab'.$key.'" role="tabpanel">';
}
$navTabs .='</ul>';
$tab_open = '<div class="custom_tabs">'.$navTabs.'<div class="tab-content"><div class="tab-pane active" id="tab0" role="tabpanel">';
$tab_close = '</div></div></div>';


    /**
     * Initiate the metabox
     */


    $cmb = new_cmb2_box( array(
        'id'            => 'solution_information',
        'title'         => __( 'Solution Information', 'cmb2' ),
        'object_types'  => array( 'solution' ), // Post type
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true
        ) );
    $cmb->add_field(array(
        'before_row' => $tab_open,
        'name'       => __( 'Software name', 'skilled' ),
        'id'         => $prefix . 'post_title',
        'type'       => 'text',
        ) );
    $cmb->add_field( array(
        'name'     => 'Software category',
        'id'       => $prefix.'software_type',
        'type'           => 'select',
        'options_cb'     => 'cmb2_get_term_options',
        'get_terms_args' => array(
            'taxonomy'   => 'software_type_term',
            'hide_empty' => false,
            )
        ) );


    $cmb->add_field( array(
        'name'    => 'Logo',
        'desc'    => 'Upload an image or enter an URL.',
        'id'         => $prefix . 'logo',
        'type'    => 'file',
        'options' => array(
            'url' => false,
            ),
        'text'    => array(
            'add_upload_file_text' => 'Add File' 
            ),
        ) );

    $cmb->add_field( array(
        'name'       => __( 'Tagline', 'cmb2' ),
        'desc'       => __( '', 'cmb2' ),
        'id'         => $prefix . 'tagline',
        'type'       => 'text'
        ) );
    
    $cmb->add_field( array(

        'name'       => __( 'Directory short description', 'skilled' ),
        'id'         => $prefix . 'shortdesc',
        'type'       => 'wysiwyg',
        'options'    => array(
            'textarea_rows' => 3,
            'media_buttons' => false,
            ),
        ) );
    $cmb->add_field( array(

        'name'       => __( 'Software summary', 'skilled' ),
        'id'         => $prefix . 'summary',
        'type'       => 'wysiwyg',
        'options'    => array(
            'textarea_rows' => 3,
            'media_buttons' => false,
            ),
        ) );
    $cmb->add_field( array(
        'name'       => __( 'Website URL', 'cmb2' ),
        'desc'       => __( '', 'cmb2' ),
        'id'         => $prefix . 'url',
        'type'       => 'text'
        ) );
    $cmb->add_field( array(
        'name'       => __( 'Key CLients', 'skilled' ),
        'desc'      => __('Enter names of key clients. If confidential, use descriptions e.g. Fortune 500 packaged goods company (500 character maximum).','skilled'),
        'id'         => $prefix . 'client_key',
        'type'       => 'wysiwyg',
        'options'    => array(
            'textarea_rows' => 3,
            'media_buttons' => false,
            ),
        ) );
    $cmb->add_field( array(
        'name'     => 'Employees',
        'id'       => $prefix.'company_size',
        'type'           => 'select',
        'options_cb'     => 'cmb2_get_term_options',
        'get_terms_args' => array(
            'taxonomy'   => 'company_size_term',
            'hide_empty' => false,
            )
        ) );

    $cmb->add_field( array(
        'id'         => $prefix . 'support',
        'name'       => __( 'Support', 'skilled' ),
        'desc'       => __('Please provide a short, bulleted list of support options (email, chat, phone, etc.)', 'skilled'),
        'type'       => 'wysiwyg',
        'options'    => array(
            'textarea_rows' => 3,
            'media_buttons' => false,
            ),
        ) );
    

    $cmb->add_field( array(
        'before_row' => $tabPane[1],
        'id'   => $prefix . 'pricing_summary',
        'name'       => __( 'Pricing Summary', 'skilled' ),
        'type'       => 'wysiwyg',
        'options'    => array(
            'textarea_rows' => 3,
            'media_buttons' => false,
            ),
        ) );
    $cmb->add_field( array(
        'name'       => __( 'Price range', 'cmb2' ),
        'desc'       => __( '', 'cmb2' ),
        'id'         => $prefix . 'pricing_range',
        'type'       => 'text'
        ) );
    $cmb->add_field( array(
        'name'       => __( 'Pricing options', 'cmb2' ),
        'desc'       => __( '', 'cmb2' ),
        'id'         => $prefix . 'pricing_option',
        'type'    => 'multicheck',
        'options' => array(
            'Monthly subscription' => 'Monthly subscription',
            'Annual subscription' => 'Annual subscription',
            'One time license' => 'One time license',
            )
        ) );
    $cmb->add_field( array(
        'id'   => $prefix . 'pricing_plan_summary',
        'name'       => __( 'Pricing plan summary', 'skilled' ),
        'type'       => 'wysiwyg',
        'options'    => array(
            'textarea_rows' => 3,
            'media_buttons' => false,
            ),
        ) );
    $cmb->add_field( array(
        'name'       => __( 'Pricing Page URL', 'cmb2' ),
        'desc'       => __( '', 'cmb2' ),
        'id'         => $prefix . 'pricing_url',
        'type'       => 'text'
        ) );
    $cmb->add_field( array(
        'name'       => __( 'Price Min', 'cmb2' ),
        'desc'       => __( '', 'cmb2' ),
        'id'         => $prefix . 'pricing_min',
        'type'       => 'text'
        ) );
    $cmb->add_field( array(
        'name'       => __( 'Price Max', 'cmb2' ),
        'desc'       => __( '', 'cmb2' ),
        'id'         => $prefix . 'pricing_max',
        'type'       => 'text'
        ) );
    $cmb->add_field( 
        array(
            'after_row' => $tab_close,
            'type' => 'title',
            'id'   => 'close_title'
            )
        );
}
