<?php


add_action( 'cmb2_init', 'cmb2_portfolio_information' );



/**
 * Define the metabox and field configurations.
 */
function cmb2_portfolio_information() {

    $prefix = '_portfolio_';

    $tabs = array(
        "Information"
        );
    $navTabs = '<ul class="nav nav-tabs" role="tablist">';
    $tabPane = array();
    foreach ($tabs as $key => $tab) {
        $navTabs .='<li class="nav-item ">
        <a class="nav-link '.(($key === 0) ? "active": false).'" data-toggle="tab" href="#tab'.$key.'" role="tab">'.$tab.'</a>
    </li>';
    $tabPane[$key] = '</div><div class="tab-pane" id="tab'.$key.'" role="tabpanel">';
}
$navTabs .='</ul>';
$tab_open = '<div class="custom_tabs">'.$navTabs.'<div class="tab-content"><div class="tab-pane active" id="tab0" role="tabpanel">';
$tab_close = '</div></div></div>';


    /**
     * Initiate the metabox
     */


    $cmb = new_cmb2_box( array(
        'id'            => 'portfolio_information',
        'title'         => __( 'Portfolio Information', 'cmb2' ),
        'object_types'  => array( 'portfolio' ), // Post type
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true
        ) );
    $cmb->add_field(array(
        'before_row' => $tab_open,
        'name'       => __( 'Title', 'skilled' ),
        'id'         => $prefix . 'post_title',
        'type'       => 'text',
        ) );
  
    $cmb->add_field( array(
        'name'    => 'Image',
        'desc'    => 'Upload an image or enter an URL.',
        'id'         => $prefix . 'image',
        'type'    => 'file',
        'options' => array(
            'url' => false,
            ),
        'text'    => array(
            'add_upload_file_text' => 'Add File' 
            ),
        ) );
    $cmb->add_field( array(
        'name'       => __( 'Media Embed', 'cmb2' ),
        'desc'       => __( 'Enter a youtube, vimeo URL', 'cmb2' ),
        'id'         => $prefix . 'media',
        'type'       => 'oembed'
        ) );

    $cmb->add_field( array(

        'name'       => __( 'Description', 'skilled' ),
        'id'         => $prefix . 'description',
        'type'       => 'wysiwyg',
        'options'    => array(
            'textarea_rows' => 3,
            'media_buttons' => false,
            ),
        ) );

    

    $cmb->add_field( 
        array(
            'after_row' => $tab_close,
            'type' => 'title',
            'id'   => 'close_title'
            )
        );
}
