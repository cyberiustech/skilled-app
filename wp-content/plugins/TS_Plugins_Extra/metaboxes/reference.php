<?php


add_action( 'cmb2_init', 'cmb2_reference_information' );



/**
 * Define the metabox and field configurations.
 */
function cmb2_reference_information() {

    $prefix = '_reference_';

    $tabs = array(
        "Contact",
        "Project Information"
        );
    $navTabs = '<ul class="nav nav-tabs" role="tablist">';
    $tabPane = array();
    foreach ($tabs as $key => $tab) {
        $navTabs .='<li class="nav-item ">
        <a class="nav-link '.(($key === 0) ? "active": false).'" data-toggle="tab" href="#tab'.$key.'" role="tab">'.$tab.'</a>
    </li>';
    $tabPane[$key] = '</div><div class="tab-pane" id="tab'.$key.'" role="tabpanel">';
}
$navTabs .='</ul>';
$tab_open = '<div class="custom_tabs">'.$navTabs.'<div class="tab-content"><div class="tab-pane active" id="tab0" role="tabpanel">';
$tab_close = '</div></div></div>';


    /**
     * Initiate the metabox
     */


    $cmb = new_cmb2_box( array(
        'id'            => 'reference_information',
        'title'         => __( 'Reference Information', 'cmb2' ),
        'object_types'  => array( 'reference' ), // Post type
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true
        ) );
    
    $cmb->add_field(array(
        
        'name'       => __( 'Name', 'skilled' ),
        'id'         => $prefix . 'post_title',
        'type'       => 'text',
        ) );

    $cmb->add_field( array(
        'name'       => __( 'Company', 'cmb2' ),
        'desc'       => __( '', 'cmb2' ),
        'id'         => $prefix . 'company',
        'type'       => 'text'
        ) );
   $cmb->add_field( array(
    'before_row' => $tab_open,
        'name'       => __( 'Position', 'cmb2' ),
        'desc'       => __( '', 'cmb2' ),
        'id'         => $prefix . 'position',
        'type'       => 'text'
        ) );
  
    $cmb->add_field( array(
         
        'name'       => __( 'Contact Information', 'cmb2' ),
        'desc'       => __( '', 'cmb2' ),
        'id'         => $prefix . 'contact_info',
        'type'       => 'contact'
        ) );
   $cmb->add_field( array(
        'before_row' => $tabPane[1],
        'name'       => __( 'Project Name', 'cmb2' ),
        'desc'       => __( '', 'cmb2' ),
        'id'         => $prefix . 'project_name',
        'type'       => 'text'
        ) );
    $cmb->add_field( array(
        
        'name'       => __( 'Project Detail', 'cmb2' ),
        'desc'       => __( '', 'cmb2' ),
        'id'         => $prefix . 'project_info',
        'type'       => 'project_info'
        ) );
  
    $cmb->add_field( 
        array(
            'after_row' => $tab_close,
            'type' => 'title',
            'id'   => 'close_title'
            )
        );
}
