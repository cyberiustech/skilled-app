<?php 
//add_action( 'cmb2_init', 'cmb2_company_information' );
//
//
//
///**
// * Define the metabox and field configurations.
// */
//function cmb2_company_information() {
//
//    $prefix = '_company_';
//
//    $tabs = array(
//        "Profile Summary",
//        "Profile Detail",
//        "Location",
//        "Review"
//        );
//    $navTabs = '<ul class="nav nav-tabs" role="tablist">';
//    $tabPane = array();
//    foreach ($tabs as $key => $tab) {
//      /*  $navTabs .='<li class="nav-item ">
//        <a class="nav-link '.(($key === 0) ? "active": false).'" data-toggle="tab" href="#tab'.$key.'" role="tab">'.$tab.'</a>
//    </li>'; */
//    $tabPane[$key] = '</div><div class="tab-pane" id="tab'.$key.'" role="tabpanel">';
//}
//$navTabs .='</ul>';
//$tab_open = '<div class="custom_tabs">'.$navTabs.'<div class="tab-content"><div class="tab-pane active" id="tab0" role="tabpanel">';
//$tab_close = '</div></div></div>';
//
//
//    /**
//     * Initiate the metabox
//     */
//
//
//    $cmb = new_cmb2_box( array(
//        'id'            => 'company_information',
//        'title'         => __( 'Company Information', 'cmb2' ),
//        'object_types'  => array( 'company' ), // Post type
//        'context'       => 'normal',
//        'priority'      => 'high',
//        'show_names'    => true
//        ) );
//    $cmb->add_field(array(
//        
//        'name'       => __( 'Company name', 'skilled' ),
//        'id'         => $prefix . 'post_title',
//        'type'       => 'text',
//        ) );
//
//
//
//    $cmb->add_field( array(
//        
//        'name'    => 'Logo',
//        'desc'    => 'Upload an image or enter an URL.',
//        'id'         => $prefix . 'logo',
//        'type'    => 'file',
//        'options' => array(
//            'url' => false,
//            ),
//        'text'    => array(
//            'add_upload_file_text' => 'Add File' 
//            ),
//        ) );
//
//    $cmb->add_field( array(
//        'name'       => __( 'Tagline', 'cmb2' ),
//        'desc'       => __( '', 'cmb2' ),
//        'id'         => $prefix . 'tagline',
//        'type'       => 'text'
//        ) );
//    $cmb->add_field( array(
//        'before_row' => $tab_open,
//        'name'       => __( 'Founded', 'cmb2' ),
//        'desc'       => __( '', 'cmb2' ),
//        'id'         => $prefix . 'founded',
//        'type'       => 'text'
//        ) );
//    $cmb->add_field( array(
//        'name'     => 'Employees',
//        'id'       => $prefix.'employee',
//        'type'           => 'select',
//        'options_cb'     => 'cmb2_get_term_options',
//        'get_terms_args' => array(
//            'taxonomy'   => 'company_size_term',
//            'hide_empty' => false,
//            )
//        ) );
//    $cmb->add_field( array(
//        'name'     => 'Min. project size',
//        'id'       => $prefix.'size',
//        'type'           => 'select',
//        'options_cb'     => 'cmb2_get_term_options',
//        'get_terms_args' => array(
//            'taxonomy'   => 'project_size_term',
//            'hide_empty' => false,
//            )
//        ) );
//    $cmb->add_field( array(
//        'name'     => 'Avg. hourly rate',
//        'id'       => $prefix.'rate',
//        'type'           => 'select',
//        'options_cb'     => 'cmb2_get_term_options',
//        'get_terms_args' => array(
//            'taxonomy'   => 'company_rate_term',
//            'hide_empty' => false,
//            )
//        ) );
//    $cmb->add_field( array(
//        'name' => __( 'Website URL', 'cmb2' ),
//        'desc' => __( '', 'cmb2' ),
//        'id'   => $prefix . 'url',
//        'type' => 'text_url'
//        ) );
//    $cmb->add_field( array(
//        'name' => __( 'Phone', 'cmb2' ),
//        'desc' => __( '', 'cmb2' ),
//        'id'   => $prefix . 'phone',
//        'type' => 'text'
//        ) );
//    $cmb->add_field( array(
//        'name' => __( 'Email', 'cmb2' ),
//        'desc' => __( '', 'cmb2' ),
//        'id'   => $prefix . 'email',
//        'type' => 'text_email'
//        ) );
//    $cmb->add_field( array(
//        'name' => __( 'Twitter', 'cmb2' ),
//        'desc' => __( '', 'cmb2' ),
//        'id'   => $prefix . 'twitter',
//        'type' => 'text'
//        ) );
//
//    $cmb->add_field( array(
//        'after_row' => $tabPane[1],
//        'name'       => __( 'Company summary', 'skilled' ),
//        'id'         => $prefix . 'summary',
//        'type'       => 'wysiwyg',
//        'options'    => array(
//            'textarea_rows' => 3,
//            'media_buttons' => false,
//            ),
//        ) );
//    $cmb->add_field( array(
//        'name'       => __( 'Keys Client', 'skilled' ),
//        'id'         => $prefix . 'client_key',
//        'type'       => 'wysiwyg',
//        'options'    => array(
//            'textarea_rows' => 3,
//            'media_buttons' => false,
//            ),
//        ) );
//    $cmb->add_field( array(
//        'name'       => __( 'Certifications', 'skilled' ),
//        'id'         => $prefix . 'certification',
//        'type'       => 'wysiwyg',
//        'options'    => array(
//            'textarea_rows' => 3,
//            'media_buttons' => false,
//            ),
//        ) );
//    $cmb->add_field( array(
//        'name'       => __( 'Accolades', 'skilled' ),
//        'id'         => $prefix . 'accolades',
//        'type'       => 'wysiwyg',
//        'options'    => array(
//            'textarea_rows' => 3,
//            'media_buttons' => false,
//            ),
//        ) );
//    $cmb->add_field( array(
//        'after_row' =>  $tabPane[2],
//        'name'       => __( 'Detailed description', 'skilled' ),
//        'id'         => $prefix . 'description',
//        'type'       => 'wysiwyg',
//        'options'    => array(
//            'textarea_rows' => 3,
//            'media_buttons' => false,
//            ),
//        ) );
//
//    $group_field_id = $cmb->add_field( array(
//        'id'          => $prefix.'location',
//        'type'        => 'group',
//        'repeatable'  => true, 
//        'options'     => array(
//            'group_title'   => __( 'Location {#}', 'cmb2' ),
//            'add_button'    => __( 'Add Another Location', 'cmb2' ),
//            'remove_button' => __( 'Remove Location', 'cmb2' ),
//            'sortable'      => true, 
//            'closed'     => false,
//            ),
//        ) );
//    $cmb->add_group_field( $group_field_id, array(
//        'id'   => $prefix . 'location',
//        'type' => 'address',
//        ) );
//
//    $cmb->add_field( array(
//        'before_row' => $tabPane[3],
//        'name'       => __( 'Quality', 'cmb2' ),
//        'desc'       => __( '', 'cmb2' ),
//        'id'         => $prefix . 'rate_quality',
//        'type'       => 'star_rating'
//        ) );
//
//
//    $cmb->add_field( array(
//        'name'       => __( 'Schedule', 'cmb2' ),
//        'desc'       => __( '', 'cmb2' ),
//        'id'         => $prefix . 'rate_schedule',
//        'type'       => 'star_rating'
//        ) );
//
//
//
//
//    $cmb->add_field( array(
//        'name'       => __( 'Cost', 'cmb2' ),
//        'desc'       => __( '', 'cmb2' ),
//        'id'         => $prefix . 'rate_cost',
//        'type'       => 'star_rating'
//        ) );
//    $cmb->add_field( array(
//        'name'       => __( 'Willing to refer', 'cmb2' ),
//        'desc'       => __( '', 'cmb2' ),
//        'id'         => $prefix . 'rate_recomended',
//        'type'       => 'star_rating'
//        ) );
//
//    $cmb->add_field( array(
//        'name'       => __( 'Overal Rating', 'cmb2' ),
//        'desc'       => __( '', 'cmb2' ),
//        'id'         => $prefix . 'rate_overal',
//        'type'       => 'star_rating'
//        ) );
//
//
//$group_field_id = $cmb->add_field( array(
//        'id'          => $prefix.'service',
//        'type'        => 'group',
//        'repeatable'  => true, 
//        'options'     => array(
//            'group_title'   => __( 'Service {#}', 'cmb2' ),
//            'add_button'    => __( 'Add Another Service', 'cmb2' ),
//            'remove_button' => __( 'Remove Service', 'cmb2' ),
//            'sortable'      => true, 
//            'closed'     => false,
//            ),
//        ) );
//    $cmb->add_group_field( $group_field_id, array(
//        'id'   => $prefix . 'service_title',
//        'name'       => __( 'Title', 'skilled' ),
//        'type' => 'text',
//        ) );
//     $cmb->add_group_field( $group_field_id, array(
//        'id'   => $prefix . 'service_description',
//        'name'       => __( 'Description', 'skilled' ),
//        'type' => 'textarea',
//        ) );
//
//
//     $group_field_id = $cmb->add_field( array(
//        'id'          => $prefix.'portfolio',
//        'type'        => 'group',
//        'repeatable'  => true, 
//        'options'     => array(
//            'group_title'   => __( 'Portfolio {#}', 'cmb2' ),
//            'add_button'    => __( 'Add Another Portfolio', 'cmb2' ),
//            'remove_button' => __( 'Remove Portfolio', 'cmb2' ),
//            'sortable'      => true, 
//            'closed'     => false,
//            ),
//        ) );
//    $cmb->add_group_field( $group_field_id, array(
//        'id'   => $prefix . 'portfolio_title',
//        'name'       => __( 'Title', 'skilled' ),
//        'type' => 'text',
//        ) );
//     $cmb->add_group_field( $group_field_id, array(
//        'id'   => $prefix . 'portfolio_description',
//        'name'       => __( 'About', 'skilled' ),
//        'type' => 'textarea',
//        ) );
//     $cmb->add_group_field( $group_field_id, array(
//        'id'   => $prefix . 'portfolio_client',
//        'name'       => __( 'Client', 'skilled' ),
//        'type' => 'textarea',
//        ) );
//   $cmb->add_group_field( $group_field_id, array(
//        'id'   => $prefix . 'portfolio_url',
//        'name'       => __( 'Full URL', 'skilled' ),
//        'type' => 'text',
//        ) );
//$cmb->add_group_field( $group_field_id, array(
//        'id'   => $prefix . 'portfolio_img',
//        'name'       => __( 'Full URL', 'skilled' ),
//        'type'    => 'file',
//        'options' => array(
//            'url' => false,
//            ),
//        'text'    => array(
//            'add_upload_file_text' => 'Add File' 
//            ),
//        ) );
//
//
//
//
//    $cmb->add_field( 
//        array(
//            'after_row' => $tab_close,
//            'type' => 'title',
//            'id'   => 'close_title'
//            )
//        );
//}
