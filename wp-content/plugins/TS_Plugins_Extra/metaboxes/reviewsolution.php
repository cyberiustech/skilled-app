<?php

/*
add_action( 'cmb2_init', 'cmb2_review_solution_information' );



/**
 * Define the metabox and field configurations.
 *
function cmb2_review_solution_information() {

    $prefix = '_review_';

    $tabs = array(
        "Overview",
        "Rating",
        "Company Information",
        "Contact"
        );
    $navTabs = '<ul class="nav nav-tabs" role="tablist">';
    $tabPane = array();
    foreach ($tabs as $key => $tab) {
        $navTabs .='<li class="nav-item ">
        <a class="nav-link '.(($key === 0) ? "active": false).'" data-toggle="tab" href="#tab'.$key.'" role="tab">'.$tab.'</a>
    </li>';
    $tabPane[$key] = '</div><div class="tab-pane" id="tab'.$key.'" role="tabpanel">';
}
$navTabs .='</ul>';
$tab_open = '<div class="custom_tabs">'.$navTabs.'<div class="tab-content"><div class="tab-pane active" id="tab0" role="tabpanel">';
$tab_close = '</div></div></div>';


    /**
     * Initiate the metabox
     *


    $cmb = new_cmb2_box( array(
        'id'            => 'reviewsolution_information',
        'title'         => __( 'Review Solution Information', 'cmb2' ),
        'object_types'  => array( 'review' ), // Post type
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true
        ) );
    $cmb->add_field( array(
    'id'         => $prefix . 'type',
    'default' => 'solution',
    'type' => 'hidden',
) );
    $cmb->add_field( array(
        'name'        => __( 'Solution being reviewed *' ),
        'id'         => $prefix . 'solution_id',
        'type'           => 'select',
        'options_cb'     => 'cmb2_get_posttype_options',
        'get_posttype_args' => array(
            'post_type'   => 'solution',
            'post_status' => 'publish'
            )
        ) );
    $cmb->add_field(array(
        'name'       => __( 'Review title *', 'skilled' ),
        'id'         => $prefix . 'post_title',
        'type'       => 'text',
        ) );
    $cmb->add_field( array(
        'before_row' => $tab_open,
        'name'       => __( 'Background', 'skilled' ),
        'id'         => $prefix . 'background',
        'type'       => 'wysiwyg',
        'options'    => array(
            'textarea_rows' => 3,
            'media_buttons' => false,
            ),
        ) );
    $cmb->add_field( array(

        'name'       => __( 'Experience', 'skilled' ),
        'id'         => $prefix . 'experience',
        'type'       => 'wysiwyg',
        'options'    => array(
            'textarea_rows' => 3,
            'media_buttons' => false,
            ),
        ) );
    $cmb->add_field( array(

        'name'       => __( 'Pros', 'skilled' ),
        'id'         => $prefix . 'pros',
        'type'       => 'wysiwyg',
        'options'    => array(
            'textarea_rows' => 3,
            'media_buttons' => false,
            ),
        ) );
    $cmb->add_field( array(
        'name'       => __( 'Cons', 'skilled' ),
        'id'         => $prefix . 'cons',
        'type'       => 'wysiwyg',
        'options'    => array(
            'textarea_rows' => 3,
            'media_buttons' => false,
            ),
        ) );
    $cmb->add_field( array(
        'name'       => __( 'Recomendation', 'skilled' ),
        'id'         => $prefix . 'recomendation',
        'type'       => 'wysiwyg',
        'options'    => array(
            'textarea_rows' => 3,
            'media_buttons' => false,
            ),
        ) );





    $cmb->add_field( array(
        'before_row' => $tabPane[1],
        'name'       => __( 'Features', 'cmb2' ),
        'desc'       => __( '', 'cmb2' ),
        'id'         => $prefix . 'rate_feature',
        'type'       => 'star_rating'
        ) );



    $cmb->add_field( array(
        'name'       => __( 'Ease of use', 'cmb2' ),
        'desc'       => __( '', 'cmb2' ),
        'id'         => $prefix . 'rate_friendly',
        'type'       => 'star_rating'
        ) );



    $cmb->add_field( array(
        'name'       => __( 'Support', 'cmb2' ),
        'desc'       => __( '', 'cmb2' ),
        'id'         => $prefix . 'rate_support',
        'type'       => 'star_rating'
        ) );




    $cmb->add_field( array(
        'name'       => __( 'Willing to refer', 'cmb2' ),
        'desc'       => __( '', 'cmb2' ),
        'id'         => $prefix . 'rate_recomended',
        'type'       => 'star_rating'
        ) );



    $cmb->add_field( array(
        'name'       => __( 'Overal Rating', 'cmb2' ),
        'desc'       => __( '', 'cmb2' ),
        'id'         => $prefix . 'rate_overal',
        'type'       => 'star_rating'
        ) );

    $cmb->add_field( array(
       'before_row' => $tabPane[2],
       'name'     => 'Company Detail',
       'id'       => $prefix.'customer',
       'type'           => 'customer_info',
       ) );

    $cmb->add_field( array(
        'before_row' => $tabPane[3],
        'name'       => __( 'Attribution *', 'cmb2' ),
        'desc'       => __( 'By default reviews are attributed. Select anonymous to have all personally identifiable information removed.', 'cmb2' ),
        'id'         => $prefix . 'contact_attribution',
        'type'    => 'radio',
        'options' => array(
            'attributed' => 'Attributed',
            'anonymous' => 'Anonymous'
            )
        ) );
    $cmb->add_field( array(
     'name'     => 'Contact Information',
     'id'       => $prefix.'contact',
     'type'           => 'contact'
     ) );


    $cmb->add_field(
        array(
            'after_row' => $tab_close,
            'type' => 'title',
            'id'   => 'close_title'
            )
        );
}
*/