<?php
/*
Plugin Name: TS Plugin Extra
Plugin URI: http://totalstudio.co/
Description: This plugin is created by Total Studio to manage additional or extra plugin which is used by Skilled Inc.
Version: 0.1
Author: Iwan Safrudin
Author URI: https://totalstudio.co/about-us/
Text Domain: totalstudio
*/

if ( ! defined( 'SKILLED_APP_PATH' ) )
{
	define('TS_PLUGINS_EXTRA_PATH', WP_PLUGIN_DIR . '/TS_Plugins_Extra/');
}

require_once TS_PLUGINS_EXTRA_PATH. 'bootstrap.php';

/***************************************************************
* include the necessary functions files for the plugin
***************************************************************/
require_once dirname( __FILE__ ) . '/taxonomies/service.php';
require_once dirname( __FILE__ ) . '/taxonomies/company_size.php';
require_once dirname( __FILE__ ) . '/taxonomies/company_rate.php';
require_once dirname( __FILE__ ) . '/taxonomies/project_size.php';
require_once dirname( __FILE__ ) . '/taxonomies/client_focus.php';
require_once dirname( __FILE__ ) . '/taxonomies/industry.php';
//require_once dirname( __FILE__ ) . '/taxonomies/customer_level.php';
//require_once dirname( __FILE__ ) . '/taxonomies/customer_size.php';
//require_once dirname( __FILE__ ) . '/taxonomies/software_type.php';


/***************************************************************
* include the necessary functions files for the plugin
***************************************************************/

/* custom */
require_once dirname( __FILE__ ) . '/metaboxes/options/country.php';
require_once dirname( __FILE__ ) . '/metaboxes/options/state.php';
require_once dirname( __FILE__ ) . '/metaboxes/options/taxonomy.php';
require_once dirname( __FILE__ ) . '/metaboxes/options/posttype.php';


/* custom */
require_once dirname( __FILE__ ) . '/metaboxes/types/address.php';
require_once dirname( __FILE__ ) . '/metaboxes/types/contact.php';
require_once dirname( __FILE__ ) . '/metaboxes/types/project_info.php';
require_once dirname( __FILE__ ) . '/metaboxes/types/company_info.php';
require_once dirname( __FILE__ ) . '/metaboxes/types/customer_info.php';
require_once dirname( __FILE__ ) . '/metaboxes/types/customer.php';
require_once dirname( __FILE__ ) . '/metaboxes/types/customer_size.php';
require_once dirname( __FILE__ ) . '/metaboxes/types/industry.php';
require_once dirname( __FILE__ ) . '/metaboxes/types/rating.php';
require_once dirname( __FILE__ ) . '/metaboxes/types/service.php';
require_once dirname( __FILE__ ) . '/metaboxes/types/pricing.php';
require_once dirname( __FILE__ ) . '/metaboxes/types/search.php';



require_once dirname( __FILE__ ) . '/metaboxes/company.php';
require_once dirname( __FILE__ ) . '/metaboxes/solution.php';
require_once dirname( __FILE__ ) . '/metaboxes/portfolio.php';
require_once dirname( __FILE__ ) . '/metaboxes/reference.php';
require_once dirname( __FILE__ ) . '/metaboxes/resource.php';
require_once dirname( __FILE__ ) . '/metaboxes/reviewcompany.php';
require_once dirname( __FILE__ ) . '/metaboxes/reviewsolution.php';
