<?php
/*
  Plugin Name: Organization (Company Guide)
  Plugin URI: http://totalstudio.co/
  Description: This plugin is created by Total Studio to manage company information which is used by Skilled Inc.
  Version: 0.1
  Author: Danang Widiantoro
  Author URI: https://totalstudio.co/about-us/
  Text Domain: totalstudio
 */


add_action('init', 'ts_organization_init', 0);

//add_action('admin_init', 'ts_organization_allow', 999);

function ts_organization_init() {
    if (!function_exists('ts_plugin')) {

        function organization_plugin_failed() {
            ?>
            <div class="notice notice-error">
                <p><?php _e('TS Organization Plugin only works along TS Plugin.', 'skilled'); ?></p>
            </div>
            <?php
        }

        add_action('admin_notices', 'organization_plugin_failed');
    } else {
        $ts = ts_plugin('organization', 'Company Guide', 'Company Guides');

        $slug = "companies";
        switch(DB_NAME){
            case "skilled_fr":
                $slug = "entreprises";
                break;
            case "skilled_nl":
                $slug = "bedrijven";
                break;
        }

        // DEFINE POST TYPE
        $args = [
            'supports' => ['title', 'editor', 'page-attributes', 'comments'],
            'rewrite' => [
                "slug" => $slug,
                "feeds" => true
            ],
            'capability_type' => ['organization', 'organizations'],
        ];

        $ts->postype($args);
    }
}
