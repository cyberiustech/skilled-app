<?php
/*
  Plugin Name: Proposal
  Plugin URI: http://totalstudio.co/
  Description: This plugin is created by Total Studio to manage Proposal Request to a Company.
  Version: 0.1
  Author: Suko Tyas Pernanda
  Author URI: https://totalstudio.co/about-us/
  Text Domain: totalstudio
 */


add_action('init', 'ts_proposal_init', 0);

//add_action('admin_init', 'ts_organization_allow', 999);
function proposal_plugin_failed() {
    ?>
    <div class="notice notice-error">
        <p><?php _e('TS Location Plugin only works along TS Plugin.', 'skilled'); ?></p>
    </div>
    <?php
}

function ts_proposal_init() {
    if (!function_exists('ts_plugin')) {
        add_action('admin_notices', 'proposal_plugin_failed');
    } else {
        $ts = ts_plugin('proposal', 'Proposal', 'Proposals');
        $args = [
            'supports' => ['title', 'editor'],
            'capability_type' => ['proposal', 'proposals'],
        ];

        $ts->postype($args);
    }
}

add_action('cmb2_init', 'cmb2_proposal');

function cmb2_proposal() {
    $cmb_proposal = new_cmb2_box(array(
        'id' => 'proposal_detail',
        'title' => 'Proposal Detail',
        'object_types' => array('proposal'), // Post type
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true
    ));

    $cmb_proposal->add_field(array(
        'name' => 'Company',
        'id' => 'proposal_company',
        'type' => 'select',
        'options_cb' => 'cmb2_get_posttype_options',
        'get_posttype_args' => array(
            'post_type' => 'company',
            'post_status' => 'publish',
            'showposts' => -1
        )
    ));

    $cmb_proposal->add_field(array(
        'name' => 'Name',
        'id' => 'proposal_name',
        'type' => 'text',
    ));

    $cmb_proposal->add_field(array(
        'name' => 'Email Address',
        'id' => 'proposal_email',
        'type' => 'text',
    ));

    $cmb_proposal->add_field(array(
        'name' => 'Contact Number',
        'id' => 'proposal_contact',
        'type' => 'text',
    ));
}

add_action('wp_ajax_save_proposal', 'save_proposal');
add_action('wp_ajax_nopriv_save_proposal', 'save_proposal');

function save_proposal() {

    if (isset($_REQUEST)) {
        $data = $_POST['formData'];

        $my_post = array(
            'post_title' => "Proposal Request by " . $data['name'],
            'post_status' => 'publish',
            'post_type' => 'proposal',
            'post_content' => $data['editor']
        );
        $the_post_id = wp_insert_post($my_post);

        update_post_meta($the_post_id, 'proposal_company', $data['company']);
        update_post_meta($the_post_id, 'proposal_name', $data['name']);
        update_post_meta($the_post_id, 'proposal_email', $data['email']);
        update_post_meta($the_post_id, 'proposal_contact', $data['contact']);

        if ($the_post_id) {
            $to = get_bloginfo('admin_email');
            $subject = $data['name'].' request proposal to '.get_post_meta($data['company'],'_company_post_title',true);
            $body = '<!DOCTYPE html>
                        <html>
                          <head>
                            <meta name="viewport" content="width=device-width">
                            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                            <title>Skilled Account Information</title>
                            <style type="text/css">
                            /* -------------------------------------
                                INLINED WITH https://putsmail.com/inliner
                            ------------------------------------- */
                            /* -------------------------------------
                                RESPONSIVE AND MOBILE FRIENDLY STYLES
                            ------------------------------------- */
                            @media only screen and (max-width: 620px) {
                              table[class=body] h1 {
                                font-size: 28px !important;
                                margin-bottom: 10px !important; }
                              table[class=body] p,
                              table[class=body] ul,
                              table[class=body] ol,
                              table[class=body] td,
                              table[class=body] span,
                              table[class=body] a {
                                font-size: 16px !important; }
                              table[class=body] .wrapper,
                              table[class=body] .article {
                                padding: 10px !important; }
                              table[class=body] .content {
                                padding: 0 !important; }
                              table[class=body] .container {
                                padding: 0 !important;
                                width: 100% !important; }
                              table[class=body] .main {
                                border-left-width: 0 !important;
                                border-radius: 0 !important;
                                border-right-width: 0 !important; }
                              table[class=body] .btn table {
                                width: 100% !important; }
                              table[class=body] .btn a {
                                width: 100% !important; }
                              table[class=body] .img-responsive {
                                height: auto !important;
                                max-width: 100% !important;
                                width: auto !important; }}
                            /* -------------------------------------
                                PRESERVE THESE STYLES IN THE HEAD
                            ------------------------------------- */
                            @media all {
                              .ExternalClass {
                                width: 100%; }
                              .ExternalClass,
                              .ExternalClass p,
                              .ExternalClass span,
                              .ExternalClass font,
                              .ExternalClass td,
                              .ExternalClass div {
                                line-height: 100%; }
                              .apple-link a {
                                color: inherit !important;
                                font-family: inherit !important;
                                font-size: inherit !important;
                                font-weight: inherit !important;
                                line-height: inherit !important;
                                text-decoration: none !important; }
                              .btn-primary table td:hover {
                                background-color: #34495e !important; }
                              .btn-primary a:hover {
                                background-color: #34495e !important;
                                border-color: #34495e !important; } }
                            </style>
                          </head>
                          <body class="" style="background-color:#f6f6f6;font-family:sans-serif;-webkit-font-smoothing:antialiased;font-size:14px;line-height:1.4;margin:0;padding:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;">
                            <table border="0" cellpadding="0" cellspacing="0" class="body" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;background-color:#f6f6f6;width:100%;">
                              <tr>
                                <td style="font-family:sans-serif;font-size:14px;vertical-align:top;">&nbsp;</td>
                                <td class="container" style="font-family:sans-serif;font-size:14px;vertical-align:top;display:block;max-width:580px;padding:10px;width:580px;Margin:0 auto !important;">
                                  <div class="content" style="box-sizing:border-box;display:block;Margin:0 auto;max-width:580px;padding:10px;">
                                    <!-- START CENTERED WHITE CONTAINER -->
                                    <span class="preheader" style="color:transparent;display:none;height:0;max-height:0;max-width:0;opacity:0;overflow:hidden;mso-hide:all;visibility:hidden;width:0;">This is preheader text. Some clients will show this text as a preview.</span>
                                    <table class="main" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;background:#fff;border-radius:3px;width:100%;">
                                      <!-- START MAIN CONTENT AREA -->
                                      <tr>
                                        <td class="wrapper" style="font-family:sans-serif;font-size:14px;vertical-align:top;box-sizing:border-box;padding:20px;">
                                          <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;width:100%;">
                                            <tr>
                                              <td style="font-family:sans-serif;font-size:14px;vertical-align:top;">
                                                <p style="font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;">Hi Skilled Admin,</p>
                                                <p style="font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;">'.$data['name'].' request proposal to '.get_post_meta($data['company'],'_company_post_title',true).'</p>
                                                <p> Name : '.$data['name'].'</p>
                                                <p> Email : '.$data['email'].'</p>
                                                <p> Phone : '.$data['contact'].'</p>
                                                <p> Message : '.$data['editor'].'</p>
                                                <p style="font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;">Good luck!</p>
                                              </td>
                                            </tr>
                                          </table>
                                        </td>
                                      </tr>
                                      <!-- END MAIN CONTENT AREA -->
                                    </table>
                                    <!-- START FOOTER -->
                                    <div class="footer" style="clear:both;padding-top:10px;text-align:center;width:100%;">
                                      <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;width:100%;">
                                        <tr>
                                          <td class="content-block" style="font-family:sans-serif;font-size:14px;vertical-align:top;color:#999999;font-size:12px;text-align:center;">
                                            
                                          </td>
                                        </tr>
                                        <tr>
                                          <td class="content-block powered-by" style="font-family:sans-serif;font-size:14px;vertical-align:top;color:#999999;font-size:12px;text-align:center;">
                                            
                                          </td>
                                        </tr>
                                      </table>
                                    </div>
                                    <!-- END FOOTER -->
                                    <!-- END CENTERED WHITE CONTAINER -->
                                  </div>
                                </td>
                                <td style="font-family:sans-serif;font-size:14px;vertical-align:top;">&nbsp;</td>
                              </tr>
                            </table>
                          </body>
                        </html>';

            $headers = array('Content-Type: text/html; charset=UTF-8', 'From: Skilled <no-reply@skilled.co>');

            wp_mail($to, $subject, $body, $headers);

            $to = get_post_meta($data['company'],'_company_email',true);
            $name = get_post_meta($data['company'],'_company_post_title',true);
            $subject = 'Your got new proposal from '.$data['name'];
            $body = '<!DOCTYPE html>
                        <html>
                          <head>
                            <meta name="viewport" content="width=device-width">
                            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                            <title>Skilled Account Information</title>
                            <style type="text/css">
                            /* -------------------------------------
                                INLINED WITH https://putsmail.com/inliner
                            ------------------------------------- */
                            /* -------------------------------------
                                RESPONSIVE AND MOBILE FRIENDLY STYLES
                            ------------------------------------- */
                            @media only screen and (max-width: 620px) {
                              table[class=body] h1 {
                                font-size: 28px !important;
                                margin-bottom: 10px !important; }
                              table[class=body] p,
                              table[class=body] ul,
                              table[class=body] ol,
                              table[class=body] td,
                              table[class=body] span,
                              table[class=body] a {
                                font-size: 16px !important; }
                              table[class=body] .wrapper,
                              table[class=body] .article {
                                padding: 10px !important; }
                              table[class=body] .content {
                                padding: 0 !important; }
                              table[class=body] .container {
                                padding: 0 !important;
                                width: 100% !important; }
                              table[class=body] .main {
                                border-left-width: 0 !important;
                                border-radius: 0 !important;
                                border-right-width: 0 !important; }
                              table[class=body] .btn table {
                                width: 100% !important; }
                              table[class=body] .btn a {
                                width: 100% !important; }
                              table[class=body] .img-responsive {
                                height: auto !important;
                                max-width: 100% !important;
                                width: auto !important; }}
                            /* -------------------------------------
                                PRESERVE THESE STYLES IN THE HEAD
                            ------------------------------------- */
                            @media all {
                              .ExternalClass {
                                width: 100%; }
                              .ExternalClass,
                              .ExternalClass p,
                              .ExternalClass span,
                              .ExternalClass font,
                              .ExternalClass td,
                              .ExternalClass div {
                                line-height: 100%; }
                              .apple-link a {
                                color: inherit !important;
                                font-family: inherit !important;
                                font-size: inherit !important;
                                font-weight: inherit !important;
                                line-height: inherit !important;
                                text-decoration: none !important; }
                              .btn-primary table td:hover {
                                background-color: #34495e !important; }
                              .btn-primary a:hover {
                                background-color: #34495e !important;
                                border-color: #34495e !important; } }
                            </style>
                          </head>
                          <body class="" style="background-color:#f6f6f6;font-family:sans-serif;-webkit-font-smoothing:antialiased;font-size:14px;line-height:1.4;margin:0;padding:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;">
                            <table border="0" cellpadding="0" cellspacing="0" class="body" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;background-color:#f6f6f6;width:100%;">
                              <tr>
                                <td style="font-family:sans-serif;font-size:14px;vertical-align:top;">&nbsp;</td>
                                <td class="container" style="font-family:sans-serif;font-size:14px;vertical-align:top;display:block;max-width:580px;padding:10px;width:580px;Margin:0 auto !important;">
                                  <div class="content" style="box-sizing:border-box;display:block;Margin:0 auto;max-width:580px;padding:10px;">
                                    <!-- START CENTERED WHITE CONTAINER -->
                                    <span class="preheader" style="color:transparent;display:none;height:0;max-height:0;max-width:0;opacity:0;overflow:hidden;mso-hide:all;visibility:hidden;width:0;">This is preheader text. Some clients will show this text as a preview.</span>
                                    <table class="main" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;background:#fff;border-radius:3px;width:100%;">
                                      <!-- START MAIN CONTENT AREA -->
                                      <tr>
                                        <td class="wrapper" style="font-family:sans-serif;font-size:14px;vertical-align:top;box-sizing:border-box;padding:20px;">
                                          <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;width:100%;">
                                            <tr>
                                              <td style="font-family:sans-serif;font-size:14px;vertical-align:top;">
                                                <p style="font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;">Hi '.$name.',</p>
                                                <p style="font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;">You got a proposal from '.$data['name'].'</p>
                                                <p> Name : '.$data['name'].'</p>
                                                <p> Email : '.$data['email'].'</p>
                                                <p> Phone : '.$data['contact'].'</p>
                                                <p> Message : '.$data['editor'].'</p>
                                                <p style="font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;">Good luck!</p>
                                              </td>
                                            </tr>
                                          </table>
                                        </td>
                                      </tr>
                                      <!-- END MAIN CONTENT AREA -->
                                    </table>
                                    <!-- START FOOTER -->
                                    <div class="footer" style="clear:both;padding-top:10px;text-align:center;width:100%;">
                                      <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;width:100%;">
                                        <tr>
                                          <td class="content-block" style="font-family:sans-serif;font-size:14px;vertical-align:top;color:#999999;font-size:12px;text-align:center;">
                                            
                                          </td>
                                        </tr>
                                        <tr>
                                          <td class="content-block powered-by" style="font-family:sans-serif;font-size:14px;vertical-align:top;color:#999999;font-size:12px;text-align:center;">
                                            
                                          </td>
                                        </tr>
                                      </table>
                                    </div>
                                    <!-- END FOOTER -->
                                    <!-- END CENTERED WHITE CONTAINER -->
                                  </div>
                                </td>
                                <td style="font-family:sans-serif;font-size:14px;vertical-align:top;">&nbsp;</td>
                              </tr>
                            </table>
                          </body>
                        </html>';

            $headers = array('Content-Type: text/html; charset=UTF-8', 'From: Skilled <no-reply@skilled.co>');

            wp_mail($to, $subject, $body, $headers);
            echo json_encode([
                "status" => true,
                "message" => "Proposal Submitted.",
                "post_id" => $the_post_id
            ]);
        } else {
            echo json_encode([
                "status" => false,
                "message" => "Proposal submission failed. Please try again.",
            ]);
        }
    }

    wp_die();
}
