<?php
/*
  Plugin Name: Resource
  Plugin URI: http://totalstudio.co/
  Description: This plugin is created by Total Studio to manage resource for company which is used by Skilled Inc.
  Version: 0.1
  Author: Iwan Safrudin
  Author URI: https://totalstudio.co/about-us/
  Text Domain: totalstudio
 */

add_action('init', 'ts_resource_init', 0);

function ts_resource_init() {
    if (!function_exists('ts_plugin')) {

        function resource_plugin_failed() {
            ?>
            <div class="notice notice-error">
                <p><?php _e('TS Resource Plugin only works along TS Plugin.', 'skilled'); ?></p>
            </div>
            <?php
        }

        add_action('admin_notices', 'resource_plugin_failed');
    } else {
        $ts = ts_plugin('resources', 'Resource', 'Resources');

        $slug = "resources";
        switch(DB_NAME){
            case "skilled_fr":
                $slug = "ressources";
                break;
            case "skilled_nl":
                $slug = "bronnen";
                break;
        }
        // DEFINE POST TYPE
        $args = array(
            'supports' => ['title', 'editor', 'page-attributes', 'thumbnail'],
            "rewrite" => [
                "slug" => $slug,
                "feeds" => true
            ],
            'capability_type' => ['resource', 'resources'],
        );

        $ts->postype($args);

        //$ts->metabox('_resource_infographic_url', 'Infographic URL');
        //$ts->metabox('_resource_backlink_url', 'Backlink URL');

        //$ts->taxonomy("tag", "Resource Tag", "Resource Tags", '', ["hierarchical" => false]);
    }
}

//if (function_exists('ts_plugin')) {

add_action('cmb2_init', 'cmb2_resource_information');

function cmb2_resource_information() {
    $prefix = '_resource_';

    $cmb = new_cmb2_box(array(
        'id' => 'resource_information',
        'title' => __('Resource Information', 'cmb2'),
        'object_types' => array('resources'), // Post type
        'context' => 'advanced',
        'priority' => 'low',
        'show_names' => true
            )
    );

//    $group_field_id = $cmb->add_field(array(
//        'id' => $prefix . 'reff',
//        'type' => 'group',
//        'repeatable' => true,
//        'options' => array(
//            'group_title' => __('Reff {#}', 'cmb2'),
//            'add_button' => __('Add Another Reff', 'cmb2'),
//            'remove_button' => __('Remove Reff', 'cmb2'),
//            'sortable' => true,
//            'closed' => false,
//        ),
//            )
//    );
//    $cmb->add_group_field($group_field_id, array(
//        'id' => $prefix . 'reff_url',
//        'name' => __('URL', 'skilled'),
//        'type' => 'text',
//            )
//    );
//    $cmb->add_group_field($group_field_id, array(
//        'id' => $prefix . 'reff_follow',
//        'name' => __('Do Follow', 'skilled'),
//        'type' => 'radio',
//        'show_option_none' => true,
//        'options' => array(
//            'yes' => __('Yes', 'cmb2')
//        )
//            )
//    );

    $group_field_id = $cmb->add_field(array(
        'id' => $prefix . 'infographic_url',
        'type' => 'group',
        'repeatable' => true,
        'options' => array(
            'group_title' => __('Infographic {#}', 'cmb2'),
            'add_button' => __('Add Another Infographic ', 'cmb2'),
            'remove_button' => __('Remove Infographic ', 'cmb2'),
            'sortable' => true,
            'closed' => false,
        ),
    ));
    $cmb->add_group_field($group_field_id, array(
        'id' => $prefix . 'infographic_url',
        'type' => 'text_url',
    ));

    $cmb->add_field(array(
        'name' => __('Backlink URL', 'cmb2'),
        'desc' => __('', 'cmb2'),
        'id' => $prefix . 'backlink_url',
        'type' => 'text_url'
    ));
}

//}