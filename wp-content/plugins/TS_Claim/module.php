<?php

/*
  Plugin Name: Skilled Claim Company
  Plugin URI: http://faqihamruddin.com
  Description: This plugin made with love by faqih amruddin yusuf.
  Version: 1.0
  Author: Faqih Amruddin Yusuf
  Author URI: http://faqihamruddin.com
  License: GPLv2
 */
function TS_create_database_activation() {
	global $table_prefix, $wpdb;

    $tblname = 'claim';
    $charset_collate = $wpdb->get_charset_collate();
    $wp_track_table = $table_prefix . "$tblname";

    #Check to see if the table exists already, if not, then create it

    if($wpdb->get_var( "show tables like '$wp_track_table'" ) != $wp_track_table) 
    {

        $sql = "CREATE TABLE `". $wp_track_table . "` ( ";
        $sql .= "  `id`  int(12)   NOT NULL auto_increment, ";
        $sql .= "  `id_company`  int(12)   NOT NULL, ";
        $sql .= "  `id_user`  int(12)   NOT NULL, ";
        $sql .= "  `status`  int(12)   NOT NULL, ";
        $sql .= "  PRIMARY KEY `id` (`id`) "; 
        $sql .= ") $charset_collate ; ";
        require_once( ABSPATH . '/wp-admin/includes/upgrade.php' );
        dbDelta($sql);
    }
}
register_activation_hook(__FILE__, 'TS_create_database_activation');
add_action('wp_ajax_save_claim', 'save_claim');
add_action('wp_ajax_nopriv_save_claim', 'save_claim');

add_action('admin_menu', 'claimed_company_list');

function claimed_company_list() {
    add_menu_page('Claimed Company', 'Claimed Companies', 'manage_options', 'TS_Claim/claim.php', '', 'dashicons-networking', 90);
}


function save_claim() {
	global $table_prefix, $wpdb, $current_user;
    if (isset($_REQUEST)) {
        $data = $_POST['formData'];
        $id = $data['id_company'];
        $s = "SELECT * FROM wpos_claim WHERE id_company = $id and id_user = $current_user->ID";
        $user = $wpdb->get_results($s);

        if (count($user) > 0) {
        	echo json_encode([
                "status" => false,
                "message" => "You already claimed this company before",
            ]);
        }else{
	        $the_post_id = $wpdb->insert($table_prefix . "claim", array(
			    'id_company' => $data['id_company'],
			    'id_user' => $current_user->ID,
			    'status' => ''
			));

	        if ($the_post_id) {
	            $to = get_bloginfo('admin_email');
	            $subject = $current_user->display_name.' request claim to '.get_post_meta($data['id_company'],'_company_post_title',true);
	            $body = '<!DOCTYPE html>
	                        <html>
	                          <head>
	                            <meta name="viewport" content="width=device-width">
	                            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	                            <title>Skilled Account Information</title>
	                            <style type="text/css">
	                            /* -------------------------------------
	                                INLINED WITH https://putsmail.com/inliner
	                            ------------------------------------- */
	                            /* -------------------------------------
	                                RESPONSIVE AND MOBILE FRIENDLY STYLES
	                            ------------------------------------- */
	                            @media only screen and (max-width: 620px) {
	                              table[class=body] h1 {
	                                font-size: 28px !important;
	                                margin-bottom: 10px !important; }
	                              table[class=body] p,
	                              table[class=body] ul,
	                              table[class=body] ol,
	                              table[class=body] td,
	                              table[class=body] span,
	                              table[class=body] a {
	                                font-size: 16px !important; }
	                              table[class=body] .wrapper,
	                              table[class=body] .article {
	                                padding: 10px !important; }
	                              table[class=body] .content {
	                                padding: 0 !important; }
	                              table[class=body] .container {
	                                padding: 0 !important;
	                                width: 100% !important; }
	                              table[class=body] .main {
	                                border-left-width: 0 !important;
	                                border-radius: 0 !important;
	                                border-right-width: 0 !important; }
	                              table[class=body] .btn table {
	                                width: 100% !important; }
	                              table[class=body] .btn a {
	                                width: 100% !important; }
	                              table[class=body] .img-responsive {
	                                height: auto !important;
	                                max-width: 100% !important;
	                                width: auto !important; }}
	                            /* -------------------------------------
	                                PRESERVE THESE STYLES IN THE HEAD
	                            ------------------------------------- */
	                            @media all {
	                              .ExternalClass {
	                                width: 100%; }
	                              .ExternalClass,
	                              .ExternalClass p,
	                              .ExternalClass span,
	                              .ExternalClass font,
	                              .ExternalClass td,
	                              .ExternalClass div {
	                                line-height: 100%; }
	                              .apple-link a {
	                                color: inherit !important;
	                                font-family: inherit !important;
	                                font-size: inherit !important;
	                                font-weight: inherit !important;
	                                line-height: inherit !important;
	                                text-decoration: none !important; }
	                              .btn-primary table td:hover {
	                                background-color: #34495e !important; }
	                              .btn-primary a:hover {
	                                background-color: #34495e !important;
	                                border-color: #34495e !important; } }
	                            </style>
	                          </head>
	                          <body class="" style="background-color:#f6f6f6;font-family:sans-serif;-webkit-font-smoothing:antialiased;font-size:14px;line-height:1.4;margin:0;padding:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;">
	                            <table border="0" cellpadding="0" cellspacing="0" class="body" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;background-color:#f6f6f6;width:100%;">
	                              <tr>
	                                <td style="font-family:sans-serif;font-size:14px;vertical-align:top;">&nbsp;</td>
	                                <td class="container" style="font-family:sans-serif;font-size:14px;vertical-align:top;display:block;max-width:580px;padding:10px;width:580px;Margin:0 auto !important;">
	                                  <div class="content" style="box-sizing:border-box;display:block;Margin:0 auto;max-width:580px;padding:10px;">
	                                    <!-- START CENTERED WHITE CONTAINER -->
	                                    <span class="preheader" style="color:transparent;display:none;height:0;max-height:0;max-width:0;opacity:0;overflow:hidden;mso-hide:all;visibility:hidden;width:0;">This is preheader text. Some clients will show this text as a preview.</span>
	                                    <table class="main" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;background:#fff;border-radius:3px;width:100%;">
	                                      <!-- START MAIN CONTENT AREA -->
	                                      <tr>
	                                        <td class="wrapper" style="font-family:sans-serif;font-size:14px;vertical-align:top;box-sizing:border-box;padding:20px;">
	                                          <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;width:100%;">
	                                            <tr>
	                                              <td style="font-family:sans-serif;font-size:14px;vertical-align:top;">
	                                                <p style="font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;">Hi Skilled Admin,</p>
	                                                <p style="font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;">'.$current_user->display_name.' has been requested to claim '.get_post_meta($data['id_company'],'_company_post_title',true).'</p>
	                                                <p style="font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;">Good luck!</p>
	                                              </td>
	                                            </tr>
	                                          </table>
	                                        </td>
	                                      </tr>
	                                      <!-- END MAIN CONTENT AREA -->
	                                    </table>
	                                    <!-- START FOOTER -->
	                                    <div class="footer" style="clear:both;padding-top:10px;text-align:center;width:100%;">
	                                      <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;width:100%;">
	                                        <tr>
	                                          <td class="content-block" style="font-family:sans-serif;font-size:14px;vertical-align:top;color:#999999;font-size:12px;text-align:center;">
	                                            
	                                          </td>
	                                        </tr>
	                                        <tr>
	                                          <td class="content-block powered-by" style="font-family:sans-serif;font-size:14px;vertical-align:top;color:#999999;font-size:12px;text-align:center;">
	                                            
	                                          </td>
	                                        </tr>
	                                      </table>
	                                    </div>
	                                    <!-- END FOOTER -->
	                                    <!-- END CENTERED WHITE CONTAINER -->
	                                  </div>
	                                </td>
	                                <td style="font-family:sans-serif;font-size:14px;vertical-align:top;">&nbsp;</td>
	                              </tr>
	                            </table>
	                          </body>
	                        </html>';

	            $headers = array('Content-Type: text/html; charset=UTF-8', 'From: Skilled <no-reply@skilled.co>');

	            wp_mail($to, $subject, $body, $headers);

	            $to = get_post_meta($data['id_company'],'_company_email',true);
	            $name = get_post_meta($data['id_company'],'_company_post_title',true);
	            $subject = $current_user->display_name.' want to claim your company';
	            $body = '<!DOCTYPE html>
	                        <html>
	                          <head>
	                            <meta name="viewport" content="width=device-width">
	                            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	                            <title>Skilled Account Information</title>
	                            <style type="text/css">
	                            /* -------------------------------------
	                                INLINED WITH https://putsmail.com/inliner
	                            ------------------------------------- */
	                            /* -------------------------------------
	                                RESPONSIVE AND MOBILE FRIENDLY STYLES
	                            ------------------------------------- */
	                            @media only screen and (max-width: 620px) {
	                              table[class=body] h1 {
	                                font-size: 28px !important;
	                                margin-bottom: 10px !important; }
	                              table[class=body] p,
	                              table[class=body] ul,
	                              table[class=body] ol,
	                              table[class=body] td,
	                              table[class=body] span,
	                              table[class=body] a {
	                                font-size: 16px !important; }
	                              table[class=body] .wrapper,
	                              table[class=body] .article {
	                                padding: 10px !important; }
	                              table[class=body] .content {
	                                padding: 0 !important; }
	                              table[class=body] .container {
	                                padding: 0 !important;
	                                width: 100% !important; }
	                              table[class=body] .main {
	                                border-left-width: 0 !important;
	                                border-radius: 0 !important;
	                                border-right-width: 0 !important; }
	                              table[class=body] .btn table {
	                                width: 100% !important; }
	                              table[class=body] .btn a {
	                                width: 100% !important; }
	                              table[class=body] .img-responsive {
	                                height: auto !important;
	                                max-width: 100% !important;
	                                width: auto !important; }}
	                            /* -------------------------------------
	                                PRESERVE THESE STYLES IN THE HEAD
	                            ------------------------------------- */
	                            @media all {
	                              .ExternalClass {
	                                width: 100%; }
	                              .ExternalClass,
	                              .ExternalClass p,
	                              .ExternalClass span,
	                              .ExternalClass font,
	                              .ExternalClass td,
	                              .ExternalClass div {
	                                line-height: 100%; }
	                              .apple-link a {
	                                color: inherit !important;
	                                font-family: inherit !important;
	                                font-size: inherit !important;
	                                font-weight: inherit !important;
	                                line-height: inherit !important;
	                                text-decoration: none !important; }
	                              .btn-primary table td:hover {
	                                background-color: #34495e !important; }
	                              .btn-primary a:hover {
	                                background-color: #34495e !important;
	                                border-color: #34495e !important; } }
	                            </style>
	                          </head>
	                          <body class="" style="background-color:#f6f6f6;font-family:sans-serif;-webkit-font-smoothing:antialiased;font-size:14px;line-height:1.4;margin:0;padding:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;">
	                            <table border="0" cellpadding="0" cellspacing="0" class="body" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;background-color:#f6f6f6;width:100%;">
	                              <tr>
	                                <td style="font-family:sans-serif;font-size:14px;vertical-align:top;">&nbsp;</td>
	                                <td class="container" style="font-family:sans-serif;font-size:14px;vertical-align:top;display:block;max-width:580px;padding:10px;width:580px;Margin:0 auto !important;">
	                                  <div class="content" style="box-sizing:border-box;display:block;Margin:0 auto;max-width:580px;padding:10px;">
	                                    <!-- START CENTERED WHITE CONTAINER -->
	                                    <span class="preheader" style="color:transparent;display:none;height:0;max-height:0;max-width:0;opacity:0;overflow:hidden;mso-hide:all;visibility:hidden;width:0;">This is preheader text. Some clients will show this text as a preview.</span>
	                                    <table class="main" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;background:#fff;border-radius:3px;width:100%;">
	                                      <!-- START MAIN CONTENT AREA -->
	                                      <tr>
	                                        <td class="wrapper" style="font-family:sans-serif;font-size:14px;vertical-align:top;box-sizing:border-box;padding:20px;">
	                                          <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;width:100%;">
	                                            <tr>
	                                              <td style="font-family:sans-serif;font-size:14px;vertical-align:top;">
	                                                <p style="font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;">Hi '.$name.',</p>
	                                                <p style="font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;">'.$current_user->display_name.' want to claim your company</p>
	                                                <p> Please contact Skilled administrator if is not one of you</p>
	                                                <p style="font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;">Thank you!</p>
	                                              </td>
	                                            </tr>
	                                          </table>
	                                        </td>
	                                      </tr>
	                                      <!-- END MAIN CONTENT AREA -->
	                                    </table>
	                                    <!-- START FOOTER -->
	                                    <div class="footer" style="clear:both;padding-top:10px;text-align:center;width:100%;">
	                                      <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;width:100%;">
	                                        <tr>
	                                          <td class="content-block" style="font-family:sans-serif;font-size:14px;vertical-align:top;color:#999999;font-size:12px;text-align:center;">
	                                            
	                                          </td>
	                                        </tr>
	                                        <tr>
	                                          <td class="content-block powered-by" style="font-family:sans-serif;font-size:14px;vertical-align:top;color:#999999;font-size:12px;text-align:center;">
	                                            
	                                          </td>
	                                        </tr>
	                                      </table>
	                                    </div>
	                                    <!-- END FOOTER -->
	                                    <!-- END CENTERED WHITE CONTAINER -->
	                                  </div>
	                                </td>
	                                <td style="font-family:sans-serif;font-size:14px;vertical-align:top;">&nbsp;</td>
	                              </tr>
	                            </table>
	                          </body>
	                        </html>';

	            $headers = array('Content-Type: text/html; charset=UTF-8', 'From: Skilled <no-reply@skilled.co>');

	            wp_mail($to, $subject, $body, $headers);
	            echo json_encode([
	                "status" => true,
	                "message" => "Claim Submitted.",
	                "post_id" => $the_post_id
	            ]);
	        } else {
	            echo json_encode([
	                "status" => false,
	                "message" => "Claim submission failed. Please try again.",
	            ]);
	        }
	    }
    }

    wp_die();
}

add_shortcode('skilled_claimed_company', 'display_claimed_company');

function display_claimed_company() {
	global $table_prefix, $wpdb, $current_user;
    $action = $_GET['action'];
    $paged = $_GET['paged'] != "" ? $_GET['paged'] : 1;
    $posts_per_page = 10;
    $offset = ($paged*$posts_per_page)-$posts_per_page;

    if (empty($action)) {
    	$all = "SELECT wpos_posts.post_title,wpos_users.display_name,wpos_claim.* FROM wpos_claim, wpos_posts, wpos_users WHERE wpos_claim.id_company = wpos_posts.ID AND wpos_claim.id_user = wpos_users.ID AND wpos_claim.status = ''";

        $all_companies = $wpdb->get_results($all);

        $sql = "SELECT wpos_posts.ID, wpos_posts.post_title,wpos_users.display_name,wpos_users.user_email,wpos_claim.* FROM wpos_claim, wpos_posts, wpos_users WHERE wpos_claim.id_company = wpos_posts.ID AND wpos_claim.id_user = wpos_users.ID AND wpos_claim.status = '' LIMIT $posts_per_page OFFSET $offset";
		$companies = $wpdb->get_results($sql);
        ob_start();
        ?>
        <style>
            .skilled_company_logo{
                max-width: 72px;
                max-height: 24px;
                width: auto;
                height: auto;
                display: block;
            }
        </style>
        <div class="wrap">
            <h1>Claimed Company</h1>
            <hr class="wp-header-end">
            <br>
            <table class="wp-list-table widefat fixed striped pages">
                <thead>
                    <tr>
                        <th style="width: 40px; text-align: center;">ID</th>
                        <th>User </th>
                        <th>Company Name</th>
                        <th>Company Services</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($companies as $c) {
                        echo "<tr>";
                        echo "<td style='width: 40px; text-align: center;'>" . $c->id . "</td>";
                        echo "<td>" . $c->display_name;
                        echo "<div>";
                            echo "<span class='trash'>";
                            echo " <a class='submitdelete' href='mailto:".$c->user_email."'>".$c->user_email."</a>";
                            echo "</span>";
                        echo "</div>";
                        echo "</td>";
                        echo "<td>" . $c->post_title;
                        echo "<div>";
                            echo "<span class='trash'>";
                            echo " <a class='submitdelete' href='".get_bloginfo('url')."/wp-admin/post.php?post=".$c->ID."&action=edit'>Edit company</a>";
                            echo "</span> | ";
                            echo "<span class='trash'>";
                            echo " <a class='submitdelete' href='".get_the_permalink($c->ID)."'>View company</a>";
                            echo "</span>";
                        echo "</div>";
                        echo "</td>";
                        echo '<td class="taxonomy-service_term column-taxonomy-service_term" data-colname="Company Services">';
                        $terms = get_post_meta($c->ID,'_company_guide',true);
                        //var_dump($terms);
                        foreach ($terms as $key) {
                        	echo '<a>'.get_the_title($key).'</a>,'; 
                        }
                        echo '</td>';
                        echo "<td>";
                        echo "<a class='button-primary' href='" . get_home_url() . "/wp-admin/admin.php?page=TS_Claim/claim.php&cid=" . $c->id . "&action=change&status=accept'>Accept</a> ";

                        echo "<a class='button-secondary' href='" . get_home_url() . "/wp-admin/admin.php?page=TS_Claim/claim.php&cid=" . $c->id . "&action=change&status=decline'>Decline</a>";
                        echo "</td>";
                        echo "</tr>";
                    }
                    ?>
                </tbody>
            </table>
            <?php styleForThisPageOnlyLOL(); ?>
            <div class="pagination-links">
                <?php
                $num_of_pages = ceil(count($all_companies) / $posts_per_page);

                for ($i = 1; $i <= $num_of_pages; $i++) {
                    if ($i == $paged) {
                        echo "<a class='active' href='admin.php?page=TS_Company%claim.php&paged=" . $i . "'>" . $i . "</a>";
                    } else {
                        echo "<a href='admin.php?page=TS_Company%claim.php&paged=" . $i . "'>" . $i . "</a>";
                    }
                }
                ?>
            </div>
        </div>
        <?php
        $display = ob_get_clean();
        echo $display;
    } elseif ($action == "change") {
        $new_status = $_GET['status'];
        $id = $_GET['cid'];
        $wpdb->update($table_prefix . "claim", array('status'=>$new_status), array('id'=>$id));
        $user_id = $wpdb->get_row("select * from wpos_claim where id = $id");
        $user_info = get_userdata($user_id->id_user);
        $email = $user_info->user_email;
        if ($new_status == 'accept') {
        	$wpdb->delete( 'wpos_usermeta', array( 'meta_key' => 'company_id', 'meta_value' => $user_id->id_company) );
        	update_user_meta($user_id->id_user, 'company_id', $user_id->id_company);
        	$status = 'appoved';
        }elseif ($new_status == 'decline') {
        	$status = 'declined';
        }
        $to = $email;
        $subject = "Skilled claim company status";
        $body = '<!DOCTYPE html>
                    <html>
                      <head>
                        <meta name="viewport" content="width=device-width">
                        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                        <title>Skilled Account Information</title>
                        <style type="text/css">
                        /* -------------------------------------
                            INLINED WITH https://putsmail.com/inliner
                        ------------------------------------- */
                        /* -------------------------------------
                            RESPONSIVE AND MOBILE FRIENDLY STYLES
                        ------------------------------------- */
                        @media only screen and (max-width: 620px) {
                          table[class=body] h1 {
                            font-size: 28px !important;
                            margin-bottom: 10px !important; }
                          table[class=body] p,
                          table[class=body] ul,
                          table[class=body] ol,
                          table[class=body] td,
                          table[class=body] span,
                          table[class=body] a {
                            font-size: 16px !important; }
                          table[class=body] .wrapper,
                          table[class=body] .article {
                            padding: 10px !important; }
                          table[class=body] .content {
                            padding: 0 !important; }
                          table[class=body] .container {
                            padding: 0 !important;
                            width: 100% !important; }
                          table[class=body] .main {
                            border-left-width: 0 !important;
                            border-radius: 0 !important;
                            border-right-width: 0 !important; }
                          table[class=body] .btn table {
                            width: 100% !important; }
                          table[class=body] .btn a {
                            width: 100% !important; }
                          table[class=body] .img-responsive {
                            height: auto !important;
                            max-width: 100% !important;
                            width: auto !important; }}
                        /* -------------------------------------
                            PRESERVE THESE STYLES IN THE HEAD
                        ------------------------------------- */
                        @media all {
                          .ExternalClass {
                            width: 100%; }
                          .ExternalClass,
                          .ExternalClass p,
                          .ExternalClass span,
                          .ExternalClass font,
                          .ExternalClass td,
                          .ExternalClass div {
                            line-height: 100%; }
                          .apple-link a {
                            color: inherit !important;
                            font-family: inherit !important;
                            font-size: inherit !important;
                            font-weight: inherit !important;
                            line-height: inherit !important;
                            text-decoration: none !important; }
                          .btn-primary table td:hover {
                            background-color: #34495e !important; }
                          .btn-primary a:hover {
                            background-color: #34495e !important;
                            border-color: #34495e !important; } }
                        </style>
                      </head>
                      <body class="" style="background-color:#f6f6f6;font-family:sans-serif;-webkit-font-smoothing:antialiased;font-size:14px;line-height:1.4;margin:0;padding:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;">
                        <table border="0" cellpadding="0" cellspacing="0" class="body" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;background-color:#f6f6f6;width:100%;">
                          <tr>
                            <td style="font-family:sans-serif;font-size:14px;vertical-align:top;">&nbsp;</td>
                            <td class="container" style="font-family:sans-serif;font-size:14px;vertical-align:top;display:block;max-width:580px;padding:10px;width:580px;Margin:0 auto !important;">
                              <div class="content" style="box-sizing:border-box;display:block;Margin:0 auto;max-width:580px;padding:10px;">
                                <!-- START CENTERED WHITE CONTAINER -->
                                <span class="preheader" style="color:transparent;display:none;height:0;max-height:0;max-width:0;opacity:0;overflow:hidden;mso-hide:all;visibility:hidden;width:0;">This is preheader text. Some clients will show this text as a preview.</span>
                                <table class="main" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;background:#fff;border-radius:3px;width:100%;">
                                  <!-- START MAIN CONTENT AREA -->
                                  <tr>
                                    <td class="wrapper" style="font-family:sans-serif;font-size:14px;vertical-align:top;box-sizing:border-box;padding:20px;">
                                      <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;width:100%;">
                                        <tr>
                                          <td style="font-family:sans-serif;font-size:14px;vertical-align:top;">
                                            <p style="font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;">Hi '.$user_info->display_name.',</p>
                                            <p style="font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;">We\'d like to inform you that your claim status has been '.$status.'</p>
                                            <p style="font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;">Thank you for your request</p>
                                          </td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                  <!-- END MAIN CONTENT AREA -->
                                </table>
                                <!-- START FOOTER -->
                                <div class="footer" style="clear:both;padding-top:10px;text-align:center;width:100%;">
                                  <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;width:100%;">
                                    <tr>
                                      <td class="content-block" style="font-family:sans-serif;font-size:14px;vertical-align:top;color:#999999;font-size:12px;text-align:center;">
                                        
                                      </td>
                                    </tr>
                                    <tr>
                                      <td class="content-block powered-by" style="font-family:sans-serif;font-size:14px;vertical-align:top;color:#999999;font-size:12px;text-align:center;">
                                        
                                      </td>
                                    </tr>
                                  </table>
                                </div>
                                <!-- END FOOTER -->
                                <!-- END CENTERED WHITE CONTAINER -->
                              </div>
                            </td>
                            <td style="font-family:sans-serif;font-size:14px;vertical-align:top;">&nbsp;</td>
                          </tr>
                        </table>
                      </body>
                    </html>';

        $headers = array('Content-Type: text/html; charset=UTF-8', 'From: Skilled <no-reply@skilled.co>');

        wp_mail($to, $subject, $body, $headers);
        echo "<meta http-equiv='refresh' content='0;url=" . get_home_url() . "/wp-admin/admin.php?page=TS_Claim/claim.php&changed=true'>";
    }
}